#include <stdio.h>
#include <stdlib.h>
 
typedef struct node
{
    int data;
    struct node* next;
} node;

node* head = NULL;

/*
    create a new node
    initialize the data and next field
    return the newly created node
*/
node* create(int data,node* next)
{
    node* new_node = (node*)malloc(sizeof(node));
    if(new_node == NULL)
    {
        printf("Error creating a new node.\n");
        exit(0);
    }
    new_node->data = data;
    new_node->next = next;
 
    return new_node;
}
 
/*
    add a new node at the beginning of the list
*/
void prepend(int data)
{
    node* new_node = create(data,head);
    head = new_node;
}
 
/*
    add a new node at the end of the list
*/
void append(int data)
{
    if(head == NULL)
    {
	head->data = data;
	head->next = NULL;
    }

    /* go to the last node */
    node *cursor = head;
    while(cursor->next != NULL)
        cursor = cursor->next;
 
    /* create a new node */
    node* new_node =  create(data,NULL);
    cursor->next = new_node;
}
 
/*
    traverse the linked list
*/
void traverse()
{
    node* cursor = head;
    while(cursor != NULL)
    {
        printf("%d ", cursor->data);
        cursor = cursor->next;
    }
}
 
/*
    remove all element of the list
*/
void dispose()
{
    node *cursor, *tmp;
 
    if(head != NULL)
    {
        cursor = head->next;
        head->next = NULL;
        while(cursor != NULL)
        {
            tmp = cursor->next;
            free(cursor);
            cursor = tmp;
        }
	free(head);
    }
}

/*
    insert node
*/
void insert(node* found_node, int data)
{
    node* new_node = (node*)malloc(sizeof(node));
    new_node->data = data;
    new_node->next = found_node->next;
    found_node->next = new_node;
}

/*
    search element
    if searchType == 1, then search for insert after
    else if 2, then search for insert before
*/
node* search(int data,int searchType)
{
    int flag = 0;
    node* cursor = head;

    if(searchType == 1)
    {
    	while(cursor != NULL)
    	{
	    if(cursor->data == data)
	    {
		return cursor;
	    }
	    else
	    {
    		cursor = cursor->next;
	    }
    	}
    }
    else if(searchType == 2)
    {
    	while(cursor != NULL)
    	{
	    if(cursor->next->data == data)
	    {
		return cursor;
	    }
	    else
	    {
    		cursor = cursor->next;
	    }
    	}
    }

    if(flag == 0)
    {
	printf("Data not found\n");
	exit(0);
    }
}

/*
    delete last element from list
*/
void delete_last()
{
    node* cursor = head;
    while(cursor != NULL)
    {
	if(cursor->next != NULL)
	{
		node *temp_node = (node*)malloc(sizeof(node));
		temp_node = cursor->next;
		if(temp_node->next == NULL)			
			cursor->next = NULL;
	}
	cursor = cursor->next;
    }
}

/*
    delete element from list
*/
void delete_element(node* delete_node)
{
	int flag = 0;
        node* cursor = head;
        while(cursor != NULL)
        {
                if(cursor->next == delete_node)
                {
			node* found_node = cursor->next;
			cursor->next = found_node->next;
			found_node->next = NULL;
			free(found_node);
			flag = 1;
                }
		cursor = cursor->next;
        }
	if(flag == 0)
	{
		printf("After element not found\n");
		exit(0);
	}
}

/*
    check if list is empty
*/
void is_empty()
{
	if(head == NULL)
	{
		printf("List is empty\n");
	}
	else
	{
		printf("List is not empty\n");
	}
}

/*
    query the data
*/
void query_data(int data)
{
    int flag = 0;
    node* cursor = head;
    while(cursor != NULL)
    {
        if(cursor->data == data)
	{
		printf("Data found\n");
		flag = 1;
	}
	cursor = cursor->next;
    }
    if(flag == 0)
    {
	printf("Data not found\n");
    }
}

/*
    display the menu
*/
void menu()
{
	printf("--- C Linked List Demonstration --- \n\n");
	printf("0.menu\n");
	printf("------------------------------------- \n\n");
	printf("1.Insert element at beginning of list\n");
	printf("2.Insert element at end of list\n");
	printf("3.Insert node after element\n");
	printf("4.Insert node before element\n");
	printf("------------------------------------- \n\n");
	printf("5.Delete element from beginning of list\n");
	printf("6.Delete element from end of list\n");
	printf("7.Delete element\n");
	printf("8.Delete the list\n");
	printf("------------------------------------- \n\n");
	printf("9.Is list empty\n");
	printf("10.Query data\n");
	printf("11.Display list\n");
	printf("------------------------------------- \n\n");
	printf("-1.quit\n");
}
 
int main()
{
	int command = 0;
	int data,element;

	node* tmp = NULL;
	node* found_node = NULL;

	menu();

	while(1)
	{
		printf("\nEnter a command");
		scanf("%d",&command);

		if(command == -1)
			break;
		switch(command)
		{
			case 0:
				menu();
				break;
			case 1:
				printf("Please enter a number to prepend:");
				scanf("%d",&data);
				prepend(data);
				break;
			case 2:
				printf("Please enter a number to append:");
				scanf("%d",&data);
				append(data);
				break;
			case 3:
				printf("Please enter after element:");
				scanf("%d",&element);

				printf("Please enter number to insert after:");
				scanf("%d",&data);

				found_node = search(element,1);

				insert(found_node,data);
				break;
			case 4:
				printf("Please enter before element:");
				scanf("%d",&element);

				printf("Please enter number to insert before:");
				scanf("%d",&data);

				found_node = search(element,2);

				insert(found_node,data);
				break;
			case 5:
				if(head != NULL)
				{
					tmp = head;
					head = head->next;
					free(tmp);
				}
				break;
			case 6:
				delete_last();
				break;
			case 7:
				printf("Please enter element to delete");
				scanf("%d",&data);

				found_node = search(data,1);
				delete_element(found_node);
				break;
			case 8:
				dispose();
				break;
			case 9:
				is_empty();
				break;
			case 10:
				printf("Please enter a element to find");
				scanf("%d",&data);
				query_data(data);
				break;
			case 11:
				traverse();
				break;
		}
	}
	return 0;
}
