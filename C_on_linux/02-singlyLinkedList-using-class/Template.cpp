#include <stdio.h>
#include <stdlib.h>
 
typedef struct node
{
    int data;
    struct node* next;
} node;

class SinglyLinkedList
{
	private :
		node *head = NULL,*tail = NULL,*found_node = NULL;
	
	public :
		int data;

		/***********************************
			GENERIC FUNCTIONS
		***********************************/

		/*
		    display the menu
		*/
		void menu()
		{
			printf("--- C Linked List Demonstration --- \n\n");
			printf("0.menu\n");
			printf("------------------------------------- \n\n");
			printf("1.Create linked list\n");
			printf("2.Insert element at beginning of list\n");
			printf("3.Insert element at end of list\n");
			printf("4.Insert node before element\n");
			printf("5.Insert node after element\n");
			printf("------------------------------------- \n\n");
			printf("6.Delete element from beginning of list\n");
			printf("7.Delete element from end of list\n");
			printf("8.Delete element\n");
			printf("9.Delete the list\n");
			printf("------------------------------------- \n\n");
			printf("10.Is list empty\n");
			printf("11.Query data\n");
			printf("12.Display list\n");
			printf("------------------------------------- \n\n");
			printf("-1.quit\n");
		}

		/*
		    Search element
		    if searchType == 1, then search for insert after
		    else if 2, then search for insert before
		*/
		node* search(int data,int searchType)
		{
			int flag = 0;
			node* cursor = head;

			if(searchType == 1)
			{
				while(cursor != NULL)
				{
					if(cursor->data == data)
					{
						return cursor;
					}
					else
					{
						cursor = cursor->next;
					}
				}
			}
			else if(searchType == 2)
			{
				while(cursor != NULL)
				{
					if(cursor->next->data == data)
					{
						return cursor;
					}
					else
					{
						cursor = cursor->next;
					}
				}
			}

			if(flag == 0)
			{
				printf("Data not found\n");
				exit(0);
			}
		}

		/*
		    Traverse the list
		*/
		int traverse()
		{
			node* cursor = head;
			if(cursor != NULL)
			{
				while(cursor != NULL)
				{
					printf("%d ", cursor->data);
					cursor = cursor->next;
				}
				printf("\n");

				return 1;
			}
			else
			{
				return 0;
			}
		}

		/*
		    Insert node
		*/
		void insert(node* found_node, int data)
		{
			node* new_node = (node*)malloc(sizeof(node));
			new_node->data = data;
			new_node->next = found_node->next;
			found_node->next = new_node;
		}

		/**********************************
			LIST FUNCTIONS
		**********************************/

		/*
			Create list
		*/
		int create()
		{
			if(head == NULL)
			{
				head = NULL;
				tail = NULL;
				return 1;
			}
			else
			{
				return 0;
			}
		}

		/*
			Insert element at beginning of list
		*/
		int insert_begin()
		{
			printf("Enter data to insert at begin : ");
			scanf("%d",&data);

			node *temp = (node*)malloc(sizeof(node));
			if(head == NULL)
			{
				temp->data = data;
				temp->next = NULL;
				head = temp;
				tail = temp;
				return 1;
			}
			else
			{
				temp->data = data;
				temp->next = head->next;
				head = temp;
				return 1;
			}
		}

		/*
			Insert element at end of list
		*/
		int insert_end()
		{
			printf("Enter data to insert at end : ");
			scanf("%d",&data);

			node *temp = (node*)malloc(sizeof(node));
			if(head == NULL)
			{
				temp->data = data;
				temp->next = NULL;
				head = temp;
				tail = temp;
				return 1;
			}
			else
			{
				temp->data = data;
				temp->next = NULL;
				tail->next = temp;
				tail = temp;
				return 1;
			}	
		}

		/*
			Insert node before element
		*/
		int insert_before()
		{
			int before_element;

			printf("Enter before element to insert : ");
			scanf("%d",&before_element);

			printf("Enter data to insert : ");
			scanf("%d",&data);

			found_node = search(before_element,2);
			insert(found_node,data);

			return 1;
		}

		/*
			Insert node after element
		*/
		int insert_after()
		{
			int after_element;

			printf("Enter after element to insert : ");
			scanf("%d",&after_element);

			printf("Enter data to insert : ");
			scanf("%d",&data);

			found_node = search(after_element,1);
			insert(found_node,data);

			return 1;
		}

		/*
			Delete element from begining
		*/
		int delete_from_begining()
		{
			node *temp;
			if(head != NULL)
			{
				temp = head;
				head = head->next;
				free(temp);
				return 1;
			}
			else if(head == NULL)
			{
				return 0;
			}
		}

		/*
			Delete element from end
		*/
		int delete_from_end()
		{
			node* cursor = head;
			while(cursor != NULL)
			{
				if(cursor->next != NULL)
				{
					node *temp_node = (node*)malloc(sizeof(node));
					temp_node = cursor->next;
					if(temp_node->next == NULL)
					{	
						cursor->next = NULL;
						return 1;
					}
				}
				cursor = cursor->next;
			}
			return 0;
		}

		/*
		    Delete element
		*/
		int delete_element()
		{

			printf("Please enter element to delete : ");
			scanf("%d",&data);

			found_node = search(data,1);

			node* cursor = head;
			while(cursor != NULL)
			{
				if(cursor->next == found_node)
				{
					node* found_node = cursor->next;
					cursor->next = found_node->next;
					found_node->next = NULL;
					free(found_node);
					return 1;
				}
				cursor = cursor->next;
			}
			return 0;
		}

		/*
		    Remove all element from the list
		*/
		int dispose()
		{
			node *cursor, *temp;

			if(head != NULL)
			{
				cursor = head;
				while(cursor != NULL)
				{
					temp = cursor->next;
					free(cursor);
					cursor = temp;
					head = temp;
				}
				free(head);
				head = NULL;
				return 1;
			}
			else
			{
				return 0;
			}
		}

		/*
		    Check if list is empty
		*/
		void is_empty()
		{
			if(head == NULL)
			{
				printf("List is empty\n");
			}
			else
			{
				printf("List is not empty\n");
			}
		}

		/*
		    Query the data
		*/
		int query_data()
		{
			printf("Please enter a element to find : ");
			scanf("%d",&data);

			node* cursor = head;
			while(cursor != NULL)
			{
				if(cursor->data == data)
				{
					return 1;
				}
				cursor = cursor->next;
			}
			return 0;
		}
};

int main()
{
	int command,status;

	SinglyLinkedList s1;
	s1.menu();

	while(1)
	{
		printf("\nEnter command : ");
		scanf("%d",&command);

		if(command == -1)
		{
			break;
		}
		switch(command)
		{
			case 1:
				status = s1.create();
				if(status == 1)
					printf("List created successfully\n");
				else
					printf("List can not be created, please try again\n");
				break;
			case 2:
				status = s1.insert_begin();
				if(status == 1)
					printf("Element inserted at begin of list successfully\n");
				else if(status == 0)
					printf("Element can not be inserted");
				break;
			case 3:
				status = s1.insert_end();
				if(status == 1)
					printf("Element inserted at end of list successfully\n");
				else if(status == 0)
					printf("Element can not be inserted\n");
				break;
			case 4:
				status = s1.insert_before();
				if(status == 1)
					printf("Element inserted before successfully\n");
				else if(status == 0)
					printf("Before element insertion failed\n");
				break;
			case 5:
				status = s1.insert_after();
				if(status == 1)
					printf("Element inserted after successfully\n");
				else if(status == 0)
					printf("After element insertion failed\n");
				break;
			case 6:
				status = s1.delete_from_begining();
				if(status == 1)
					printf("Element deleted from begining successfully\n");
				else if(status == 0)
					printf("Delete element from begining failed\n");
				break;
			case 7:
				status = s1.delete_from_end();
				if(status == 1)
					printf("Element deleted from end successfully\n");
				else if(status == 0)
					printf("Delete element from end failed\n");
				break;
			case 8:
				status = s1.delete_element();
				if(status == 1)
					printf("Element deleted successfully\n");
				else if(status == 0)
					printf("Delete element failed\n");
				break;
			case 9:
				status = s1.dispose();
				if(status == 1)
					printf("List deleted successfully\n");
				else if(status == 0)
					printf("Delete list failed\n");
				break;
			case 10:
				s1.is_empty();
				break;
			case 11:
				status = s1.query_data();
				if(status == 1)
					printf("Element found\n");
				else if(status == 0)
					printf("Element not found\n");
				break;
			case 12:
				status = s1.traverse();
				if(status == 0)
				{
					printf("List is empty\n");
				}
				break;
				
		}
	}
	return 0;
}
