
#include <stdio.h>
#include <stdlib.h>

#define SUCCESS 1
#define FAILURE 0

typedef int result_t;

class SinglyLinkedList
{
	private :

		typedef struct node
		{
		    int data;
		    struct node* next;
		} node;

		node *head = NULL,*tail = NULL;
	
	public :
		SinglyLinkedList();
		void menu();
		result_t traverse();
		result_t insert(node*,int);
		result_t is_empty();
		result_t search(int);
		result_t create();
		result_t insert_begin(int);
		result_t insert_end(int);
		result_t insert_before(int,int);
		result_t insert_after(int,int);
		result_t delete_from_begining();
		result_t delete_from_end();
		result_t delete_element(int);
		result_t dispose();
		result_t query_data(int);
};

SinglyLinkedList::SinglyLinkedList()
{
	create();
}

void SinglyLinkedList::menu()
{
	printf("--- C Linked List Demonstration --- \n\n");
	printf("--Menu--\n");
	printf("------------------------------------- \n\n");
	printf("1.Insert element at beginning of list\n");
	printf("2.Insert element at end of list\n");
	printf("3.Insert node before element\n");
	printf("4.Insert node after element\n");
	printf("------------------------------------- \n\n");
	printf("5.Delete element from beginning of list\n");
	printf("6.Delete element from end of list\n");
	printf("7.Delete element\n");
	printf("8.Delete the list\n");
	printf("------------------------------------- \n\n");
	printf("9.Is list empty\n");
	printf("10.Query data\n");
	printf("11.Display list\n");
	printf("------------------------------------- \n\n");
	printf("-1.quit\n");
}

result_t SinglyLinkedList::is_empty()
{
	if(head->next == NULL)
	{
		return SUCCESS;
	}
	else
	{
		return FAILURE;
	}
}

result_t SinglyLinkedList::search(int data)
{
	node* cursor = head->next;
	while(cursor != NULL)
	{
		if(cursor->data == data)
		{
			return SUCCESS;
		}
		else
		{
			cursor = cursor->next;
		}
	}
	return FAILURE;
}

result_t SinglyLinkedList::create()
{
	if(head == NULL)
	{
		head = (node*)malloc(sizeof(node));
		head->next = NULL;
		tail = NULL;
		return SUCCESS;
	}
	else
	{
		return FAILURE;
	}
}

result_t SinglyLinkedList::insert_begin(int data)
{
	node *temp = (node*)malloc(sizeof(node));
	if(head->next == NULL)
	{
		head = (node*)malloc(sizeof(node));
		temp->data = data;
		temp->next = NULL;
		head->next = temp;
		tail = temp;
		return SUCCESS;
	}
	else
	{
		temp->data = data;
		temp->next = head->next;
		head->next = temp;
		return SUCCESS;
	}
}

result_t SinglyLinkedList::insert_end(int data)
{
	node *temp = (node*)malloc(sizeof(node));
	if(tail == NULL)
	{
		temp->data = data;
		temp->next = NULL;
		tail = temp;
		head->next = temp;
		return SUCCESS;
		
	}
	else
	{
		node* cursor = head;
		while(cursor->next != NULL)
		{
			cursor = cursor->next;
			continue;
		}
		temp->data = data;
		temp->next = NULL;
		cursor->next = temp;
		tail = temp;
		return SUCCESS;
	}
}

result_t SinglyLinkedList::insert_before(int before_element,int data)
{
	node *cursor = head;
	while(cursor->next != NULL)
	{
		if(cursor->next->data == before_element)
		{
			node *temp = (node*)malloc(sizeof(node));
			temp->data = data;
			temp->next = cursor->next;
			cursor->next = temp;
			return SUCCESS;
		}
		cursor = cursor->next;
	}
	return FAILURE;
}

result_t SinglyLinkedList::insert_after(int after_element,int data)
{
	node *cursor = head;
	while(cursor->next != NULL)
	{
		if(cursor->next->data == after_element)
		{
			node *temp = (node*)malloc(sizeof(node));
			temp->data = data;
			temp->next = cursor->next->next;
			cursor->next->next = temp;
			return SUCCESS;
		}
		cursor = cursor->next;
	}
	return FAILURE;
}

result_t SinglyLinkedList::delete_from_begining()
{
	node *cursor = head->next;
	if(cursor->next == NULL)
	{
		head->next = NULL;
		return SUCCESS;
	}
	else
	{
		head->next = cursor->next;
		cursor->data = 0;
		cursor->next = NULL;
		free(cursor);
		return SUCCESS;
	}
}

result_t SinglyLinkedList::delete_from_end()
{
	node *cursor = head->next;
	if(cursor->data == tail->data)
	{
		cursor->data = 0;
		cursor->next = NULL;
		free(cursor);
		head->next = NULL;
		return SUCCESS;
	}
	else
	{
		while(cursor->next->next != NULL)
		{
			cursor = cursor->next;
			continue;
		}
		cursor->next = NULL;
		free(cursor->next);
		tail = cursor;
		return SUCCESS;
	}
}

result_t SinglyLinkedList::delete_element(int element)
{
	node *cursor = head;
	while(cursor->next != NULL)
	{
		if(cursor->next->data == element)
		{
			node *temp = cursor->next;
			cursor->next = temp->next;
			free(temp);
			return SUCCESS;
		}
		cursor = cursor->next;
	}
}

result_t SinglyLinkedList::dispose()
{
	node *cursor = head;
	while(cursor->next != NULL)
	{
		node *temp = cursor->next;
		cursor->next = temp->next;
	}
	return SUCCESS;
}

result_t SinglyLinkedList::traverse()
{
	node* cursor = head->next;
	if(cursor != NULL)
	{
		while(cursor != NULL)
		{
			printf("%d ", cursor->data);
			cursor = cursor->next;
		}
		printf("\n");

		return 1;
	}
	else
	{
		return 0;
	}
}

result_t SinglyLinkedList::query_data(int data)
{
	node *cursor = head->next;
	while(cursor != NULL)
	{
		if(cursor->data == data)
			return SUCCESS;
		cursor = cursor->next;
	}
	return FAILURE;
}

int main()
{
	int command,data,element;
	result_t status;

	SinglyLinkedList s1;
	s1.menu();

	while(1)
	{
		printf("\nEnter command : ");
		scanf("%d",&command);

		if(command == -1)
		{
			break;
		}
		switch(command)
		{
			case 1:
				printf("Enter data to insert at begin : ");
				scanf("%d",&data);

				status = s1.insert_begin(data);
				if(status == SUCCESS)
					printf("Element inserted at begin of list successfully\n");
				else if(status == FAILURE)
					printf("Element can not be inserted");
				break;
			case 2:
				printf("Enter data to insert at end : ");
				scanf("%d",&data);

				status = s1.insert_end(data);
				if(status == SUCCESS)
					printf("Element inserted at end of list successfully\n");
				else if(status == FAILURE)
					printf("Element can not be inserted\n");
				break;
			case 3:
				printf("Enter element to insert before : ");
				scanf("%d",&element);

				printf("Enter data to insert : ");
				scanf("%d",&data);

				status = s1.search(element);
				if(status == SUCCESS)
				{
					status = s1.insert_before(element,data);
					if(status == SUCCESS)
					{
						printf("Element inserted before successfully\n");
					}
					else if(status == FAILURE)
					{
						printf("Before element insertion failed\n");
					}
				}
				else if(status == FAILURE)
				{
					printf("Before element not found\n");
				}
				break;
			case 4:
				printf("Enter element to insert after : ");
				scanf("%d",&element);

				printf("Enter data to insert : ");
				scanf("%d",&data);

				status = s1.search(element);
				if(status == SUCCESS)
				{
					status = s1.insert_after(element,data);
					if(status == SUCCESS)
					{
						printf("Element inserted after successfully\n");
					}
					else if(status == FAILURE)
					{
						printf("After element insertion failed\n");
					}
				}
				else if(status == FAILURE)
				{
					printf("After element not found\n");
				}
				break;
			case 5:
				status = s1.is_empty();
				if(status == SUCCESS)
				{
					printf("List is empty\n");
				}
				else
				{
					status = s1.delete_from_begining();
					if(status == SUCCESS)
						printf("Element deleted from begining successfully\n");
					else if(status == FAILURE)
						printf("Delete element from begining failed\n");
				}
				break;
			case 6:
				status = s1.is_empty();
				if(status == SUCCESS)
				{
					printf("List is empty\n");
				}
				else
				{
					status = s1.delete_from_end();
					if(status == SUCCESS)
						printf("Element deleted from end successfully\n");
					else if(status == FAILURE)
						printf("Delete element from end failed\n");
				}
				break;
			case 7:
				printf("Enter element to delete : ");
				scanf("%d",&element);

				status = s1.search(element);
				if(status == SUCCESS)
				{
					status = s1.delete_element(element);
					if(status == SUCCESS)
						printf("Element deleted successfully\n");
					else if(status == FAILURE)
						printf("Delete element failed\n");
				}
				else
				{
					printf("Delete element not found\n");
				}
				break;
			case 8:
				status = s1.is_empty();
				if(status == SUCCESS)
				{
					printf("List is empty\n");
				}
				else
				{
					status = s1.dispose();
					if(status == SUCCESS)
						printf("List deleted successfully\n");
					else if(status == FAILURE)
						printf("Delete list failed\n");
				}
				break;
			case 9:
				status = s1.is_empty();
				if(status == SUCCESS)
				{
					printf("List is empty\n");
				}
				else if(status == FAILURE)
				{
					printf("List is not empty\n");
				}
				break;
			case 10:
				printf("Enter data to find : ");
				scanf("%d",&data);

				status = s1.is_empty();
				if(status == SUCCESS)
				{
					printf("List is empty\n");
				}
				else
				{
					status = s1.query_data(data);
					if(status == SUCCESS)
						printf("Data found\n");
					else if(status == FAILURE)
						printf("Data not found\n");
				}
				break;
			case 11:
				status = s1.is_empty();
				if(status == SUCCESS)
				{
					printf("List is empty\n");
				}
				else if(status == FAILURE)
				{
					status = s1.traverse();
				}
				
				break;
			default:
				break;
				
		}
	}
	return 0;
}
