#include <windows.h>
#include <stdio.h>

// for GLSL extensions IMPORTANT : This line should be before #include<gl\gl.h> and #include<gl\glu.h>
#include <gl\glew.h>

#include <gl\GL.h>
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations

#define PI 3.1415

FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao;
GLuint gVbo_position;
GLuint gVbo_color;
GLuint gMVPUniform;

mat4 gPerspectiveProjectionMatrix;

float incrementFlag  = 0.0f;

// eye vector coordinates for camera
/*
vmath::vec3 eye[15] = {
	{ -0.4994314908981323, 0.4276775121688843, 1.8590971231460571  },

	//{ -0.49847251176834106, -1.7175962924957275, 0.4276775121688843 },
	{ -0.49847251176834106, 0.4276775121688843, 1.7175962924957275,  },

	//{ -0.49767032265663147, -1.5760952234268188, 0.42767754197120667 },
	{ -0.49767032265663147, 0.42767754197120667, 1.5760952234268188 },

	{ -0.4969762861728668, -1.4345929622650146, 0.4276774525642395 },
	{ -0.4964524507522583, -1.2930908203125, 0.4276774525642395 },
	{ -0.49620184302330017, -1.1515883207321167, 0.4276774823665619 },
	{ -0.49635717272758484, -1.010085940361023, 0.42767754197120667 },
	{ -0.49720823764801025, -0.8685873746871948, 0.4276775121688843 },
	{ -0.49895429611206055, -0.727095365524292, 0.42767757177352905 },
	{ -0.500070333480835, -0.5855920314788818, 0.4276775121688843 },
	{ -0.4978017210960388, -0.44410455226898193, 0.42767754197120667 },

	//{ -0.4793265461921692, -0.30403828620910645, 0.42767754197120667 },
	{ -0.4793265461921692, 0.42767754197120667, 0.30403828620910645 },

	{ -0.42133617401123047, -0.17585980892181396, 0.4276775121688843 },
	{ -0.31926047801971436, -0.0793723464012146, 0.4276775121688843 },
	{ -0.18830418586730957, -0.02756919339299202, 0.42767754197120667 }
};

// center vector coordinates for camera
vmath::vec3 center[15] =	{ 
						{ -0.4994317293167114, -1.8590971231460571, 0.0053758020512759686 },
						{ -0.49847251176834106, -1.7175965309143066, 0.005375802516937256 },
						{ -0.49767011404037476, -1.5760952234268188, 0.005375802982598543 },

						//{ -0.4969763457775116, -1.4345933198928833, 0.005375772714614868 },
						//{ -0.4969763457775116, 0.0, -1.4345933198928833 },
						//{ -0.4969763457775116, 0.005375772714614868, 1.4345933198928833 },
						//{ -0.4969763457775116, 0.005375772714614868, 1.0 },
						{ -0.4969763457775116, 0.005375772714614868, 1.4345933198928833 },

						//{ -0.49645280838012695, -1.2930904626846313, 0.005375772248953581 },
						{ -0.49645280838012695, 0.005375772248953581, 1.2930904626846313,  },

						//{ -0.49620160460472107, -1.151587963104248, 0.005375711712986231 },
						{ -0.49620160460472107, 0.005375711712986231, 1.151587963104248,  },

						{ -0.49635714292526245, -1.0100855827331543, 0.0053757717832922935 },
						{ -0.49720829725265503, -0.8685868978500366, 0.005375772248953581 },
						{ -0.4989543855190277, -0.7270948886871338, 0.005375771317631006 },
						{ -0.5000704526901245, -0.5855917930603027, 0.0053757717832922935 },
						{ -0.4978015422821045, -0.4441044330596924, 0.005375773645937443 },
						{ -0.479325532913208, -0.3040374517440796, 0.005375772248953581 },
						{ -0.4213331937789917, -0.17585933208465576, 0.005375772248953581 },
						{ -0.31925535202026367, -0.0793723464012146, 0.0053757415153086185 },

						//{ -0.18829822540283203, -0.027570635080337524, 0.005375772248953581 }
						{ -0.18829822540283203, 0.005375772248953581, 0.027570635080337524 }
					};
*/

// eye vector coordinates for camera
vmath::vec3 eye[100] = {
	{ -0.4994314908981323, -1.8590971231460571, 0.4276779592037201 },
	{ -0.49847251176834106, -1.7175962924957275, 0.4276779592037201 },
	{ -0.49767032265663147, -1.5760952234268188, 0.4276779890060425 },
	{ -0.4969762861728668, -1.4345929622650146, 0.4276778995990753 },
	{ -0.4964524507522583, -1.2930908203125, 0.4276778995990753 },
	{ -0.49620184302330017, -1.1515883207321167, 0.4276779294013977 },
	{ -0.49635717272758484, -1.010085940361023, 0.4276779890060425 },
	{ -0.49720823764801025, -0.8685873746871948, 0.4276779592037201 },
	{ -0.49895429611206055, -0.727095365524292, 0.42767801880836487 },
	{ -0.500070333480835, -0.5855920314788818, 0.4276779592037201 },
	{ -0.4978017210960388, -0.44410455226898193, 0.4276779890060425 },
	{ -0.4793265461921692, -0.30403828620910645, 0.4276779890060425 },
	{ -0.42133617401123047, -0.17585980892181396, 0.4276779592037201 },
	{ -0.31926047801971436, -0.0793723464012146, 0.4276779592037201 },
	{ -0.18830418586730957, -0.02756919339299202, 0.4276779890060425 },
	{ -0.04826080799102783, -0.008124858140945435, 0.4276779592037201 },
	{ 0.09311151504516602, -0.0019986629486083984, 0.4276778995990753 },
	{ 0.23461008071899414, -0.00031808018684387207, 0.4276779592037201 },
	{ 0.3761157989501953, -8.690357208251953e-05, 0.4276779294013977 },
	{ 0.5176198482513428, -0.0003103017807006836, 0.4276779890060425 },
	{ 0.6591229438781738, -0.0006315410137176514, 0.4276779294013977 },
	{ 0.8006269931793213, -0.0009247064590454102, 0.4276779592037201 },
	{ 0.9421286582946777, -0.0008452236652374268, 0.4276779294013977 },
	{ 1.0836081504821777, 0.0008988082408905029, 0.4276778995990753 },
	{ 1.2246692180633545, 0.010234728455543518, 0.4276779294013977 },
	{ 1.3578037023544312, 0.0535312294960022, 0.4276779890060425 },
	{ 1.452507495880127, 0.15581846237182617, 0.4276779890060425 },
	{ 1.496904969215393, 0.2892172336578369, 0.4276779592037201 },
	{ 1.5102322101593018, 0.4298948049545288, 0.4276779890060425 },
	{ 1.5109822750091553, 0.5713450908660889, 0.4276779890060425 },
	{ 1.5008480548858643, 0.7123434543609619, 0.42767801880836487 },
	{ 1.46278977394104, 0.847719669342041, 0.4276779592037201 },
	{ 1.3689723014831543, 0.9499268531799316, 0.4276769161224365 },
	{ 1.2352689504623413, 0.9920660257339478, 0.42761802673339844 },
	{ 1.0941356420516968, 0.9991260766983032, 0.4271598160266876 },
	{ 0.9526312351226807, 0.9992243051528931, 0.42687785625457764 },
	{ 0.8111274242401123, 0.999230682849884, 0.42692914605140686 },
	{ 0.669623851776123, 0.9992244243621826, 0.42707559466362 },
	{ 0.5281200408935547, 0.9992225170135498, 0.4272555112838745 },
	{ 0.3866158723831177, 0.9992405772209167, 0.42743635177612305 },
	{ 0.24511277675628662, 0.9992890357971191, 0.4275825619697571 },
	{ 0.10360896587371826, 0.999356210231781, 0.4276556372642517 },
	{ -0.03789472579956055, 0.9993990659713745, 0.4276750087738037 },
	{ -0.1793978214263916, 0.9994149208068848, 0.4276779890060425 },
	{ -0.32090234756469727, 0.999417781829834, 0.4276781976222992 },
	{ -0.46240687370300293, 0.9994245767593384, 0.4276781678199768 },
	{ -0.6039092540740967, 0.9994696378707886, 0.4276781380176544 },
	{ -0.7454140186309814, 0.9996766448020935, 0.4276781380176544 },
	{ -0.8869187831878662, 1.0004141330718994, 0.4276781976222992 },
	{ -1.1699150800704956, 1.0034067630767822, 0.4276781678199768 },
	{ -1.3106846809387207, 1.0160694122314453, 0.4276750385761261 },
	{ -1.4417567253112793, 1.0669920444488525, 0.4274776875972748 },
	{ -1.525254726409912, 1.176985740661621, 0.42638903856277466 },
	{ -1.5374970436096191, 1.3173680305480957, 0.42631271481513977 },
	{ -1.5374740362167358, 1.4588677883148193, 0.4271009862422943 },
	{ -1.537484884262085, 1.6003708839416504, 0.42741701006889343 },
	{ -1.5375038385391235, 1.7418746948242188, 0.4275607168674469 },
	{ -1.5375233888626099, 1.8833775520324707, 0.4276279807090759 },
	{ -1.537541389465332, 2.02488112449646, 0.4276582896709442 },
	{ -1.5375570058822632, 2.166384696960449, 0.4276706278324127 },
	{ -1.5375691652297974, 2.307889223098755, 0.4276743531227112 },
	{ -1.5375772714614868, 2.449392318725586, 0.4276750087738037 },
	{ -1.5375828742980957, 2.5908970832824707, 0.4276749789714813 },
	{ -1.5375759601593018, 2.73240065574646, 0.4276750087738037 },
	{ -1.5375100374221802, 2.873905897140503, 0.4276749789714813 },
	{ -1.537274718284607, 3.0154097080230713, 0.4276750385761261 },
	{ -1.5366735458374023, 3.1569135189056396, 0.4276750087738037 },
	{ -1.5353479385375977, 3.2984137535095215, 0.4276750385761261 },
	{ -1.5326735973358154, 3.4398961067199707, 0.4276750385761261 },
	{ -1.5274064540863037, 3.5813076496124268, 0.4276750385761261 },
	{ -1.5164909362792969, 3.7223873138427734, 0.4276750385761261 },
	{ -1.4903157949447632, 3.861236810684204, 0.4276750683784485 },
	{ -1.4085702896118164, 3.9712162017822266, 0.4276750087738037 },
	{ -1.2706990242004395, 3.996736526489258, 0.4276750385761261 },
	{ -1.1291956901550293, 3.9979000091552734, 0.4276750385761261 },
	{ -0.9876891374588013, 3.997264862060547, 0.4276750087738037 },
	{ -0.8464692831039429, 3.989015579223633, 0.4276750385761261 },
	{ -0.7100967168807983, 3.9533207416534424, 0.4276750087738037 },
	{ -0.5963731408119202, 3.8710274696350098, 0.4276750981807709 },
	{ -0.5275167226791382, 3.748832941055298, 0.4276750385761261 },
	{ -0.5085028409957886, 3.6090025901794434, 0.42767494916915894 },
	{ -0.5055590867996216, 3.467526912689209, 0.4276750087738037 },
	{ -0.4993320405483246, 3.3262102603912354, 0.4276750683784485 },
	{ -0.47291624546051025, 3.1879191398620605, 0.4276750385761261 },
	{ -0.38671112060546875, 3.0801618099212646, 0.4276750683784485 },
	{ -0.25418782234191895, 3.034658193588257, 0.4276750087738037 },
	{ -0.11372542381286621, 3.01870059967041, 0.4276750087738037 },
	{ 0.027557969093322754, 3.0113303661346436, 0.4276750683784485 },
	{ 0.16899216175079346, 3.0072689056396484, 0.4276750385761261 },
	{ 0.3104696273803711, 3.004768133163452, 0.4276750385761261 },
	{ 0.45196449756622314, 3.003337860107422, 0.4276750683784485 },
	{ 0.5934643745422363, 3.002448558807373, 0.4276750087738037 },
	{ 0.7349646091461182, 3.0018441677093506, 0.4276750683784485 },
	{ 0.8764677047729492, 3.0015013217926025, 0.4276750385761261 },
	{ 1.0179705619812012, 3.0013160705566406, 0.4276750385761261 },
	{ 1.1594750881195068, 3.001149892807007, 0.4276750087738037 },
	{ 1.3009791374206543, 3.001124620437622, 0.4276750385761261 },
	{ 1.4424827098846436, 3.0011024475097656, 0.4276750087738037 },
	{ 1.583986759185791, 3.0010809898376465, 0.4276750385761261 },
	{ 1.583986759185791, 3.0010809898376465, 0.4276750385761261 },
};

// center vector coordinates for camera
vmath::vec3 center[100] = {
	{ -0.49696972966194153, -1.4345935583114624, -0.008174649439752102 },
	{ -0.4964461922645569, -1.2930907011032104, -0.008174648508429527 },
	{ -0.496194988489151, -1.1515882015228271, -0.008174706250429153 },
	{ -0.4963505268096924, -1.0100858211517334, -0.008174648508429527 },
	{ -0.4972016513347626, -0.8685871362686157, -0.008174649439752102 },
	{ -0.49894776940345764, -0.7270951271057129, -0.008174648508429527 },
	{ -0.5000637769699097, -0.5855920314788818, -0.008174648508429527 },
	{ -0.4977949261665344, -0.4441049098968506, -0.008174651302397251 },
	{ -0.4793192148208618, -0.3040393590927124, -0.008174649439752102 },
	{ -0.42132794857025146, -0.17586326599121094, -0.008174649439752102 },
	{ -0.31925201416015625, -0.07937806844711304, -0.008174678310751915 },
	{ -0.18829703330993652, -0.027577130123972893, -0.008174650371074677 },
	{ -0.04825401306152344, -0.00813281536102295, -0.008174650371074677 },
	{ 0.09311747550964355, -0.0020070672035217285, -0.008174619637429714 },
	{ 0.23461651802062988, -0.0003267526626586914, -0.008174678310751915 },
	{ 0.37612223625183105, -9.560585021972656e-05, -0.008174651302397251 },
	{ 0.5176265239715576, -0.00031879544258117676, -0.008174650371074677 },
	{ 0.6591300964355469, -0.0006400644779205322, -0.008174649439752102 },
	{ 0.8006336688995361, -0.0009329319000244141, -0.008174621500074863 },
	{ 0.9421350955963135, -0.0008534789085388184, -0.008174650371074677 },
	{ 1.0836150646209717, 0.0008902549743652344, -0.008174648508429527 },
	{ 1.224677324295044, 0.01022714376449585, -0.008174650371074677 },
	{ 1.3578124046325684, 0.05352640151977539, -0.008174649439752102 },
	{ 1.452515959739685, 0.15581679344177246, -0.008174621500074863 },
	{ 1.4969123601913452, 0.28921711444854736, -0.008174649439752102 },
	{ 1.5102390050888062, 0.42989516258239746, -0.00817467924207449 },
	{ 1.510988712310791, 0.571345329284668, -0.008174649439752102 },
	{ 1.5008540153503418, 0.7123439311981201, -0.008174589835107327 },
	{ 1.4627938270568848, 0.8477218151092529, -0.00817467924207449 },
	{ 1.368972659111023, 0.9499286413192749, -0.008175719529390335 },
	{ 1.2352755069732666, 0.9920647144317627, -0.008234640583395958 },
	{ 1.0941665172576904, 0.999122142791748, -0.008692699484527111 },
	{ 0.9526300430297852, 0.9992202520370483, -0.008974834345281124 },
	{ 0.8111171722412109, 0.9992270469665527, -0.008923483081161976 },
	{ 0.6696107387542725, 0.9992207884788513, -0.008776997216045856 },
	{ 0.5281059741973877, 0.9992189407348633, -0.00859711691737175 },
	{ 0.3866029977798462, 0.9992362260818481, -0.008416244760155678 },
	{ 0.24510300159454346, 0.9992848634719849, -0.008270032703876495 },
	{ 0.10360360145568848, 0.999352216720581, -0.008196969516575336 },
	{ -0.03789854049682617, 0.999395489692688, -0.008177588693797588 },
	{ -0.17940163612365723, 0.9994108080863953, -0.00817466713488102 },
	{ -0.3209054470062256, 0.9994137287139893, -0.008174457587301731 },
	{ -0.46240997314453125, 0.999421238899231, -0.008174457587301731 },
	{ -0.6039130687713623, 0.9994659423828125, -0.00817448552697897 },
	{ -0.7454168796539307, 0.9996728301048279, -0.008174457587301731 },
	{ -0.8869216442108154, 1.0004103183746338, -0.008174427784979343 },
	{ -1.028420329093933, 1.0016167163848877, -0.008174428716301918 },
	{ -1.1699179410934448, 1.0034033060073853, -0.008174456655979156 },
	{ -1.3106863498687744, 1.016066551208496, -0.008177606388926506 },
	{ -1.4417319297790527, 1.066978931427002, -0.008374928496778011 },
	{ -1.5251855850219727, 1.1769256591796875, -0.009463133290410042 },
	{ -1.5374326705932617, 1.3174357414245605, -0.00953933410346508 },
	{ -1.5374095439910889, 1.4588935375213623, -0.008751314133405685 },
	{ -1.5374205112457275, 1.6003825664520264, -0.008435329422354698 },
	{ -1.5374393463134766, 1.741880178451538, -0.008291698060929775 },
	{ -1.537458896636963, 1.8833801746368408, -0.008224453777074814 },
	{ -1.5374774932861328, 2.0248827934265137, -0.008194095455110073 },
	{ -1.537492275238037, 2.1663858890533447, -0.00818182434886694 },
	{ -1.5375044345855713, 2.307889461517334, -0.008178017102181911 },
	{ -1.5375127792358398, 2.4493942260742188, -0.008177385665476322 },
	{ -1.53751802444458, 2.590898275375366, -0.00817735493183136 },
	{ -1.5375120639801025, 2.7324016094207764, -0.008177325129508972 },
	{ -1.5374460220336914, 2.8739068508148193, -0.008177356794476509 },
	{ -1.5372097492218018, 3.0154104232788086, -0.008177384734153748 },
	{ -1.5366088151931763, 3.156914472579956, -0.008177386596798897 },
	{ -1.5352835655212402, 3.2984132766723633, -0.008177385665476322 },
	{ -1.5326093435287476, 3.4398951530456543, -0.008177324198186398 },
	{ -1.5273419618606567, 3.5813052654266357, -0.008177355863153934 },
	{ -1.5164265632629395, 3.7223801612854004, -0.00817735493183136 },
	{ -1.490253210067749, 3.861217737197876, -0.00817735493183136 },
	{ -1.4085352420806885, 3.9711594581604004, -0.008177384734153748 },
	{ -1.2706904411315918, 3.9966704845428467, -0.008177354000508785 },
	{ -1.1291892528533936, 3.9978325366973877, -0.00817735306918621 },
	{ -0.987683892250061, 3.9971985816955566, -0.00817735493183136 },
	{ -0.8464707136154175, 3.9889492988586426, -0.00817735493183136 },
	{ -0.7101168632507324, 3.953258514404297, -0.00817735493183136 },
	{ -0.596415102481842, 3.870978832244873, -0.008177354000508785 },
	{ -0.5275731682777405, 3.7488081455230713, -0.008177326060831547 },
	{ -0.5085620880126953, 3.6089930534362793, -0.008177383802831173 },
	{ -0.505618691444397, 3.4675190448760986, -0.008177326060831547 },
	{ -0.49939095973968506, 3.3261964321136475, -0.008177354000508785 },
	{ -0.4729706048965454, 3.187889575958252, -0.00817735493183136 },
	{ -0.3867391347885132, 3.080104351043701, -0.00817735493183136 },
	{ -0.254192590713501, 3.0345916748046875, -0.00817741360515356 },
	{ -0.11372292041778564, 3.018634796142578, -0.008177384734153748 },
	{ 0.027562260627746582, 3.011265754699707, -0.008177326060831547 },
	{ 0.1689971685409546, 3.0072031021118164, -0.008177384734153748 },
	{ 0.3104759454727173, 3.004701614379883, -0.008177325129508972 },
	{ 0.4519706964492798, 3.0032711029052734, -0.008177354000508785 },
	{ 0.593470573425293, 3.0023818016052246, -0.00817735306918621 },
	{ 0.734971284866333, 3.001777172088623, -0.008177326992154121 },
	{ 0.8764736652374268, 3.0014350414276123, -0.008177355863153934 },
	{ 1.0179774761199951, 3.0012495517730713, -0.00817735493183136 },
	{ 1.159482479095459, 3.001084566116333, -0.00817735493183136 },
	{ 1.3009862899780273, 3.00105881690979, -0.008177385665476322 },
	{ 1.4424898624420166, 3.0010366439819336, -0.00817735493183136 },
	{ 1.5839941501617432, 3.001014471054077, -0.008177355863153934 }
};

// up vector coordinates for camera
vmath::vec3 up = { 0.0f,1.0f,0.0f };

// true - camera movement is started
// false - camera movement is not started or stopped
GLboolean moveCamera = false;

// i have devided this walkthrough into eight parts for understanding and calculations
// first path starts when camera movement start after pressing m key and it continues uptill camera reaches at starting position of first tile
GLboolean gbFirstPathComplete = false;
// second path is from first tile to third tile
GLboolean gbSecondPathComplete = false;
// third path start from third tile to fourth tile
GLboolean gbThirdPathComplete = false;
// fourth path start from fourth tile to seventh tile and it goes on
GLboolean gbFourthPathComplete = false;
GLboolean gbFifthPathComplete = false;
GLboolean gbSixthPathComplete = false;
GLboolean gbSeventhPathComplete = false;
GLboolean gbEighthPathComplete = false;

// each path has two phases
// first is straight camera movement and after that circular camera movement to enter in next path
// this flag indicate if straight movement of particular path is completed or not
// true - straight movement is completed and proceed for circular movement for particular path
// false - straight movement is not completed for particular path
// NOTE : this flag will reset to false after completion of both phases of particular path
GLboolean gbStraightMovement = false;

// This indicates by how much distance the camera will move
GLfloat gfMovement = 0.0001f;
GLfloat xMovementBy = 0.0001f;

// angle used for circular camera movement
double gfAngle = 0.0f;

float gfRadian;

// radius of each circular camera movement
GLfloat gfRadius = 0.5f;

// eye coordinate of X and Z axis for circular path
GLfloat gfCircularPathEyeXCoordinate;
GLfloat gfCircularPathEyeZCoordinate;

// center coordinate of X and Z axis for circular path
GLfloat gfCircularPathCenterXCoordinate;
GLfloat gfCircularPathCenterZCoordinate;

int i = 0;
GLfloat temp;
GLint noOfSteps = 200;
GLfloat stepIncrementBy = 0;

GLfloat xEyeInc = 0.0;
GLfloat yEyeInc = 0.0;
GLfloat zEyeInc = 0.0;

GLfloat xCenterInc = 0.0;
GLfloat yCenterInc = 0.0;
GLfloat zCenterInc = 0.0;

GLint currentStep = 0;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created\nExiting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Simple Walkthrough - Programmable Pipeline"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4D:
			moveCamera = true;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// *** VERTEX SHADER *** //
	// create vertex shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHAER *** //
	// create fragment shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 430" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create program
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-building of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	// pre-building of shader program object with vertex shader color attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	// vertices, colors, shader attribs, vbo, vao initializations
	const GLfloat quadVertices[] =
	{
		-1.0f,0.0f,-0.5f,
		-1.0f,0.0f,0.5f,
		0.0f,0.0f,0.5f,
		0.0f,0.0f,-0.5f,

		0.1f,0.0f,-0.5f,
		0.1f,0.0f,0.5f,
		1.0f,0.0f,0.5f,
		1.0f,0.0f,-0.5f,

		1.1f,0.0f,-0.5f,
		1.1f,0.0f,0.5f,
		2.0f,0.0f,0.5f,
		2.0f,0.0f,-0.5f,

		1.1f,0.0f,-1.5f,
		1.1f,0.0f,-0.6f,
		2.0f,0.0f,-0.6f,
		2.0f,0.0f,-1.5f,

		0.1f,0.0f,-1.5f,
		0.1f,0.0f,-0.6f,
		1.0f,0.0f,-0.6f,
		1.0f,0.0f,-1.5f,

		-1.0f,0.0f,-1.5f,
		-1.0f,0.0f,-0.6f,
		0.0f,0.0f,-0.6f,
		0.0f,0.0f,-1.5f,

		-2.0f,0.0f,-1.5f,
		-2.0f,0.0f,-0.6f,
		-1.1f,0.0f,-0.6f,
		-1.1f,0.0f,-1.5f,

		-2.0f,0.0f,-2.5f,
		-2.0f,0.0f,-1.6f,
		-1.1f,0.0f,-1.6f,
		-1.1f,0.0f,-2.5f,

		-2.0f,0.0f,-3.5f,
		-2.0f,0.0f,-2.6f,
		-1.1f,0.0f,-2.6f,
		-1.1f,0.0f,-3.5f,

		-2.0f,0.0f,-4.5f,
		-2.0f,0.0f,-3.6f,
		-1.1f,0.0f,-3.6f,
		-1.1f,0.0f,-4.5f,

		-1.0f,0.0f,-4.5f,
		-1.0f,0.0f,-3.6f,
		0.0f,0.0f,-3.6f,
		0.0f,0.0f,-4.5f,

		-1.0f,0.0f,-3.5f,
		-1.0f,0.0f,-2.6f,
		0.0f,0.0f,-2.6f,
		0.0f,0.0f,-3.5f,

		0.1f,0.0f,-3.5f,
		0.1f,0.0f,-2.6f,
		1.0f,0.0f,-2.6f,
		1.0f,0.0f,-3.5f,

		1.1f,0.0f,-3.5f,
		1.1f,0.0f,-2.6f,
		2.0f,0.0f,-2.6f,
		2.0f,0.0f,-3.5f,
	};

	// generate vao
	glGenVertexArrays(1, &gVao);

	// bind with vao
	glBindVertexArray(gVao);

	// ******************
	// VBO FOR VERTICES
	// ******************
	glGenBuffers(1, &gVbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// ******************
	// COLOR FOR QUAD
	// ******************
	glVertexAttrib3f(VDG_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);

	// unbind from vao
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// set perspective matrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);

	xEyeInc = (-eye[i][0] + eye[i + 1][0]) / noOfSteps;
	yEyeInc = (-eye[i][1] + eye[i + 1][1]) / noOfSteps;
	zEyeInc = (-eye[i][2] + eye[i + 1][2]) / noOfSteps;

	fprintf(gpFile, "eye %f %f\n", eye[i][0], eye[i + 1][0]);
	fprintf(gpFile, "%f\n", xEyeInc);

	xCenterInc = (-center[i][0] + center[i + 1][0]) / noOfSteps;
	yCenterInc = (-center[i][1] + center[i + 1][1]) / noOfSteps;
	zCenterInc = (-center[i][2] + center[i + 1][2]) / noOfSteps;

	fprintf(gpFile, "center %f %f\n", center[i][0], center[i+1][0]);
	fprintf(gpFile, "%f\n", xCenterInc);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// start using opengl program object
	glUseProgram(gShaderProgramObject);

	//vmath::mat4 lookAtMatrix = vmath::lookat(eye[49], center[49], up);

	/*if (i <= 49)
	{
		temp = eye[i][1];
		eye[i][1] = eye[i][2];
		eye[i][2] = -temp;

		temp = center[i][1];
		center[i][1] = center[i][2];
		center[i][2] = -temp;
	}*/

	/*fprintf(gpFile, "%d - Eye - %f %f %f .\n",i,eye[i][0], eye[i][1], eye[i][2]);
	fprintf(gpFile, "%d - Center - %f %f %f .\n", i, center[i][0], center[i][1], center[i][2]);
	fprintf(gpFile, "-------------------------------------------------------------------\n");*/
	vmath::vec3 eyeLookAt;
	vmath::vec3 centerLookAt;

	eyeLookAt[0] = eye[i][0];
	eyeLookAt[1] = eye[i][2];
	eyeLookAt[2] = -eye[i][1];

	centerLookAt[0] = center[i][0];
	centerLookAt[1] = center[i][2];
	centerLookAt[2] = -center[i][1];

	vmath::mat4 lookAtMatrix = vmath::lookat(eyeLookAt, centerLookAt, up);

	// opengl drawing
	// set modelview & modelviewprojection  matrices to identity
	mat4 modelViewMatrix = mat4::identity();
	modelViewMatrix = modelViewMatrix * lookAtMatrix;
	mat4 modelViewProjectionMatrix = mat4::identity();

	// multiply modelview and orthographic matrix to get modelviewprojection matrix
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

	// pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
	// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	// bind vao
	glBindVertexArray(gVao);

	// draw either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 4(each with its x,y,z) vertices in quadVertices array
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 28, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 32, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 36, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 40, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 44, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 48, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 52, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 56, 4);

	// unbind vao
	glBindVertexArray(0);

	// stop using opengl program object
	glUseProgram(0);

	//if (incrementFlag > 20.0f)
	//{
	//	if (i < 45)
	//	{
	//		i++;
	//		temp = eye[i][1];
	//		eye[i][1] = eye[i][2];
	//		eye[i][2] = -temp;

	//		temp = center[i][1];
	//		center[i][1] = center[i][2];
	//		center[i][2] = -temp;
	//	}
	//	incrementFlag = 0.0f;
	//}	
	//incrementFlag += 0.1f;

	if (moveCamera == true)
	{
		if (currentStep < noOfSteps)
		{
			eye[i][0] = eye[i][0] + (xEyeInc);
			eye[i][1] = eye[i][1] + (yEyeInc);
			eye[i][2] = eye[i][2] + (zEyeInc);

			center[i][0] = center[i][0] + (xCenterInc);
			center[i][1] = center[i][1] + (yCenterInc);
			center[i][2] = center[i][2] + (zCenterInc);



			fprintf(gpFile, "centerX %f\n", center[i][0]);
			fprintf(gpFile, "centerY %f\n", center[i][1]);
			fprintf(gpFile, "centerZ %f\n", center[i][2]);

			fprintf(gpFile, "eyeX %f\n", eye[i][0]);
			fprintf(gpFile, "eyeY %f\n", eye[i][1]);
			fprintf(gpFile, "eyeZ %f\n", eye[i][2]);

			currentStep++;
		}
		else
		{
			//if (i < 45)
			//{
			i++;
			xEyeInc = (-eye[i][0] + eye[i + 1][0]) / noOfSteps;
			yEyeInc = (-eye[i][1] + eye[i + 1][1]) / noOfSteps;
			zEyeInc = (-eye[i][2] + eye[i + 1][2]) / noOfSteps;

			xCenterInc = (-center[i][0] + center[i + 1][0]) / noOfSteps;
			yCenterInc = (-center[i][1] + center[i + 1][1]) / noOfSteps;
			zCenterInc = (-center[i][2] + center[i + 1][2]) / noOfSteps;


			currentStep = 0;
			fprintf(gpFile, "i %d\n", i);
			fprintf(gpFile, "xCenter %f\n", xCenterInc);
			fprintf(gpFile, "center %f %f\n", center[i][0], center[i + 1][0]);
			fprintf(gpFile, "yCenter %f\n", yCenterInc);
			fprintf(gpFile, "center %f %f\n", center[i][1], center[i + 1][1]);
			fprintf(gpFile, "zCenter %f\n", zCenterInc);
			fprintf(gpFile, "center %f %f\n", center[i][2], center[i + 1][2]);

			fprintf(gpFile, "xEye %f\n", xEyeInc);
			fprintf(gpFile, "Eye %f %f\n", eye[i][0], eye[i + 1][0]);
			fprintf(gpFile, "yEye %f\n", yEyeInc);
			fprintf(gpFile, "Eye %f %f\n", eye[i][1], eye[i + 1][1]);
			fprintf(gpFile, "zEye %f\n", zEyeInc);
			fprintf(gpFile, "Eye %f %f\n", eye[i][2], eye[i + 1][2]);

			//}
		}
	}

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// detach vao
	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}
	if (gVbo_position)
	{
		glDeleteBuffers(1, &gVbo_position);
		gVbo_position = 0;
	}
	if (gVbo_color)
	{
		glDeleteBuffers(1, &gVbo_color);
		gVbo_color = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	// deselect rendering context
	wglMakeCurrent(NULL, NULL);

	// delete rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	// delete device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
