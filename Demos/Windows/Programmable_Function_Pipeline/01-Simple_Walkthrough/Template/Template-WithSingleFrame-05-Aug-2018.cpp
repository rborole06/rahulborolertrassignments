#include <windows.h>
#include <stdio.h>

// for GLSL extensions IMPORTANT : This line should be before #include<gl\gl.h> and #include<gl\glu.h>
#include <gl\glew.h>

#include <gl\GL.h>
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations

#define PI 3.1415

FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao;
GLuint gVbo_position;
GLuint gVbo_color;
GLuint gMVPUniform;

mat4 gPerspectiveProjectionMatrix;

// eye vector coordinates for camera
vmath::vec3 eye[15] = {
	{ -0.4994314908981323, 0.4276775121688843, 1.8590971231460571  },

	//{ -0.49847251176834106, -1.7175962924957275, 0.4276775121688843 },
	{ -0.49847251176834106, 0.4276775121688843, 1.7175962924957275,  },

	//{ -0.49767032265663147, -1.5760952234268188, 0.42767754197120667 },
	{ -0.49767032265663147, 0.42767754197120667, 1.5760952234268188 },

	{ -0.4969762861728668, -1.4345929622650146, 0.4276774525642395 },
	{ -0.4964524507522583, -1.2930908203125, 0.4276774525642395 },
	{ -0.49620184302330017, -1.1515883207321167, 0.4276774823665619 },
	{ -0.49635717272758484, -1.010085940361023, 0.42767754197120667 },
	{ -0.49720823764801025, -0.8685873746871948, 0.4276775121688843 },
	{ -0.49895429611206055, -0.727095365524292, 0.42767757177352905 },
	{ -0.500070333480835, -0.5855920314788818, 0.4276775121688843 },
	{ -0.4978017210960388, -0.44410455226898193, 0.42767754197120667 },

	//{ -0.4793265461921692, -0.30403828620910645, 0.42767754197120667 },
	{ -0.4793265461921692, 0.42767754197120667, 0.30403828620910645 },

	{ -0.42133617401123047, -0.17585980892181396, 0.4276775121688843 },
	{ -0.31926047801971436, -0.0793723464012146, 0.4276775121688843 },
	{ -0.18830418586730957, -0.02756919339299202, 0.42767754197120667 }
};

// center vector coordinates for camera
vmath::vec3 center[15] =	{ 
						{ -0.4994317293167114, -1.8590971231460571, 0.0053758020512759686 },
						{ -0.49847251176834106, -1.7175965309143066, 0.005375802516937256 },
						{ -0.49767011404037476, -1.5760952234268188, 0.005375802982598543 },

						//{ -0.4969763457775116, -1.4345933198928833, 0.005375772714614868 },
						//{ -0.4969763457775116, 0.0, -1.4345933198928833 },
						//{ -0.4969763457775116, 0.005375772714614868, 1.4345933198928833 },
						//{ -0.4969763457775116, 0.005375772714614868, 1.0 },
						{ -0.4969763457775116, 0.005375772714614868, 1.4345933198928833 },

						//{ -0.49645280838012695, -1.2930904626846313, 0.005375772248953581 },
						{ -0.49645280838012695, 0.005375772248953581, 1.2930904626846313,  },

						//{ -0.49620160460472107, -1.151587963104248, 0.005375711712986231 },
						{ -0.49620160460472107, 0.005375711712986231, 1.151587963104248,  },

						{ -0.49635714292526245, -1.0100855827331543, 0.0053757717832922935 },
						{ -0.49720829725265503, -0.8685868978500366, 0.005375772248953581 },
						{ -0.4989543855190277, -0.7270948886871338, 0.005375771317631006 },
						{ -0.5000704526901245, -0.5855917930603027, 0.0053757717832922935 },
						{ -0.4978015422821045, -0.4441044330596924, 0.005375773645937443 },
						{ -0.479325532913208, -0.3040374517440796, 0.005375772248953581 },
						{ -0.4213331937789917, -0.17585933208465576, 0.005375772248953581 },
						{ -0.31925535202026367, -0.0793723464012146, 0.0053757415153086185 },

						//{ -0.18829822540283203, -0.027570635080337524, 0.005375772248953581 }
						{ -0.18829822540283203, 0.005375772248953581, 0.027570635080337524 }
					};

// up vector coordinates for camera
vmath::vec3 up = { 0.0f,1.0f,0.0f };

// true - camera movement is started
// false - camera movement is not started or stopped
GLboolean moveCamera = false;

// i have devided this walkthrough into eight parts for understanding and calculations
// first path starts when camera movement start after pressing m key and it continues uptill camera reaches at starting position of first tile
GLboolean gbFirstPathComplete = false;
// second path is from first tile to third tile
GLboolean gbSecondPathComplete = false;
// third path start from third tile to fourth tile
GLboolean gbThirdPathComplete = false;
// fourth path start from fourth tile to seventh tile and it goes on
GLboolean gbFourthPathComplete = false;
GLboolean gbFifthPathComplete = false;
GLboolean gbSixthPathComplete = false;
GLboolean gbSeventhPathComplete = false;
GLboolean gbEighthPathComplete = false;

// each path has two phases
// first is straight camera movement and after that circular camera movement to enter in next path
// this flag indicate if straight movement of particular path is completed or not
// true - straight movement is completed and proceed for circular movement for particular path
// false - straight movement is not completed for particular path
// NOTE : this flag will reset to false after completion of both phases of particular path
GLboolean gbStraightMovement = false;

// This indicates by how much distance the camera will move
GLfloat gfMovement = 0.0001f;
GLfloat xMovementBy = 0.0001f;

// angle used for circular camera movement
double gfAngle = 0.0f;

float gfRadian;

// radius of each circular camera movement
GLfloat gfRadius = 0.5f;

// eye coordinate of X and Z axis for circular path
GLfloat gfCircularPathEyeXCoordinate;
GLfloat gfCircularPathEyeZCoordinate;

// center coordinate of X and Z axis for circular path
GLfloat gfCircularPathCenterXCoordinate;
GLfloat gfCircularPathCenterZCoordinate;

int i = 0;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created\nExiting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Simple Walkthrough - Programmable Pipeline"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4D:
			moveCamera = true;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// *** VERTEX SHADER *** //
	// create vertex shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHAER *** //
	// create fragment shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 430" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create program
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-building of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	// pre-building of shader program object with vertex shader color attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	// vertices, colors, shader attribs, vbo, vao initializations
	const GLfloat quadVertices[] =
	{
		-1.0f,0.0f,-0.5f,
		-1.0f,0.0f,0.5f,
		0.0f,0.0f,0.5f,
		0.0f,0.0f,-0.5f,

		0.1f,0.0f,-0.5f,
		0.1f,0.0f,0.5f,
		1.0f,0.0f,0.5f,
		1.0f,0.0f,-0.5f,

		1.1f,0.0f,-0.5f,
		1.1f,0.0f,0.5f,
		2.0f,0.0f,0.5f,
		2.0f,0.0f,-0.5f,

		1.1f,0.0f,-1.5f,
		1.1f,0.0f,-0.6f,
		2.0f,0.0f,-0.6f,
		2.0f,0.0f,-1.5f,

		0.1f,0.0f,-1.5f,
		0.1f,0.0f,-0.6f,
		1.0f,0.0f,-0.6f,
		1.0f,0.0f,-1.5f,

		-1.0f,0.0f,-1.5f,
		-1.0f,0.0f,-0.6f,
		0.0f,0.0f,-0.6f,
		0.0f,0.0f,-1.5f,

		-2.0f,0.0f,-1.5f,
		-2.0f,0.0f,-0.6f,
		-1.1f,0.0f,-0.6f,
		-1.1f,0.0f,-1.5f,

		-2.0f,0.0f,-2.5f,
		-2.0f,0.0f,-1.6f,
		-1.1f,0.0f,-1.6f,
		-1.1f,0.0f,-2.5f,

		-2.0f,0.0f,-3.5f,
		-2.0f,0.0f,-2.6f,
		-1.1f,0.0f,-2.6f,
		-1.1f,0.0f,-3.5f,

		-2.0f,0.0f,-4.5f,
		-2.0f,0.0f,-3.6f,
		-1.1f,0.0f,-3.6f,
		-1.1f,0.0f,-4.5f,

		-1.0f,0.0f,-4.5f,
		-1.0f,0.0f,-3.6f,
		0.0f,0.0f,-3.6f,
		0.0f,0.0f,-4.5f,

		-1.0f,0.0f,-3.5f,
		-1.0f,0.0f,-2.6f,
		0.0f,0.0f,-2.6f,
		0.0f,0.0f,-3.5f,

		0.1f,0.0f,-3.5f,
		0.1f,0.0f,-2.6f,
		1.0f,0.0f,-2.6f,
		1.0f,0.0f,-3.5f,

		1.1f,0.0f,-3.5f,
		1.1f,0.0f,-2.6f,
		2.0f,0.0f,-2.6f,
		2.0f,0.0f,-3.5f,
	};

	// generate vao
	glGenVertexArrays(1, &gVao);

	// bind with vao
	glBindVertexArray(gVao);

	// ******************
	// VBO FOR VERTICES
	// ******************
	glGenBuffers(1, &gVbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// ******************
	// COLOR FOR QUAD
	// ******************
	glVertexAttrib3f(VDG_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);

	// unbind from vao
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// set perspective matrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// start using opengl program object
	glUseProgram(gShaderProgramObject);

	vmath::mat4 lookAtMatrix = vmath::lookat(eye[11], center[14], up);

	// opengl drawing
	// set modelview & modelviewprojection  matrices to identity
	mat4 modelViewMatrix = mat4::identity();
	modelViewMatrix = modelViewMatrix * lookAtMatrix;
	mat4 modelViewProjectionMatrix = mat4::identity();

	// multiply modelview and orthographic matrix to get modelviewprojection matrix
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

	// pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
	// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	// bind vao
	glBindVertexArray(gVao);

	// draw either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 4(each with its x,y,z) vertices in quadVertices array
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 28, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 32, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 36, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 40, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 44, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 48, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 52, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 56, 4);

	// unbind vao
	glBindVertexArray(0);

	// stop using opengl program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(90.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// detach vao
	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}
	if (gVbo_position)
	{
		glDeleteBuffers(1, &gVbo_position);
		gVbo_position = 0;
	}
	if (gVbo_color)
	{
		glDeleteBuffers(1, &gVbo_color);
		gVbo_color = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	// deselect rendering context
	wglMakeCurrent(NULL, NULL);

	// delete rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	// delete device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
