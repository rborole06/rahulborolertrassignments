#include <windows.h>
#include <stdio.h>
#include <math.h>

#define PI 3.1415

int main(int argc, char * argv)
{
	float x, z;
	
	// create log file
	FILE *gpFile = NULL;
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		//MessageBox(NULL, TEXT("Log file can not be created\nExiting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}

	for (float angle = 180.0; angle >= 90; angle--)
	{
		float temp = angle * (PI / 180.0);

		//x = cos(temp);
		//z = -sin(temp);

		x = (cos(temp) * 0.5)+0;
		z = (-sin(temp) * 0.5)+1.3;
		fprintf(gpFile, "x : %f, z : %f, angle : %f\n", x, z, angle);
	}

	return 1;
}