
1 - This application is for exposure tone mapping
2 - Vertex and Fragment Shader code can be obtained from Superbible 7 code snippets / Superbible 7 Edition Book
3 - This code is compatible with KTX file format only
4 - To load bmp file, refer hdradaptive demo application
5 - KTX File format info - https://www.khronos.org/opengles/sdk/tools/KTX/