#include <windows.h>
#include <stdio.h>

//  for GLSL extensions IMPORTANT : This line should be before #include<gl\gl.h> and #include<gl\glu.h>
#include <gl\glew.h>

#include <gl\GL.h>
#include "Sphere.h"
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

int iWidth;
int iHeight;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gBlurFragmentShaderObject;
GLuint gBloomFragmentShaderObject;
GLuint gShaderProgramObject;
GLuint gBloomShaderProgramObject;
GLuint gBlurShaderProgramObject;

GLuint gNumVertices;
GLuint gNumElements;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

// light uniforms
GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

// first column, first material
GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

bool gbLight = false;

// bloom threshold min and max
GLfloat bloom_thresh_min = 0.0f;
GLfloat bloom_thresh_max = 10.0f;

GLfloat light_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_Diffuse[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_Specular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_Position[] = { 0.0f,0.0f,0.0f,0.0f };

// define material array for first sphere of first column
GLfloat material_1_1_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
GLfloat material_1_1_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
GLfloat material_1_1_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
GLfloat material_1_1_shininess = { 0.6f * 128.0f };

// define material array for second sphere of first column
GLfloat material_1_2_ambient[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_1_2_diffuse[] = { 0.54f,0.89f,0.63f,1.0f };
GLfloat material_1_2_specular[] = { 0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_1_2_shininess = { 0.1f * 128.0f };

// define material array for third sphere of first column
GLfloat material_1_3_ambient[] = { 0.05375f,0.05f,0.06625f,1.0f };
GLfloat material_1_3_diffuse[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_1_3_specular[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_1_3_shininess = { 0.3f * 128.0f };

// define material array for fourth sphere of first column
GLfloat material_1_4_ambient[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_1_4_diffuse[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_1_4_specular[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_1_4_shininess = { 0.088f * 128.0f };

// define material array for fifth sphere of first column
GLfloat material_1_5_ambient[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_1_5_diffuse[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_1_5_specular[] = { 0.727811f,0.626959f,0.626959f,1.0f };
GLfloat material_1_5_shininess = { 0.6f * 128.0f };

// define material array for six sphere of first column
GLfloat material_1_6_ambient[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_1_6_diffuse[] = { 0.396f,0.74151f,0.69102f,1.0f };
GLfloat material_1_6_specular[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_1_6_shininess = { 0.6f * 128.0f };

// define material array for first sphere of second column
GLfloat material_2_1_ambient[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_2_1_diffuse[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_2_1_specular[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_2_1_shininess = { 0.21794872f * 128.0f };

// define material array for second sphere of second column
GLfloat material_2_2_ambient[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_2_2_diffuse[] = { 0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_2_2_specular[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_2_2_shininess = { 0.2f * 128.0f };

// define material array for third sphere of second column
GLfloat material_2_3_ambient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat material_2_3_diffuse[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_2_3_specular[] = { 0.774597f,0.774597f,0.774597f,1.0f };
GLfloat material_2_3_shininess = { 0.6f * 128.0f };

// define material array for fourth sphere of second column
GLfloat material_2_4_ambient[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_2_4_diffuse[] = { 0.7038f,0.27048f,0.0828f,1.0f };
GLfloat material_2_4_specular[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_2_4_shininess = { 0.1f * 128.0f };

// define material array for fifth sphere of second column
GLfloat material_2_5_ambient[] = { 0.24725f,0.1995f,0.0745f,1.0f };
GLfloat material_2_5_diffuse[] = { 0.75164f,0.60648f,0.22648f,1.0f };
GLfloat material_2_5_specular[] = { 0.628281f,0.555802f,0.366065f,1.0f };
GLfloat material_2_5_shininess = { 0.4f * 128.0f };

// define material array for six sphere of second column
GLfloat material_2_6_ambient[] = { 0.19225f,0.19225f,0.19225f,1.0f };
GLfloat material_2_6_diffuse[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_2_6_specular[] = { 0.508273f,0.508273f,0.508273f,1.0f };
GLfloat material_2_6_shininess = { 0.4f * 128.0f };

// define material array for first sphere of third column
GLfloat material_3_1_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_3_1_specular[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_3_1_shininess = { 0.25f * 128.0f };

// define material array for second sphere of third column
GLfloat material_3_2_ambient[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_3_2_diffuse[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_3_2_specular[] = { 0.50196078f,0.50196078f,0.50196078f,1.0f };
GLfloat material_3_2_shininess = { 0.25f * 128.0f };

// define material array for third sphere of third column
GLfloat material_3_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_3_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_3_3_specular[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_3_3_shininess = { 0.25f * 128.0f };

// define material array for fourth sphere of third column
GLfloat material_3_4_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_4_diffuse[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_3_4_specular[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_3_4_shininess = { 0.25f * 128.0f };

// define material array for fifth sphere of third column
GLfloat material_3_5_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_5_diffuse[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_3_5_specular[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_3_5_shininess = { 0.25f * 128.0f };

// define material array for six sphere of third column
GLfloat material_3_6_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_6_diffuse[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_3_6_specular[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_3_6_shininess = { 0.25f * 128.0f };

// define material array for first sphere of fourth column
GLfloat material_4_1_ambient[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_4_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_4_1_specular[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_4_1_shininess = { 0.078125f * 128.0f };

// define material array for second sphere of fourth column
GLfloat material_4_2_ambient[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_4_2_diffuse[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_4_2_specular[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_4_2_shininess = { 0.078125f * 128.0f };

// define material array for third sphere of fourth column
GLfloat material_4_3_ambient[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_4_3_diffuse[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_4_3_specular[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_4_3_shininess = { 0.078125f * 128.0f };

// define material array for fourth sphere of fourth column
GLfloat material_4_4_ambient[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_4_4_diffuse[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_4_4_specular[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_4_4_shininess = { 0.078125f * 128.0f };

// define material array for fifth sphere of fourth column
GLfloat material_4_5_ambient[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_4_5_diffuse[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_4_5_specular[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_4_5_shininess = { 0.078125f * 128.0f };

// define material array for six sphere of fourth column
GLfloat material_4_6_ambient[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_4_6_diffuse[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_4_6_specular[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_4_6_shininess = { 0.078125f * 128.0f };

// color angle
GLfloat angleWhiteLight = 0.0f;
GLint keyPressed;

GLint viewportX;
GLint viewportY;
GLint viewportWidth;
GLint viewportHeight;
GLint spaceFromTopInEachViewport;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created\nExiting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	// Initialize Width and Height of window
	iWidth = WIN_WIDTH;
	// minus 30 from winow height because caption bar also considered as part of window height 
	// so that any drawing will not get overlapped by caption bar
	iHeight = WIN_HEIGHT - 30;
	viewportWidth = iWidth / 4;
	viewportHeight = iHeight / 6;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Material Using Programmable Function Pipeline"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();
			if (gbLight == true)
			{
				update();
			}
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int, int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// variable declarations
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(0, 0, LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x4C:	// for L or l
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;

				light_Position[0] = { 0.0f };
				light_Position[1] = { 0.0f };
				light_Position[2] = { 1.0f };
				light_Position[3] = { 0.0f };
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		case 0x46:	// for F or f
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}

			iWidth = GetSystemMetrics(SM_CXFULLSCREEN);
			iHeight = GetSystemMetrics(SM_CYFULLSCREEN);

			if (gbFullscreen == true)
			{
				viewportWidth = iWidth / 4;
				viewportHeight = iHeight / 6;
			}
			else
			{
				// Ideally, there is no need to right this statement because we already getting width and height of window on line 403 and 404
				// but i don't know why, its giving me different values
				// Need to figure out why it is happening
				iWidth = WIN_WIDTH;
				iHeight = WIN_HEIGHT - 30;

				viewportWidth = iWidth / 4;
				viewportHeight = iHeight / 6;
			}

			break;
		case 0x58:
			keyPressed = 1;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		case 0x59:
			keyPressed = 2;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		case 0x5A:
			keyPressed = 3;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int, int, int);
	void LoadGLTexture(void);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// *** VERTEX SHADER *** //
	// create vertex shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// vPosition - position vertices for sphere
	// vNormal - normal coordinates
	// u_model_matrix - model matrix
	// u_view_matrix - view matrix
	// u_projection_matrix - projection matrix
	// u_lighting_enabled - flag to check if lighting is enabled
	// u_La - Light ambient component
	// u_Ld - light diffuse component
	// u_Ls - light specular component
	// u_light_position - light position coordinates
	// u_Ka - light ambient material component
	// u_Kd - light diffuse material component
	// u_Ks - light specular material component
	// u_material_shininess - material shininess
	// phong_ads_color - phong lighting model ambient diffuse specular color
	// viewer_vector - opposite of where eye is looking
	// ambient(Ia) - La * Ka
	// diffuse(Id) - Ld * Kd * (light direction * normals)
	// specular(Is) - Ls * Ks * pow((reflection,view_vector),power)
	// phong_ads_color(I) - Ia + Id + Is

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled == 1)" \
		"{" \
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
		"viewer_vector = -eyeCoordinates.xyz;" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHAER *** //
	// create fragment shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 430 core" \
		"\n" \

		"layout (location = 0) out vec4 color0;" \
		"layout (location = 1) out vec4 color1;" \

		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \

		"uniform float bloom_thresh_min = 0.8;" \
		"uniform float bloom_thresh_max = 1.2;" \

		"void main(void)" \
		"{" \
		"vec3 f_phong_ads_color;" \
		"if(u_lighting_enabled == 1)" \
		"{" \
		"vec3 normalized_transformed_normals = normalize(transformed_normals);" \
		"vec3 normalized_light_direction = normalize(light_direction);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);" \
		"f_phong_ads_color = ambient + diffuse + specular;" \

		// write final color to the framebuffer
		"color0 = vec4(f_phong_ads_color, 1.0);" \

		// calculate luminance
		"float Y = dot(f_phong_ads_color, vec3(0.299, 0.587, 0.144));" \

		// Threshold color based on its luminance and write it to
		// the second output
		"f_phong_ads_color = f_phong_ads_color * 4.0 * smoothstep(100.8, 100.2, Y);" \
		"color1 = vec4(f_phong_ads_color,1.0);"

		"}"\
		"else" \
		"{" \
		"color1 = vec4(1.0,1.0,1.0,1.0);" \
		"}" \
		"FragColor = color1;" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}	

	// *** SHADER PROGRAM ***
	// create program
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// attach bloom fragment shader to shader program
	glAttachShader(gShaderProgramObject, gBloomFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

	// pre-link binding of shader program object with vertex shader texture attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** BLUR SHADER *** //
	// create fragment shader
	gBlurFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *blurFragmentShaderSourceCode =
		"#version 430 core" \
		"\n"
		"layout(binding = 0) uniform sampler2D hdr_image;" \

		"out vec4 color;" \

		"const float weights[] = float[](0.0024499299678342," \
		"0.0043538453346397,"\
		"0.0073599963704157,"\
		"0.0118349786570722,"\
		"0.0181026699707781," \
		"0.0263392293891488," \
		"0.0364543006660986," \
		"0.0479932050577658," \
		"0.0601029809166942," \
		"0.0715974486241365," \
		"0.0811305381519717," \
		"0.0874493212267511," \
		"0.0896631113333857," \
		"0.0874493212267511," \
		"0.0811305381519717," \
		"0.0715974486241365," \
		"0.0601029809166942," \
		"0.0479932050577658," \
		"0.0364543006660986," \
		"0.0263392293891488," 
		"0.0181026699707781," \
		"0.0118349786570722," \
		"0.0073599963704157," \
		"0.0043538453346397," \
		"0.0024499299678342);" \

		"void main(void)" \
		"{" \
		"vec4 c = vec4(0.0);" \
			"ivec2 P = ivec2(gl_FragCoord.yx) - ivec2(0, weights.length() >> 1);" \
			"int i;" \

			"for (i = 0; i < weights.length(); i++)" \
			"{" \
			"c += texelFetch(hdr_image, P + ivec2(0, i), 0) * weights[i];" \
			"}" \

			"color = c;" \
			"}";

	glShaderSource(gBlurFragmentShaderObject, 1, (const GLchar **)&blurFragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gBlurFragmentShaderObject);
	glGetShaderiv(gBlurFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gBlurFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gBlurFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Blur Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** BLUR SHADER PROGRAM ***
	// create program
	gBlurShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gBlurShaderProgramObject, gVertexShaderObject);

	// attach bloom fragment shader to shader program
	glAttachShader(gBlurShaderProgramObject, gBlurFragmentShaderObject);

	// link shader
	glLinkProgram(gBlurShaderProgramObject);
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gBlurShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gBlurShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gBlurShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Blur Shader program link log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** BLOOM SHADER *** //
	// create fragment shader

	gBloomFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *bloomFragmentShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"layout(binding = 0) uniform sampler2D hdr_image;" \
		"layout(binding = 1) uniform sampler2D bloom_image;" \

		"in vec4 FragColor;" \
		"uniform float exposure = 0.9;"\
		"uniform float bloom_factor = 1.0;" \
		"uniform float scene_factor = 1.0;" \
		"out vec4 color;" \
		"void main(void)" \
		"{" \
		"vec4 c = vec4(0.0);" \
		"c += texelFetch(hdr_image, ivec2(gl_FragCoord.xy), 0) * scene_factor;" \
		"c += texelFetch(bloom_image, ivec2(gl_FragCoord.xy), 0) * bloom_factor;" \
		"c.rgb = vec3(1.0) - exp(-c.rgb * exposure);" \
		"color = FragColor;" \
		"}";

	glShaderSource(gBloomShaderProgramObject, 1, (const GLchar **)&bloomFragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gBloomShaderProgramObject);
	glGetShaderiv(gBloomShaderProgramObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gBloomShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gBloomShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Bloom Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform location
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	// L or l key pressed or not
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// ambient color intensity of light
	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	// diffuse color intensity of light
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	// specular color intensity of light
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
	// position of light
	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	// amient reflective color intensity of light
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of light
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of light
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material (value is conventionally between 0 to 200)
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	// get bloom threshold min location
	bloom_thresh_min = glGetUniformLocation(gShaderProgramObject, "bloom_thresh_min");
	// get bloom threshold max location
	bloom_thresh_min = glGetUniformLocation(gShaderProgramObject, "bloom_thresh_max");

	// *** vertices, colors, shader attribs, vbo, vao initialization *** //
	// get sphere vertex data
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// *************************
	// VAO FOR SPHERE
	// *************************

	// generate and bind vao for sphere
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// ******************
	// VBO FOR POSITION
	// ******************
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// ******************
	// VBO FOR NORMAL
	// ******************
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// ******************
	// VBO FOR ELEMENT
	// ******************
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind from vao for sphere
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	// set perspective matrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(0, 0, WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	void resize(int, int, int, int);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// start using opengl program object
	glUseProgram(gShaderProgramObject);
	glUseProgram(gBlurShaderProgramObject);
	glUseProgram(gBloomShaderProgramObject);

	// opengl drawing

	if (gbLight == true)
	{
		// set u_lighting_enabled uniform
		glUniform1i(L_KeyPressed_uniform, 1);

		// setting light properties
		glUniform3fv(La_uniform, 1, light_Ambient);
		glUniform3fv(Ld_uniform, 1, light_Diffuse);
		glUniform3fv(Ls_uniform, 1, light_Specular);
		
		// set threshold min and max values
		glUniform1f(bloom_thresh_min, 0.0);
		glUniform1f(bloom_thresh_max, 10.0);

		mat4 rotationMatrix = mat4::identity();

		if (keyPressed == 1)
		{
			light_Position[1] = 100.0f * cos(angleWhiteLight);
			light_Position[2] = 100.0f * sin(angleWhiteLight);
			glUniform4fv(light_position_uniform, 1, light_Position);
		}
		else if (keyPressed == 2)
		{
			light_Position[0] = 100.0f * sin(angleWhiteLight);
			light_Position[2] = 100.0f * cos(angleWhiteLight);
			glUniform4fv(light_position_uniform, 1, light_Position);
		}
		else if (keyPressed == 3)
		{
			light_Position[0] = 100.0f * cos(angleWhiteLight);
			light_Position[1] = 100.0f * sin(angleWhiteLight);
			glUniform4fv(light_position_uniform, 1, light_Position);
		}
	}
	else
	{
		// set u_lighting_enabled uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	// Opengl drawing

	// -------------------
	// FIRST COLUMN SPHERE
	// -------------------

	// DRAW FIRST COLUMN, FIRST SPHERE

	// make model and view matrix to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	// pass view and projection matrix to shader through uniform
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// set viewport
	viewportX = iWidth - (viewportWidth * 4);	// x axis starting position for viewport
	viewportY = iHeight - (viewportHeight * 1);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_1_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_1_specular);
	glUniform1f(material_shininess_uniform, material_1_1_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FIRST COLUMN, SECOND SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 2);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// Ideally there is no need to reset model matrix and translate it by same x,y and z axis every time
	// but to avoid any uncertain output, i am doing it

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_1_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_2_specular);
	glUniform1f(material_shininess_uniform, material_1_2_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FIRST COLUMN, THIRD SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 3);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_1_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_3_specular);
	glUniform1f(material_shininess_uniform, material_1_3_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FIRST COLUMN, FOURTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 4);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_1_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_4_specular);
	glUniform1f(material_shininess_uniform, material_1_4_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FIRST COLUMN, FIFTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 5);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_1_5_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_5_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_5_specular);
	glUniform1f(material_shininess_uniform, material_1_5_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FIRST COLUMN, SIXTH SPHERE

	// set viwport
	viewportY = iHeight - (viewportHeight * 6);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_1_6_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_6_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_6_specular);
	glUniform1f(material_shininess_uniform, material_1_6_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// -------------------
	// SECOND COLUMN SPHERE
	// -------------------

	// DRAW SECOND COLUMN, FIRST SPHERE

	// set viewport
	viewportX = iWidth - (viewportWidth * 3);	// x axis starting position for viewport
	viewportY = iHeight - (viewportHeight * 1);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_2_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_1_specular);
	glUniform1f(material_shininess_uniform, material_2_1_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW SECOND COLUMN, SECOND SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 2);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_2_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_2_specular);
	glUniform1f(material_shininess_uniform, material_2_2_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW SECOND COLUMN, THIRD SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 3);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_2_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_3_specular);
	glUniform1f(material_shininess_uniform, material_2_3_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW SECOND COLUMN, FOURTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 4);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_2_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_4_specular);
	glUniform1f(material_shininess_uniform, material_2_4_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW SECOND COLUMN, FIFTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 5);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_2_5_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_5_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_5_specular);
	glUniform1f(material_shininess_uniform, material_2_5_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW SECOND COLUMN, SIXTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 6);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_2_6_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_6_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_6_specular);
	glUniform1f(material_shininess_uniform, material_2_6_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// -------------------
	// THIRD COLUMN SPHERE
	// -------------------

	// DRAW THIRD COLUMN, FIRST SPHERE

	// set viewport
	viewportX = iWidth - (viewportWidth * 2);	// x axis starting position for viewport
	viewportY = iHeight - (viewportHeight * 1);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_3_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_1_specular);
	glUniform1f(material_shininess_uniform, material_3_1_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW THIRD COLUMN, SECOND SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 2);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_3_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_2_specular);
	glUniform1f(material_shininess_uniform, material_3_2_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW THIRD COLUMN, THIRD SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 3);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_3_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_3_specular);
	glUniform1f(material_shininess_uniform, material_3_3_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW THIRD COLUMN, FOURTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 4);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_3_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_4_specular);
	glUniform1f(material_shininess_uniform, material_3_4_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW THIRD COLUMN, FIFTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 5);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_3_5_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_5_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_5_specular);
	glUniform1f(material_shininess_uniform, material_3_5_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW THIRD COLUMN, SIXTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 6);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_3_6_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_6_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_6_specular);
	glUniform1f(material_shininess_uniform, material_3_6_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// -------------------
	// FOURTH COLUMN SPHERE
	// -------------------

	// DRAW FOURTH COLUMN, FIRST SPHERE

	// set viewport
	viewportX = iWidth - (viewportWidth * 1);	// x axis starting position for viewport
	viewportY = iHeight - (viewportHeight * 1);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_4_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_1_specular);
	glUniform1f(material_shininess_uniform, material_4_1_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FOURTH COLUMN, SECOND SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 2);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_4_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_2_specular);
	glUniform1f(material_shininess_uniform, material_4_2_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FOURTH COLUMN, THIRD SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 3);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_4_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_3_specular);
	glUniform1f(material_shininess_uniform, material_4_3_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FOURTH COLUMN, FOURTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 4);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_4_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_4_specular);
	glUniform1f(material_shininess_uniform, material_4_4_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FOURTH COLUMN, FIFTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 5);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_4_5_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_5_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_5_specular);
	glUniform1f(material_shininess_uniform, material_4_5_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// DRAW FOURTH COLUMN, FIFTH SPHERE

	// set viewport
	viewportY = iHeight - (viewportHeight * 6);	// y axis starting position for viewport
	resize(viewportX, viewportY, viewportWidth, viewportHeight);

	// make model matrix to identity
	modelMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.5
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

	// pass model matrix and material components to shader through uniform
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniform3fv(Ka_uniform, 1, material_4_6_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_6_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_6_specular);
	glUniform1f(material_shininess_uniform, material_4_6_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// stop using opengl program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{
	angleWhiteLight = angleWhiteLight + 0.001f;
	if (angleWhiteLight >= 360)
	{
		angleWhiteLight = 0.1f;
	}
}

void resize(int x, int y, int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(x, y, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}
	// destroy position vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	// destroy normal vbo
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	// destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	// deselect rendering context
	wglMakeCurrent(NULL, NULL);

	// delete rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	// delete device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
