#include <windows.h>
#include <stdio.h>

//  for GLSL extensions IMPORTANT : This line should be before #include<gl\gl.h> and #include<gl\glu.h>
#include <gl\glew.h>

#include <gl\GL.h>
#include "Sphere.h"
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

int iWidth;
int iHeight;

GLuint gVertexShaderObject;
GLuint gVertexShaderObjectFilter;

GLuint gFragmentShaderObjectScene;
GLuint gFragmentShaderObjectFilter;
GLuint gBloomFragmentShaderObject;

GLuint gShaderProgramObjectScene;
GLuint gShaderProgramObjectFilter;

GLuint gNumVertices;
GLuint gNumElements;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVao_filter;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

// light uniforms
GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

// first column, first material
GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

bool gbLight = false;

// bloom threshold min and max
GLfloat bloom_thresh_min = 0.8f;
GLfloat bloom_thresh_max = 1.2f;
GLuint bloom_thresh_min_uniform;
GLuint bloom_thresh_max_uniform;

int i;
static const GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };

enum
{
	MAX_SCENE_WIDTH = 2048,
	MAX_SCENE_HEIGHT = 2048,
	SPHERE_COUNT = 32,
};

GLuint      tex_src;
GLuint      tex_lut;
GLuint      tex_scene;
GLuint      tex_brightpass;
GLuint      tex_depth;
GLuint      tex_filter[2];
GLuint      render_fbo;
GLuint      filter_fbo[2];

GLfloat light_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_Diffuse[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_Specular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_Position[] = { 0.0f,0.0f,0.0f,0.0f };

// define material array for first sphere of first column
GLfloat material_1_1_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
GLfloat material_1_1_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
GLfloat material_1_1_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
GLfloat material_1_1_shininess = { 0.6f * 128.0f };

// define material array for second sphere of first column
GLfloat material_1_2_ambient[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_1_2_diffuse[] = { 0.54f,0.89f,0.63f,1.0f };
GLfloat material_1_2_specular[] = { 0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_1_2_shininess = { 0.1f * 128.0f };

// define material array for third sphere of first column
GLfloat material_1_3_ambient[] = { 0.05375f,0.05f,0.06625f,1.0f };
GLfloat material_1_3_diffuse[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_1_3_specular[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_1_3_shininess = { 0.3f * 128.0f };

// define material array for fourth sphere of first column
GLfloat material_1_4_ambient[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_1_4_diffuse[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_1_4_specular[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_1_4_shininess = { 0.088f * 128.0f };

// define material array for fifth sphere of first column
GLfloat material_1_5_ambient[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_1_5_diffuse[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_1_5_specular[] = { 0.727811f,0.626959f,0.626959f,1.0f };
GLfloat material_1_5_shininess = { 0.6f * 128.0f };

// define material array for six sphere of first column
GLfloat material_1_6_ambient[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_1_6_diffuse[] = { 0.396f,0.74151f,0.69102f,1.0f };
GLfloat material_1_6_specular[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_1_6_shininess = { 0.6f * 128.0f };

// define material array for first sphere of second column
GLfloat material_2_1_ambient[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_2_1_diffuse[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_2_1_specular[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_2_1_shininess = { 0.21794872f * 128.0f };

// define material array for second sphere of second column
GLfloat material_2_2_ambient[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_2_2_diffuse[] = { 0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_2_2_specular[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_2_2_shininess = { 0.2f * 128.0f };

// define material array for third sphere of second column
GLfloat material_2_3_ambient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat material_2_3_diffuse[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_2_3_specular[] = { 0.774597f,0.774597f,0.774597f,1.0f };
GLfloat material_2_3_shininess = { 0.6f * 128.0f };

// define material array for fourth sphere of second column
GLfloat material_2_4_ambient[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_2_4_diffuse[] = { 0.7038f,0.27048f,0.0828f,1.0f };
GLfloat material_2_4_specular[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_2_4_shininess = { 0.1f * 128.0f };

// define material array for fifth sphere of second column
GLfloat material_2_5_ambient[] = { 0.24725f,0.1995f,0.0745f,1.0f };
GLfloat material_2_5_diffuse[] = { 0.75164f,0.60648f,0.22648f,1.0f };
GLfloat material_2_5_specular[] = { 0.628281f,0.555802f,0.366065f,1.0f };
GLfloat material_2_5_shininess = { 0.4f * 128.0f };

// define material array for six sphere of second column
GLfloat material_2_6_ambient[] = { 0.19225f,0.19225f,0.19225f,1.0f };
GLfloat material_2_6_diffuse[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_2_6_specular[] = { 0.508273f,0.508273f,0.508273f,1.0f };
GLfloat material_2_6_shininess = { 0.4f * 128.0f };

// define material array for first sphere of third column
GLfloat material_3_1_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_3_1_specular[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_3_1_shininess = { 0.25f * 128.0f };

// define material array for second sphere of third column
GLfloat material_3_2_ambient[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_3_2_diffuse[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_3_2_specular[] = { 0.50196078f,0.50196078f,0.50196078f,1.0f };
GLfloat material_3_2_shininess = { 0.25f * 128.0f };

// define material array for third sphere of third column
GLfloat material_3_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_3_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_3_3_specular[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_3_3_shininess = { 0.25f * 128.0f };

// define material array for fourth sphere of third column
GLfloat material_3_4_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_4_diffuse[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_3_4_specular[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_3_4_shininess = { 0.25f * 128.0f };

// define material array for fifth sphere of third column
GLfloat material_3_5_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_5_diffuse[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_3_5_specular[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_3_5_shininess = { 0.25f * 128.0f };

// define material array for six sphere of third column
GLfloat material_3_6_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_6_diffuse[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_3_6_specular[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_3_6_shininess = { 0.25f * 128.0f };

// define material array for first sphere of fourth column
GLfloat material_4_1_ambient[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_4_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_4_1_specular[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_4_1_shininess = { 0.078125f * 128.0f };

// define material array for second sphere of fourth column
GLfloat material_4_2_ambient[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_4_2_diffuse[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_4_2_specular[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_4_2_shininess = { 0.078125f * 128.0f };

// define material array for third sphere of fourth column
GLfloat material_4_3_ambient[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_4_3_diffuse[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_4_3_specular[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_4_3_shininess = { 0.078125f * 128.0f };

// define material array for fourth sphere of fourth column
GLfloat material_4_4_ambient[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_4_4_diffuse[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_4_4_specular[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_4_4_shininess = { 0.078125f * 128.0f };

// define material array for fifth sphere of fourth column
GLfloat material_4_5_ambient[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_4_5_diffuse[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_4_5_specular[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_4_5_shininess = { 0.078125f * 128.0f };

// define material array for six sphere of fourth column
GLfloat material_4_6_ambient[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_4_6_diffuse[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_4_6_specular[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_4_6_shininess = { 0.078125f * 128.0f };

// color angle
GLfloat angleWhiteLight = 0.0f;
GLint keyPressed;

GLint viewportX;
GLint viewportY;
GLint viewportWidth;
GLint viewportHeight;
GLint spaceFromTopInEachViewport;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created\nExiting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	// Initialize Width and Height of window
	iWidth = WIN_WIDTH;
	// minus 30 from winow height because caption bar also considered as part of window height 
	// so that any drawing will not get overlapped by caption bar
	iHeight = WIN_HEIGHT - 30;
	viewportWidth = iWidth / 4;
	viewportHeight = iHeight / 6;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("hdrbloom - Programmable Function Pipeline"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();
			if (gbLight == true)
			{
				update();
			}
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int, int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// variable declarations
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(0, 0, LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x4C:	// for L or l
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;

				light_Position[0] = { 0.0f };
				light_Position[1] = { 0.0f };
				light_Position[2] = { 1.0f };
				light_Position[3] = { 0.0f };
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		case 0x46:	// for F or f
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}

			iWidth = GetSystemMetrics(SM_CXFULLSCREEN);
			iHeight = GetSystemMetrics(SM_CYFULLSCREEN);

			if (gbFullscreen == true)
			{
				viewportWidth = iWidth / 4;
				viewportHeight = iHeight / 6;
			}
			else
			{
				// Ideally, there is no need to right this statement because we already getting width and height of window on line 403 and 404
				// but i don't know why, its giving me different values
				// Need to figure out why it is happening
				iWidth = WIN_WIDTH;
				iHeight = WIN_HEIGHT - 30;

				viewportWidth = iWidth / 4;
				viewportHeight = iHeight / 6;
			}

			break;
		case 0x58:
			keyPressed = 1;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		case 0x59:
			keyPressed = 2;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		case 0x5A:
			keyPressed = 3;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		case VK_OEM_PLUS:
			bloom_thresh_min = bloom_thresh_min + 0.1;
			break;
		case VK_OEM_MINUS:
			bloom_thresh_min = bloom_thresh_min - 0.1;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int, int, int);
	void LoadGLTexture(void);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// START RENDER SCENE PROGRAM

		// create vertex shader
		gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
		// provide source code to shader
		const GLchar *vertexShaderSourceCode =
			"#version 430 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;" \
			"uniform mat4 u_model_matrix;" \
			"uniform mat4 u_view_matrix;" \
			"uniform mat4 u_projection_matrix;" \
			"uniform vec4 u_light_position;" \
			"uniform int u_lighting_enabled;" \
			"out vec3 transformed_normals;" \
			"out vec3 light_direction;" \
			"out vec3 viewer_vector;" \
			"void main(void)" \
			"{" \
			"if(u_lighting_enabled == 1)" \
			"{" \
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
			"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
			"light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
			"viewer_vector = -eyeCoordinates.xyz;" \
			"}" \
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
			"}";

		glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

		// compile shader
		glCompileShader(gVertexShaderObject);
		GLint iInfoLogLength = 0;
		GLint iShaderCompiledStatus = 0;
		char *szInfoLog = NULL;
		glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "gShaderProgramObjectScene - Vertex Shader Compilation Log : %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}

		// create fragment shader
		gFragmentShaderObjectScene = glCreateShader(GL_FRAGMENT_SHADER);

		// provide source code to shader
		const GLchar *fragmentShaderSourceCode =
			"#version 430 core" \
			"\n" \
			"layout (location = 0) out vec4 color0;" \
			"layout (location = 1) out vec4 color1;" \

			"in vec3 transformed_normals;" \
			"in vec3 light_direction;" \
			"in vec3 viewer_vector;" \
			"out vec4 FragColor;" \
			"uniform vec3 u_La;" \
			"uniform vec3 u_Ld;" \
			"uniform vec3 u_Ls;" \
			"uniform vec3 u_Ka;" \
			"uniform vec3 u_Kd;" \
			"uniform vec3 u_Ks;" \
			"uniform float u_material_shininess;" \
			"uniform int u_lighting_enabled;" \

			"uniform float bloom_thresh_min;" \
			"uniform float bloom_thresh_max;" \

			"void main(void)" \
			"{" \
			"vec3 f_phong_ads_color;" \
			"vec3 normalized_transformed_normals = normalize(transformed_normals);" \
			"vec3 normalized_light_direction = normalize(light_direction);" \
			"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
			"vec3 ambient = u_La * u_Ka;" \
			"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);" \
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
			"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);" \
			"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);" \
			"f_phong_ads_color = ambient + diffuse + specular;" \

			// write final color to the framebuffer
			"color0 = vec4(f_phong_ads_color, 1.0);" \

			// calculate luminance
			"float Y = dot(f_phong_ads_color, vec3(0.299, 0.587, 0.144));" \

			// Threshold color based on its luminance and write it to
			// the second output
			"f_phong_ads_color = f_phong_ads_color * 4.0 * smoothstep(bloom_thresh_min, bloom_thresh_max, Y);" \
			"color1 = vec4(f_phong_ads_color,1.0);" \
			//"FragColor = color1;" \ 
			"}";
		glShaderSource(gFragmentShaderObjectScene, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

		// compile shader
		glCompileShader(gFragmentShaderObjectScene);
		glGetShaderiv(gFragmentShaderObjectScene, GL_COMPILE_STATUS, &iShaderCompiledStatus);
		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObjectScene, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gFragmentShaderObjectScene, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "gShaderProgramObjectScene - Fragment Shader Compilation Log : %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}

		// *** SHADER PROGRAM ***
		// create program
		gShaderProgramObjectScene = glCreateProgram();

		// attach vertex shader to shader program
		glAttachShader(gShaderProgramObjectScene, gVertexShaderObject);

		// attach fragment shader to shader program
		glAttachShader(gShaderProgramObjectScene, gFragmentShaderObjectScene);

		// attach bloom fragment shader to shader program
		//glAttachShader(gShaderProgramObjectScene, gBloomFragmentShaderObject);

		// pre-link binding of shader program object with vertex shader position attribute
		glBindAttribLocation(gShaderProgramObjectScene, VDG_ATTRIBUTE_VERTEX, "vPosition");

		// pre-link binding of shader program object with vertex shader texture attribute
		glBindAttribLocation(gShaderProgramObjectScene, VDG_ATTRIBUTE_NORMAL, "vNormal");

		// link shader
		glLinkProgram(gShaderProgramObjectScene);
		GLint iShaderProgramLinkStatus = 0;
		glGetProgramiv(gShaderProgramObjectScene, GL_LINK_STATUS, &iShaderProgramLinkStatus);
		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObjectScene, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (iInfoLogLength>0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObjectScene, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "gShaderProgramObjectScene - Shader program link log : %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}

		// get uniform location
		model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_model_matrix");
		view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_view_matrix");
		projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_projection_matrix");

		// L or l key pressed or not
		L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_lighting_enabled");

		// ambient color intensity of light
		La_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_La");
		// diffuse color intensity of light
		Ld_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_Ld");
		// specular color intensity of light
		Ls_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_Ls");
		// position of light
		light_position_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_light_position");

		// amient reflective color intensity of light
		Ka_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_Ka");
		// diffuse reflective color intensity of light
		Kd_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_Kd");
		// specular reflective color intensity of light
		Ks_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_Ks");
		// shininess of material (value is conventionally between 0 to 200)
		material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectScene, "u_material_shininess");

		// get bloom threshold min location
		bloom_thresh_min_uniform = glGetUniformLocation(gShaderProgramObjectScene, "bloom_thresh_min");
		// get bloom threshold max location
		bloom_thresh_max_uniform = glGetUniformLocation(gShaderProgramObjectScene, "bloom_thresh_max");

		// *** vertices, colors, shader attribs, vbo, vao initialization *** //
		// get sphere vertex data
		getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
		gNumVertices = getNumberOfSphereVertices();
		gNumElements = getNumberOfSphereElements();

		// *************************
		// VAO FOR SPHERE
		// *************************

		// generate and bind vao for sphere
		glGenVertexArrays(1, &gVao_sphere);
		glBindVertexArray(gVao_sphere);

		// ******************
		// VBO FOR POSITION
		// ******************
		glGenBuffers(1, &gVbo_sphere_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
		glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// ******************
		// VBO FOR NORMAL
		// ******************
		glGenBuffers(1, &gVbo_sphere_normal);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
		glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
		glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// ******************
		// VBO FOR ELEMENT
		// ******************
		glGenBuffers(1, &gVbo_sphere_element);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		// unbind from vao for sphere
		glBindVertexArray(0);

	// END RENDER SCENE PROGRAM


	// START FILTER SHADER PROGRAM

		// create vertex shader

		gVertexShaderObjectFilter = glCreateShader(GL_VERTEX_SHADER);

		// provide source code to shader
		const GLchar *vertexShaderSourceCodeFilter =
			"#version 430 core" \
			"\n" \
			"void main(void)" \
			"{" \
			"const vec4 vertices[] = vec4[](vec4(-1.0, -1.0, 0.5, 1.0)," \
			"vec4( 1.0, -1.0, 0.5, 1.0)," \
			"vec4(-1.0,  1.0, 0.5, 1.0)," \
			"vec4( 1.0,  1.0, 0.5, 1.0));" \
			"gl_Position = vertices[gl_VertexID];" \
			"}";

		glShaderSource(gVertexShaderObjectFilter, 1, (const GLchar **)&vertexShaderSourceCodeFilter, NULL);

		// compile shader
		glCompileShader(gVertexShaderObjectFilter);
		iInfoLogLength = 0;
		iShaderCompiledStatus = 0;
		szInfoLog = NULL;
		glGetShaderiv(gVertexShaderObjectFilter, GL_COMPILE_STATUS, &iShaderCompiledStatus);
		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObjectFilter, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObjectFilter, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "gVertexShaderObjectFilter - Vertex Shader Compilation Log : %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}

		// create fragment shader
		gFragmentShaderObjectFilter = glCreateShader(GL_FRAGMENT_SHADER);

		// provide source code to shader
		const GLchar *fragmentShaderSourceCodeFilter =
			"#version 430 core" \
			"\n"
			"layout(binding = 0) uniform sampler2D hdr_image;" \

			"out vec4 color;" \

			"const float weights[] = float[](0.0024499299678342," \
			"0.0043538453346397,"\
			"0.0073599963704157,"\
			"0.0118349786570722,"\
			"0.0181026699707781," \
			"0.0263392293891488," \
			"0.0364543006660986," \
			"0.0479932050577658," \
			"0.0601029809166942," \
			"0.0715974486241365," \
			"0.0811305381519717," \
			"0.0874493212267511," \
			"0.0896631113333857," \
			"0.0874493212267511," \
			"0.0811305381519717," \
			"0.0715974486241365," \
			"0.0601029809166942," \
			"0.0479932050577658," \
			"0.0364543006660986," \
			"0.0263392293891488," \
			"0.0181026699707781," \
			"0.0118349786570722," \
			"0.0073599963704157," \
			"0.0043538453346397," \
			"0.0024499299678342);" \
			"void main(void)" \
			"{" \
			"vec4 c = vec4(0.0);" \
			"ivec2 P = ivec2(gl_FragCoord.yx) - ivec2(0, weights.length() >> 1);" \
			"int i;" \
			"for (i = 0; i < weights.length(); i++)" \
			"{" \
			"c += texelFetch(hdr_image, P + ivec2(0, i), 0) * weights[i];" \
			"}" \
			"color = c;" \
			"}";

		glShaderSource(gFragmentShaderObjectFilter, 1, (const GLchar **)&fragmentShaderSourceCodeFilter, NULL);

		// compile shader
		glCompileShader(gFragmentShaderObjectFilter);
		glGetShaderiv(gFragmentShaderObjectFilter, GL_COMPILE_STATUS, &iShaderCompiledStatus);
		if (iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObjectFilter, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gFragmentShaderObjectFilter, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "gFragmentShaderObjectFilter - Fragment Shader Compilation Log : %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}

		// create program
		gShaderProgramObjectFilter = glCreateProgram();

		// attach vertex shader to shader program
		glAttachShader(gShaderProgramObjectFilter, gVertexShaderObjectFilter);

		// attach bloom fragment shader to shader program
		glAttachShader(gShaderProgramObjectFilter, gFragmentShaderObjectFilter);

		// link shader
		glLinkProgram(gShaderProgramObjectFilter);
		iShaderProgramLinkStatus = 0;
		glGetProgramiv(gShaderProgramObjectFilter, GL_LINK_STATUS, &iShaderProgramLinkStatus);
		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObjectFilter, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (iInfoLogLength>0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(gShaderProgramObjectFilter, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "gShaderProgramObjectFilter - Shader program link log : %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}

	// END FILTER SHADER PROGRAM

	glGenVertexArrays(1, &gVao_filter);
	glBindVertexArray(gVao_filter);

	static const GLfloat exposureLUT[20] = { 11.0f, 6.0f, 3.2f, 2.8f, 2.2f, 1.90f, 1.80f, 1.80f, 1.70f, 1.70f,  1.60f, 1.60f, 1.50f, 1.50f, 1.40f, 1.40f, 1.30f, 1.20f, 1.10f, 1.00f };

	// generate framebuffer object names
	// 1 - specify how many framebuffer object we want to generate
	// &render_fbo - pointer to an array where generated framebuffer object names will be stored
	glGenFramebuffers(1, &render_fbo);
	// binds previously generated framebuffer object
	// GL_FRAMEBUFFER - target to which framebuffer object want to bound
	// render_fbo - framebuffer object name
	glBindFramebuffer(GL_FRAMEBUFFER, render_fbo);

	// generates 1 texture object with name tex_scene, tex_scene is pointer to array and will contain names of textures generated
	// 1 - how many texture names to generate
	// &tex_scene - pointer to array where generated texture names will be stored
	glGenTextures(1, &tex_scene);
	// binds a texture object to target
	// GL_TEXTURE_2D - target to which texture name to bound
	// tex_scene - texture name
	glBindTexture(GL_TEXTURE_2D, tex_scene);
	// specify storage for all levels of 2D and 1D array texture
	// GL_TEXTURE_2D - specify target to which texture object is bound above
	// 1 - specify number of texture levels
	// GL_RGBA16F - internal format used to store texture data
	// MAX_SCENE_WIDTH - width of texture in texels
	// MAX_SCENE_HEIGHT - height of texture in texels
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	// attach a level of texture object as a logical buffer of a framebuffer object
	// GL_FRAMEBUFFER - specify the target to which texture object is bound
	// GL_COLOR_ATTACHMENT0 - point where texture object will get attached to in GL_FRAMEBUFFER
	// tex_scene - texture object to attach
	// 0 - specify mipmap level of texture
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex_scene, 0);

	glGenTextures(1, &tex_brightpass);
	glBindTexture(GL_TEXTURE_2D, tex_brightpass);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, tex_brightpass, 0);

	glGenTextures(1, &tex_depth);
	glBindTexture(GL_TEXTURE_2D, tex_depth);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex_depth, 0);

	// Function specify the array of buffers where fragment shader output data will be written and that is `buffers`
	// i.e. color0 from fragment shader will come in GL_COLOR_ATTACHMENT0 of `buffers`
	// color1 from fragment shader will come in GL_COLOR_ATTACHMENT1 of `buffers`
	glDrawBuffers(2, buffers);

	glGenFramebuffers(2, &filter_fbo[0]);
	glGenTextures(2, &tex_filter[0]);
	for (i = 0; i < 2; i++)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, filter_fbo[i]);
		glBindTexture(GL_TEXTURE_2D, tex_filter[i]);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16F, i ? MAX_SCENE_WIDTH : MAX_SCENE_HEIGHT, i ? MAX_SCENE_HEIGHT : MAX_SCENE_WIDTH);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex_filter[i], 0);
		glDrawBuffers(1, buffers);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glGenTextures(1, &tex_lut);
	glBindTexture(GL_TEXTURE_1D, tex_lut);
	// specify storage for all one dimensional texture
	// GL_TEXTURE_1D - target to which texture object need to bound
	// 1 - specify no of texture level
	// GL_R32F - internal format used to store texture image data
	// 20 - width of texture
	glTexStorage1D(GL_TEXTURE_1D, 1, GL_R32F, 20);
	// specify one dimensional texture subimage
	// GL_TEXTURE_1D - target for one dimensional subimage (same from glBindTexture)
	// 0 - specify level-of-detail number
	// 0 - texel offset in x direction within texture array
	// 20 - width of texture subimage
	// GL_RED - specify format of pixel data
	// GL_FLOAT - data type of pixel data
	// exposureLUT - pointer to data image
	glTexSubImage1D(GL_TEXTURE_1D, 0, 0, 20, GL_RED, GL_FLOAT, exposureLUT);
	// set texture parameters
	// GL_TEXTURE_1D - target to which texture is bound above (same from glBindTexture)
	// GL_TEXTURE_MIN_FILTER - symbolic name of a single-valued texture parameter
	// GL_LINEAR - specifies the value of pname
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	// set perspective matrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(0, 0, WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	void resize(int, int, int, int);

	static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	static const GLfloat one = 1.0f;

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// START USING PROGRAM SCENE

		glBindFramebuffer(GL_FRAMEBUFFER, render_fbo);
		glClearBufferfv(GL_COLOR, 0, black);
		glClearBufferfv(GL_COLOR, 1, black);
		glClearBufferfv(GL_DEPTH, 0, &one);

		// start using opengl program object
		glUseProgram(gShaderProgramObjectScene);

		if (gbLight == true)
		{
			// set u_lighting_enabled uniform
			glUniform1i(L_KeyPressed_uniform, 1);

			// setting light properties
			glUniform3fv(La_uniform, 1, light_Ambient);
			glUniform3fv(Ld_uniform, 1, light_Diffuse);
			glUniform3fv(Ls_uniform, 1, light_Specular);

			// set threshold min and max values
			// 100.8, 100.2 
			glUniform1f(bloom_thresh_min_uniform, bloom_thresh_min);
			glUniform1f(bloom_thresh_max_uniform, bloom_thresh_max);

			mat4 rotationMatrix = mat4::identity();

			if (keyPressed == 1)
			{
				light_Position[1] = 100.0f * cos(angleWhiteLight);
				light_Position[2] = 100.0f * sin(angleWhiteLight);
				glUniform4fv(light_position_uniform, 1, light_Position);
			}
			else if (keyPressed == 2)
			{
				light_Position[0] = 100.0f * sin(angleWhiteLight);
				light_Position[2] = 100.0f * cos(angleWhiteLight);
				glUniform4fv(light_position_uniform, 1, light_Position);
			}
			else if (keyPressed == 3)
			{
				light_Position[0] = 100.0f * cos(angleWhiteLight);
				light_Position[1] = 100.0f * sin(angleWhiteLight);
				glUniform4fv(light_position_uniform, 1, light_Position);
			}
		}
		else
		{
			// set u_lighting_enabled uniform
			glUniform1i(L_KeyPressed_uniform, 0);
		}

		// -------------------
		// FIRST COLUMN SPHERE
		// -------------------

		// DRAW FIRST COLUMN, FIRST SPHERE

		// make model and view matrix to identity
		mat4 modelMatrix = mat4::identity();
		mat4 viewMatrix = mat4::identity();

		// pass view and projection matrix to shader through uniform
		glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		// set viewport
		viewportX = iWidth - (viewportWidth * 4);	// x axis starting position for viewport
		viewportY = iHeight - (viewportHeight * 1);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_1_1_ambient);
		glUniform3fv(Kd_uniform, 1, material_1_1_diffuse);
		glUniform3fv(Ks_uniform, 1, material_1_1_specular);
		glUniform1f(material_shininess_uniform, material_1_1_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FIRST COLUMN, SECOND SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 2);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// Ideally there is no need to reset model matrix and translate it by same x,y and z axis every time
		// but to avoid any uncertain output, i am doing it

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_1_2_ambient);
		glUniform3fv(Kd_uniform, 1, material_1_2_diffuse);
		glUniform3fv(Ks_uniform, 1, material_1_2_specular);
		glUniform1f(material_shininess_uniform, material_1_2_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FIRST COLUMN, THIRD SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 3);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_1_3_ambient);
		glUniform3fv(Kd_uniform, 1, material_1_3_diffuse);
		glUniform3fv(Ks_uniform, 1, material_1_3_specular);
		glUniform1f(material_shininess_uniform, material_1_3_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FIRST COLUMN, FOURTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 4);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_1_4_ambient);
		glUniform3fv(Kd_uniform, 1, material_1_4_diffuse);
		glUniform3fv(Ks_uniform, 1, material_1_4_specular);
		glUniform1f(material_shininess_uniform, material_1_4_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FIRST COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 5);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_1_5_ambient);
		glUniform3fv(Kd_uniform, 1, material_1_5_diffuse);
		glUniform3fv(Ks_uniform, 1, material_1_5_specular);
		glUniform1f(material_shininess_uniform, material_1_5_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FIRST COLUMN, SIXTH SPHERE

		// set viwport
		viewportY = iHeight - (viewportHeight * 6);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_1_6_ambient);
		glUniform3fv(Kd_uniform, 1, material_1_6_diffuse);
		glUniform3fv(Ks_uniform, 1, material_1_6_specular);
		glUniform1f(material_shininess_uniform, material_1_6_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// -------------------
		// SECOND COLUMN SPHERE
		// -------------------

		// DRAW SECOND COLUMN, FIRST SPHERE

		// set viewport
		viewportX = iWidth - (viewportWidth * 3);	// x axis starting position for viewport
		viewportY = iHeight - (viewportHeight * 1);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_2_1_ambient);
		glUniform3fv(Kd_uniform, 1, material_2_1_diffuse);
		glUniform3fv(Ks_uniform, 1, material_2_1_specular);
		glUniform1f(material_shininess_uniform, material_2_1_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW SECOND COLUMN, SECOND SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 2);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_2_2_ambient);
		glUniform3fv(Kd_uniform, 1, material_2_2_diffuse);
		glUniform3fv(Ks_uniform, 1, material_2_2_specular);
		glUniform1f(material_shininess_uniform, material_2_2_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW SECOND COLUMN, THIRD SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 3);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_2_3_ambient);
		glUniform3fv(Kd_uniform, 1, material_2_3_diffuse);
		glUniform3fv(Ks_uniform, 1, material_2_3_specular);
		glUniform1f(material_shininess_uniform, material_2_3_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW SECOND COLUMN, FOURTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 4);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_2_4_ambient);
		glUniform3fv(Kd_uniform, 1, material_2_4_diffuse);
		glUniform3fv(Ks_uniform, 1, material_2_4_specular);
		glUniform1f(material_shininess_uniform, material_2_4_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW SECOND COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 5);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_2_5_ambient);
		glUniform3fv(Kd_uniform, 1, material_2_5_diffuse);
		glUniform3fv(Ks_uniform, 1, material_2_5_specular);
		glUniform1f(material_shininess_uniform, material_2_5_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW SECOND COLUMN, SIXTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 6);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_2_6_ambient);
		glUniform3fv(Kd_uniform, 1, material_2_6_diffuse);
		glUniform3fv(Ks_uniform, 1, material_2_6_specular);
		glUniform1f(material_shininess_uniform, material_2_6_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// -------------------
		// THIRD COLUMN SPHERE
		// -------------------

		// DRAW THIRD COLUMN, FIRST SPHERE

		// set viewport
		viewportX = iWidth - (viewportWidth * 2);	// x axis starting position for viewport
		viewportY = iHeight - (viewportHeight * 1);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_3_1_ambient);
		glUniform3fv(Kd_uniform, 1, material_3_1_diffuse);
		glUniform3fv(Ks_uniform, 1, material_3_1_specular);
		glUniform1f(material_shininess_uniform, material_3_1_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW THIRD COLUMN, SECOND SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 2);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_3_2_ambient);
		glUniform3fv(Kd_uniform, 1, material_3_2_diffuse);
		glUniform3fv(Ks_uniform, 1, material_3_2_specular);
		glUniform1f(material_shininess_uniform, material_3_2_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW THIRD COLUMN, THIRD SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 3);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_3_3_ambient);
		glUniform3fv(Kd_uniform, 1, material_3_3_diffuse);
		glUniform3fv(Ks_uniform, 1, material_3_3_specular);
		glUniform1f(material_shininess_uniform, material_3_3_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW THIRD COLUMN, FOURTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 4);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_3_4_ambient);
		glUniform3fv(Kd_uniform, 1, material_3_4_diffuse);
		glUniform3fv(Ks_uniform, 1, material_3_4_specular);
		glUniform1f(material_shininess_uniform, material_3_4_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW THIRD COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 5);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_3_5_ambient);
		glUniform3fv(Kd_uniform, 1, material_3_5_diffuse);
		glUniform3fv(Ks_uniform, 1, material_3_5_specular);
		glUniform1f(material_shininess_uniform, material_3_5_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW THIRD COLUMN, SIXTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 6);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_3_6_ambient);
		glUniform3fv(Kd_uniform, 1, material_3_6_diffuse);
		glUniform3fv(Ks_uniform, 1, material_3_6_specular);
		glUniform1f(material_shininess_uniform, material_3_6_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// -------------------
		// FOURTH COLUMN SPHERE
		// -------------------

		// DRAW FOURTH COLUMN, FIRST SPHERE

		// set viewport
		viewportX = iWidth - (viewportWidth * 1);	// x axis starting position for viewport
		viewportY = iHeight - (viewportHeight * 1);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_4_1_ambient);
		glUniform3fv(Kd_uniform, 1, material_4_1_diffuse);
		glUniform3fv(Ks_uniform, 1, material_4_1_specular);
		glUniform1f(material_shininess_uniform, material_4_1_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FOURTH COLUMN, SECOND SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 2);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_4_2_ambient);
		glUniform3fv(Kd_uniform, 1, material_4_2_diffuse);
		glUniform3fv(Ks_uniform, 1, material_4_2_specular);
		glUniform1f(material_shininess_uniform, material_4_2_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FOURTH COLUMN, THIRD SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 3);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_4_3_ambient);
		glUniform3fv(Kd_uniform, 1, material_4_3_diffuse);
		glUniform3fv(Ks_uniform, 1, material_4_3_specular);
		glUniform1f(material_shininess_uniform, material_4_3_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FOURTH COLUMN, FOURTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 4);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_4_4_ambient);
		glUniform3fv(Kd_uniform, 1, material_4_4_diffuse);
		glUniform3fv(Ks_uniform, 1, material_4_4_specular);
		glUniform1f(material_shininess_uniform, material_4_4_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FOURTH COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 5);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_4_5_ambient);
		glUniform3fv(Kd_uniform, 1, material_4_5_diffuse);
		glUniform3fv(Ks_uniform, 1, material_4_5_specular);
		glUniform1f(material_shininess_uniform, material_4_5_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// DRAW FOURTH COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = iHeight - (viewportHeight * 6);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);

		// make model matrix to identity
		modelMatrix = mat4::identity();

		// apply z axis translation to go deep into the screen by -2.5
		// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
		modelMatrix = vmath::translate(0.0f, 0.0f, -2.5f);

		// pass model matrix and material components to shader through uniform
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniform3fv(Ka_uniform, 1, material_4_6_ambient);
		glUniform3fv(Kd_uniform, 1, material_4_6_diffuse);
		glUniform3fv(Ks_uniform, 1, material_4_6_specular);
		glUniform1f(material_shininess_uniform, material_4_6_shininess);

		// bind to vao of sphere
		glBindVertexArray(gVao_sphere);

		// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// unbind from vao of sphere
		glBindVertexArray(0);

		// stop using opengl program object
		glUseProgram(0);

	// END USING PROGRAM SCENE

	// START USING PROGRAM FILTER

		glUseProgram(gShaderProgramObjectFilter);

		glBindVertexArray(gVao_filter);

		glBindFramebuffer(GL_FRAMEBUFFER, filter_fbo[0]);
		glBindTexture(GL_TEXTURE_2D, tex_brightpass);
		glViewport(0, 0, viewportWidth, viewportHeight);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		glBindFramebuffer(GL_FRAMEBUFFER, filter_fbo[1]);
		glBindTexture(GL_TEXTURE_2D, tex_filter[0]);
		glViewport(0, 0, viewportWidth, viewportHeight);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		glUseProgram(0);

	// END USING PROGRAM FILTER
	SwapBuffers(ghdc);
}

void update(void)
{
	angleWhiteLight = angleWhiteLight + 0.001f;
	if (angleWhiteLight >= 360)
	{
		angleWhiteLight = 0.1f;
	}
}

void resize(int x, int y, int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(x, y, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}
	// destroy position vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	// destroy normal vbo
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	// destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObjectScene, gVertexShaderObject);
	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObjectScene, gFragmentShaderObjectScene);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObjectScene);
	gFragmentShaderObjectScene = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObjectScene);
	gShaderProgramObjectScene = 0;

	// unlink shader program
	glUseProgram(0);

	// deselect rendering context
	wglMakeCurrent(NULL, NULL);

	// delete rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	// delete device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
