#include <windows.h>
#include<stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning( disable: 4838 )
#include "XNAMath\xnamath.h"    // library required for math calculations

#include "Sphere.h"

#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"D3dcompiler.lib")
#pragma comment (lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global varibale declarations
FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

int iWidth;
int iHeight;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Color = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;

ID3D11InputLayout *gpID3D11InputLayout = NULL;

ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

// a struct to represent constant buffer(names here and those in shader need not to be same)
// Sir kept name same but case difference (i.e. here names are 'Hungarian' while in shader they are 'Camel')
struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR la;
	XMVECTOR ld;
	XMVECTOR ls;
	XMVECTOR light_position;
	XMVECTOR ka;
	XMVECTOR kd;
	XMVECTOR ks;
	float material_shininess;
	unsigned int gLKeyPressedUniform;
};

XMMATRIX gPerspectiveProjectionMatrix;

bool gbLight = false;
float angle = 0.0f;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

ID3D11Buffer *gpID3D11Buffer_IndexBuffer = NULL;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

float light_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
float light_Diffuse[] = { 1.0f,1.0f,1.0f,0.0f };
float light_Specular[] = { 1.0f,1.0f,1.0f,0.0f };
float light_Position[] = { 0.0f,0.0f,0.0f,0.0f };

// store ambient, diffuse, specular, position values of different materials
float material_array[4][6][4][4] = {
	{
		{
			// first sphere on first column, emerald material
			{ 0.0215f,0.1745f,0.0215f,1.0f },		// ambient
			{ 0.07568f,0.61424f,0.07568f,1.0f },	// diffuse
			{ 0.633f,0.727811f,0.633f,1.0f },		// specular
			{ 0.6f * 128.0f }						// position
		},
		{
			// second sphere on first column, jade material
			{ 0.135f,0.2225f,0.1575f,1.0f },
			{ 0.54f,0.89f,0.63f,1.0f },
			{ 0.316228f,0.316228f,0.316228f,1.0f },
			{ 0.1f * 128.0f }
		},
		{
			// third sphere on first column, obsidian material
			{ 0.05375f,0.05f,0.06625f,1.0f },
			{ 0.18275f,0.17f,0.22525f,1.0f },
			{ 0.332741f,0.328634f,0.346435f,1.0f },
			{ 0.3f * 128.0f }
		},
		{
			// fourth sphere on first column, pearl material
			{ 0.25f,0.20725f,0.20725f,1.0f },
			{ 1.0f,0.829f,0.829f,1.0f },
			{ 0.296648f,0.296648f,0.296648f,1.0f },
			{ 0.088f * 128.0f }
		},
		{
			// fifth sphere on first column, ruby material
			{ 0.1745f,0.01175f,0.01175f,1.0f },
			{ 0.61424f,0.04136f,0.04136f,1.0f },
			{ 0.727811f,0.626959f,0.626959f,1.0f },
			{ 0.6f * 128.0f }
		},
		{
			// sixth sphere on first column, turquoise material
			{ 0.1f,0.18725f,0.1745f,1.0f },
			{ 0.396f,0.74151f,0.69102f,1.0f },
			{ 0.297254f,0.30829f,0.306678f,1.0f },
			{ 0.6f * 128.0f }
		}
	},
	{
		{
			// first sphere on second column, brass material
			{ 0.329412f,0.223529f,0.027451f,1.0f },
			{ 0.780392f,0.568627f,0.113725f,1.0f },
			{ 0.992157f,0.941176f,0.807843f,1.0f },
			{ 0.21794872f * 128.0f }
		},
		{
			// second sphere on second column, bronze material
			{ 0.2125f,0.1275f,0.054f,1.0f },
			{ 0.714f,0.4284f,0.18144f,1.0f },
			{ 0.393548f,0.271906f,0.166721f,1.0f },
			{ 0.2f * 128.0f }
		},
		{
			// third sphere on second column, chrome material
			{ 0.25f,0.25f,0.25f,1.0f },
			{ 0.4f,0.4f,0.4f,1.0f },
			{ 0.774597f,0.774597f,0.774597f,1.0f },
			{ 0.6f * 128.0f }
		},
		{
			// fourth sphere on second column, copper material
			{ 0.19125f,0.0735f,0.0225f,1.0f },
			{ 0.7038f,0.27048f,0.0828f,1.0f },
			{ 0.256777f,0.137622f,0.086014f,1.0f },
			{ 0.1f * 128.0f }
		},
		{
			// fifth sphere on second column, gold material
			{ 0.24725f,0.1995f,0.0745f,1.0f },
			{ 0.75164f,0.60648f,0.22648f,1.0f },
			{ 0.628281f,0.555802f,0.366065f,1.0f },
			{ 0.4f * 128.0f }
		},
		{
			// sixth sphere on second column, silver material
			{ 0.19225f,0.19225f,0.19225f,1.0f },
			{ 0.50754f,0.50754f,0.50754f,1.0f },
			{ 0.508273f,0.508273f,0.508273f,1.0f },
			{ 0.4f * 128.0f }
		}
	},
	{
		{
			// first sphere on third column, black material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.01f,0.01f,0.01f,1.0f },
			{ 0.50f,0.50f,0.50f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// second sphere on third column, cyan material
			{ 0.0f,0.1f,0.06f,1.0f },
			{ 0.0f,0.50980392f,0.50980392f,1.0f },
			{ 0.50196078f,0.50196078f,0.50196078f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// third sphere on third column, green material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.1f,0.35f,0.1f,1.0f },
			{ 0.45f,0.55f,0.45f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// fourth sphere on third column, red material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.5f,0.0f,0.0f,1.0f },
			{ 0.7f,0.6f,0.6f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// fifth sphere on third column, white material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.55f,0.55f,0.55f,1.0f },
			{ 0.70f,0.70f,0.70f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// sixth sphere on third column, yellow plastic material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.5f,0.5f,0.0f,1.0f },
			{ 0.60f,0.60f,0.50f,1.0f },
			{ 0.25f * 128.0f }
		}
	},
	{
		{
			// first sphere on fourth column, black material
			{ 0.02f,0.02f,0.02f,1.0f },
			{ 0.01f,0.01f,0.01f,1.0f },
			{ 0.4f,0.4f,0.4f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// second sphere on fourth column, cyan material
			{ 0.0f,0.05f,0.05f,1.0f },
			{ 0.4f,0.5f,0.5f,1.0f },
			{ 0.04f,0.7f,0.7f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// third sphere on fourth column, green material
			{ 0.0f,0.05f,0.0f,1.0f },
			{ 0.4f,0.5f,0.4f,1.0f },
			{ 0.04f,0.7f,0.04f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// fourth sphere on fourth column, red material
			{ 0.05f,0.0f,0.0f,1.0f },
			{ 0.5f,0.4f,0.4f,1.0f },
			{ 0.7f,0.04f,0.04f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// fifth sphere on fourth column, white material
			{ 0.05f,0.05f,0.05f,1.0f },
			{ 0.5f,0.5f,0.5f,1.0f },
			{ 0.7f,0.7f,0.7f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// sixth sphere on fourth column, yellow rubber material
			{ 0.05f,0.05f,0.0f,1.0f },
			{ 0.5f,0.5f,0.4f,1.0f },
			{ 0.7f,0.7f,0.04f,1.0f },
			{ 0.078125f * 128.0f }
		}
	},
};

// color angles
float angleWhiteLight = 0.0f;
int keyPressed;

int viewportX;
int viewportY;
int viewportWidth;
int viewportHeight;
int spaceFromTopInEachViewport;

// Winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Direct3D11");
	bool bDone = false;

	// code
	// create a log file
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created\nExitting..."), TEXT("Eror"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully opened.\n");
		fclose(gpFile);
	}

	// initialize WNDCLASSEX structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register WNDCLASSEX structure
	RegisterClassEx(&wndclass);

	// Initialize Width and Height of window
	iWidth = WIN_WIDTH;
	// minus 30 from winow height because caption bar also considered as part of window height 
	// so that any drawing will not get overlapped by caption bar
	iHeight = WIN_HEIGHT - 30;
	viewportWidth = iWidth / 4;
	viewportHeight = iHeight / 6;

	// create window
	hwnd = CreateWindow(szClassName, TEXT("Material - Direct3D11"), WS_OVERLAPPEDWINDOW, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// initialize D3D
	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() Failed. Exiting Now...\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() Succeeded.\n");
		fclose(gpFile);
	}

	// message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// render
			display();
			update();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

	// clean up
	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	HRESULT resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize();

	// variable declarations
	HRESULT hr;
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) // if 0, window is active
			gbActiveWindow = true;
		else    // if non zero then window is not active 
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() failed.\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() succeeded.\n");
				fclose(gpFile);
			}
		}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x4C:	// for L or l
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;

				light_Position[0] = { 0.0f };
				light_Position[1] = { 0.0f };
				light_Position[2] = { 1.0f };
				light_Position[3] = { 0.0f };
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		case 0x46:	// for F or f
			if (gbFullscreen == false)
			{
				ToggleFullScreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullscreen = false;
			}

			iWidth = GetSystemMetrics(SM_CXFULLSCREEN);
			iHeight = GetSystemMetrics(SM_CYFULLSCREEN);

			if (gbFullscreen == true)
			{
				viewportWidth = iWidth / 4;
				viewportHeight = iHeight / 6;
			}
			else
			{
				// Ideally, there is no need to right this statement because we already getting width and height of window on line 403 and 404
				// but i don't know why, its giving me different values
				// Need to figure out why it is happening
				iWidth = WIN_WIDTH;
				iHeight = WIN_HEIGHT - 30;

				viewportWidth = iWidth / 4;
				viewportHeight = iHeight / 6;
			}
			break;
		case 0x58:
			keyPressed = 1;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		case 0x59:
			keyPressed = 2;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		case 0x5A:
			keyPressed = 3;
			// resest light position
			light_Position[0] = { 0.0f };
			light_Position[1] = { 0.0f };
			light_Position[2] = { 0.0f };
			light_Position[3] = { 0.0f };
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	// function declarations
	void uninitialize(void);
	HRESULT resize(int, int);

	// variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default lowest
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;  // based upon d3dFeatureLevel_required

								// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);  // calculating size of array

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,    // Adapter
			d3dDriverType,   // Driver Type
			NULL,    // Software
			createDeviceFlags,   // Flags
			&d3dFeatureLevel_required,   // Feature Level
			numFeatureLevels,   // Num Feature Levels
			D3D11_SDK_VERSION,   // SDK Version
			&dxgiSwapChainDesc, // Swap Chain Desc
			&gpIDXGISwapChain,  // Swap Chain
			&gpID3D11Device,    // Device
			&d3dFeatureLevel_acquired,  // Feature Level
			&gpID3D11DeviceContext      // Device Context
		);

		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(gpFile, "The chosen driver is of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "Warp Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type.\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type.\n");
		}

		fprintf_s(gpFile, "The supported highest feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown\n");
		}
		fclose(gpFile);
	}

	// initialize shader, input layout, constant buffers etc.
	const char *vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldMatrix;" \
		"float4x4 viewMatrix;" \
		"float4x4 projectionMatrix;" \
		"float4 la;"\
		"float4 ld;"\
		"float4 ls;"\
		"float4 light_position;"\
		"float4 ka;"\
		"float4 kd;"\
		"float4 ks;"\
		"float material_shininess;"\
		"uint gLKeyPressedUniform;"\
		"}" \
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float4 phong_ads_color:COLOR;"\
		"float3 transformed_normals:NORMAL0;"\
		"float3 light_direction:NORMAL1;"\
		"float3 view_vector:NORMAL2;"\
		"};"\
		"vertex_output main(float4 pos:POSITION, float4 normal:NORMAL)" \
		"{" \
		"vertex_output output;"\
		"if(gLKeyPressedUniform == 1)"
		"{"\
		"float4 eyeCoordinates = mul(worldMatrix,pos);"\
		"eyeCoordinates = mul(viewMatrix,eyeCoordinates);"\
		"output.transformed_normals = mul((float3x3)worldMatrix, (float3)normal);"\
		"output.transformed_normals = mul((float3x3)viewMatrix,output.transformed_normals);"\
		"output.light_direction = (float3)light_position - eyeCoordinates.xyz;"\
		"output.view_vector = -eyeCoordinates.xyz;"\
		"}"\
		"float4 position = mul(worldMatrix,pos);"\
		"position = mul(viewMatrix,position);" \
		"position = mul(projectionMatrix,position);" \
		"output.position = position;"\
		"return(output);" \
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile failed for vertex shader : %s. \n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile succedded for vertex shader\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device:CreateVertexShader() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device:CreateVertexShader Succeeded.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);

	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldMatrix;" \
		"float4x4 viewMatrix;" \
		"float4x4 projectionMatrix;" \
		"float4 la;"\
		"float4 ld;"\
		"float4 ls;"\
		"float4 light_position;"\
		"float4 ka;"\
		"float4 kd;"\
		"float4 ks;"\
		"float material_shininess;"\
		"uint gLKeyPressedUniform;"\
		"}" \
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float4 phong_ads_color:COLOR;"\
		"float3 transformed_normals:NORMAL0;"\
		"float3 light_direction:NORMAL1;"\
		"float3 view_vector:NORMAL2;"\
		"};"\
		"float4 main(float4 pos:SV_POSITION, vertex_output input):SV_TARGET"\
		"{"\
		"float4 output_color;"\
		"if(gLKeyPressedUniform == 1)"
		"{"\
		"float3 normalized_transformed_normals = normalize(input.transformed_normals);"\
		"float3 normalized_light_direction = normalize(input.light_direction);"\
		"float3 normalized_view_vector = normalize(input.view_vector);"\
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"\
		"float3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
		"float4 ambient = la * ka;"\
		"float4 diffuse = ld * kd * tn_dot_ld;"\
		"float4 specular = ls * ks * pow(max(dot(reflection_vector,normalized_view_vector),0.0),material_shininess);"\
		"output_color = ambient + diffuse + specular;"\
		"}"\
		"else"\
		"{"\
		"output_color = float4(1.0,1.0,1.0,1.0);"\
		"}"\
		"return(output_color);"\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for pixel shader:%s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile succeeded for pixel shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader succeeded.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// create vertex buffer for sphere position
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Sphere_Position);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer failed for sphere vertex buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer succeeded for sphere vertex buffer.\n");
		fclose(gpFile);
	}

	// copy sphere vertices into above buffer
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Sphere_Position, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, sizeof(sphere_vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Sphere_Position, NULL);

	// create vertex buffer for sphere normals
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Sphere_Normal);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer failed for sphere normal buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer succeeded for sphere normal buffer.\n");
		fclose(gpFile);
	}

	// copy sphere normals into above buffer
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Sphere_Normal, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_normals, sizeof(sphere_normals));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Sphere_Normal, NULL);

	// create index buffer for sphere indices
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = gNumElements * sizeof(short);
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_IndexBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer failed for sphere index buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer succeeded for sphere normal buffer.\n");
		fclose(gpFile);
	}

	// copy indices into above index buffer
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_IndexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, gNumElements * sizeof(short));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_IndexBuffer, NULL);

	// create and set input layout for position
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0;

	// create and set input layout for normal
	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc, 2, pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout succeeded.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	// define and set constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer failed for constant buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer succeeded for constant buffer.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	// d3d clear color(black)
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	// set projection matrix to identity matrix
	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// make back face culling off because by default, it is on, in directx
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);
	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	// NOTE : IF YOU WANT BACK FACE CULLING ON THEN COMMENT ABOVE RASTERIZER CODE

	// call resize for first time
	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() succeeded\n");
		fclose(gpFile);
	}
	return(S_OK);
}

HRESULT resize(int width, int height)
{
	// code
	HRESULT hr = S_OK;

	// free any depth stencil view
	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// free any size-dependent resources
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// again get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// Though below code looks related to texture but its not 
	// because swap chain does not have depth buffer, that's why we are using texture buffer and converting it to depth buffer later

	// create texture buffer
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = width;  // This is texture width but it is going to be depth buffer width after conversion
	textureDesc.Height = height;    // This is texture height but it is going to be depth buffer height after conversion
	textureDesc.ArraySize = 1;  // This refers to texture array size but we are going to consider it as depth, hence there is no array and its value is 1
	textureDesc.MipLevels = 1;  // This refers to mipmap levels but as we are going to use it as depth, hence its value will be at least 1

								// If count is 4 and quality is 1 here, then application crashes failing CreateTexture2D()
								// If you expect the application should run with count 4 and quality 1 then SampleDesc must have same values in initialize also
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;

	textureDesc.Format = DXGI_FORMAT_D32_FLOAT; // This is analogous to depthBits in pixel format descriptor
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer = NULL;
	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Failed.\n");
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Succeeded.\n");
		fclose(gpFile);
	}

	// now proceed for converting above texture buffer to depth stencil view buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;   // This indicates makes DSV's dimentions TEXTURE2DMS capable (MS stands for MultiSampling)

																			// crete depth stencil view
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Failed.\n");
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	// set render target view as render target
	// 1 indicates how many buffers are there in 2nd paramter
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// set viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	// set perspective matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);
}

void display(void)
{
	// code
	// clear render target view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	// clear depth stencil view
	// first parameter indicates who is going to be depth stencil view
	// second parameter is D3D11_CLEAR_FLAGS enum and it have 2 member as D3D11_CLEAR_DEPTH and D3D11_CLEAR_STENCIL
	// third parameter is for to Clear the depth buffer with this value. This value will be clamped between 0 and 1.
	// fourth paramter is for to Clear the stencil buffer with this value.
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);

	// START RENDERING FOR PYRAMID

	// select which vertex buffer to display
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	// 0 - is equal to input slot
	// 1 - indicates there is only one buffer in that (i.e not interleaved)
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Sphere_Position, &stride, &offset);

	// select which normal buffer to display
	stride = sizeof(float) * 3;
	offset = 0;
	// 2 - is equal to input slot
	// 1 - indicates there is only one buffer in that (i.e not interleaved)
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Sphere_Normal, &stride, &offset);

	// 1st parameter is globally declared buffer for index buffer which contains indices of sphere
	// 2nd parameter is format of DXGI
	// 3rd parameter indicates position of your buffer in index buffer because index buffer is a array
	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	// select grometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// translation is concerned with world matrix transformation
	float xAxis = 0.0f;
	float yAxis = 0.0f;
	float zAxis = 3.0f;
	XMMATRIX worldMatrix = XMMatrixIdentity();
	worldMatrix = XMMatrixTranslation(xAxis, yAxis, zAxis);
	XMMATRIX viewMatrix = XMMatrixIdentity();

	// load the data into constant buffer
	CBUFFER constantBuffer;
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;

	if (gbLight == true)
	{
		constantBuffer.gLKeyPressedUniform = 1;

		// set light properties and position
		constantBuffer.la = XMVectorSet(light_Ambient[0], light_Ambient[1], light_Ambient[2], light_Ambient[3]);
		constantBuffer.ld = XMVectorSet(light_Diffuse[0], light_Diffuse[1], light_Diffuse[2], light_Diffuse[3]);
		constantBuffer.ls = XMVectorSet(light_Specular[0], light_Specular[1], light_Specular[2], light_Specular[3]);
		if (keyPressed == 1)
		{
			light_Position[1] = 100.0f * cos(angleWhiteLight);
			light_Position[2] = 100.0f * sin(angleWhiteLight);
		}
		else if (keyPressed == 2)
		{
			light_Position[0] = 100.0f * sin(angleWhiteLight);
			light_Position[2] = 100.0f * cos(angleWhiteLight);
		}
		else if (keyPressed == 3)
		{
			light_Position[0] = 100.0f * cos(angleWhiteLight);
			light_Position[1] = 100.0f * sin(angleWhiteLight);
		}
		constantBuffer.light_position = XMVectorSet(light_Position[0], light_Position[1], light_Position[2], light_Position[3]);
	}
	else
	{
		constantBuffer.gLKeyPressedUniform = 0;
	}

	fopen_s(&gpFile, gszLogFileName, "a+");
	fprintf_s(gpFile, "iWidth - %d, iHeight - %d.\n",iWidth,iHeight);
	fprintf_s(gpFile, "viewportWidth - %d, viewportHeight - %d.\n", viewportWidth, viewportHeight);
	fclose(gpFile);

	// apply materials to all sphere
	int temp = 4;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			// set viewport
			viewportX = iWidth - (viewportWidth * temp);	// x axis starting position for viewport
			viewportY = viewportHeight * j;
			//viewportY = iHeight - (viewportHeight * (j));	// y axis starting position for viewport

			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "viewportX - %d, viewportY - %d.\n", viewportX, viewportY);
			fclose(gpFile);

			D3D11_VIEWPORT d3dViewPort;
			d3dViewPort.TopLeftX = (float)viewportX;
			d3dViewPort.TopLeftY = (float)viewportY;
			d3dViewPort.Width = (float)viewportWidth;
			d3dViewPort.Height = (float)viewportHeight;
			d3dViewPort.MinDepth = 0.0f;
			d3dViewPort.MaxDepth = 1.0f;
			gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

			gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)viewportWidth / (float)viewportHeight, 0.1f, 100.0f);
			//constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;

			constantBuffer.ka = XMVectorSet(material_array[i][j][0][0], material_array[i][j][0][1], material_array[i][j][0][2], material_array[i][j][0][3]);
			constantBuffer.kd = XMVectorSet(material_array[i][j][1][0], material_array[i][j][1][1], material_array[i][j][1][2], material_array[i][j][1][3]);
			constantBuffer.ks = XMVectorSet(material_array[i][j][2][0], material_array[i][j][2][1], material_array[i][j][2][2], material_array[i][j][2][3]);
			constantBuffer.material_shininess = material_array[i][j][3][0];

			// draw
			gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

			constantBuffer.WorldMatrix = worldMatrix;

			gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

			yAxis = yAxis - 0.8f;
		}
		xAxis = xAxis + 0.9f;
		yAxis = 1.9f;
		temp--;
		viewportX += 200;
	}

	// END RENDERING FOR SPHERE

	// switch between front and back buffer
	gpIDXGISwapChain->Present(0, 0);
}

void update()
{
	angleWhiteLight = angleWhiteLight - 0.5f;
	if (angleWhiteLight < -360.0f)
	{
		angleWhiteLight = 0.0f;
	}
}

void uninitialize(void)
{
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}
	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}
	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}
	if (gpID3D11Buffer_IndexBuffer)
	{
		gpID3D11Buffer_IndexBuffer->Release();
		gpID3D11Buffer_IndexBuffer = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer_Sphere_Normal)
	{
		gpID3D11Buffer_VertexBuffer_Sphere_Normal->Release();
		gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer_Sphere_Position)
	{
		gpID3D11Buffer_VertexBuffer_Sphere_Position->Release();
		gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
	}
	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}
	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}
	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}
	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "uninitialize() succeeded.\n");
		fprintf_s(gpFile, "Log file is successfully closed.\n");
		fclose(gpFile);
	}
}
