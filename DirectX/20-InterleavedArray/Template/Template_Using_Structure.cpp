#include <windows.h>
#include<stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>
#include "WICTextureLoader.h"

#pragma warning( disable: 4838 )
#include "XNAMath\xnamath.h"    // library required for math calculations

#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"D3dcompiler.lib")
#pragma comment (lib, "DirectXTK.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global varibale declarations
FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

int iWidth;
int iHeight;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;

ID3D11InputLayout *gpID3D11InputLayout = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Cube = NULL;

ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11ShaderResourceView *gpID3D11ShaderResourceView_Cube_Texture = NULL;
ID3D11SamplerState *gpID3D11SamplerState_Cube_Texture = NULL;

// a struct to represent constant buffer(names here and those in shader need not to be same)
// Sir kept name same but case difference (i.e. here names are 'Hungarian' while in shader they are 'Camel')
struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR la;
	XMVECTOR ld;
	XMVECTOR ls;
	XMVECTOR light_position;
	XMVECTOR ka;
	XMVECTOR kd;
	XMVECTOR ks;
	float material_shininess;
	unsigned int gLKeyPressedUniform;
};

XMMATRIX gPerspectiveProjectionMatrix;

bool gbLight = false;
float angleCube = 0.0f;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

ID3D11Buffer *gpID3D11Buffer_IndexBuffer = NULL;

float lightAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
float lightDiffuse[] = { 1.0f,1.0f,1.0f,0.0f };
float lightSpecular[] = { 1.0f,1.0f,1.0f,0.0f };
float lightPosition[] = { 0.0f,0.0f,0.0f,0.0f };

float materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float materialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialShininess = { 128.0 };

struct structCubeVCNT
{
	float position[3];
	float color[3];
	float normal[3];
	float texture[2];
};

// Winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declaration
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Direct3D11");
	bool bDone = false;

	// code
	// create a log file
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created\nExitting..."), TEXT("Eror"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully opened.\n");
		fclose(gpFile);
	}

	// initialize WNDCLASSEX structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register WNDCLASSEX structure
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szClassName, TEXT("Interleaved- Direct3D11"), WS_OVERLAPPEDWINDOW, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// initialize D3D
	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() Failed. Exiting Now...\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() Succeeded.\n");
		fclose(gpFile);
	}

	// message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// render
			display();
			update();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}
	}

	// clean up
	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	HRESULT resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize();

	// variable declarations
	HRESULT hr;
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) // if 0, window is active
			gbActiveWindow = true;
		else    // if non zero then window is not active 
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() failed.\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() succeeded.\n");
				fclose(gpFile);
			}
		}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x4C:	// for L or l
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		case 0x46:	// for F or f
			if (gbFullscreen == false)
			{
				ToggleFullScreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	// function declarations
	HRESULT LoadD3DTexture(const wchar_t *, ID3D11ShaderResourceView **);
	void uninitialize(void);
	HRESULT resize(int, int);

	// variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default lowest
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;  // based upon d3dFeatureLevel_required

								// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);  // calculating size of array

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,    // Adapter
			d3dDriverType,   // Driver Type
			NULL,    // Software
			createDeviceFlags,   // Flags
			&d3dFeatureLevel_required,   // Feature Level
			numFeatureLevels,   // Num Feature Levels
			D3D11_SDK_VERSION,   // SDK Version
			&dxgiSwapChainDesc, // Swap Chain Desc
			&gpIDXGISwapChain,  // Swap Chain
			&gpID3D11Device,    // Device
			&d3dFeatureLevel_acquired,  // Feature Level
			&gpID3D11DeviceContext      // Device Context
		);

		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(gpFile, "The chosen driver is of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "Warp Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type.\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type.\n");
		}

		fprintf_s(gpFile, "The supported highest feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown\n");
		}
		fclose(gpFile);
	}

	// initialize shader, input layout, constant buffers etc.
	const char *vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldMatrix;" \
		"float4x4 viewMatrix;" \
		"float4x4 projectionMatrix;" \
		"float4 la;"\
		"float4 ld;"\
		"float4 ls;"\
		"float4 lightPosition;"\
		"float4 ka;"\
		"float4 kd;"\
		"float4 ks;"\
		"float material_shininess;"\
		"uint gLKeyPressedUniform;"\
		"}" \

		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float4 color:COLOR;"\
		"float3 transformed_normals:NORMAL0;"\
		"float3 light_direction:NORMAL1;"\
		"float3 view_vector:NORMAL2;"\
		"float2 texcoord:TEXCOORD;"\
		"};"\

		// in inputElementDesc we have passed as POSITION that's why here it is POSITION 
		// you can pass any symantic value in input element, as you want, like MYPOSITION, HERPOSITION
		// same applies for COLOR also
		// if you don't want to pass any symantic value, then it should be SYSTEM VALUE
		// else it will giver error
		// but in books of Frank Luna, conventionally they have used POSITION and COLOR etc...

		// in opengl, type for input normals is vec3 but in directx it is float4, why ?
		// because, they are all 4 vertices coordinates not 3, if you see any normals related opengl application, in that they are passed as 4 vertices coordinates only from application
		// and shader was typecasting them to 3 vertices coordinates during taking as input
		// but directx doesnot support self-typecasting
		"vertex_output main(float4 pos:POSITION, float4 color:COLOR, float4 normal:NORMAL, float2 texcoord:TEXCOORD)" \
		"{" \
		"vertex_output output;"\

		"if(gLKeyPressedUniform == 1)" \
		"{"\
		"float4 eyeCoordinates = mul(worldMatrix,pos);"\
		"eyeCoordinates = mul(viewMatrix,eyeCoordinates);"\

		// Actually we are passing normals as input to vertex shader as float4
		// but, as we need to take top left 3x3 matrix from world matrix, we need to convert them in float3
		"output.transformed_normals = mul((float3x3)worldMatrix, (float3)normal);"\
		"output.transformed_normals = mul((float3x3)viewMatrix,output.transformed_normals);"\

		// light direction
		"output.light_direction = (float3)lightPosition - eyeCoordinates.xyz;"\

		"output.view_vector = -eyeCoordinates.xyz;"\
		"}"\

		// calculate position
		"output.position = mul(worldMatrix,pos);"\
		"output.position = mul(viewMatrix,output.position);" \
		"output.position = mul(projectionMatrix,output.position);" \

		"output.texcoord = texcoord;"\

		"output.color = color;"\

		"return(output);" \
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile failed for vertex shader : %s. \n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile succedded for vertex shader\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device:CreateVertexShader() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device:CreateVertexShader Succeeded.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);

	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldMatrix;" \
		"float4x4 viewMatrix;" \
		"float4x4 projectionMatrix;" \
		"float4 la;"\
		"float4 ld;"\
		"float4 ls;"\
		"float4 lightPosition;"\
		"float4 ka;"\
		"float4 kd;"\
		"float4 ks;"\
		"float material_shininess;"\
		"uint gLKeyPressedUniform;"\
		"}" \

		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float4 color:COLOR;"\
		"float3 transformed_normals:NORMAL0;"\
		"float3 light_direction:NORMAL1;"\
		"float3 view_vector:NORMAL2;"\
		"float2 texcoord:TEXCOORD;"\
		"};"\

		"Texture2D myTexture2D;"\
		"SamplerState mySamplerState;"\

		// if you take input value as float4 pos:SV_POSITION then also it works because shader variables are shared and global
		// but that's not a proper way  because it is a pipeline and in pipeline output of one shader should be input of next shader(including symantics)
		// They are system values(SV_POSITION and COLOR) and that's why it is common/shared to all
		// but if you want user defined symantics to be available across the shaders, then you should use the concept of ID3D11ClassLinkage interface

		// SV_TARGET : It can be color buffer, depth buffer, stencil buffer, accumulation buffer
		"float4 main(float4 pos:SV_POSITION, vertex_output input):SV_TARGET"\
		"{"\
		"float4 output_color;"\
		"float4 phong_ads_color;"\
		"if(gLKeyPressedUniform == 1)"
		"{"\
		// calculate normalized transformed normals, light direction and view vector
		"float3 normalized_transformed_normals = normalize(input.transformed_normals);"\
		"float3 normalized_light_direction = normalize(input.light_direction);"\
		"float3 normalized_view_vector = normalize(input.view_vector);"\

		// dot product of transformed normals and light direction
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"\

		// calculate reflectin vector
		"float3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\

		// calculate light components
		"float4 ambient = la * ka;"\
		"float4 diffuse = ld * kd * tn_dot_ld;"\
		"float4 specular = ls * ks * pow(max(dot(reflection_vector,normalized_view_vector),0.0),material_shininess);"\
		"phong_ads_color = ambient + diffuse + specular;"\
		"}"\
		"else"\
		"{"\
		"phong_ads_color = float4(1.0,1.0,1.0,1.0);"\
		"}"\
		"output_color = myTexture2D.Sample(mySamplerState, input.texcoord) * input.color * phong_ads_color;"\
		"return(output_color);"\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for pixel shader:%s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile succeeded for pixel shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader succeeded.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);

	D3D11_INPUT_ELEMENT_DESC inputElementDesc[4] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc, 4, pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout succeeded.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	struct structCubeVCNT objStruct[36] =
	{
		// FRONT
		// triangle 1
		{
			{ -1.0f,1.0f,-1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,0.0f,-1.0f },
			{ 0.0f,0.0f }
		},
		{
			{ 1.0f,1.0f,-1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,0.0f,-1.0f },
			{ 1.0f,0.0f }
		},
		{
			{ -1.0f,-1.0f,-1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,0.0f,-1.0f },
			{ 0.0f,1.0f }
		},
		// trianlge 2
		{
			{ -1.0f,-1.0f,-1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,0.0f,-1.0f },
			{ 0.0f,1.0f }
		},
		{
			{ 1.0f,1.0f,-1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,0.0f,-1.0f },
			{ 1.0f,0.0f }
		},
		{
			{ 1.0f,-1.0f,-1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,0.0f,-1.0f },
			{ 1.0f,1.0f }
		},

		// RIGHT
		// triangle 1
		{
			{ 1.0f,-1.0f,-1.0f },
			{ 1.0f,0.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,1.0f }
		},
		{
			{ 1.0f,1.0f,-1.0f },
			{ 1.0f,0.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,0.0f }
		},
		{
			{ 1.0f,-1.0f,1.0f },
			{ 1.0f,0.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 1.0f,1.0f }
		},
		// triangle 2
		{
			{ 1.0f,-1.0f,1.0f },
			{ 1.0f,0.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 1.0f,1.0f }
		},
		{
			{ 1.0f,1.0f,-1.0f },
			{ 1.0f,0.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,0.0f }
		},
		{
			{ 1.0f,1.0f,1.0f },
			{ 1.0f,0.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 1.0f,0.0f }
		},

		// BACK
		// triangle 1
		{
			{ 1.0f,-1.0f,1.0f },
			{ 0.0f,1.0f,1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,1.0f }
		},
		{
			{ 1.0f,1.0f,1.0f },
			{ 0.0f,1.0f,1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,0.0f }
		},
		{
			{ -1.0f,-1.0f,1.0f },
			{ 0.0f,1.0f,1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 1.0f,1.0f }
		},
		// triangle 2
		{
			{ -1.0f,-1.0f,1.0f },
			{ 0.0f,1.0f,1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 1.0f,1.0f }
		},
		{
			{ 1.0f,1.0f,1.0f },
			{ 0.0f,1.0f,1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 0.0f,0.0f }
		},
		{
			{ -1.0f,1.0f,1.0f },
			{ 0.0f,1.0f,1.0f },
			{ 0.0f,0.0f,1.0f },
			{ 1.0f,0.0f }
		},

		// LEFT
		// triangle 1
		{
			{ -1.0f,1.0f,1.0f },
			{ 1.0f,1.0f,0.0f },
			{ -1.0f,0.0f,0.0f },
			{ 0.0f,0.0f }
		},
		{
			{ -1.0f,1.0f,-1.0f },
			{ 1.0f,1.0f,0.0f },
			{ -1.0f,0.0f,0.0f },
			{ 1.0f,0.0f }
		},
		{
			{ -1.0f,-1.0f,1.0f },
			{ 1.0f,1.0f,0.0f },
			{ -1.0f,0.0f,0.0f },
			{ 0.0f,1.0f }
		},
		// triangle 2
		{
			{ -1.0f,-1.0f,1.0f },
			{ 1.0f,1.0f,0.0f },
			{ -1.0f,0.0f,0.0f },
			{ 0.0f,1.0f }
		},
		{
			{ -1.0f,1.0f,-1.0f },
			{ 1.0f,1.0f,0.0f },
			{ -1.0f,0.0f,0.0f },
			{ 1.0f,0.0f }
		},
		{
			{ -1.0f,-1.0f,-1.0f },
			{ 1.0f,1.0f,0.0f },
			{ -1.0f,0.0f,0.0f },
			{ 1.0f,1.0f }
		},

		// TOP
		// triangle 1
		{
			{ -1.0f,1.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,0.0f }
		},
		{
			{ 1.0f,1.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,1.0f,0.0f },
			{ 1.0f,0.0f }
		},
		{
			{ -1.0f,1.0f,-1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,1.0f }
		},
		// triangle 2
		{
			{ -1.0f,1.0f,-1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,1.0f }
		},
		{
			{ 1.0f,1.0f,1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,1.0f,0.0f },
			{ 1.0f,0.0f }
		},
		{
			{ 1.0f,1.0f,-1.0f },
			{ 1.0f,0.0f,0.0f },
			{ 0.0f,1.0f,0.0f },
			{ 1.0f,1.0f }
		},

		// BOTTOM
		// triangle 1
		{
			{ 1.0f,-1.0f,-1.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,-1.0f,0.0f },
			{ 0.0f,0.0f }
		},
		{
			{ 1.0f,-1.0f,1.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,-1.0f,0.0f },
			{ 1.0f,0.0f }
		},
		{
			{ -1.0f,-1.0f,-1.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,-1.0f,0.0f },
			{ 0.0f,1.0f }
		},
		// trianlge 2
		{
			{ -1.0f,-1.0f,-1.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,-1.0f,0.0f },
			{ 0.0f,1.0f }
		},
		{
			{ 1.0f,-1.0f,1.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,-1.0f,0.0f },
			{ 1.0f,0.0f }
		},
		{
			{ -1.0f,-1.0f,1.0f },
			{ 0.0f,1.0f,0.0f },
			{ 0.0f,-1.0f,0.0f },
			{ 1.0f,1.0f }
		}
	};

	// create vertex buffer for cube position, color, normal and texture
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * 36 * 11;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Cube);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer failed for cube vertex buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer succeeded for cube vertex buffer.\n");
		fclose(gpFile);
	}

	// copy sphere vertices into above buffer
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Cube, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
	memcpy(mappedSubresource.pData, objStruct, sizeof(objStruct));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Cube, NULL);

	// define and set constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));

	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer failed for constant buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer succeeded for constant buffer.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	// make back face culling off because by default, it is on, in directx
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	// create rasterizer state
	hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRasterizerState() Failed For Culling.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRasterizerState() Succeeded For Culling.\n");
		fclose(gpFile);
	}

	// set rasterizer state
	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	// NOTE : IF YOU WANT BACK FACE CULLING ON THEN COMMENT ABOVE RASTERIZER CODE

	// create texture resource and texture view
	hr = LoadD3DTexture(L"marble.bmp", &gpID3D11ShaderResourceView_Cube_Texture);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "LoadD3DTexture() Failed For Cube Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "LoadD3DTexture() Succeeded For Cube Texture.\n");
		fclose(gpFile);
	}

	// create sampler state same as glTexParameteri()
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = gpID3D11Device->CreateSamplerState(&samplerDesc, &gpID3D11SamplerState_Cube_Texture);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateSamplerState() Failed For Cube Texture.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateSamplerState() Succeeded For Cube Texture.\n");
		fclose(gpFile);
	}

	// d3d clear color(black)
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	// set projection matrix to identity matrix
	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// call resize for first time
	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() succeeded\n");
		fclose(gpFile);
	}
	return(S_OK);
}

HRESULT resize(int width, int height)
{
	// code
	HRESULT hr = S_OK;

	// free any depth stencil view
	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// free any size-dependent resources
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// again get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// Though below code looks related to texture but its not 
	// because swap chain does not have depth buffer, that's why we are using texture buffer and converting it to depth buffer later

	// create texture buffer
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = width;  // This is texture width but it is going to be depth buffer width after conversion
	textureDesc.Height = height;    // This is texture height but it is going to be depth buffer height after conversion
	textureDesc.ArraySize = 1;  // This refers to texture array size but we are going to consider it as depth, hence there is no array and its value is 1
	textureDesc.MipLevels = 1;  // This refers to mipmap levels but as we are going to use it as depth, hence its value will be at least 1

								// If count is 4 and quality is 1 here, then application crashes failing CreateTexture2D()
								// If you expect the application should run with count 4 and quality 1 then SampleDesc must have same values in initialize also
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;

	textureDesc.Format = DXGI_FORMAT_D32_FLOAT; // This is analogous to depthBits in pixel format descriptor
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer = NULL;
	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Failed.\n");
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() Succeeded.\n");
		fclose(gpFile);
	}

	// now proceed for converting above texture buffer to depth stencil view buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;   // This indicates makes DSV's dimentions TEXTURE2DMS capable (MS stands for MultiSampling)

																			// crete depth stencil view
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Failed.\n");
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	// set render target view as render target
	// 1 indicates how many buffers are there in 2nd paramter
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// set viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	// set perspective matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);
}

HRESULT LoadD3DTexture(const wchar_t *textureFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView)
{
	// code
	HRESULT hr;

	// create texture
	hr = DirectX::CreateWICTextureFromFile(gpID3D11Device, gpID3D11DeviceContext, textureFileName, NULL, ppID3D11ShaderResourceView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile failed for textured resource.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateWICTextureFromFile succeeded for textured resource.\n");
		fclose(gpFile);
	}
	return(hr);
}

void display(void)
{
	// code
	// clear render target view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	// clear depth stencil view
	// first parameter indicates who is going to be depth stencil view
	// second parameter is D3D11_CLEAR_FLAGS enum and it have 2 member as D3D11_CLEAR_DEPTH and D3D11_CLEAR_STENCIL
	// third parameter is for to Clear the depth buffer with this value. This value will be clamped between 0 and 1.
	// fourth paramter is for to Clear the stencil buffer with this value.
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);

	// START RENDERING FOR PYRAMID

	// select which vertex buffer to display
	UINT stride = sizeof(float) * 11;
	UINT offset = 0;
	// 0 - is equal to input slot
	// 1 - indicates there is only one buffer in that (i.e not interleaved)
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Cube, &stride, &offset);

	// select which color buffer to display
	stride = sizeof(float) * 11;
	offset = sizeof(float) * 3;
	// 2 - is equal to input slot
	// 1 - indicates there is only one buffer in that (i.e not interleaved)
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Cube, &stride, &offset);

	// select which color buffer to display
	stride = sizeof(float) * 11;
	offset = sizeof(float) * 6;
	// 2 - is equal to input slot
	// 1 - indicates there is only one buffer in that (i.e not interleaved)
	gpID3D11DeviceContext->IASetVertexBuffers(2, 1, &gpID3D11Buffer_VertexBuffer_Cube, &stride, &offset);

	// select which color buffer to display
	stride = sizeof(float) * 11;
	offset = sizeof(float) * 9;
	// 2 - is equal to input slot
	// 1 - indicates there is only one buffer in that (i.e not interleaved)
	gpID3D11DeviceContext->IASetVertexBuffers(3, 1, &gpID3D11Buffer_VertexBuffer_Cube, &stride, &offset);

	// bind texture as pixel shader resource
	// Note : This is anlogous to glBindTexture from opengl
	// This function pushes the pyramid texture to "Texture2D myTexture2D" in pixel shader
	// First paramter indicates that resource will go to 0th slot in Texture2D
	// Second parameter ask you how many members are there in third parameter because third paramter is array
	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_Cube_Texture);
	// bind sampler as pixel shader resource
	// Note : This is analogous to glUniform1i from opengl
	// This function pushes the sampler of pyramid texture to "SamplerState mySamplerState" in pixel shader
	// First paramter indicates that resource will go to 0th slot in SamplerState
	// Second parameter ask you how many members are there in third parameter because third paramter is array
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerState_Cube_Texture);

	// select grometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// translation is concerned with world matrix transformation
	XMMATRIX translationMatrix = XMMatrixIdentity();
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrixX = XMMatrixIdentity();
	XMMATRIX rotationMatrixY = XMMatrixIdentity();
	XMMATRIX rotationMatrixZ = XMMatrixIdentity();
	XMMATRIX scaleMatrix = XMMatrixIdentity();

	translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 6.0f);

	rotationMatrixX = XMMatrixRotationX(angleCube);
	rotationMatrixY = XMMatrixRotationY(angleCube);
	rotationMatrixZ = XMMatrixRotationZ(angleCube);
	rotationMatrix = rotationMatrixX * rotationMatrixY * rotationMatrixZ;

	scaleMatrix = XMMatrixScaling(0.75f, 0.75f, 0.75f);

	worldMatrix = scaleMatrix * rotationMatrix * translationMatrix;

	// load the data into constant buffer
	CBUFFER constantBuffer;
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;

	if (gbLight == true)
	{
		// light properties
		constantBuffer.la = XMVectorSet(lightAmbient[0], lightAmbient[1], lightAmbient[2], lightAmbient[3]);
		constantBuffer.ld = XMVectorSet(lightDiffuse[0], lightDiffuse[1], lightDiffuse[2], lightDiffuse[3]);
		constantBuffer.ls = XMVectorSet(lightSpecular[0], lightSpecular[1], lightSpecular[2], lightSpecular[3]);
		constantBuffer.light_position = XMVectorSet(lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

		// material properties
		constantBuffer.ka = XMVectorSet(materialAmbient[0], materialAmbient[1], materialAmbient[2], materialAmbient[3]);
		constantBuffer.kd = XMVectorSet(materialDiffuse[0], materialDiffuse[1], materialDiffuse[2], materialDiffuse[3]);
		constantBuffer.ks = XMVectorSet(materialSpecular[0], materialSpecular[1], materialSpecular[2], materialSpecular[3]);
		constantBuffer.material_shininess = materialShininess;

		constantBuffer.gLKeyPressedUniform = 1;
	}
	else
	{
		constantBuffer.gLKeyPressedUniform = 0;
	}

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	// draw vertex buffer to render target
	gpID3D11DeviceContext->Draw(6, 0);
	gpID3D11DeviceContext->Draw(6, 6);
	gpID3D11DeviceContext->Draw(6, 12);
	gpID3D11DeviceContext->Draw(6, 18);
	gpID3D11DeviceContext->Draw(6, 24);
	gpID3D11DeviceContext->Draw(6, 30);

	// END RENDERING FOR SPHERE

	// switch between front and back buffer
	gpIDXGISwapChain->Present(0, 0);
}

void update()
{
	angleCube = angleCube - 0.001f;
	if (angleCube < -360.0f)
	{
		angleCube = 0.0f;
	}
}

void uninitialize(void)
{
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}
	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}
	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer_Cube)
	{
		gpID3D11Buffer_VertexBuffer_Cube->Release();
		gpID3D11Buffer_VertexBuffer_Cube = NULL;
	}
	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}
	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}
	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "uninitialize() succeeded.\n");
		fprintf_s(gpFile, "Log file is successfully closed.\n");
		fclose(gpFile);
	}
}