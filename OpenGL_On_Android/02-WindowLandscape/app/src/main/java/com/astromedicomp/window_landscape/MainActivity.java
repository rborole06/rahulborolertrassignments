package com.astromedicomp.window_landscape;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window;	// for window class
import android.view.WindowManager;	// for window manager class
import android.content.pm.ActivityInfo;	// for activityinfo class
import android.widget.TextView;	// for textview class
import android.graphics.Color;	// for colors
import android.view.Gravity;	//

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

	// this is done to get rid of ActionBar
	this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	// this is done to make fullscreen
	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

	// force activity window to orientation landscape
	MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	// set background color of window
	this.getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));

	TextView myTextView = new TextView(this);
	myTextView.setText("Hello World");
	myTextView.setTextSize(60);
	myTextView.setTextColor(Color.GREEN);
	myTextView.setGravity(Gravity.CENTER);
        setContentView(myTextView);
    }

    @Override
    protected void onPause()
    {
	super.onPause();
    }

    @Override
    protected void onResume()
    {
	super.onResume();
    }
}
