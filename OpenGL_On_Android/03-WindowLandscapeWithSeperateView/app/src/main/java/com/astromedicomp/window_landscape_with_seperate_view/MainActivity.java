package com.astromedicomp.window_landscape_with_seperate_view;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window;	// for window class
import android.view.WindowManager;	// for windowmanager class
import android.content.pm.ActivityInfo;	// for activityinfo class
import android.graphics.Color;	// for color class
public class MainActivity extends Activity
{
    private MyView myView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

	// this is to get rid of ActionBar
	this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	// this id done to make fullscreen
	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

	// force activity window orientation to Landscape
	MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	// set background color for window
	this.getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));

	myView = new MyView(this);

	// set view as content view of activity
        setContentView(myView);
    }

    @Override
    protected void onPause()
    {
	super.onPause();
    }

    @Override
    protected void onResume()
    {
	super.onResume();
    }
}
