package com.astromedicomp.window_landscape_with_seperate_view;

import android.content.Context;	// for drawing context related
import android.graphics.Color;	// for color class
import android.widget.TextView;	// for textview class
import android.view.Gravity;	// for gravity class

public class MyView extends TextView
{
    MyView(Context context)
    {
	super(context);

	setText("Hello World With Seperate View");
	setTextSize(60);
	setTextColor(Color.GREEN);
	setGravity(Gravity.CENTER);
    }	
}