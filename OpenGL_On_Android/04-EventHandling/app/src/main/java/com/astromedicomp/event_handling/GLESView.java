package com.astromedicomp.event_handling;

import android.content.Context;	// for drawing context related
import javax.microedition.khronos.opengles.GL10;	// for opengles 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;	// for EGLConfig needed as param type EGLConfig
import android.view.MotionEvent;	// for motion event
import android.view.GestureDetector;	// for gesture detector
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.Gravity;	// for gravity class
import android.widget.TextView;	// for textview class
import android.graphics.Color;	// for color class

// a view for opengles3 graphics which also receives touch events
public class GLESView extends TextView implements OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;

		setText("Event Handling");
		setTextSize(60);
		setTextColor(Color.GREEN);
		setGravity(Gravity.CENTER);

		gestureDetector = new GestureDetector(context,this,null,false);	// this meand "handler" i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);	// this means 'handler' i.e who is going to handle
	}

	// Handling 'onTouchEvent' is the most important
	// Because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}

	// abstract method from onDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		setText("Double Tap");
		System.out.println("VDG: "+"Double Tap");
		return(true);
	}

	// abstract method from onDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		// do not write any code here because already written  'onDoubleTap'
		return(true);
	}

	// abstract method from onDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		setText("Single Tap");
		System.out.println("VDG: "+"Single Tap");
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here already written 'onSingleTapConfirmed'
		return(true);
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		setText("Long Press");
		System.out.println("VDG: "+"Long Press");
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		setText("Scroll");
		System.out.println("VDG: "+"Scroll");
		return(true);
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
}
