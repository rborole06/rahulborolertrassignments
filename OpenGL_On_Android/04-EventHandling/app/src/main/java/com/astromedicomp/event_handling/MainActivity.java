package com.astromedicomp.event_handling;

// default supplied packages by android sdk
import android.app.Activity;
import android.os.Bundle;

// later added packages
import android.view.Window;	// for window class
import android.view.WindowManager;	// for window manager class
import android.content.pm.ActivityInfo;	// for activity info class

public class MainActivity extends Activity
{
	private GLESView glesView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// this is done to get rid of ActionBar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// this is to make fullscreen
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// force activity window orientation to landscape
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		glesView = new GLESView(this);

		setContentView(glesView);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}
}