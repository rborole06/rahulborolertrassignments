package com.astromedicomp.first_opengles_window;

import android.content.Context;	// for drawing context related
import android.opengl.GLSurfaceView;	// for opengl surface view
import javax.microedition.khronos.opengles.GL10;	// for opengles 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;	// for EGLConfig needed as param type EGLConfig
import android.opengl.GLES30;	// for opengles 3.0
import android.view.MotionEvent;	// for motion event
import android.view.GestureDetector;	// for gesture detector
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// a view for opengles3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;

		// accordingle set EGLContext to current supported version of Opengl-ES
		setEGLContextClientVersion(3);

		// set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context,this,null,false);	// this meand "handler" i.e who is going to handle
		gestureDetector.setOnDoubleTapListener(this);	// this means 'handler' i.e who is going to handle
	}

	// Override method of GLSurfaceView.Renderer(Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// opengl es version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: "+version);
		initialize(gl);
	}

	// Override method off GLSurfaceView.Renderer(Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
	}

	// overriden method of GLSurfaceView.Renderer(Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	// Handling 'onTouchEvent' is the most important
	// Because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}

	// abstract method from onDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("VDG: "+"Double Tap");
		return(true);
	}

	// abstract method from onDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		// do not write any code here because already written  'onDoubleTap'
		return(true);
	}

	// abstract method from onDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("VDG: "+"Single Tap");
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here already written 'onSingleTapConfirmed'
		return(true);
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: "+"Long Press");
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: "+"Scroll");
		System.exit(0);
		return(true);
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	// abstract method from onGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// set background frame color
		GLES30.glClearColor(0.0f,0.0f,1.0f,0.0f);
	}

	private void resize(int width, int height)
	{
		// adjust the viewport based on geometry changes
		// such as screen rotation
		GLES30.glViewport(0, 0, width, height);
	}

	public void draw()
	{
		// draw background color
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
	}
}
