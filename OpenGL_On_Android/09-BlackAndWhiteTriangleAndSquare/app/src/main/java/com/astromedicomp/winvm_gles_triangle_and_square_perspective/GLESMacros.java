package com.astromedicomp.winvm_gles_triangle_and_square_perspective;

public class GLESMacros
{
	public static final int VDG_ATTRIBUTE_VERTEX = 0;
	public static final int VDG_ATTRIBUTE_COLOR = 1;
	public static final int VDG_ATTRIBUTE_NORMAL = 2;
	public static final int VDG_ATTRIBUTE_TEXTURE0 = 3;
}
