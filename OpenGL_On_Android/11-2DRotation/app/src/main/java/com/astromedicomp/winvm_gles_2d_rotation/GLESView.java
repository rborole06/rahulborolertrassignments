package com.astromedicomp.winvm_gles_2d_rotation;

import android.content.Context;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES30;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;	// for matrix match

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vao_triangle = new int[1];
	private int[] vao_square = new int[1];
	private int[] vbo_position = new int[1];
	private int[] vbo_color = new int[1];
	private int mvpUniform;

	private float angleTri = 0.0f;
	private float angleSquare = 0.0f;

	private float perspectiveProjectionMatrix[] = new float[16];

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// get opengles version
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version = "+glesVersion);

		// get glsl version
		String glslVersion = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL version = "+glslVersion);

		initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}

	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);	
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// *****************************************************************
		// VERTEX SHADER
		// *****************************************************************

		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec4 vColor;"+
		"uniform mat4 u_mvp_matrix;"+
		"out vec4 out_color;"+
		"void main(void)"+
		"{"+
		"gl_Position = u_mvp_matrix * vPosition;"+
		"out_color = vColor;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// *****************************************************************
		// FRAGMENT SHADER
		// *****************************************************************

		// create shader
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);

		// fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec4 out_color;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
		"FragColor = out_color;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// create shader program
		shaderProgramObject = GLES30.glCreateProgram();

		// attach vertex shader to shader program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);

		// attach fragment shader to shader program
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);

		// pre-link binding of shader program object with vertex shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

		// link the two shaders together to shader program object
		GLES30.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// get MVP uniform location
		mvpUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		// vertices, colors, shader attribs, vbo, vao_triangle, initializations
		final float triangleVertices[] = new float[]
		{
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f
		};

		final float colorVertices[] = new float[]
		{
			1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,0.0f,1.0f
		};

		final float squareVertices[] = new float[]
		{
			-1.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f,
			1.0f,1.0f,0.0f
		};

		// *****************************
		// VAO FOR TRIANGLE
		// *****************************

		// generate vao for triangle
		GLES30.glGenVertexArrays(1,vao_triangle,0);
		// bind vao to triangle
		GLES30.glBindVertexArray(vao_triangle[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************
		GLES30.glGenBuffers(1,vbo_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_position[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(triangleVertices);
		verticesBuffer.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,triangleVertices.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR COLOR
		// *****************************
		GLES30.glGenBuffers(1,vbo_color,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_color[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(colorVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(colorVertices);
		colorBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,colorVertices.length*4,colorBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_COLOR);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// unbind vao for triangle
		GLES30.glBindVertexArray(0);

		// *****************************
		// VAO FOR SQUARE
		// *****************************

		// generate vao for square
		GLES30.glGenVertexArrays(1,vao_square,0);
		// bind vao to square
		GLES30.glBindVertexArray(vao_square[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************
		GLES30.glGenBuffers(1,vbo_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_position[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(squareVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(squareVertices);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,squareVertices.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		GLES30.glVertexAttrib3f(GLESMacros.VDG_ATTRIBUTE_COLOR,0.3960f, 0.5764f, 0.9607f);

		// unbind vao for square
		GLES30.glBindVertexArray(0);

		// enable depth testing
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		// depth test to do
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		// cull back faces for better performance
		//GLES30.glEnable(GLES30.GL_CULL_FACE);

		// set background color
		GLES30.glClearColor(0.0f,0.0f,0.0f,1.0f);

		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width, int height)
	{
		GLES30.glViewport(0,0,width,height);

		// perspective projection 
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
	}

	public void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

		// use shader program
		GLES30.glUseProgram(shaderProgramObject);

		// opengles drawing

		// ****************************************************
		// TRIANGLE BLOCK
		// ****************************************************

		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];

		// set modelview & modelviewprojection matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		// translate model-view matrix by -1.5 in x axis direction and -6.0 in z axis direction 
		Matrix.translateM(modelViewMatrix,0,-1.5f,0.0f,-6.0f);

		// rotation
		Matrix.rotateM(rotationMatrix,0,angleTri,0.0f,1.0f,0.0f);

		// multiply modelview by rotation matrix
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		// multiply modelview and projection matrix to get modelviewprojection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		// pass above modelviewprojecttion matrix to vertex shader in "u_mvp_matrix" shader variable
		// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		// bind to triangle vao
		GLES30.glBindVertexArray(vao_triangle[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,0,3);	// 3(each with its x,y,z) vertices in triangleVertices array

		// unbind from triangle vao
		GLES30.glBindVertexArray(0);

		// ****************************************************
		// SQUARE BLOCK
		// ****************************************************

		modelViewMatrix = new float[16];
		modelViewProjectionMatrix = new float[16];
		rotationMatrix = new float[16];

		// set modelview & modelviewprojection matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		// translate model-view matrix by 1.5 in x axis direction and -6.0 in z axis direction 
		Matrix.translateM(modelViewMatrix,0,1.5f,0.0f,-6.0f);

		// rotation
		Matrix.rotateM(rotationMatrix,0,angleSquare,1.0f,0.0f,0.0f);

		// multiply model view matrix by rotation matrix
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		// multiply modelview and projection matrix to get modelviewprojection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		// pass above modelviewprojecttion matrix to vertex shader in "u_mvp_matrix" shader variable
		// whose position value we already calculated in inintWithFrame() by using glGetUniformLocation()
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		// bind to vao of square
		GLES30.glBindVertexArray(vao_square[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,0,4);	// 4(each with its x,y,z) vertices in squareVertices array

		// unbind from vao of square
		GLES30.glBindVertexArray(0);

		// unuse shader program
		GLES30.glUseProgram(0);

		update();

		// render/flush
		requestRender();
	}

	private void update()
	{
		angleTri = angleTri + 0.5f;
		if (angleTri >= 360)
		{
			angleTri = 0.0f;
		}

		angleSquare = angleSquare - 0.5f;
		if (angleSquare <= -360)
		{
			angleSquare = 0.0f;
		}
	}

	void uninitialize()
	{
		// destroy vao
		if(vao_triangle[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao_triangle,0);
			vao_triangle[0] = 0;
		}
		if(vao_square[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao_square,0);
			vao_square[0] = 0;
		}

		if(vbo_position[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_position,0);
			vbo_position[0] = 0;
		}
		if(vbo_color[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_color,0);
			vbo_color[0] = 0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,vertexShaderObject);

				// detach vertex shader from shader program object
				GLES30.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,fragmentShaderObject);

				GLES30.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if(shaderProgramObject != 0)
		{
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
