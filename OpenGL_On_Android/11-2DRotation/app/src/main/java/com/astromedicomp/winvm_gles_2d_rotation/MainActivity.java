package com.astromedicomp.winvm_gles_2d_rotation;

// default supplied packages by android sdk
import android.app.Activity;
import android.os.Bundle;

// later added packages
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;

public class MainActivity extends Activity
{
	private GLESView glesView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// this is done to get rid of ActionBar
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// this is done to make fullscreen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		glesView = new GLESView(this);

		setContentView(glesView);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}
}