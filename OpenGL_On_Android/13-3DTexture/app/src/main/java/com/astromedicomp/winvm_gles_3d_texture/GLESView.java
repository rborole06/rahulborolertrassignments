package com.astromedicomp.winvm_gles_3d_texture;

//  
import android.content.Context;	// for drawing context related
import android.opengl.GLSurfaceView;	// for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10;	// for opengles 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;	// for EGLConfig needed as param type EGLConfig
import android.opengl.GLES30;	// for opengles 3.0
import android.view.MotionEvent;	// for motion event
import android.view.GestureDetector;	// for gesture detector
import android.view.GestureDetector.OnGestureListener;	// onGestureListener
import android.view.GestureDetector.OnDoubleTapListener;	// for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;	// for matrix math

import android.graphics.BitmapFactory;	// texture factory
import android.graphics.Bitmap;	// for PNG image
import android.opengl.GLUtils;	// for texImage2D()

// A view for opengles graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vao_pyramid = new int[1];
	private int[] vbo_pyramid_position = new int[1];	
	private int[] vbo_pyramid_texture = new int[1];

	private int[] vao_cube = new int[1];
	private int[] vbo_cube_position = new int[1];
	private int[] vbo_cube_texture = new int[1];

	private int mvpUniform;
	private int texture0_sampler_uniform;

	private int[] texture_kundali = new int[1];
	private int[] texture_stone = new int[1];

	private float anglePyramid = 0.0f;
	private float angleCube = 0.0f;

	private float perspectiveProjectionMatrix[] = new float[16];	// 4x4 matrix

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		// accordingly set EGLContext to current supported version of opengles
		setEGLContextClientVersion(3);

		// set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	// overriden method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// get opengles version
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version = "+glesVersion);

		// get glsl version
		String glslVersion = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL version = "+glslVersion);

		initialize(gl);
	}

	// overriden method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
	}

	// overriden method of GLSurfaceView.Renderer( Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}

	// Handling 'onTouchEvent' is the most important
	// because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here because already written 'onSingleTapConfirmed'
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);	
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// *****************************************************************
		// VERTEX SHADER
		// *****************************************************************

		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec2 vTexture0_Coord;"+
		"out vec2 out_texture0_coord;"+
		"uniform mat4 u_mvp_matrix;"+
		"void main(void)"+
		"{"+
		"gl_Position = u_mvp_matrix * vPosition;"+
		"out_texture0_coord = vTexture0_Coord;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// *****************************************************************
		// FRAGMENT SHADER
		// *****************************************************************

		// create shader
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);

		// fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec2 out_texture0_coord;"+
		"uniform highp sampler2D u_texture0_sampler;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
		"FragColor = texture(u_texture0_sampler,out_texture0_coord);"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// create shader program
		shaderProgramObject = GLES30.glCreateProgram();

		// attach vertex shader to shader program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);

		// attach fragment shader to shader program
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);

		// pre-link binding of shader program object with vertex shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

		// pre-link binding of shader program object with texture shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

		// link the two shaders together to shader program object
		GLES30.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// get MVP uniform location
		mvpUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		// get texture sampler uniform location
		texture0_sampler_uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");

		// load textures
		texture_stone[0] = loadGLTexture(R.raw.stone);
		texture_kundali[0] = loadGLTexture(R.raw.vijay_kundali_horz_inverted);

		// vertices, colors, shader attribs, vbo, vao_pyramid, initializations
		// PYRAMID VERTICES
		final float pyramidVertices[] = new float[]
		{
			// FRONT FACE
			0.0f,1.0f,0.0f,	// apex
			-1.0f,-1.0f,1.0f,	// left-bottom
			1.0f,-1.0f,1.0f,	// right-bottom

			// RIGHT FACE
			0.0f,1.0f,0.0f,
			1.0f,-1.0f,1.0f,
			1.0f,-1.0f,-1.0f,

			// BACK FACE
			0.0f,1.0f,0.0f,
			1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,

			// LEFT FACE
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,1.0f
		};

		// pyramid texture coordinates
		final float pyramidTexcoords[] = new float[]
		{
			// FRONT FACE
			0.5f,1.0f,	// front-top
			0.0f,0.0f,	// front-left
			1.0f,0.0f,	// front-right

			// RIGHT FACE
			0.5f,1.0f,	// right-top
			1.0f,0.0f,	// right-left
			0.0f,0.0f,	// right-right

			// BACK FACE
			0.5f,1.0f,	// back-top
			1.0f,0.0f,	// back-left
			0.0f,0.0f,	// back-right

			// LEFT FACE
			0.5f,1.0f,	// left-top
			0.0f,0.0f,	// left-left
			1.0f,0.0f	// left-right
		};

		final float cubeVertices[] = new float[]
		{
			// TOP FACE
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			// BOTTOM FACE
			1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,

			// FRONT FACE
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// BACK FACE
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,

			// LEFT FACE
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,

			// RIGHT FACE
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
		};

		// cube color vertices
		final float cubeTexcoords[] = new float[]
		{
			// TOP FACE
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			// BOTTOM FACE
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			// FRONT FACE
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			// BACK FACE
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			// LEFT FACE
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			// RIGHT FACE
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
		};

		// *****************************
		// VAO FOR PYRAMID
		// *****************************

		// generate and bind vao for triangle
		GLES30.glGenVertexArrays(1,vao_pyramid,0);
		GLES30.glBindVertexArray(vao_pyramid[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************
		GLES30.glGenBuffers(1,vbo_pyramid_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_pyramid_position[0]);

		// *****************************************************************************************************************************************************
		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		// *****************************************************************************************************************************************************

		// allocate required size to buffer
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		// detect and arrange byte as per native style (little endian or big endian)
		byteBuffer.order(ByteOrder.nativeOrder());
		// assign data-type to contents of buffer
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		// put data in buffer
		verticesBuffer.put(pyramidVertices);
		// start from this position in buffer
		verticesBuffer.position(0);
		
		// creates and initializes a buffer object's data store
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,pyramidVertices.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);

		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR TEXTURE
		// *****************************
		GLES30.glGenBuffers(1,vbo_pyramid_texture,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_pyramid_texture[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(pyramidTexcoords.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(pyramidTexcoords);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,pyramidTexcoords.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0,2,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// unbind vao for triangle
		GLES30.glBindVertexArray(0);

		// *****************************
		// VAO FOR CUBE
		// *****************************

		// generate and bind vao for cube
		GLES30.glGenVertexArrays(1,vao_cube,0);
		GLES30.glBindVertexArray(vao_cube[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************
		GLES30.glGenBuffers(1,vbo_cube_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_cube_position[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(cubeVertices);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,cubeVertices.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);


		// *****************************
		// VBO FOR TEXTURE
		// *****************************
		GLES30.glGenBuffers(1,vbo_cube_texture,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_cube_texture[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(cubeTexcoords.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(cubeTexcoords);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,cubeTexcoords.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0,2,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// unbind vao for cube
		GLES30.glBindVertexArray(0);

		// enable depth testing
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		// depth test to do
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		// cull back faces for better performance
		//GLES30.glEnable(GLES30.GL_CULL_FACE);

		// set background color
		GLES30.glClearColor(0.0f,0.0f,0.0f,1.0f);

		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private int loadGLTexture(int imageFileResourceID)
	{
		// code
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;

		// read in the resource
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),imageFileResourceID,options);

		int[] texture = new int[1];

		// create a texture object to apply to model
		GLES30.glGenTextures(1,texture,0);

		// indicate that pixel rows are tightly packed (defaults to stride of 4 which is kind of only good for RGBA or FLOAT data types)
		GLES30.glPixelStorei(GLES30.GL_UNPACK_ALIGNMENT,1);

		// bind with the texture
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D,texture[0]);

		// setup filter and wrap modes for this texture object
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D,GLES30.GL_TEXTURE_MAG_FILTER,GLES30.GL_LINEAR);
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D,GLES30.GL_TEXTURE_MIN_FILTER,GLES30.GL_LINEAR_MIPMAP_LINEAR);

		// load bitmap into bound texture
		GLUtils.texImage2D(GLES30.GL_TEXTURE_2D,0,bitmap,0);

		// generate mipmap
		GLES30.glGenerateMipmap(GLES30.GL_TEXTURE_2D);

		return(texture[0]);
	}

	private void resize(int width, int height)
	{
		GLES30.glViewport(0,0,width,height);

		// perspective projection 
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
	}

	public void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

		// use shader program
		GLES30.glUseProgram(shaderProgramObject);

		// opengles drawing

		// ****************************************************
		// PYRAMID BLOCK
		// ****************************************************

		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];

		// set modelview & modelviewprojection matrix to identity
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		// rotation
		Matrix.rotateM(rotationMatrix,0,anglePyramid,0.0f,1.0f,0.0f);

		// translate model-view matrix by -1.5 in x axis direction and -6.0 in z axis direction 
		Matrix.translateM(modelViewMatrix,0,-1.5f,0.0f,-6.0f);

		// multiply modelview by rotation matrix
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		// multiply modelview and projection matrix to get modelviewprojection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		// pass above modelviewprojecttion matrix to vertex shader in "u_mvp_matrix" shader variable
		// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		// bind to pyramid vao
		GLES30.glBindVertexArray(vao_pyramid[0]);

		// bind with pyramid texture
		GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D,texture_stone[0]);
		// 0th sampler enable (as we have only 1 texture sampler in fragment shader)
		GLES30.glUniform1i(texture0_sampler_uniform,0);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,0,12);	// 3(each with its x,y,z) vertices in pyramidVertices array

		// unbind from pyramid vao
		GLES30.glBindVertexArray(0);

		// ****************************************************
		// CUBE BLOCK
		// ****************************************************

		modelViewMatrix = new float[16];
		modelViewProjectionMatrix = new float[16];
		rotationMatrix = new float[16];
		float scaleMatrix[] = new float[16];

		// set rotation, scale, modelview & modelviewprojection matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);

		// scale model-view matrix by 1.5 in x axis direction and -6.0 in z axis direction 
		Matrix.scaleM(scaleMatrix,0,0.75f,0.75f,0.75f);
		// multiply model view matrix by scale matrix
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);

		// rotation
		Matrix.rotateM(rotationMatrix,0,modelViewMatrix,0,angleCube,1.0f,1.0f,1.0f);

		// translate model-view matrix by 1.5 in x axis direction and -6.0 in z axis direction 
		Matrix.translateM(modelViewMatrix,0,1.5f,0.0f,-6.0f);

		// multiply model view matrix by rotation matrix
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		// multiply modelview and projection matrix to get modelviewprojection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		// pass above modelviewprojecttion matrix to vertex shader in "u_mvp_matrix" shader variable
		// whose position value we already calculated in inintWithFrame() by using glGetUniformLocation()
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		// bind to vao of square
		GLES30.glBindVertexArray(vao_cube[0]);

		// bind with cube texture
		GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D,texture_kundali[0]);
		// 0th sampler enable (as we have only 1 texture sampler in fragment shader)
		GLES30.glUniform1i(texture0_sampler_uniform,0);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		// actually 2 triangles make 1 square, so there should be 6 vertices
		// but as 2 triangles while making square meet each other at diagonal
		// 2 of 6 vertices are common to both triangles and hence 6-2=4
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,0,4);	// 4(each with its x,y,z) vertices in cubeVertices array
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,4,4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,8,4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,12,4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,16,4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,20,4);

		// unbind from vao of square
		GLES30.glBindVertexArray(0);

		// unuse shader program
		GLES30.glUseProgram(0);

		update();

		// render/flush
		requestRender();
	}

	private void update()
	{
		anglePyramid = anglePyramid + 0.5f;
		if (anglePyramid >= 360)
		{
			anglePyramid = 0.0f;
		}

		angleCube = angleCube - 0.5f;
		if (angleCube <= -360)
		{
			angleCube = 0.0f;
		}
	}

	void uninitialize()
	{
		// PYRAMID
		// destroy vao
		if(vao_pyramid[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao_pyramid,0);
			vao_pyramid[0] = 0;
		}
		// destroy position vbo
		if(vbo_pyramid_position[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_pyramid_position,0);
			vbo_pyramid_position[0] = 0;
		}
		// destroy texture vbo
		if(vbo_pyramid_texture[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_pyramid_texture,0);
			vbo_pyramid_texture[0] = 0;
		}

		// CUBE
		// destroy vao
		if(vao_cube[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao_cube,0);
			vao_cube[0] = 0;
		}
		// destroy position vbo
		if(vbo_cube_position[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_cube_position,0);
			vbo_cube_position[0] = 0;
		}
		// destroy texture vbo
		if(vbo_cube_texture[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_cube_texture,0);
			vbo_cube_texture[0] = 0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,vertexShaderObject);

				// detach vertex shader from shader program object
				GLES30.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,fragmentShaderObject);

				GLES30.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if(shaderProgramObject != 0)
		{
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
		
		// delete texture objects
		if(texture_stone[0] != 0)
		{
			GLES30.glDeleteTextures(1,texture_stone,0);
			texture_stone[0] = 0;
		}
		if(texture_kundali[0] != 0)
		{
			GLES30.glDeleteTextures(1,texture_kundali,0);
			texture_kundali[0] = 0;
		}
	}
}
