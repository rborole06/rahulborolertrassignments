package com.astromedicomp.winvm_gles_twik_smiley_texture;

public class GLESMacros
{
	public static final int VDG_ATTRIBUTE_VERTEX = 0;
	public static final int VDG_ATTRIBUTE_COLOR = 1;
	public static final int VDG_ATTRIBUTE_NORMAL = 2;
	public static final int VDG_ATTRIBUTE_TEXTURE0 = 3;
}
