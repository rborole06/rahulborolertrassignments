package com.astromedicomp.winvm_gles_checkerboard_texture;

import android.content.Context;	// for drawing context related
import android.opengl.GLSurfaceView;	// for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10;	// for opengles 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;	// for EGLConfig needed as param type EGLConfig
import android.opengl.GLES30;	// for opengles 3.0
import android.view.MotionEvent;	// for motion event
import android.view.GestureDetector;	// for gesture detector
import android.view.GestureDetector.OnGestureListener;	// onGestureListener
import android.view.GestureDetector.OnDoubleTapListener;	// for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;	// for matrix math

import android.graphics.BitmapFactory;	// texture factory
import android.graphics.Bitmap;	// for PNG image
import android.opengl.GLUtils;	// for texImage2D()

// A view for opengles graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vao = new int[1];
	private int[] vbo_position = new int[1];	
	private int[] vbo_texture = new int[1];

	private int mvpUniform;
	private int texture0_sampler_uniform;

	private float perspectiveProjectionMatrix[] = new float[16];	// 4x4 matrix

	private int checkImageWidth = 64;
	private int checkImageHeight = 64;

	int[] Image = new int[checkImageHeight * checkImageWidth];
	//private int texName;

	int[] texture = new int[1];

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		// accordingly set EGLContext to current supported version of opengles
		setEGLContextClientVersion(3);

		// set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	// overriden method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// get opengles version
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version = "+glesVersion);

		// get glsl version
		String glslVersion = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL version = "+glslVersion);

		initialize(gl);
	}

	// overriden method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
	}

	// overriden method of GLSurfaceView.Renderer( Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}

	// Handling 'onTouchEvent' is the most important
	// because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here because already written 'onSingleTapConfirmed'
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);	
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// *****************************************************************
		// VERTEX SHADER
		// *****************************************************************

		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec2 vTexture0_Coord;"+
		"out vec2 out_texture0_coord;"+
		"uniform mat4 u_mvp_matrix;"+
		"void main(void)"+
		"{"+
		"gl_Position = u_mvp_matrix * vPosition;"+
		"out_texture0_coord = vTexture0_Coord;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// *****************************************************************
		// FRAGMENT SHADER
		// *****************************************************************

		// create shader
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);

		// fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec2 out_texture0_coord;"+
		"uniform highp sampler2D u_texture0_sampler;"+
		"out vec4 FragColor;"+
		"void main(void)"+
		"{"+
		"FragColor = texture(u_texture0_sampler,out_texture0_coord);"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// create shader program
		shaderProgramObject = GLES30.glCreateProgram();

		// attach vertex shader to shader program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);

		// attach fragment shader to shader program
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);

		// pre-link binding of shader program object with vertex shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

		// pre-link binding of shader program object with texture shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

		// link the two shaders together to shader program object
		GLES30.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// get MVP uniform location
		mvpUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		// get texture sampler uniform location
		texture0_sampler_uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");

		// load textures
		loadGLTexture();

		// vertices, colors, shader attribs, vbo, vao_pyramid, initializations

		final float quadVertices[] = new float[]
		{
			-2.0f,-1.0f,0.0f,
			-2.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f,

			1.0f,-1.0f,0.0f,
			1.0f,1.0f,0.0f,
			2.41421f,1.0f,-1.41421f,
			2.41421f,-1.0f,-1.41421f,
		};

		final float quadTexcoords[] = new float[]
		{
			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f,

			0.0f,0.0f,
			0.0f,1.0f,
			1.0f,1.0f,
			1.0f,0.0f,
		};

		// *****************************
		// VAO FOR QUAD
		// *****************************

		// generate and bind vao for quad
		GLES30.glGenVertexArrays(1,vao,0);
		GLES30.glBindVertexArray(vao[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************
		GLES30.glGenBuffers(1,vbo_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_position[0]);

		// *****************************************************************************************************************************************************
		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		// *****************************************************************************************************************************************************

		// allocate required size to buffer
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(quadVertices.length * 4);
		// detect and arrange byte as per native style (little endian or big endian)
		byteBuffer.order(ByteOrder.nativeOrder());
		// assign data-type to contents of buffer
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		// put data in buffer
		verticesBuffer.put(quadVertices);
		// start from this position in buffer
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,quadVertices.length * 4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR TEXTURE
		// *****************************
		GLES30.glGenBuffers(1,vbo_texture,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_texture[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(quadTexcoords.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(quadTexcoords);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,quadTexcoords.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0,2,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// unbind vao for quad
		GLES30.glBindVertexArray(0);

		// enable depth testing
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		// depth test to do
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		// cull back faces for better performance
		//GLES30.glEnable(GLES30.GL_CULL_FACE);

		// set background color
		GLES30.glClearColor(0.0f,0.0f,0.0f,1.0f);

		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	public int MakeCheckImage()
	{
		// code
		int i, j, c;
		for (i = 0; i < checkImageHeight; i++)
		{
			for (j = 0; j < checkImageWidth; j++)
			{
				boolean x = (((i & 0x8) == 0) ^ ((j & 0x8) == 0));
				if(x == true)
				{
					c = 255;
				} else {
					c = 0;
				}
				int r = c;
				int g = c;
				int b = c;
				int a = 255;
				Image[i*checkImageWidth+j] = (a << 24) | (r << 16) | (g << 8) | b;
			}
		}
		return 1;
	}

	private int loadGLTexture()
	{
		// create checkerboard texture using procedural method
		MakeCheckImage();

		// code
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;	// dont do any manual scaling for texture image

		// read in the resource
		Bitmap bitmap = Bitmap.createBitmap(Image, 0, checkImageWidth, checkImageWidth, checkImageHeight, Bitmap.Config.ARGB_8888);

		// create a texture object to apply to model
		GLES30.glGenTextures(1,texture,0);

		// indicate that pixel rows are tightly packed (defaults to stride of 4 which is kind of only good for RGBA or FLOAT data types)
		GLES30.glPixelStorei(GLES30.GL_UNPACK_ALIGNMENT,1);

		// bind with the texture
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D,texture[0]);

		// setup filter and wrap modes for this texture object
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D,GLES30.GL_TEXTURE_MAG_FILTER,GLES30.GL_LINEAR);
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D,GLES30.GL_TEXTURE_MIN_FILTER,GLES30.GL_LINEAR_MIPMAP_LINEAR);

		// load bitmap into bound texture
		GLUtils.texImage2D(GLES30.GL_TEXTURE_2D,0,bitmap,0);

		// generate mipmap
		GLES30.glGenerateMipmap(GLES30.GL_TEXTURE_2D);

		return(texture[0]);
	}

	private void resize(int width, int height)
	{
		GLES30.glViewport(0,0,width,height);

		// perspective projection 
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,60.0f,(float)width/(float)height,1.0f,30.0f);
	}

	public void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

		// use shader program
		GLES30.glUseProgram(shaderProgramObject);

		// opengles drawing

		// ****************************************************
		// 	QUAD BLOCK
		// ****************************************************

		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];

		// set modelview & modelviewprojection matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		// translate model-view matrix by -3.6 in z axis direction
		Matrix.translateM(modelViewMatrix,0,0.0f,0.0f,-3.6f);

		// multiply modelview and projection matrix to get modelviewprojection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		// pass above modelviewprojecttion matrix to vertex shader in "u_mvp_matrix" shader variable
		// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		// bind to quad vao
		GLES30.glBindVertexArray(vao[0]);

		// bind with quad texture
		GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D,texture[0]);
		// 0th sampler enable (as we have only 1 texture sampler in fragment shader)
		GLES30.glUniform1i(texture0_sampler_uniform,0);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,0,4);	// 3(each with its x,y,z) vertices in quadVertices array
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,4,4);	// 3(each with its x,y,z) vertices in quadVertices array

		// unbind from quad vao
		GLES30.glBindVertexArray(0);

		// unuse shader program
		GLES30.glUseProgram(0);

		// render/flush
		requestRender();
	}

	void uninitialize()
	{
		// PYRAMID
		// destroy vao
		if(vao[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao,0);
			vao[0] = 0;
		}
		// destroy position vbo
		if(vbo_position[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_position,0);
			vbo_position[0] = 0;
		}
		// destroy texture vbo
		if(vbo_texture[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_texture,0);
			vbo_texture[0] = 0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,vertexShaderObject);

				// detach vertex shader from shader program object
				GLES30.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,fragmentShaderObject);

				GLES30.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if(shaderProgramObject != 0)
		{
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
		
		// delete texture objects
		if(texture[0] != 0)
		{
			GLES30.glDeleteTextures(1,texture,0);
			texture[0] = 0;
		}
	}
}
