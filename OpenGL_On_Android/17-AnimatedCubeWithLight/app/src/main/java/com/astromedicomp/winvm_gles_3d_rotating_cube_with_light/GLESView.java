package com.astromedicomp.winvm_gles_3d_rotating_cube_with_light;

//  
import android.content.Context;	// for drawing context related
import android.opengl.GLSurfaceView;	// for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10;	// for opengles 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;	// for EGLConfig needed as param type EGLConfig
import android.opengl.GLES30;	// for opengles 3.0
import android.view.MotionEvent;	// for motion event
import android.view.GestureDetector;	// for gesture detector
import android.view.GestureDetector.OnGestureListener;	// onGestureListener
import android.view.GestureDetector.OnDoubleTapListener;	// for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;	// for matrix math

// A view for opengles graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vao_cube = new int[1];
	private int[] vbo_cube_position = new int[1];	
	private int[] vbo_cube_normal = new int[1];

	private int modelViewMatrixUniform, projectionMatrixUniform;
	private int ldUniform, kdUniform, lightPositionUniform;
	private int doubleTapUniform;

	private float perspectiveProjectionMatrix[] = new float[16];	// 4x4 matrix

	private float angleCube = 0.0f;

	private int singleTap;	// for animation
	private int doubleTap;	// for lights

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		// accordingly set EGLContext to current supported version of opengles
		setEGLContextClientVersion(3);

		// set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	// overriden method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// get opengles version
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version = "+glesVersion);

		// get glsl version
		String glslVersion = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL version = "+glslVersion);

		initialize(gl);
	}

	// overriden method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
	}

	// overriden method of GLSurfaceView.Renderer( Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}

	// Handling 'onTouchEvent' is the most important
	// because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;
		if(singleTap > 1)
			singleTap = 0;
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here because already written 'onSingleTapConfirmed'
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);	
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// *****************************************************************
		// VERTEX SHADER
		// *****************************************************************

		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform mediump int u_double_tap;"+
		"uniform vec3 u_Ld;"+
		"uniform vec3 u_Kd;"+
		"uniform vec4 u_light_position;"+
		"out vec3 diffuse_light;"+
		"void main(void)"+
		"{"+
		"if(u_double_tap == 1)"+
		"{"+
		"vec4 eyeCoordinates = u_model_view_matrix * vPosition;"+
		"vec3 tnorm = normalize(mat3(u_model_view_matrix) * vNormal);"+
		"vec3 s = normalize(vec3(u_light_position - eyeCoordinates));"+
		"diffuse_light = u_Ld * u_Kd * max(dot(s,tnorm),0.0);"+
		"}"+
		"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// *****************************************************************
		// FRAGMENT SHADER
		// *****************************************************************

		// create shader
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);

		// fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 diffuse_light;"+
		"out vec4 FragColor;"+
		"uniform int u_double_tap;"+
		"void main(void)"+
		"{"+
		"vec4 color;"+
		"if(u_double_tap == 1)"+
		"{"+
		"color = vec4(diffuse_light,1.0);"+
		"}"+
		"else"+
		"{"+
		"color = vec4(1.0,1.0,1.0,1.0);"+
		"}"+
		"FragColor = color;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// create shader program
		shaderProgramObject = GLES30.glCreateProgram();

		// attach vertex shader to shader program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);

		// attach fragment shader to shader program
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);

		// pre-link binding of shader program object with vertex shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

		// pre-link binding of shader program object with normal shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

		// link the two shaders together to shader program object
		GLES30.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// get uniform location
		modelViewMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_model_view_matrix");
		projectionMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");
		doubleTapUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_double_tap");
		ldUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ld");
		kdUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Kd");
		lightPositionUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_light_position");

		// vertices, colors, shader attribs, vbo, vao_pyramid, initializations
		// CUBE VERTICES
		final float cubeVertices[] = new float[]
		{
			// top surface
			1.0f,1.0f,-1.0f,	// top right
			-1.0f,1.0f,-1.0f,	// top-left
			-1.0f,1.0f,1.0f,	// bottom-left
			1.0f,1.0f,1.0f,		// bottom-right

			// bottom surface
			1.0f,-1.0f,1.0f,
			-1.0f,-1.0f,1.0f,
			-1.0f,-1.0f,-1.0f,
			1.0f,-1.0f,-1.0f,

			// front surface
			1.0f,1.0f,1.0f,
			-1.0f,1.0f,1.0f,
			-1.0f,-1.0f,1.0f,
			1.0f,-1.0f,1.0f,

			// back surface
			1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,1.0f,-1.0f,
			1.0f,1.0f,-1.0f,

			// left surface
			-1.0f,1.0f,1.0f,
			-1.0f,1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,1.0f,

			// right surface
			1.0f,1.0f,-1.0f,
			1.0f,1.0f,1.0f,
			1.0f,-1.0f,1.0f,
			1.0f,-1.0f,-1.0f,
		};

		// if above -1.0f or +1.0f values make cube much larger than pyramid
		// then follow the code in following loop which will convert all 1s and -1s to -0.75 and 0.75
		for(int i=0;i<72;i++)
		{
			if(cubeVertices[i] < 0.0f)
				cubeVertices[i] = cubeVertices[i]+0.25f;
			else if(cubeVertices[i] > 0.0f)
				cubeVertices[i] = cubeVertices[i]-0.25f;
			else
				cubeVertices[i] = cubeVertices[i];
		}

		float cubeNormals[] = new float[]
		{
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f,

			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f,
			0.0f,-1.0f,0.0f,

			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f,

			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,
			0.0f,0.0f,-1.0f,

			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f,

			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
		};

		// *****************************
		// VAO FOR QUAD
		// *****************************

		// generate and bind vao for cube
		GLES30.glGenVertexArrays(1,vao_cube,0);
		GLES30.glBindVertexArray(vao_cube[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************
		GLES30.glGenBuffers(1,vbo_cube_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_cube_position[0]);

		// *****************************************************************************************************************************************************
		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		// *****************************************************************************************************************************************************

		// allocate required size to buffer
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		// detect and arrange byte as per native style (little endian or big endian)
		byteBuffer.order(ByteOrder.nativeOrder());
		// assign data-type to contents of buffer
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		// put data in buffer
		verticesBuffer.put(cubeVertices);
		// start from this position in buffer
		verticesBuffer.position(0);
		
		// creates and initializes a buffer object's data store
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,cubeVertices.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);

		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR NORMAL
		// *****************************
		GLES30.glGenBuffers(1,vbo_cube_normal,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_cube_normal[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(cubeNormals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(cubeNormals);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,cubeNormals.length*4,verticesBuffer,GLES30.GL_DYNAMIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// unbind vao for cube
		GLES30.glBindVertexArray(0);

		// enable depth testing
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		// depth test to do
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		// cull back faces for better performance
		//GLES30.glEnable(GLES30.GL_CULL_FACE);

		// set background color
		GLES30.glClearColor(0.0f,0.0f,0.0f,1.0f);

		// initialization
		singleTap = 0;
		doubleTap = 0;

		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width, int height)
	{
		GLES30.glViewport(0,0,width,height);

		// perspective projection 
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
	}

	public void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

		// use shader program
		GLES30.glUseProgram(shaderProgramObject);

		if(doubleTap == 1)
		{
			GLES30.glUniform1i(doubleTapUniform,1);
			
			// setting light properties
			GLES30.glUniform3f(ldUniform,1.0f,1.0f,1.0f);
			// diffuse intensity of light
			GLES30.glUniform3f(kdUniform,0.5f,0.5f,0.5f);
			// diffuse reflectivity of material
			float[] lightPosition = {0.0f,0.0f,2.0f,1.0f};
			GLES30.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
		}
		else
		{
			GLES30.glUniform1i(doubleTapUniform, 0);
		}

		// opengles drawing
		float modelMatrix[] = new float[16];
		float modelViewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];

		// ****************************************************
		// CUBE BLOCK
		// ****************************************************

		// set modelview & modelviewprojection matrix to identity
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		// apply z axis transition to go deep into the screen by -5.0
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-5.0f);

		Matrix.setRotateM(rotationMatrix,0,angleCube,1.0f,1.0f,1.0f);	// all axis rotation by angleCube angle

		// multiply modelview and rotation matrix to get modelviewprojection matrix
		Matrix.multiplyMM(modelViewMatrix,0,modelMatrix,0,rotationMatrix,0);

		// pass above modelviewprojecttion matrix to vertex shader in "u_model_view_matrix" shader variable
		// whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
		GLES30.glUniformMatrix4fv(modelViewMatrixUniform,1,false,modelViewMatrix,0);

		// pass projection matrix to the vertex shader in 'u_projection_matrix' shader variable
		GLES30.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

		// bind to cube vao
		GLES30.glBindVertexArray(vao_cube[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,0,4);	// 3(each with its x,y,z) vertices in pyramidVertices array
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,4,4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,8,4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,12,4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,16,4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,20,4);

		// unbind from cube vao
		GLES30.glBindVertexArray(0);

		// unuse shader program
		GLES30.glUseProgram(0);

		// keep rotating
		if(singleTap == 1)
		{
			angleCube = angleCube - 0.75f;
		}

		// render/flush
		requestRender();
	}

	void uninitialize()
	{
		// CUBE
		// destroy vao
		if(vao_cube[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao_cube,0);
			vao_cube[0] = 0;
		}
		// destroy position vbo
		if(vbo_cube_position[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_cube_position,0);
			vbo_cube_position[0] = 0;
		}
		// destroy texture vbo
		if(vbo_cube_normal[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_cube_normal,0);
			vbo_cube_normal[0] = 0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,vertexShaderObject);

				// detach vertex shader from shader program object
				GLES30.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,fragmentShaderObject);

				GLES30.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if(shaderProgramObject != 0)
		{
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
