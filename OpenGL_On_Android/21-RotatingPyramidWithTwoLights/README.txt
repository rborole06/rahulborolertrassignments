
1 - This application shows how to render rotating pyramid with lighting using perspective projection in programmable function pipeline
2 - There will be  lights on pyramid, one from left side and another one from right side
3 - Using Phong Per Fragment Lighting Model
4 - To start animation, single tap on screen
5 - To enable lighting, double tap on screen