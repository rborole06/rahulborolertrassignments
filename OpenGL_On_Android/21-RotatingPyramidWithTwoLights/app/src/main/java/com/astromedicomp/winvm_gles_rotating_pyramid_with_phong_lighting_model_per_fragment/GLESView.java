package com.astromedicomp.winvm_gles_rotating_pyramid_with_phong_lighting_model_per_fragment;

//  
import android.content.Context;	// for drawing context related
import android.opengl.GLSurfaceView;	// for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10;	// for opengles 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;	// for EGLConfig needed as param type EGLConfig
import android.opengl.GLES30;	// for opengles 3.0
import android.view.MotionEvent;	// for motion event
import android.view.GestureDetector;	// for gesture detector
import android.view.GestureDetector.OnGestureListener;	// onGestureListener
import android.view.GestureDetector.OnDoubleTapListener;	// for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix;	// for matrix math

// A view for opengles graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vao_pyramid = new int[1];
	private int[] vbo_pyramid_position = new int[1];	
	private int[] vbo_pyramid_normal = new int[1];

	private float light_1_ambient[] = {0.0f,0.0f,0.0f,0.0f};
	private float light_1_diffuse[] = {1.0f,0.0f,0.0f,0.0f};
	private float light_1_specular[] = {1.0f,0.0f,0.0f,0.0f};
	private float light_1_position[] = {2.0f,0.8f,1.0f,0.0f};

	private float light_2_ambient[] = {0.0f,0.0f,0.0f,0.0f};
	private float light_2_diffuse[] = {0.0f,0.0f,1.0f,0.0f};
	private float light_2_specular[] = {0.0f,0.0f,1.0f,0.0f};
	private float light_2_position[] = {-2.0f,0.8f,1.0f,0.0f};

	private float material_ambient[] = {0.0f,0.0f,0.0f,0.0f};
	private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
	private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
	private float material_shininess = 50.0f;

	private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;

	private int la_1_Uniform, ld_1_Uniform, ls_1_Uniform, light_1_PositionUniform;
	private int la_2_Uniform, ld_2_Uniform, ls_2_Uniform, light_2_PositionUniform;
	private int kaUniform, kdUniform, ksUniform, materialShininessUniform;

	private int doubleTapUniform;

	private float perspectiveProjectionMatrix[] = new float[16];	// 4x4 matrix

	private float anglePyramid = 0.0f;
	private int singleTap;	// for animation
	private int doubleTap;	// for lights

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		// accordingly set EGLContext to current supported version of opengles
		setEGLContextClientVersion(3);

		// set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	// overriden method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// get opengles version
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version = "+glesVersion);

		// get glsl version
		String glslVersion = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL version = "+glslVersion);

		initialize(gl);
	}

	// overriden method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
	}

	// overriden method of GLSurfaceView.Renderer( Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
		
	}

	// Handling 'onTouchEvent' is the most important
	// because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;
		if(singleTap > 1)
			singleTap = 0;
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here because already written 'onSingleTapConfirmed'
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);	
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// *****************************************************************
		// VERTEX SHADER
		// *****************************************************************

		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform mediump int u_double_tap;"+
		"uniform vec4 u_light_1_position;"+
		"uniform vec4 u_light_2_position;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_1_direction;"+
		"out vec3 light_2_direction;"+
		"out vec3 viewer_vector;"+
		"void main(void)"+
		"{"+
		"if(u_double_tap == 1)"+
		"{"+
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"+
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
		"light_1_direction = vec3(u_light_1_position) - eye_coordinates.xyz;"+
		"light_2_direction = vec3(u_light_2_position) - eye_coordinates.xyz;"+
		"viewer_vector = -eye_coordinates.xyz;"+
		"}"+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// *****************************************************************
		// FRAGMENT SHADER
		// *****************************************************************

		// create shader
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);

		// fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 transformed_normals;"+
		"in vec3 light_1_direction;"+
		"in vec3 light_2_direction;"+
		"in vec3 viewer_vector;"+
		"out vec4 FragColor;"+
		"uniform vec3 u_La_1;"+
		"uniform vec3 u_Ld_1;"+
		"uniform vec3 u_Ls_1;"+
		"uniform vec3 u_La_2;"+
		"uniform vec3 u_Ld_2;"+
		"uniform vec3 u_Ls_2;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"uniform int u_double_tap;"+
		"void main(void)"+
		"{"+
		"vec3 phong_ads_color;"+
		"if(u_double_tap == 1)"+
		"{"+
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
		"vec3 normalized_light_1_direction = normalize(light_1_direction);"+
		"vec3 normalized_light_2_direction = normalize(light_2_direction);"+
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
		"vec3 ambient_1 = u_La_1 * u_Ka;"+
		"vec3 ambient_2 = u_La_2 * u_Ka;"+
		"float tn1_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_1_direction),0.0);"+
		"float tn2_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_2_direction),0.0);"+
		"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;"+
		"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;"+
		"vec3 reflection_vector_1 = reflect(-normalized_light_1_direction,normalized_transformed_normals);"+
		"vec3 reflection_vector_2 = reflect(-normalized_light_2_direction,normalized_transformed_normals);"+
		"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,normalized_viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,normalized_viewer_vector),0.0),u_material_shininess);"+
		"phong_ads_color = (ambient_1+ambient_2) + (diffuse_1+diffuse_2) + (specular_1+specular_2);"+
		"}"+
		"else"+
		"{"+
		"phong_ads_color = vec3(1.0,1.0,1.0);"+
		"}"+
		"FragColor = vec4(phong_ads_color, 1.0);"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// create shader program
		shaderProgramObject = GLES30.glCreateProgram();

		// attach vertex shader to shader program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);

		// attach fragment shader to shader program
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);

		// pre-link binding of shader program object with vertex shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

		// pre-link binding of shader program object with normal shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

		// link the two shaders together to shader program object
		GLES30.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// get uniform location
		modelMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_model_matrix");
		viewMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_view_matrix");
		projectionMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");

		doubleTapUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_double_tap");

		la_1_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_La_1");
		ld_1_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ld_1");
		ls_1_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ls_1");
		light_1_PositionUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_light_1_position");

		la_2_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_La_2");
		ld_2_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ld_2");
		ls_2_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ls_2");
		light_2_PositionUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_light_2_position");

		kaUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ka");
		kdUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Kd");
		ksUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ks");
		materialShininessUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_material_shininess");

		// *** vertices, colors, shader attribs, vbo, vao_pyramid, initializations ***		
		final float pyramidVertices[] =
		{
			// FRONT FACE
			0.0f,1.0f,0.0f,	// apex
			-1.0f,-1.0f,1.0f,	// left-bottom
			1.0f,-1.0f,1.0f,	// right-bottom

			// RIGHT FACE
			0.0f,1.0f,0.0f,
			1.0f,-1.0f,1.0f,
			1.0f,-1.0f,-1.0f,

			// BACK FACE
			0.0f,1.0f,0.0f,
			1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,

			// LEFT FACE
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,1.0f
		};

		final float pyramidNormals[] =
		{
			0.0f,0.447214f,0.894427f,	// normal for front face
			0.0f,0.447214f,0.894427f,
			0.0f,0.447214f,0.894427f,

			0.894427f,0.447214f,0.0f,	// normal for right face
			0.894427f,0.447214f,0.0f,
			0.894427f,0.447214f,0.0f,

			0.0f,0.447214f,-0.894427f,	// normal for back face
			0.0f,0.447214f,-0.894427f,
			0.0f,0.447214f,-0.894427f,

			-0.894427f,0.447214f,0.0f,	// normal for left face
			-0.894427f,0.447214f,0.0f,
			-0.894427f,0.447214f,0.0f
		};
		// *****************************
		// VAO FOR PYRAMID
		// *****************************

		// generate and bind vao for pyramid
		GLES30.glGenVertexArrays(1,vao_pyramid,0);
		GLES30.glBindVertexArray(vao_pyramid[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************

		GLES30.glGenBuffers(1,vbo_pyramid_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_pyramid_position[0]);

		// *****************************************************************************************************************************************************
		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		// *****************************************************************************************************************************************************

		// allocate required size to buffer
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		// detect and arrange byte as per native style (little endian or big endian)
		byteBuffer.order(ByteOrder.nativeOrder());
		// assign data-type to contents of buffer
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		// put data in buffer
		verticesBuffer.put(pyramidVertices);
		// start from this position in buffer
		verticesBuffer.position(0);
		
		// creates and initializes a buffer object's data store
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,pyramidVertices.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);

		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR NORMAL
		// *****************************
		GLES30.glGenBuffers(1,vbo_pyramid_normal,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_pyramid_normal[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(pyramidNormals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(pyramidNormals);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,pyramidNormals.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		// unbind vao for pyramid
		GLES30.glBindVertexArray(0);

		// enable depth testing
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		// depth test to do
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		// cull back faces for better performance
		GLES30.glEnable(GLES30.GL_CULL_FACE);

		// set background color
		GLES30.glClearColor(0.0f,0.0f,0.0f,1.0f);

		// initialization
		doubleTap = 0;

		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width, int height)
	{
		GLES30.glViewport(0,0,width,height);

		// perspective projection 
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
	}

	public void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

		// use shader program
		GLES30.glUseProgram(shaderProgramObject);

		if(doubleTap == 1)
		{
			GLES30.glUniform1i(doubleTapUniform,1);
			
			// setting light properties
			GLES30.glUniform3fv(la_1_Uniform,1,light_1_ambient,0);
			GLES30.glUniform3fv(ld_1_Uniform,1,light_1_diffuse,0);
			GLES30.glUniform3fv(ls_1_Uniform,1,light_1_specular,0);
			GLES30.glUniform4fv(light_1_PositionUniform,1,light_1_position,0);

			GLES30.glUniform3fv(la_2_Uniform,1,light_2_ambient,0);
			GLES30.glUniform3fv(ld_2_Uniform,1,light_2_diffuse,0);
			GLES30.glUniform3fv(ls_2_Uniform,1,light_2_specular,0);
			GLES30.glUniform4fv(light_2_PositionUniform,1,light_2_position,0);

			// setting material properties
			GLES30.glUniform3fv(kaUniform,1,material_ambient,0);
			GLES30.glUniform3fv(kdUniform,1,material_diffuse,0);
			GLES30.glUniform3fv(ksUniform,1,material_specular,0);

			GLES30.glUniform1f(materialShininessUniform,material_shininess);
		}
		else
		{
			GLES30.glUniform1i(doubleTapUniform, 0);
		}

		// opengles drawing
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];

		// ****************************************************
		// CUBE BLOCK
		// ****************************************************

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		// apply z axis transition to go deep into the screen by -5.0
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-5.0f);

		Matrix.setRotateM(rotationMatrix,0,anglePyramid,0.0f,1.0f,0.0f);	// rotate by anglePyramid angle

		// multiply model and rotation matrix to get modelviewprojection matrix
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,rotationMatrix,0);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

		// bind to pyramid vao
		GLES30.glBindVertexArray(vao_pyramid[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,0,12);	// 3(each with its x,y,z) vertices in pyramidVertices array

		// unbind from pyramid vao
		GLES30.glBindVertexArray(0);

		// unuse shader program
		GLES30.glUseProgram(0);

		// keep rotating
		if(singleTap == 1)
		{
			anglePyramid = anglePyramid + 0.5f;
		}

		// render/flush
		requestRender();
	}

	void uninitialize()
	{
		// PYRAMID
		// destroy vao
		if(vao_pyramid[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao_pyramid,0);
			vao_pyramid[0] = 0;
		}
		// destroy position vbo
		if(vbo_pyramid_position[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_pyramid_position,0);
			vbo_pyramid_position[0] = 0;
		}
		// destroy texture vbo
		if(vbo_pyramid_normal[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_pyramid_normal,0);
			vbo_pyramid_normal[0] = 0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,vertexShaderObject);

				// detach vertex shader from shader program object
				GLES30.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,fragmentShaderObject);

				GLES30.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if(shaderProgramObject != 0)
		{
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
