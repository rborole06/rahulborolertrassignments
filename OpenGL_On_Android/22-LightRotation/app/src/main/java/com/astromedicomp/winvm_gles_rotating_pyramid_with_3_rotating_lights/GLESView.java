package com.astromedicomp.winvm_gles_rotating_pyramid_with_3_rotating_lights;

//  
import android.content.Context;	// for drawing context related
import android.opengl.GLSurfaceView;	// for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10;	// for opengles 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;	// for EGLConfig needed as param type EGLConfig
import android.opengl.GLES30;	// for opengles 3.0
import android.view.MotionEvent;	// for motion event
import android.view.GestureDetector;	// for gesture detector
import android.view.GestureDetector.OnGestureListener;	// onGestureListener
import android.view.GestureDetector.OnDoubleTapListener;	// for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.lang.Math;

import android.opengl.Matrix;	// for matrix math

// A view for opengles graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int numElements;
	private int numVertices;

	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_position = new int[1];	
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_element = new int[1];

	private float light_1_ambient[] = {0.0f,0.0f,0.0f,0.0f};
	private float light_1_diffuse[] = {1.0f,0.0f,0.0f,0.0f};
	private float light_1_specular[] = {1.0f,0.0f,0.0f,0.0f};
	private float light_1_position[] = {0.0f,0.0f,0.0f,0.0f};

	private float light_2_ambient[] = {0.0f,0.0f,0.0f,0.0f};
	private float light_2_diffuse[] = {0.0f,1.0f,0.0f,0.0f};
	private float light_2_specular[] = {0.0f,1.0f,0.0f,0.0f};
	private float light_2_position[] = {0.0f,0.0f,0.0f,0.0f};

	private float light_3_ambient[] = {0.0f,0.0f,0.0f,0.0f};
	private float light_3_diffuse[] = {0.0f,0.0f,1.0f,0.0f};
	private float light_3_specular[] = {0.0f,0.0f,1.0f,0.0f};
	private float light_3_position[] = {0.0f,0.0f,0.0f,0.0f};

	private float material_ambient[] = {0.0f,0.0f,0.0f,1.0f};
	private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
	private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
	private float material_shininess = 50.0f;

	private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;

	private int la_1_Uniform, ld_1_Uniform, ls_1_Uniform, light_1_PositionUniform;
	private int la_2_Uniform, ld_2_Uniform, ls_2_Uniform, light_2_PositionUniform;
	private int la_3_Uniform, ld_3_Uniform, ls_3_Uniform, light_3_PositionUniform;

	private int kaUniform, kdUniform, ksUniform, materialShininessUniform;

	private int doubleTapUniform;
	private int V_KeyPressed_uniform;

	// If 1, then per vertex lighting
	// If 0, per fragment lighting
	private int perVertexLighting = 1;

	private float perspectiveProjectionMatrix[] = new float[16];	// 4x4 matrix

	private int doubleTap;	// for lights

	// color angles
	float angleRedLight = 0.0f;
	float angleGreenLight = 0.0f;
	float angleBlueLight = 0.0f;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		// accordingly set EGLContext to current supported version of opengles
		setEGLContextClientVersion(3);

		// set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	// overriden method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// get opengles version
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version = "+glesVersion);

		// get glsl version
		String glslVersion = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL version = "+glslVersion);

		initialize(gl);
	}

	// overriden method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
	}

	// overriden method of GLSurfaceView.Renderer( Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}

	// Handling 'onTouchEvent' is the most important
	// because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		perVertexLighting++;
		if(perVertexLighting > 1)
			perVertexLighting = 0;
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here because already written 'onSingleTapConfirmed'
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);	
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// *****************************************************************
		// VERTEX SHADER
		// *****************************************************************

		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform int u_v_key_pressed;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform vec4 u_light_1_position;"+
		"uniform vec4 u_light_2_position;"+
		"uniform vec4 u_light_3_position;"+
		"uniform mediump int u_double_tap;"+
		"uniform vec3 u_La_1;"+
		"uniform vec3 u_Ld_1;"+
		"uniform vec3 u_Ls_1;"+
		"uniform vec3 u_La_2;"+
		"uniform vec3 u_Ld_2;"+
		"uniform vec3 u_Ls_2;"+
		"uniform vec3 u_La_3;"+
		"uniform vec3 u_Ld_3;"+
		"uniform vec3 u_Ls_3;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_1_direction;"+
		"out vec3 light_2_direction;"+
		"out vec3 light_3_direction;"+
		"out vec3 viewer_vector;"+
		"out vec3 v_phong_ads_color;"+
		"void main(void)"+
		"{"+
		"if(u_double_tap == 1)"+
		"{"+
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"+
		"if(u_v_key_pressed == 1)"+
		"{"+
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
		"vec3 light_1_direction = normalize(vec3(u_light_1_position) - eyeCoordinates.xyz);"+
		"vec3 light_2_direction = normalize(vec3(u_light_2_position) - eyeCoordinates.xyz);"+
		"vec3 light_3_direction = normalize(vec3(u_light_3_position) - eyeCoordinates.xyz);"+
		"float tn1_dot_ld1 = max(dot(transformed_normals,light_1_direction),0.0);"+
		"float tn2_dot_ld2 = max(dot(transformed_normals,light_2_direction),0.0);"+
		"float tn3_dot_ld3 = max(dot(transformed_normals,light_3_direction),0.0);"+
		"vec3 ambient_1 = u_La_1 * u_Ka;"+
		"vec3 ambient_2 = u_La_2 * u_Ka;"+
		"vec3 ambient_3 = u_La_3 * u_Ka;"+
		"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;"+
		"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;"+
		"vec3 diffuse_3 = u_Ld_3 * u_Kd * tn3_dot_ld3;"+
		"vec3 reflection_vector_1 = reflect(-light_1_direction,transformed_normals);"+
		"vec3 reflection_vector_2 = reflect(-light_2_direction,transformed_normals);"+
		"vec3 reflection_vector_3 = reflect(-light_3_direction,transformed_normals);"+
		"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);"+
		"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_3 = u_Ls_3 * u_Ks * pow(max(dot(reflection_vector_3,viewer_vector),0.0),u_material_shininess);"+
		"v_phong_ads_color = ambient_1 + ambient_2 + ambient_3 + diffuse_1 + diffuse_2 + diffuse_3 + specular_1 + specular_2 + specular_3;"+
		"}"+
		"else"+
		"{"+
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
		"light_1_direction = vec3(u_light_1_position) - eyeCoordinates.xyz;"+
		"light_2_direction = vec3(u_light_2_position) - eyeCoordinates.xyz;"+
		"light_3_direction = vec3(u_light_3_position) - eyeCoordinates.xyz;"+
		"viewer_vector = -eyeCoordinates.xyz;"+
		"}"+
		"}"+
		"else"+
		"{"+
		"v_phong_ads_color = vec3(1.0,1.0,1.0);"+
		"}"+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// *****************************************************************
		// FRAGMENT SHADER
		// *****************************************************************

		// create shader
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);

		// fragment shader source code
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 v_phong_ads_color;"+
		"in vec3 transformed_normals;"+
		"in vec3 light_1_direction;"+
		"in vec3 light_2_direction;"+
		"in vec3 light_3_direction;"+
		"in vec3 viewer_vector;"+
		"out vec4 FragColor;"+
		"uniform int u_v_key_pressed;"+
		"uniform vec3 u_La_1;"+
		"uniform vec3 u_Ld_1;"+
		"uniform vec3 u_Ls_1;"+
		"uniform vec3 u_La_2;"+
		"uniform vec3 u_Ld_2;"+
		"uniform vec3 u_Ls_2;"+
		"uniform vec3 u_La_3;"+
		"uniform vec3 u_Ld_3;"+
		"uniform vec3 u_Ls_3;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"uniform int u_double_tap;"+
		"void main(void)"+
		"{"+
		"vec3 f_phong_ads_color;"+
		"if(u_double_tap == 1)"+
		"{"+
		"if(u_v_key_pressed == 0)"+
		"{"+
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
		"vec3 normalized_light_1_direction = normalize(light_1_direction);"+
		"vec3 normalized_light_2_direction = normalize(light_2_direction);"+
		"vec3 normalized_light_3_direction = normalize(light_3_direction);"+
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
		"vec3 ambient_1 = u_La_1 * u_Ka;"+
		"vec3 ambient_2 = u_La_2 * u_Ka;"+
		"vec3 ambient_3 = u_La_3 * u_Ka;"+
		"float tn1_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_1_direction),0.0);"+
		"float tn2_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_2_direction),0.0);"+
		"float tn3_dot_ld3 = max(dot(normalized_transformed_normals,normalized_light_3_direction),0.0);"+
		"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;"+
		"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;"+
		"vec3 diffuse_3 = u_Ld_3 * u_Kd * tn3_dot_ld3;"+
		"vec3 reflection_vector_1 = reflect(-normalized_light_1_direction,normalized_transformed_normals);"+
		"vec3 reflection_vector_2 = reflect(-normalized_light_2_direction,normalized_transformed_normals);"+
		"vec3 reflection_vector_3 = reflect(-normalized_light_3_direction,normalized_transformed_normals);"+
		"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,normalized_viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,normalized_viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_3 = u_Ls_3 * u_Ks * pow(max(dot(reflection_vector_3,normalized_viewer_vector),0.0),u_material_shininess);"+
		"f_phong_ads_color = ambient_1 + ambient_2 + ambient_3 + diffuse_1 + diffuse_2 + diffuse_3 + specular_1 + specular_2 + specular_3;"+
		"}"+
		"else"+
		"{"+
		"FragColor = vec4(v_phong_ads_color,1.0);"+
		"}"+
		"}"+
		"if(u_v_key_pressed == 0)"+
		"{"+
		"FragColor = vec4(f_phong_ads_color,1.0);"+
		"}"+
		"else"+
		"{"+
		"FragColor = vec4(v_phong_ads_color,1.0);"+
		"}"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// create shader program
		shaderProgramObject = GLES30.glCreateProgram();

		// attach vertex shader to shader program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);

		// attach fragment shader to shader program
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);

		// pre-link binding of shader program object with vertex shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

		// pre-link binding of shader program object with normal shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

		// link the two shaders together to shader program object
		GLES30.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// get uniform location
		modelMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_model_matrix");
		viewMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_view_matrix");
		projectionMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");

		doubleTapUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_double_tap");
		V_KeyPressed_uniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_v_key_pressed");

		// ambient color intensity of light 1
		la_1_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_La_1");
		// diffuse color intensity of light 1
		ld_1_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ld_1");
		// specular color intensity of light 1
		ls_1_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ls_1");
		// position of light 1
		light_1_PositionUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_light_1_position");

		// ambient color intensity of light 2
		la_2_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_La_2");
		// diffuse color intensity of light 2
		ld_2_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ld_2");
		// specular color intensity of light 2
		ls_2_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ls_2");
		// position of light 2
		light_2_PositionUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_light_2_position");

		// ambient color intensity of light 3
		la_3_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_La_3");
		// diffuse color intensity of light 3
		ld_3_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ld_3");
		// specular color intensity of light 3
		ls_3_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ls_3");
		// position of light 3
		light_3_PositionUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_light_3_position");

		// amient reflective color intensity of light
		kaUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ka");
		// diffuse reflective color intensity of light
		kdUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Kd");
		// specular reflective color intensity of light
		ksUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ks");
		// shininess of material (value is conventionally between 0 to 200)
		materialShininessUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_material_shininess");

		// *** vertices, colors, shader attribs, vbo, vao_pyramid, initializations ***
		Sphere sphere = new Sphere();
		float sphere_vertices[] = new float[1146];
		float sphere_normals[] = new float[1146];
		float sphere_textures[] = new float[764];
		short sphere_elements[] = new short[2280];

		sphere.getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);
		numVertices = sphere.getNumberOfSphereVertices();
		numElements = sphere.getNumberOfSphereElements();
		

		// *****************************
		// VAO FOR SPHERE
		// *****************************

		// generate and bind vao for sphere
		GLES30.glGenVertexArrays(1,vao_sphere,0);
		GLES30.glBindVertexArray(vao_sphere[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************

		GLES30.glGenBuffers(1,vbo_sphere_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_position[0]);

		// *****************************************************************************************************************************************************
		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		// *****************************************************************************************************************************************************

		// allocate required size to buffer
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
		// detect and arrange byte as per native style (little endian or big endian)
		byteBuffer.order(ByteOrder.nativeOrder());
		// assign data-type to contents of buffer
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		// put data in buffer
		verticesBuffer.put(sphere_vertices);
		// start from this position in buffer
		verticesBuffer.position(0);
		
		// creates and initializes a buffer object's data store
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,sphere_vertices.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);

		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR NORMAL
		// *****************************
		GLES30.glGenBuffers(1,vbo_sphere_normal,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_normals);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,sphere_normals.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR ELEMENT
		// *****************************
		GLES30.glGenBuffers(1,vbo_sphere_element,0);
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
		byteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
		elementsBuffer.put(sphere_elements);
		elementsBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ELEMENT_ARRAY_BUFFER,sphere_elements.length*2,elementsBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,0);
		
		// unbind vao for sphere
		GLES30.glBindVertexArray(0);

		// enable depth testing
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		// depth test to do
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		// cull back faces for better performance
		GLES30.glEnable(GLES30.GL_CULL_FACE);

		// set background color
		GLES30.glClearColor(0.0f,0.0f,0.0f,1.0f);

		// initialization
		doubleTap = 0;

		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width, int height)
	{
		GLES30.glViewport(0,0,width,height);

		// perspective projection 
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
	}

	void update()
	{
		angleRedLight = angleRedLight + 0.01f;
		if (angleRedLight >= 360)
		{
			angleRedLight = 0.0f;
		}
		angleGreenLight = angleGreenLight + 0.01f;
		if (angleGreenLight >= 360)
		{
			angleGreenLight = 0.0f;
		}
		angleBlueLight = angleBlueLight + 0.01f;
		if (angleBlueLight >= 360)
		{
			angleBlueLight = 0.0f;
		}
	}

	public void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

		// use shader program
		GLES30.glUseProgram(shaderProgramObject);

		if(doubleTap == 1)
		{
			GLES30.glUniform1i(doubleTapUniform,1);
			GLES30.glUniform1i(V_KeyPressed_uniform, perVertexLighting);

			// setting light properties
			GLES30.glUniform3fv(la_1_Uniform,1,light_1_ambient,0);
			GLES30.glUniform3fv(ld_1_Uniform,1,light_1_diffuse,0);
			GLES30.glUniform3fv(ls_1_Uniform,1,light_1_specular,0);
			light_1_position[1] = 100.0f * (float)Math.cos(angleRedLight);
			light_1_position[2] = 100.0f * (float)Math.sin(angleRedLight);
			GLES30.glUniform4fv(light_1_PositionUniform,1,light_1_position,0);

			GLES30.glUniform3fv(la_2_Uniform,1,light_2_ambient,0);
			GLES30.glUniform3fv(ld_2_Uniform,1,light_2_diffuse,0);
			GLES30.glUniform3fv(ls_2_Uniform,1,light_2_specular,0);
			light_2_position[0] = 100.0f * (float)Math.sin(angleGreenLight);
			light_2_position[2] = 100.0f * (float)Math.cos(angleGreenLight);
			GLES30.glUniform4fv(light_2_PositionUniform,1,light_2_position,0);

			GLES30.glUniform3fv(la_3_Uniform,1,light_3_ambient,0);
			GLES30.glUniform3fv(ld_3_Uniform,1,light_3_diffuse,0);
			GLES30.glUniform3fv(ls_3_Uniform,1,light_3_specular,0);
			light_3_position[0] = 100.0f * (float)Math.cos(angleBlueLight);
			light_3_position[1] = 100.0f * (float)Math.sin(angleBlueLight);
			GLES30.glUniform4fv(light_3_PositionUniform,1,light_3_position,0);

			// setting material properties
			GLES30.glUniform3fv(kaUniform,1,material_ambient,0);
			GLES30.glUniform3fv(kdUniform,1,material_diffuse,0);
			GLES30.glUniform3fv(ksUniform,1,material_specular,0);

			GLES30.glUniform1f(materialShininessUniform,material_shininess);
		}
		else
		{
			GLES30.glUniform1i(doubleTapUniform, 0);
			GLES30.glUniform1i(V_KeyPressed_uniform, perVertexLighting);

		}

		// opengles drawing
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];

		// ****************************************************
		// SPHERE BLOCK
		// ****************************************************

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		// apply z axis transition to go deep into the screen by -1.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-1.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// unuse shader program
		GLES30.glUseProgram(0);

		if(doubleTap == 1)
			update();

		// render/flush
		requestRender();
	}

	void uninitialize()
	{
		// SPHERE
		// destroy vao
		if(vao_sphere[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao_sphere,0);
			vao_sphere[0] = 0;
		}
		// destroy position vbo
		if(vbo_sphere_position[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_sphere_position,0);
			vbo_sphere_position[0] = 0;
		}
		// destroy texture vbo
		if(vbo_sphere_normal[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_sphere_normal,0);
			vbo_sphere_normal[0] = 0;
		}
		// destroy elment vbo
		if(vbo_sphere_element[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_sphere_element,0);
			vbo_sphere_element[0] = 0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,vertexShaderObject);

				// detach vertex shader from shader program object
				GLES30.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,fragmentShaderObject);

				GLES30.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if(shaderProgramObject != 0)
		{
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
