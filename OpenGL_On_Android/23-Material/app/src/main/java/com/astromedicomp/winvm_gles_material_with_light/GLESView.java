package com.astromedicomp.winvm_gles_material_with_light;

//  
import android.content.Context;	// for drawing context related
import android.opengl.GLSurfaceView;	// for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10;	// for opengles 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;	// for EGLConfig needed as param type EGLConfig
import android.opengl.GLES30;	// for opengles 3.0
import android.view.MotionEvent;	// for motion event
import android.view.GestureDetector;	// for gesture detector
import android.view.GestureDetector.OnGestureListener;	// onGestureListener
import android.view.GestureDetector.OnDoubleTapListener;	// for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.lang.Math;

import android.opengl.Matrix;	// for matrix math

// A view for opengles graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int numElements;
	private int numVertices;

	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_position = new int[1];	
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_element = new int[1];

	private float light_ambient[] = {0.0f,0.0f,0.0f,0.0f};
	private float light_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
	private float light_specular[] = {1.0f,1.0f,1.0f,1.0f};
	private float light_position[] = {0.0f,0.0f,0.0f,0.0f};

	// define material array for first sphere of first column
	private float material_1_1_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
	private float material_1_1_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
	private float material_1_1_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
	private float material_1_1_shininess = 0.6f * 128.0f;

	// define material array for second sphere of first column
	private float material_1_2_ambient[] = { 0.135f,0.2225f,0.1575f,1.0f };
	private float material_1_2_diffuse[] = { 0.54f,0.89f,0.63f,1.0f };
	private float material_1_2_specular[] = { 0.316228f,0.316228f,0.316228f,1.0f };
	private float material_1_2_shininess = 0.1f * 128.0f;

	// define material array for third sphere of first column
	private float material_1_3_ambient[] = { 0.05375f,0.05f,0.06625f,1.0f };
	private float material_1_3_diffuse[] = { 0.18275f,0.17f,0.22525f,1.0f };
	private float material_1_3_specular[] = { 0.332741f,0.328634f,0.346435f,1.0f };
	private float material_1_3_shininess = 0.3f * 128.0f;

	// define material array for fourth sphere of first column
	private float material_1_4_ambient[] = { 0.25f,0.20725f,0.20725f,1.0f };
	private float material_1_4_diffuse[] = { 1.0f,0.829f,0.829f,1.0f };
	private float material_1_4_specular[] = { 0.296648f,0.296648f,0.296648f,1.0f };
	private float material_1_4_shininess = 0.088f * 128.0f;

	// define material array for fifth sphere of first column
	private float material_1_5_ambient[] = { 0.1745f,0.01175f,0.01175f,1.0f };
	private float material_1_5_diffuse[] = { 0.61424f,0.04136f,0.04136f,1.0f };
	private float material_1_5_specular[] = { 0.727811f,0.626959f,0.626959f,1.0f };
	private float material_1_5_shininess = 0.6f * 128.0f;

	// define material array for six sphere of first column
	private float material_1_6_ambient[] = { 0.1f,0.18725f,0.1745f,1.0f };
	private float material_1_6_diffuse[] = { 0.396f,0.74151f,0.69102f,1.0f };
	private float material_1_6_specular[] = { 0.297254f,0.30829f,0.306678f,1.0f };
	private float material_1_6_shininess = 0.6f * 128.0f;

	// define material array for first sphere of second column
	private float material_2_1_ambient[] = { 0.329412f,0.223529f,0.027451f,1.0f };
	private float material_2_1_diffuse[] = { 0.780392f,0.568627f,0.113725f,1.0f };
	private float material_2_1_specular[] = { 0.992157f,0.941176f,0.807843f,1.0f };
	private float material_2_1_shininess = 0.21794872f * 128.0f;

	// define material array for second sphere of second column
	private float material_2_2_ambient[] = { 0.2125f,0.1275f,0.054f,1.0f };
	private float material_2_2_diffuse[] = { 0.714f,0.4284f,0.18144f,1.0f };
	private float material_2_2_specular[] = { 0.393548f,0.271906f,0.166721f,1.0f };
	private float material_2_2_shininess = 0.2f * 128.0f;

	// define material array for third sphere of second column
	private float material_2_3_ambient[] = { 0.25f,0.25f,0.25f,1.0f };
	private float material_2_3_diffuse[] = { 0.4f,0.4f,0.4f,1.0f };
	private float material_2_3_specular[] = { 0.774597f,0.774597f,0.774597f,1.0f };
	private float material_2_3_shininess = 0.6f * 128.0f;

	// define material array for fourth sphere of second column
	private float material_2_4_ambient[] = { 0.19125f,0.0735f,0.0225f,1.0f };
	private float material_2_4_diffuse[] = { 0.7038f,0.27048f,0.0828f,1.0f };
	private float material_2_4_specular[] = { 0.256777f,0.137622f,0.086014f,1.0f };
	private float material_2_4_shininess = 0.1f * 128.0f;

	// define material array for fifth sphere of second column
	private float material_2_5_ambient[] = { 0.24725f,0.1995f,0.0745f,1.0f };
	private float material_2_5_diffuse[] = { 0.75164f,0.60648f,0.22648f,1.0f };
	private float material_2_5_specular[] = { 0.628281f,0.555802f,0.366065f,1.0f };
	private float material_2_5_shininess = 0.4f * 128.0f;

	// define material array for six sphere of second column
	private float material_2_6_ambient[] = { 0.19225f,0.19225f,0.19225f,1.0f };
	private float material_2_6_diffuse[] = { 0.50754f,0.50754f,0.50754f,1.0f };
	private float material_2_6_specular[] = { 0.508273f,0.508273f,0.508273f,1.0f };
	private float material_2_6_shininess = 0.4f * 128.0f;

	// define material array for first sphere of third column
	private float material_3_1_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float material_3_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
	private float material_3_1_specular[] = { 0.50f,0.50f,0.50f,1.0f };
	private float material_3_1_shininess = 0.25f * 128.0f;

	// define material array for second sphere of third column
	private float material_3_2_ambient[] = { 0.0f,0.1f,0.06f,1.0f };
	private float material_3_2_diffuse[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
	private float material_3_2_specular[] = { 0.50196078f,0.50196078f,0.50196078f,1.0f };
	private float material_3_2_shininess = 0.25f * 128.0f;

	// define material array for third sphere of third column
	private float material_3_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float material_3_3_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
	private float material_3_3_specular[] = { 0.45f,0.55f,0.45f,1.0f };
	private float material_3_3_shininess = 0.25f * 128.0f;

	// define material array for fourth sphere of third column
	private float material_3_4_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float material_3_4_diffuse[] = { 0.5f,0.0f,0.0f,1.0f };
	private float material_3_4_specular[] = { 0.7f,0.6f,0.6f,1.0f };
	private float material_3_4_shininess = 0.25f * 128.0f;

	// define material array for fifth sphere of third column
	private float material_3_5_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float material_3_5_diffuse[] = { 0.55f,0.55f,0.55f,1.0f };
	private float material_3_5_specular[] = { 0.70f,0.70f,0.70f,1.0f };
	private float material_3_5_shininess = 0.25f * 128.0f;

	// define material array for six sphere of third column
	private float material_3_6_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float material_3_6_diffuse[] = { 0.5f,0.5f,0.0f,1.0f };
	private float material_3_6_specular[] = { 0.60f,0.60f,0.50f,1.0f };
	private float material_3_6_shininess = 0.25f * 128.0f;

	// define material array for first sphere of fourth column
	private float material_4_1_ambient[] = { 0.02f,0.02f,0.02f,1.0f };
	private float material_4_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
	private float material_4_1_specular[] = { 0.4f,0.4f,0.4f,1.0f };
	private float material_4_1_shininess = 0.078125f * 128.0f;

	// define material array for second sphere of fourth column
	private float material_4_2_ambient[] = { 0.0f,0.05f,0.05f,1.0f };
	private float material_4_2_diffuse[] = { 0.4f,0.5f,0.5f,1.0f };
	private float material_4_2_specular[] = { 0.04f,0.7f,0.7f,1.0f };
	private float material_4_2_shininess = 0.078125f * 128.0f;

	// define material array for third sphere of fourth column
	private float material_4_3_ambient[] = { 0.0f,0.05f,0.0f,1.0f };
	private float material_4_3_diffuse[] = { 0.4f,0.5f,0.4f,1.0f };
	private float material_4_3_specular[] = { 0.04f,0.7f,0.04f,1.0f };
	private float material_4_3_shininess = 0.078125f * 128.0f;

	// define material array for fourth sphere of fourth column
	private float material_4_4_ambient[] = { 0.05f,0.0f,0.0f,1.0f };
	private float material_4_4_diffuse[] = { 0.5f,0.4f,0.4f,1.0f };
	private float material_4_4_specular[] = { 0.7f,0.04f,0.04f,1.0f };
	private float material_4_4_shininess = 0.078125f * 128.0f;

	// define material array for fifth sphere of fourth column
	private float material_4_5_ambient[] = { 0.05f,0.05f,0.05f,1.0f };
	private float material_4_5_diffuse[] = { 0.5f,0.5f,0.5f,1.0f };
	private float material_4_5_specular[] = { 0.7f,0.7f,0.7f,1.0f };
	private float material_4_5_shininess = 0.078125f * 128.0f;

	// define material array for six sphere of fourth column
	private float material_4_6_ambient[] = { 0.05f,0.05f,0.0f,1.0f };
	private float material_4_6_diffuse[] = { 0.5f,0.5f,0.4f,1.0f };
	private float material_4_6_specular[] = { 0.7f,0.7f,0.04f,1.0f };
	private float material_4_6_shininess = 0.078125f * 128.0f;

	private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;

	private int la_Uniform, ld_Uniform, ls_Uniform, light_positionUniform;
	private int kaUniform, kdUniform, ksUniform, materialShininessUniform;

	private int doubleTapUniform;

	private float perspectiveProjectionMatrix[] = new float[16];	// 4x4 matrix

	private int singleTap = 0;	// for rotation around x,y,z axis
	private int doubleTap;	// for lights

	private int surfaceWidth;
	private int surfaceHeight;
	private int viewportX;
	private int viewportY;
	private int viewportWidth;
	private int viewportHeight;

	// color angles
	float angleWhiteLight = 0.0f;

	public GLESView(Context drawingContext, int width, int height)
	{
		super(drawingContext);

		context = drawingContext;

		// accordingly set EGLContext to current supported version of opengles
		setEGLContextClientVersion(3);

		// set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		// Render the view only when there is a change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);

		surfaceWidth = width;
		surfaceHeight = height;

		viewportWidth = surfaceWidth / 4;		// width of viewport
		viewportHeight = surfaceHeight / 6;		// height of viewport

	}

	// overriden method of GLSurfaceView.Renderer (Init code)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// get opengles version
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version = "+glesVersion);

		// get glsl version
		String glslVersion = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL version = "+glslVersion);

		initialize(gl);
	}

	// overriden method of GLSurfaceView.Renderer (Change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(0,0,width,height);
	}

	// overriden method of GLSurfaceView.Renderer( Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}

	// Handling 'onTouchEvent' is the most important
	// because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventaction = e.getAction();
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	// abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;
		if(singleTap == 1)
		{
			// resest light position
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			light_position[3] = 0.0f;
		}
		else if(singleTap == 2)
		{
			// resest light position
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			light_position[3] = 0.0f;
		}
		else if(singleTap == 3)
		{
			// resest light position
			light_position[0] = 0.0f;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
			light_position[3] = 0.0f;
		}
		else if(singleTap > 3)
		{
			singleTap = 0;
		}
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// do not write any code here because already written 'onSingleTapConfirmed'
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);	
		return(true);
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	// abstract method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		// *****************************************************************
		// VERTEX SHADER
		// *****************************************************************

		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform mediump int u_double_tap;"+
		"uniform vec4 u_light_position;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_direction;"+
		"out vec3 viewer_vector;"+
		"void main(void)"+
		"{"+
		"if(u_double_tap == 1)"+
		"{"+
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"+
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
		"viewer_vector = -eye_coordinates.xyz;"+
		"}"+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG: Vertex shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// *****************************************************************
		// FRAGMENT SHADER
		// *****************************************************************

		// create shader
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);

		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 transformed_normals;"+
		"in vec3 light_direction;"+
		"in vec3 viewer_vector;"+
		"out vec4 FragColor;"+
		"uniform vec3 u_La;"+
		"uniform vec3 u_Ld;"+
		"uniform vec3 u_Ls;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"uniform int u_double_tap;"+
		"void main(void)"+
		"{"+
		"vec3 phong_ads_color;"+
		"if(u_double_tap == 1)"+
		"{"+
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
		"vec3 normalized_light_direction = normalize(light_direction);"+
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
		"vec3 ambient = u_La * u_Ka;"+
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"+
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"+
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"+
		"phong_ads_color = ambient + diffuse + specular;"+
		"}"+
		"else"+
		"{"+
		"phong_ads_color = vec3(1.0,1.0,1.0);"+
		"}"+
		"FragColor = vec4(phong_ads_color, 1.0);"+
		"}"
		);

		// provide source code to shader
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		// compile shader and check for errors
		GLES30.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// create shader program
		shaderProgramObject = GLES30.glCreateProgram();

		// attach vertex shader to shader program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);

		// attach fragment shader to shader program
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);

		// pre-link binding of shader program object with vertex shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

		// pre-link binding of shader program object with normal shader attributes
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

		// link the two shaders together to shader program object
		GLES30.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderProgramLinkStatus[0] == GLES30.GL_FALSE)
		{
			GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// get uniform location
		modelMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_model_matrix");
		viewMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_view_matrix");
		projectionMatrixUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");

		doubleTapUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_double_tap");

		// ambient color intensity of light 1
		la_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_La");
		// diffuse color intensity of light 1
		ld_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ld");
		// specular color intensity of light 1
		ls_Uniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ls");
		// position of light 1
		light_positionUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_light_position");

		// amient reflective color intensity of light
		kaUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ka");
		// diffuse reflective color intensity of light
		kdUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Kd");
		// specular reflective color intensity of light
		ksUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_Ks");
		// shininess of material (value is conventionally between 0 to 200)
		materialShininessUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_material_shininess");

		// *** vertices, colors, shader attribs, vbo, vao_pyramid, initializations ***
		Sphere sphere = new Sphere();
		float sphere_vertices[] = new float[1146];
		float sphere_normals[] = new float[1146];
		float sphere_textures[] = new float[764];
		short sphere_elements[] = new short[2280];

		sphere.getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);
		numVertices = sphere.getNumberOfSphereVertices();
		numElements = sphere.getNumberOfSphereElements();
		

		// *****************************
		// VAO FOR SPHERE
		// *****************************

		// generate and bind vao for sphere
		GLES30.glGenVertexArrays(1,vao_sphere,0);
		GLES30.glBindVertexArray(vao_sphere[0]);

		// *****************************
		// VBO FOR POSITION
		// *****************************

		GLES30.glGenBuffers(1,vbo_sphere_position,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_position[0]);

		// *****************************************************************************************************************************************************
		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		// *****************************************************************************************************************************************************

		// allocate required size to buffer
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
		// detect and arrange byte as per native style (little endian or big endian)
		byteBuffer.order(ByteOrder.nativeOrder());
		// assign data-type to contents of buffer
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		// put data in buffer
		verticesBuffer.put(sphere_vertices);
		// start from this position in buffer
		verticesBuffer.position(0);
		
		// creates and initializes a buffer object's data store
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,sphere_vertices.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);

		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR NORMAL
		// *****************************
		GLES30.glGenBuffers(1,vbo_sphere_normal,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_normals);
		verticesBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,sphere_normals.length*4,verticesBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,3,GLES30.GL_FLOAT,false,0,0);
		GLES30.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		// *****************************
		// VBO FOR ELEMENT
		// *****************************
		GLES30.glGenBuffers(1,vbo_sphere_element,0);
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);

		// opengl requires byte buffer and it converts buffer, which we sent to it named as verticesBuffer, to bytes internally
		// but unfortunately java does not have pointers and that's why, we, as a programmer, are not able to tell that i am sending some bla-bla-bla size array
		// to solve this, create our own cooked buffer and send that buffer
		byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
		byteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
		elementsBuffer.put(sphere_elements);
		elementsBuffer.position(0);

		GLES30.glBufferData(GLES30.GL_ELEMENT_ARRAY_BUFFER,sphere_elements.length*2,elementsBuffer,GLES30.GL_STATIC_DRAW);
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,0);
		
		// unbind vao for sphere
		GLES30.glBindVertexArray(0);

		// enable depth testing
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		// depth test to do
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		// cull back faces for better performance
		GLES30.glEnable(GLES30.GL_CULL_FACE);

		// set background color
		GLES30.glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

		// initialization
		doubleTap = 0;

		// set projection matrix to identity matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int x, int y, int width, int height)
	{
		GLES30.glViewport(x,y,width,height);

		// perspective projection 
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f);
	}

	void update()
	{
		angleWhiteLight = angleWhiteLight + 0.01f;
		if (angleWhiteLight >= viewportX)
		{
			angleWhiteLight = 0.0f;
		}
	}

	public void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

		// use shader program
		GLES30.glUseProgram(shaderProgramObject);

		if(doubleTap == 1)
		{
			GLES30.glUniform1i(doubleTapUniform,1);

			// setting light properties
			GLES30.glUniform3fv(la_Uniform,1,light_ambient,0);
			GLES30.glUniform3fv(ld_Uniform,1,light_diffuse,0);
			GLES30.glUniform3fv(ls_Uniform,1,light_specular,0);

			if(singleTap == 1)
			{
				//light_position[1] = angleWhiteLight;
				light_position[1] = 100.0f * (float)Math.cos(angleWhiteLight);
				light_position[2] = 100.0f * (float)Math.sin(angleWhiteLight);
				GLES30.glUniform4fv(light_positionUniform,1,light_position,0);
			}
			else if(singleTap == 2)
			{
				//light_position[0] = angleWhiteLight;
				light_position[0] = 100.0f * (float)Math.sin(angleWhiteLight);
				light_position[2] = 100.0f * (float)Math.cos(angleWhiteLight);
				GLES30.glUniform4fv(light_positionUniform,1,light_position,0);
			}
			else if(singleTap == 3)
			{
				//light_position[0] = angleWhiteLight;
				light_position[0] = 100.0f * (float)Math.cos(angleWhiteLight);
				light_position[1] = 100.0f * (float)Math.sin(angleWhiteLight);
				GLES30.glUniform4fv(light_positionUniform,1,light_position,0);
			}
		}
		else
		{
			GLES30.glUniform1i(doubleTapUniform, 0);
		}

		// opengles drawing
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(viewMatrix,0);

		GLES30.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

		// ****************************************************
		// SPHERE BLOCK
		// ****************************************************

		// *******************
		// FIRST COLUMN SPHERE
		// *******************

		// DRAW FIRST COLUMN, FIRST SPHERE

		viewportX = surfaceWidth - (viewportWidth * 4);		// x axis starting position for viewport
		viewportY = surfaceHeight - (viewportHeight * 1);		// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_1_1_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_1_1_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_1_1_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_1_1_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FIRST COLUMN, SECOND SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 2);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_1_2_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_1_2_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_1_2_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_1_2_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FIRST COLUMN, THIRD SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 3);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_1_3_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_1_3_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_1_3_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_1_3_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FIRST COLUMN, FOURTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 4);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_1_4_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_1_4_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_1_4_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_1_4_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FIRST COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 5);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_1_5_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_1_5_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_1_5_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_1_5_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FIRST COLUMN, SIXTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 6);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_1_6_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_1_6_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_1_6_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_1_6_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// *******************
		// SECOND COLUMN SPHERE
		// *******************

		// DRAW SECOND COLUMN, FIRST SPHERE

		// set viewport
		viewportX = surfaceWidth - (viewportWidth * 3);		// x axis starting position for viewport
		viewportY = surfaceHeight - (viewportHeight * 1);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_2_1_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_2_1_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_2_1_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_2_1_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW SECOND COLUMN, SECOND SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 2);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_2_2_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_2_2_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_2_2_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_2_2_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW SECOND COLUMN, THIRD SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 3);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_2_3_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_2_3_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_2_3_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_2_3_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW SECOND COLUMN, FOURTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 4);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_2_4_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_2_4_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_2_4_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_2_4_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW SECOND COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 5);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_2_5_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_2_5_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_2_5_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_2_5_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW SECOND COLUMN, SIXTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 6);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_2_6_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_2_6_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_2_6_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_2_6_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// *******************
		// THIRD COLUMN SPHERE
		// *******************

		// DRAW THIRD COLUMN, FIRST SPHERE

		// set viewport
		viewportX = surfaceWidth - (viewportWidth * 2);		// x axis starting position for viewport
		viewportY = surfaceHeight - (viewportHeight * 1);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_3_1_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_3_1_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_3_1_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_3_1_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW THIRD COLUMN, SECOND SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 2);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_3_2_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_3_2_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_3_2_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_3_2_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW THIRD COLUMN, THIRD SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 3);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_3_3_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_3_3_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_3_3_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_3_3_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW THIRD COLUMN, FOURTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 4);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_3_4_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_3_4_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_3_4_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_3_4_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW THIRD COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 5);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_3_5_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_3_5_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_3_5_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_3_5_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW THIRD COLUMN, SIXTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 6);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_3_6_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_3_6_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_3_6_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_3_6_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// *******************
		// FOURTH COLUMN SPHERE
		// *******************

		// DRAW FOURTH COLUMN, FIRST SPHERE

		// set viewport
		viewportX = surfaceWidth - (viewportWidth * 1);	// x axis starting position for viewport
		viewportY = surfaceHeight - (viewportHeight * 1);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_4_1_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_4_1_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_4_1_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_4_1_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FOURTH COLUMN, SECOND SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 2);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_4_2_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_4_2_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_4_2_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_4_2_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FOURTH COLUMN, THIRD SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 3);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_4_3_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_4_3_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_4_3_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_4_3_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FOURTH COLUMN, FOURTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 4);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_4_4_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_4_4_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_4_4_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_4_4_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FOURTH COLUMN, FIFTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 5);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_4_5_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_4_5_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_4_5_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_4_5_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// DRAW FOURTH COLUMN, SIXTH SPHERE

		// set viewport
		viewportY = surfaceHeight - (viewportHeight * 6);	// y axis starting position for viewport
		resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

		// set model & view matrix to identity
		Matrix.setIdentityM(modelMatrix,0);

		// apply z axis transition to go deep into the screen by -2.5
		// so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-2.5f);

		GLES30.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

		// setting material properties
		GLES30.glUniform3fv(kaUniform,1,material_4_6_ambient,0);
		GLES30.glUniform3fv(kdUniform,1,material_4_6_diffuse,0);
		GLES30.glUniform3fv(ksUniform,1,material_4_6_specular,0);
		GLES30.glUniform1f(materialShininessUniform,material_4_6_shininess);

		// bind to sphere vao
		GLES30.glBindVertexArray(vao_sphere[0]);

		// draw either by glDrawTriangles() or glDrawArrays() or glDrawElement()
		GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,numElements,GLES30.GL_UNSIGNED_SHORT,0);

		// unbind from sphere vao
		GLES30.glBindVertexArray(0);

		// unuse shader program
		GLES30.glUseProgram(0);

		//if(doubleTap == 1)
			update();

		// render/flush
		requestRender();
	}

	void uninitialize()
	{
		// SPHERE
		// destroy vao
		if(vao_sphere[0] != 0)
		{
			GLES30.glDeleteVertexArrays(1,vao_sphere,0);
			vao_sphere[0] = 0;
		}
		// destroy position vbo
		if(vbo_sphere_position[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_sphere_position,0);
			vbo_sphere_position[0] = 0;
		}
		// destroy texture vbo
		if(vbo_sphere_normal[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_sphere_normal,0);
			vbo_sphere_normal[0] = 0;
		}
		// destroy elment vbo
		if(vbo_sphere_element[0] != 0)
		{
			GLES30.glDeleteBuffers(1,vbo_sphere_element,0);
			vbo_sphere_element[0] = 0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,vertexShaderObject);

				// detach vertex shader from shader program object
				GLES30.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				GLES30.glDetachShader(shaderProgramObject,fragmentShaderObject);

				GLES30.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if(shaderProgramObject != 0)
		{
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
