package com.astromedicomp.winvm_gles_material_with_light;

// default supplied packages by android sdk
import android.app.Activity;
import android.os.Bundle;

// later added packages
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.util.DisplayMetrics;

public class MainActivity extends Activity
{
	private GLESView glesView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// this is done to get rid of ActionBar
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// this is done to make fullscreen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		// get screen width and height
		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int height = displayMetrics.heightPixels;
		int width = displayMetrics.widthPixels;

		glesView = new GLESView(this,width,height);

		setContentView(glesView);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}
}