
1 - This application shows how to render cube with phong lighting model and blinn modification in PFPL
2 - To see effect of light on cube, press key l
3 - To use blinn modification, we need to change formula for Is little bit as per below

Origional Formula - 
--------------------------------------------------
Is = Ls * Ks * pow((r.v),f)
where r is reflection vector and v is viewer vector

Origional Formula With Blinn Modification - 
--------------------------------------------------
Is = Ls * Ks * pow((h.n),f)
where h = (l+v)/mod(l,v)
h - half vector which is nothing but half of the angle made between incident light(l) and viewer vector(v)
l - incident light direction vector
v - viewer vector
n - transformed normals
