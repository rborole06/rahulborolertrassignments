//
//  main.m
//  TrianglePerspective
//
//  Created by Yogesh Deshmukh on 18/06/18.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
