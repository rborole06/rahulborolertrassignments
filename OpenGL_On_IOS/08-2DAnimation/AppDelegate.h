//
//  AppDelegate.h
//  BlueWindow
//
//  Created by Yogesh Deshmukh on 17/06/18.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

