//
//  GLESView.m
//  Multi Colored Triangle
//
//  Created by ASTROMEDICOMP on 17/06/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// global variables
FILE *gpFile = NULL;

@implementation GLESView
{
    EAGLContext *eaglContext;

    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;

	GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
	
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;

    GLuint gVao_pyramid;
    GLuint gVao_cube;
    GLuint gVbo_position;
    GLuint gVbo_color;
    GLuint gMVPUniform;
    
    GLfloat anglePyramid;
    GLfloat angleCube;
    
    vmath::mat4 gPerspectiveProjectionMatrix;

}

- (id)initWithFrame:(CGRect)frame;
{
	self = [super initWithFrame:frame];

    anglePyramid = 0.0f;
    angleCube = 0.0f;
    
	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;

		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
		eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
		if(eaglContext == nil)
		{
			[self release];
			return(nil);
		}
		[EAGLContext setCurrentContext:eaglContext];

		glGenFramebuffers(1,&defaultFramebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

		glGenRenderbuffers(1,&colorRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);

		GLint backingWidth;
		GLint backingHeight;

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);

		glGenRenderbuffers(1,&depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1,&defaultFramebuffer);
			glDeleteRenderbuffers(1,&colorRenderbuffer);
			glDeleteRenderbuffers(1,&depthRenderbuffer);
			[self release];
			return(nil);
		}

		printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

		// hard coded initialization
		isAnimating = NO;
		animationFrameInterval = 60;	// default since IOS 8.2

                // *** VERTEX SHADER ***
                // create shader
                gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

                // provide source code to shader
                const GLchar *vertexShaderSourceCode =
                    "#version 300 core" \
                    "\n" \
                    "in vec4 vPosition;" \
                    "in vec4 vColor;" \
                    "uniform mat4 u_mvp_matrix;" \
                    "precision highp float;" \
                    "out vec4 out_color;" \
                    "void main(void)" \
                    "{" \
                    "gl_Position = u_mvp_matrix * vPosition;" \
                    "out_color = vColor;" \
                    "}";

                glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

                // compile shader
                glCompileShader(gVertexShaderObject);
                GLint iInfoLogLength = 0;
                GLint iShaderCompiledStatus = 0;
                char *szInfoLog = NULL;
                glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
                if (iShaderCompiledStatus == GL_FALSE)
                {
                        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
                        if (iInfoLogLength > 0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // *** FRAGMENT SHAER *** //
                // create fragment shader
                gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

                // provide source code to shader
                const GLchar *fragmentShaderSourceCode =
                    "#version 300 core" \
                    "\n" \
                    "precision highp float;" \
                    "in vec4 out_color;" \
                    "out vec4 FragColor;" \
                    "void main(void)" \
                    "{" \
                    "FragColor = out_color;" \
                    "}";

                glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

                // compile shader
                glCompileShader(gFragmentShaderObject);
                glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
                if (iShaderCompiledStatus == GL_FALSE)
                {
                        glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
                        if (iInfoLogLength > 0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // *** SHADER PROGRAM ***
                // create program
                gShaderProgramObject = glCreateProgram();

                // attach vertex shader to shader program
                glAttachShader(gShaderProgramObject, gVertexShaderObject);

                // attach fragment shader to shader program
                glAttachShader(gShaderProgramObject, gFragmentShaderObject);

                // pre-building of shader program object with vertex shader position attribute
                glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

                // link shader
                glLinkProgram(gShaderProgramObject);
                GLint iShaderProgramLinkStatus = 0;
                glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
                if (iShaderProgramLinkStatus == GL_FALSE)
                {
                        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
                        if (iInfoLogLength>0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                                        fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // get MVP uniform location
                gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

                // vertices, colors, shader attribs, vbo, vao initializations
                const GLfloat pyramidVertices[] =
                {
                        // FRONT FACE
                        0.0f,1.0f,0.0f,	// apex
                        -1.0f,-1.0f,1.0f,	// left-bottom
                        1.0f,-1.0f,1.0f,	// right-bottom

                        // RIGHT FACE
                        0.0f,1.0f,0.0f,
                        1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,-1.0f,

                        // BACK FACE
                        0.0f,1.0f,0.0f,
                        1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,

                        // LEFT FACE
                        0.0f,1.0f,0.0f,
                        -1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,1.0f
                };

                const GLfloat pColorVertices[] =
                {
                        // FRONT FACE
                        1.0f,0.0f,0.0f,
                        0.0f,1.0f,0.0f,
                        0.0f,0.0f,1.0f,

                        // RIGHT FACE
                        1.0f,0.0f,0.0f,
                        0.0f,0.0f,1.0f,
                        0.0f,1.0f,0.0f,

                        // BACK FACE
                        1.0f,0.0f,0.0f,
                        0.0f,1.0f,0.0f,
                        0.0f,0.0f,1.0f,

                        // LEFT FACE
                        1.0f,0.0f,0.0f,
                        0.0f,0.0f,1.0f,
                        0.0f,1.0f,0.0f,
                };

                const GLfloat cubeVertices[] =
                {
                        // FRONT FACE
                        1.0f,1.0f,1.0f,
                        -1.0f,1.0f,1.0f,
                        -1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,1.0f,

                        // RIGHT FACE
                        1.0f,1.0f,-1.0f,
                        1.0f,1.0f,1.0f,
                        1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,-1.0f,

                        // BACK FACE
                        1.0f,1.0f,-1.0f,
                        -1.0f,1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,
                        1.0f,-1.0f,-1.0f,

                        // LEFT FACE
                        -1.0f,1.0f,1.0f,
                        -1.0f,1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,1.0f,

                        // TOP FACE
                        1.0f,1.0f,-1.0f,
                        -1.0f,1.0f,-1.0f,
                        -1.0f,1.0f,1.0f,
                        1.0f,1.0f,1.0f,

                        //	BOTTOM FACE
                        1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,1.0f
                };

                const GLfloat cColorVertices[] = 
                {
                    0.0f,0.0f,1.0f,
                    0.0f,0.0f,1.0f,
                    0.0f,0.0f,1.0f,
                    0.0f,0.0f,1.0f,
                    1.0f,0.0f,1.0f,
                    1.0f,0.0f,1.0f,
                    1.0f,0.0f,1.0f,
                    1.0f,0.0f,1.0f,
                    0.0f,1.0f,1.0f,
                    0.0f,1.0f,1.0f,
                    0.0f,1.0f,1.0f,
                    0.0f,1.0f,1.0f,
                    1.0f,1.0f,0.0f,
                    1.0f,1.0f,0.0f,
                    1.0f,1.0f,0.0f,
                    1.0f,1.0f,0.0f,
                    1.0f,0.0f,0.0f,
                    1.0f,0.0f,0.0f,
                    1.0f,0.0f,0.0f,
                    1.0f,0.0f,0.0f,
                    0.0f,1.0f,0.0f,
                    0.0f,1.0f,0.0f,
                    0.0f,1.0f,0.0f,
                    0.0f,1.0f,0.0f
                };

                // *************************
                // VAO FOR PYRAMID
                // *************************

                // generate vao for pyramid
                glGenVertexArrays(1, &gVao_pyramid);

                // bind to vao for pyramid
                glBindVertexArray(gVao_pyramid);

                // ******************
                // VBO FOR VERTICES
                // ******************
                glGenBuffers(1, &gVbo_position);
                glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
                glBindBuffer(GL_ARRAY_BUFFER, 0);

                // ******************
                // VBO FOR COLOR
                // ******************
                glGenBuffers(1, &gVbo_color);
                glBindBuffer(GL_ARRAY_BUFFER,gVbo_color);
                glBufferData(GL_ARRAY_BUFFER,sizeof(pColorVertices),pColorVertices,GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
                glBindBuffer(GL_ARRAY_BUFFER,0);

                // unbind from vao for pyramid
                glBindVertexArray(0);

                // *************************
                // VAO FOR CUBE
                // *************************

                // generate vao for cube
                glGenVertexArrays(1, &gVao_cube);

                // bind to vao for cube
                glBindVertexArray(gVao_cube);

                // ******************
                // VBO FOR VERTICES
                // ******************
                glGenBuffers(1, &gVbo_position);
                glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
                glBindBuffer(GL_ARRAY_BUFFER, 0);

                // ******************
                // VBO FOR COLOR
                // ******************
                glGenBuffers(1, &gVbo_color);
                glBindBuffer(GL_ARRAY_BUFFER, gVbo_color);
                glBufferData(GL_ARRAY_BUFFER, sizeof(cColorVertices), cColorVertices, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
                glBindBuffer(GL_ARRAY_BUFFER, 0);

                // unbind from vao for cube
                glBindVertexArray(0);
    
                //glShadeModel(GL_SMOOTH);
                //glClearDepth(1.0f);
                glEnable(GL_DEPTH_TEST);
                glDepthFunc(GL_LEQUAL);
                //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
                //glEnable(GL_CULL_FACE);
    
		// clear color
		glClearColor(0.0f,0.0f,0.0f,1.0f);	// black color

		// set perspective matrix to identity matrix
    		gPerspectiveProjectionMatrix = vmath::mat4::identity();

		// GESTURE RECOGNITION
		// Tap Gesture Code
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];	// touch of 1 finger
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];

        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];	// touch of 1 finger
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];

		// this will allow to differentiate between single tap and double tap
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

		// swipe gesture
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];
		[self addGestureRecognizer:swipeGestureRecognizer];

		// long-press gesture
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return(self);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation
- (void)drawRect:(CGRect)rect
{
	// Drawing code
}
*/

+(Class)layerClass
{
	// code
	return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
	// Code
	[EAGLContext setCurrentContext:eaglContext];

	glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        // start using opengl program object
        glUseProgram(gShaderProgramObject);

        // opengl drawing

        // ****************************************************
        // PYRAMID BLOCK
        // ****************************************************

        // set modelview, modelviewprojection & rotation matrices to identity
        vmath::mat4 rotationMatrix = vmath::mat4::identity();
        vmath::mat4 modelViewMatrix = vmath::mat4::identity();
        vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();

        // rotation
        rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
        // translate modelview matrix
        modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);

        // multiply model view by rotation matrix
        modelViewMatrix = modelViewMatrix * rotationMatrix;

        // multiply modelview and perspective projection matrix to get modelviewprojection matrix
        modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

        // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        // bind to vao of pyramid
        glBindVertexArray(gVao_pyramid);

        // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
        glDrawArrays(GL_TRIANGLES, 0, 12);	// 3(each with its x,y,z) vertices in triangleVertices array

        // unbind from vao of pyramid
        glBindVertexArray(0);

        // ****************************************************
        // CUBE BLOCK
        // ****************************************************

        // set modelview, modelviewprojection & rotation matrices to identity
        modelViewMatrix = vmath::mat4::identity();
        rotationMatrix = vmath::mat4::identity();
        vmath::mat4 scaleMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();

        // scale
        scaleMatrix = vmath::scale(0.75f,0.75f,0.75f);
        // rotation
        rotationMatrix = vmath::rotate(angleCube, angleCube, angleCube);
        // translate modelview matrix
        modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);

        // multiply modelview matrix by rotation matrix
        modelViewMatrix = modelViewMatrix * rotationMatrix * scaleMatrix;

        // multiply modelview and perspective projection matrix to get modelviewprojection matrix
        modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

        // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        // bind to vao of cube
        glBindVertexArray(gVao_cube);

        // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 4(each with its x,y,z) vertices in squareVertices array
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

        // unbind from vao of cube
        glBindVertexArray(0);

        // stop using opengl program object
        glUseProgram(0);

        [self updateAngle];
        
	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
	[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)updateAngle
{
    anglePyramid = anglePyramid + 0.1f;
    if (anglePyramid >= 360)
    {
            anglePyramid = 0.0f;
    }

    angleCube = angleCube - 0.1f;
    if (angleCube <= -360)
    {
            angleCube = 0.0f;
    }
}

- (void)layoutSubviews
{
	// code
	GLint width;
	GLint height;

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
	[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);

	glGenRenderbuffers(1,&depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);

	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

	glViewport(0,0,width,height);
        gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}

        [self updateAngle];
	[self drawView:nil];
}

- (void)startAnimation
{
	if(!isAnimating)
	{
		displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
		[displayLink setPreferredFramesPerSecond:animationFrameInterval];
		[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];

		isAnimating = YES;
	}
}

-(void)stopAnimation
{
	if(isAnimating)
	{
		[displayLink invalidate];
		displayLink = nil;

		isAnimating = NO;
	}
}

// to become first responder
-(BOOL)acceptFirstResponder
{
	// code
	return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{

}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{

}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];
	exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{

}

-(void)dealloc
{
    // detach vao
    if (gVao_pyramid)
    {
            glDeleteVertexArrays(1, &gVao_pyramid);
            gVao_pyramid = 0;
    }
    if (gVao_cube)
    {
            glDeleteVertexArrays(1, &gVao_cube);
            gVao_cube = 0;
    }
    if (gVbo_position)
    {
            glDeleteBuffers(1, &gVbo_position);
            gVbo_position = 0;
    }
    if (gVbo_color)
    {
            glDeleteBuffers(1, &gVbo_color);
            gVbo_color = 0;
    }

    if(colorRenderbuffer)
    {
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            colorRenderbuffer = 0;
    }

    if(defaultFramebuffer)
    {
            glDeleteFramebuffers(1,&defaultFramebuffer);
            defaultFramebuffer = 0;
    }

    if([EAGLContext currentContext] == eaglContext)
    {
            [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;

    [super dealloc];
}

@end
