//
//  GLESView.m
//  Multi Colored Triangle
//
//  Created by ASTROMEDICOMP on 17/06/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// global variables
FILE *gpFile = NULL;

@implementation GLESView
{
    EAGLContext *eaglContext;

    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;

    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;

	GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
	
    GLuint gVao_pyramid;
    GLuint gVbo_pyramid_position;
    GLuint gVbo_pyramid_texture;

    GLuint gVao_cube;
    GLuint gVbo_cube_position;
    GLuint gVbo_cube_texture;
    GLuint gMVPUniform;
    
    GLfloat anglePyramid;
    GLfloat angleCube;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
    
    GLuint gTexture_sampler_uniform;
    GLuint pyramid_texture;
    GLuint cube_texture;

}

- (id)initWithFrame:(CGRect)frame;
{
	self = [super initWithFrame:frame];

    anglePyramid = 0.0f;
    angleCube = 0.0f;
    
	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;

		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
		eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
		if(eaglContext == nil)
		{
			[self release];
			return(nil);
		}
		[EAGLContext setCurrentContext:eaglContext];

		glGenFramebuffers(1,&defaultFramebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

		glGenRenderbuffers(1,&colorRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);

		GLint backingWidth;
		GLint backingHeight;

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);

		glGenRenderbuffers(1,&depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1,&defaultFramebuffer);
			glDeleteRenderbuffers(1,&colorRenderbuffer);
			glDeleteRenderbuffers(1,&depthRenderbuffer);
			[self release];
			return(nil);
		}

		printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

		// hard coded initialization
		isAnimating = NO;
		animationFrameInterval = 60;	// default since IOS 8.2

                // *** VERTEX SHADER ***
                // create shader
                gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

                // provide source code to shader
                const GLchar *vertexShaderSourceCode =
                    "#version 300 core" \
                    "\n" \
                    "precision highp float;" \
                    "in vec4 vPosition;" \
                    "in vec2 vTexture0_Coord;" \
                    "out vec2 out_texture0_coord;" \
                    "uniform mat4 u_mvp_matrix;" \
                    "void main(void)" \
                    "{" \
                    "gl_Position = u_mvp_matrix * vPosition;" \
                    "out_texture0_coord = vTexture0_Coord;" \
                    "}";

                glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

                // compile shader
                glCompileShader(gVertexShaderObject);
                GLint iInfoLogLength = 0;
                GLint iShaderCompiledStatus = 0;
                char *szInfoLog = NULL;
                glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
                if (iShaderCompiledStatus == GL_FALSE)
                {
                        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
                        if (iInfoLogLength > 0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // *** FRAGMENT SHAER *** //
                // create fragment shader
                gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

                // provide source code to shader
                const GLchar *fragmentShaderSourceCode =
                    "#version 300 core" \
                    "\n" \
                    "precision highp float;" \
                    "in vec2 out_texture0_coord;" \
                    "out vec4 FragColor;" \
                    "uniform sampler2D u_texture0_sampler;" \
                    "void main(void)" \
                    "{" \
                    "FragColor = texture(u_texture0_sampler,out_texture0_coord);" \
                    "}";

                glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

                // compile shader
                glCompileShader(gFragmentShaderObject);
                glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
                if (iShaderCompiledStatus == GL_FALSE)
                {
                        glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
                        if (iInfoLogLength > 0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // *** SHADER PROGRAM ***
                // create program
                gShaderProgramObject = glCreateProgram();

                // attach vertex shader to shader program
                glAttachShader(gShaderProgramObject, gVertexShaderObject);

                // attach fragment shader to shader program
                glAttachShader(gShaderProgramObject, gFragmentShaderObject);

                // pre-building of shader program object with vertex shader position attribute
                glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

                // pre-link binding of shader program object with vertex shader texture attribute
                glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

                // link shader
                glLinkProgram(gShaderProgramObject);
                GLint iShaderProgramLinkStatus = 0;
                glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
                if (iShaderProgramLinkStatus == GL_FALSE)
                {
                        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
                        if (iInfoLogLength>0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                                        fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // get MVP uniform location
                gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
                gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

                    // load textures
        pyramid_texture=[self loadTextureFromBMPFile:@"Stone" :@"bmp"];
        cube_texture=[self loadTextureFromBMPFile:@"Vijay_Kundali" :@"bmp"];
                // vertices, colors, shader attribs, vbo, vao initializations
                const GLfloat pyramidVertices[] =
                {
                        // FRONT FACE
                        0.0f,1.0f,0.0f,	// apex
                        -1.0f,-1.0f,1.0f,	// left-bottom
                        1.0f,-1.0f,1.0f,	// right-bottom

                        // RIGHT FACE
                        0.0f,1.0f,0.0f,
                        1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,-1.0f,

                        // BACK FACE
                        0.0f,1.0f,0.0f,
                        1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,

                        // LEFT FACE
                        0.0f,1.0f,0.0f,
                        -1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,1.0f
                };

                const GLfloat pyramidTexcoords[] =
                {
                        // FRONT FACE
                        0.5f,1.0f,	// front-top
                        0.0f,0.0f,	// front-left
                        1.0f,0.0f,	// front-right

                        // RIGHT FACE
                        0.5f,1.0f,	// right-top
                        1.0f,0.0f,	// right-left
                        0.0f,0.0f,	// right-right

                        // BACK FACE
                        0.5f,1.0f,	// back-top
                        1.0f,0.0f,	// back-left
                        0.0f,0.0f,	// back-right

                        // LEFT FACE
                        0.5f,1.0f,	// left-top
                        0.0f,0.0f,	// left-left
                        1.0f,0.0f	// left-right
                };

                const GLfloat cubeVertices[] =
                {
                        // FRONT FACE
                        1.0f,1.0f,1.0f,
                        -1.0f,1.0f,1.0f,
                        -1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,1.0f,

                        // RIGHT FACE
                        1.0f,1.0f,-1.0f,
                        1.0f,1.0f,1.0f,
                        1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,-1.0f,

                        // BACK FACE
                        1.0f,1.0f,-1.0f,
                        -1.0f,1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,
                        1.0f,-1.0f,-1.0f,

                        // LEFT FACE
                        -1.0f,1.0f,1.0f,
                        -1.0f,1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,1.0f,

                        // TOP FACE
                        1.0f,1.0f,-1.0f,
                        -1.0f,1.0f,-1.0f,
                        -1.0f,1.0f,1.0f,
                        1.0f,1.0f,1.0f,

                        //	BOTTOM FACE
                        1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,1.0f
                };

                // If above -1.0f or +1.0f values make cube much larger than pyramid
                // then follow the code in following loop which will convert all 1 and -1 to 0.75 and -0.75
                /*for (int i = 0; i < 72; i++)
                {
                        if (cubeVertices[i] < 0.0f)
                                cubeVertices[i] = cubeVertices[i] + 0.25f;
                        else if (cubeVertices[i] > 0.0f)
                                cubeVertices[i] = cubeVertices[i] - 0.25f;
                        else
                                cubeVertices[i] = cubeVertices[i];

                }*/
                const GLfloat cubeTexcoords[] = 
                {
                        // TOP FACE
                        0.0f,0.0f,
                        1.0f,0.0f,
                        1.0f,1.0f,
                        0.0f,1.0f,

                        // BOTTOM FACE
                        0.0f,0.0f,
                        1.0f,0.0f,
                        1.0f,1.0f,
                        0.0f,1.0f,

                        // FRONT FACE
                        0.0f,0.0f,
                        1.0f,0.0f,
                        1.0f,1.0f,
                        0.0f,1.0f,

                        // BACK FACE
                        0.0f,0.0f,
                        1.0f,0.0f,
                        1.0f,1.0f,
                        0.0f,1.0f,

                        // LEFT FACE
                        0.0f,0.0f,
                        1.0f,0.0f,
                        1.0f,1.0f,
                        0.0f,1.0f,

                        // RIGHT FACE
                        0.0f,0.0f,
                        1.0f,0.0f,
                        1.0f,1.0f,
                        0.0f,1.0f,

                };

                // *************************
                // VAO FOR PYRAMID
                // *************************

                // generate and bind vao for pyramid
                glGenVertexArrays(1, &gVao_pyramid);
                glBindVertexArray(gVao_pyramid);

                // ******************
                // VBO FOR POSITION
                // ******************
                glGenBuffers(1, &gVbo_pyramid_position);
                glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
                glBindBuffer(GL_ARRAY_BUFFER, 0);

                // ******************
                // VBO FOR TEXTURE
                // ******************
                glGenBuffers(1, &gVbo_pyramid_texture);
                glBindBuffer(GL_ARRAY_BUFFER,gVbo_pyramid_texture);
                glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidTexcoords),pyramidTexcoords,GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
                glBindBuffer(GL_ARRAY_BUFFER,0);

                // unbind from vao for pyramid
                glBindVertexArray(0);

                // *************************
                // VAO FOR CUBE
                // *************************

                // generate and bind vao for cube
                glGenVertexArrays(1, &gVao_cube);
                glBindVertexArray(gVao_cube);

                // ******************
                // VBO FOR POSITION
                // ******************
                glGenBuffers(1, &gVbo_cube_position);
                glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
                glBindBuffer(GL_ARRAY_BUFFER, 0);

                // ******************
                // VBO FOR COLOR
                // ******************
                glGenBuffers(1, &gVbo_cube_texture);
                glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_texture);
                glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoords), cubeTexcoords, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
                glBindBuffer(GL_ARRAY_BUFFER, 0);

                // unbind from vao for cube
                glBindVertexArray(0);

                //glShadeModel(GL_SMOOTH);
                //glClearDepth(1.0f);
                glEnable(GL_DEPTH_TEST);
                glDepthFunc(GL_LEQUAL);
                //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
                //glEnable(GL_CULL_FACE);
    
		// clear color
		glClearColor(0.0f,0.0f,0.0f,1.0f);	// black color

		// set perspective matrix to identity matrix
    		gPerspectiveProjectionMatrix = vmath::mat4::identity();

		// GESTURE RECOGNITION
		// Tap Gesture Code
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];	// touch of 1 finger
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];

        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];	// touch of 1 finger
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];

		// this will allow to differentiate between single tap and double tap
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

		// swipe gesture
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];
		[self addGestureRecognizer:swipeGestureRecognizer];

		// long-press gesture
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return(self);
}

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath=[[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if (!bmpImage)
    {
        NSLog(@"can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage=bmpImage.CGImage;
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4, for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    // Create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTexture);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation
- (void)drawRect:(CGRect)rect
{
	// Drawing code
}
*/

+(Class)layerClass
{
	// code
	return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    printf("in drawview\n");
	// Code
	[EAGLContext setCurrentContext:eaglContext];

	glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        // start using opengl program object
        glUseProgram(gShaderProgramObject);

        // opengl drawing

        // ****************************************************
        // PYRAMID BLOCK
        // ****************************************************

        // set modelview, modelviewprojection & rotation matrices to identity
        vmath::mat4 rotationMatrix = vmath::mat4::identity();
        vmath::mat4 modelViewMatrix = vmath::mat4::identity();
        vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();

        // rotation
        rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
        // translate modelview matrix
        modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);

        // multiply model view by rotation matrix
        modelViewMatrix = modelViewMatrix * rotationMatrix;

        // multiply modelview and perspective projection matrix to get modelviewprojection matrix
        modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

        // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        // bind with pyramid texture
        glBindTexture(GL_TEXTURE_2D, pyramid_texture);

        // bind to vao of pyramid
        glBindVertexArray(gVao_pyramid);

        // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
        glDrawArrays(GL_TRIANGLES, 0, 12);	// 3(each with its x,y,z) vertices in triangleVertices array

        // unbind from vao of pyramid
        glBindVertexArray(0);

        // ****************************************************
        // CUBE BLOCK
        // ****************************************************

        // set modelview, modelviewprojection & rotation matrices to identity
        modelViewMatrix = vmath::mat4::identity();
        rotationMatrix = vmath::mat4::identity();
        vmath::mat4 scaleMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();

        // scale
        scaleMatrix = vmath::scale(0.75f,0.75f,0.75f);
        // rotation
        rotationMatrix = vmath::rotate(angleCube, angleCube, angleCube);
        //rotationMatrix = vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
        // translate modelview matrix
        modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);

        // multiply modelview matrix by rotation matrix
        modelViewMatrix = modelViewMatrix * rotationMatrix;

        // multiply modelview and perspective projection matrix to get modelviewprojection matrix
        modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

        // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //bind with cube texture
        glBindTexture(GL_TEXTURE_2D, cube_texture);

        // bind to vao of cube
        glBindVertexArray(gVao_cube);

        // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
        // actually 2 triangles make 1 square, so there should be 6 vertices
        // but as 2 triangles while making square meet each other at diagonal
        // 2 of 6 vertices are common to both triangles and hence 6-2=4
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 4(each with its x,y,z) vertices in squareVertices array
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

        // unbind from vao of cube
        glBindVertexArray(0);

        // stop using opengl program object
        glUseProgram(0);

        [self updateAngle];
        
	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
	[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)updateAngle
{
    anglePyramid = anglePyramid + 0.1f;
    if (anglePyramid >= 360)
    {
            anglePyramid = 0.0f;
    }

    angleCube = angleCube - 0.1f;
    if (angleCube <= -360)
    {
            angleCube = 0.0f;
    }
}

- (void)layoutSubviews
{
	// code
	GLint width;
	GLint height;

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
	[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);

	glGenRenderbuffers(1,&depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);

	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

	glViewport(0,0,width,height);
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}

        [self updateAngle];
	[self drawView:nil];
}

- (void)startAnimation
{
	/*if(!isAnimating)
	{*/
		displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
		[displayLink setPreferredFramesPerSecond:animationFrameInterval];
		[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

		isAnimating = YES;
	//}
}

-(void)stopAnimation
{
	if(isAnimating)
	{
		[displayLink invalidate];
		displayLink = nil;

		isAnimating = NO;
	}
}

// to become first responder
-(BOOL)acceptFirstResponder
{
	// code
	return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{

}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{

}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];
	exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{

}

-(void)dealloc
{
    // destroy pyramid vao
    if (gVao_pyramid)
    {
            glDeleteVertexArrays(1, &gVao_pyramid);
            gVao_pyramid = 0;
    }
    // destroy pyramid position vbo
    if (gVbo_pyramid_position)
    {
            glDeleteBuffers(1, &gVbo_pyramid_position);
            gVbo_pyramid_position = 0;
    }
    // destroy pyramid texture vbo
    if (gVbo_pyramid_texture)
    {
            glDeleteBuffers(1, &gVbo_pyramid_texture);
            gVbo_pyramid_texture = 0;
    }
    if (pyramid_texture)
    {
            glDeleteTextures(1, &pyramid_texture);
            pyramid_texture = 0;
    }

    // destroy cube vao
    if (gVao_cube)
    {
            glDeleteVertexArrays(1, &gVao_cube);
            gVao_cube = 0;
    }
    // destroy cube position vbo
    if (gVbo_cube_position)
    {
            glDeleteBuffers(1, &gVbo_cube_position);
            gVbo_cube_position = 0;
    }
    // destroy cube texture vbo
    if (gVbo_cube_texture)
    {
            glDeleteBuffers(1, &gVbo_cube_texture);
            gVbo_cube_texture = 0;
    }
    if (cube_texture)
    {
            glDeleteTextures(1, &cube_texture);
            cube_texture = 0;
    }

    if(colorRenderbuffer)
    {
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            colorRenderbuffer = 0;
    }

    if(defaultFramebuffer)
    {
            glDeleteFramebuffers(1,&defaultFramebuffer);
            defaultFramebuffer = 0;
    }

    if([EAGLContext currentContext] == eaglContext)
    {
            [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;

    [super dealloc];
}

@end
