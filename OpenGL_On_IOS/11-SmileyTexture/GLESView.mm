//
//  GLESView.m
//  Multi Colored Triangle
//
//  Created by ASTROMEDICOMP on 17/06/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// global variables
FILE *gpFile = NULL;

@implementation GLESView
{
    EAGLContext *eaglContext;

    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;

	GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
	
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;

    GLuint gVao;
    GLuint gVbo_position;
    GLuint gVbo_texture;

    GLuint gMVPUniform;

    vmath::mat4 gPerspectiveProjectionMatrix;

    GLuint gTexture_sampler_uniform;
    GLuint gTexture_Smiley;
}

- (id)initWithFrame:(CGRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;

		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
		eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
		if(eaglContext == nil)
		{
			[self release];
			return(nil);
		}
		[EAGLContext setCurrentContext:eaglContext];

		glGenFramebuffers(1,&defaultFramebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

		glGenRenderbuffers(1,&colorRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);

		GLint backingWidth;
		GLint backingHeight;

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);

		glGenRenderbuffers(1,&depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1,&defaultFramebuffer);
			glDeleteRenderbuffers(1,&colorRenderbuffer);
			glDeleteRenderbuffers(1,&depthRenderbuffer);
			[self release];
			return(nil);
		}

		printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

		// hard coded initialization
		isAnimating = NO;
		animationFrameInterval = 60;	// default since IOS 8.2

                // *** VERTEX SHADER ***
                // create shader
                gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

                // provide source code to shader
                const GLchar *vertexShaderSourceCode =
                    "#version 300 core" \
                    "\n" \
                    "precision highp float;" \
                    "in vec4 vPosition;" \
                    "in vec2 vTexture0_Coord;" \
                    "out vec2 out_texture0_coord;" \
                    "uniform mat4 u_mvp_matrix;" \
                    "void main(void)" \
                    "{" \
                    "gl_Position = u_mvp_matrix * vPosition;" \
                    "out_texture0_coord = vTexture0_Coord;" \
                    "}";

                glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

                // compile shader
                glCompileShader(gVertexShaderObject);
                GLint iInfoLogLength = 0;
                GLint iShaderCompiledStatus = 0;
                char *szInfoLog = NULL;
                glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
                if (iShaderCompiledStatus == GL_FALSE)
                {
                        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
                        if (iInfoLogLength > 0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                                        fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // *** FRAGMENT SHAER *** //
                // create fragment shader
                gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

                // provide source code to shader
                const GLchar *fragmentShaderSourceCode =
                    "#version 300 core" \
                    "\n" \
                    "precision highp float;" \
                    "in vec2 out_texture0_coord;" \
                    "out vec4 FragColor;" \
                    "uniform sampler2D u_texture0_sampler;" \
                    "void main(void)" \
                    "{" \
                    "FragColor = texture(u_texture0_sampler,out_texture0_coord);" \
                    "}";

                glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

                // compile shader
                glCompileShader(gFragmentShaderObject);
                glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
                if (iShaderCompiledStatus == GL_FALSE)
                {
                        glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
                        if (iInfoLogLength > 0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                                        fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // *** SHADER PROGRAM ***
                // create program
                gShaderProgramObject = glCreateProgram();

                // attach vertex shader to shader program
                glAttachShader(gShaderProgramObject, gVertexShaderObject);

                // attach fragment shader to shader program
                glAttachShader(gShaderProgramObject, gFragmentShaderObject);

                // pre-building of shader program object with vertex shader position attribute
                glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

                // pre-link binding of shader program object with vertex shader texture attribute
                glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

                // link shader
                glLinkProgram(gShaderProgramObject);
                GLint iShaderProgramLinkStatus = 0;
                glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
                if (iShaderProgramLinkStatus == GL_FALSE)
                {
                        glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
                        if (iInfoLogLength>0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                                        fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // get MVP uniform location
                gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
                gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

                // load textures
                //gTexture_Smiley = [self loadTextureFromBMPFile:"Smiley.bmp"];
                gTexture_Smiley = [self loadTextureFromBMPFile:@"Smiley" :@"bmp"];
        
                // vertices, colors, shader attribs, vbo, vao initializations
                const GLfloat quadVertices[] =
                {
                        1.0f,1.0f,0.0f,
                        -1.0f,1.0f,0.0f,
                        1.0f,-1.0f,0.0f,
                        -1.0f,-1.0f,0.0f,
                };

                const GLfloat quadTexcoords[] =
                {
                        1.0f,1.0f,
                        0.0f,1.0f,
                    1.0f,0.0f,
                        0.0f,0.0f,
                    
                };

                // *************************
                // VAO FOR PYRAMID
                // *************************

                // generate and bind vao for quad
                glGenVertexArrays(1, &gVao);
                glBindVertexArray(gVao);

                // ******************
                // VBO FOR POSITION
                // ******************
                glGenBuffers(1, &gVbo_position);
                glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
                glBindBuffer(GL_ARRAY_BUFFER, 0);

                // ******************
                // VBO FOR TEXTURE
                // ******************
                glGenBuffers(1, &gVbo_texture);
                glBindBuffer(GL_ARRAY_BUFFER,gVbo_texture);
                glBufferData(GL_ARRAY_BUFFER,sizeof(quadTexcoords),quadTexcoords,GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
                glBindBuffer(GL_ARRAY_BUFFER,0);

                // unbind from vao for quad
                glBindVertexArray(0);

                //glShadeModel(GL_SMOOTH);
                //glClearDepth(1.0f);
                glEnable(GL_DEPTH_TEST);
                glDepthFunc(GL_LEQUAL);
                //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
                //glEnable(GL_CULL_FACE);
    
		// clear color
		glClearColor(0.0f,0.0f,0.0f,1.0f);	// black color

		// set perspective matrix to identity matrix
    		gPerspectiveProjectionMatrix = vmath::mat4::identity();

		// GESTURE RECOGNITION
		// Tap Gesture Code
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];	// touch of 1 finger
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];

        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];	// touch of 1 finger
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];

		// this will allow to differentiate between single tap and double tap
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

		// swipe gesture
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];
		[self addGestureRecognizer:swipeGestureRecognizer];

		// long-press gesture
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return(self);
}

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath=[[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if (!bmpImage)
    {
        NSLog(@"can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage=bmpImage.CGImage;
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4, for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    // Create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTexture);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation
- (void)drawRect:(CGRect)rect
{
	// Drawing code
}
*/

+(Class)layerClass
{
	// code
	return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
	// Code
	[EAGLContext setCurrentContext:eaglContext];

	glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        // start using opengl program object
        glUseProgram(gShaderProgramObject);

        // opengl drawing

        // ****************************************************
        // PYRAMID BLOCK
        // ****************************************************

        // set modelview, modelviewprojection & rotation matrices to identity
        vmath::mat4 modelViewMatrix = vmath::mat4::identity();
        vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();

        // translate modelview matrix
        modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

        // multiply modelview and perspective projection matrix to get modelviewprojection matrix
        modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

        // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        // bind with quad texture
        glActiveTexture(GL_TEXTURE0);	// 0th texture (corrospond to VDG_ATTRIBUTE_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
        glUniform1i(gTexture_sampler_uniform, 0);	// 0th sampler enable (as we have only 1 texture sampler in fragment shader)

        // bind to vao of quad
        glBindVertexArray(gVao);

        // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
        //glDrawArrays(GL_QUADS, 0, 4);	// 3(each with its x,y,z) vertices in triangleVertices array
    
        glDrawArrays(GL_TRIANGLE_FAN,0,3);
        glDrawArrays(GL_TRIANGLE_FAN,1,3);

        // unbind from vao of quad
        glBindVertexArray(0);

        // stop using opengl program object
        glUseProgram(0);
        
	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
	[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)layoutSubviews
{
	// code
	GLint width;
	GLint height;

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
	[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);

	glGenRenderbuffers(1,&depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);

	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

	glViewport(0,0,width,height);
        gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}
    
	[self drawView:nil];
}

- (void)startAnimation
{
	if(!isAnimating)
	{
		displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
		[displayLink setPreferredFramesPerSecond:animationFrameInterval];
		[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];

		isAnimating = YES;
	}
}

-(void)stopAnimation
{
	if(isAnimating)
	{
		[displayLink invalidate];
		displayLink = nil;

		isAnimating = NO;
	}
}

// to become first responder
-(BOOL)acceptFirstResponder
{
	// code
	return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{

}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{

}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];
	exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{

}

-(void)dealloc
{
    // destroy vao
    if (gVao)
    {
            glDeleteVertexArrays(1, &gVao);
            gVao = 0;
    }
    // destroy position vbo
    if (gVbo_position)
    {
            glDeleteBuffers(1, &gVbo_position);
            gVbo_position = 0;
    }
    // destroy texture vbo
    if (gVbo_texture)
    {
            glDeleteBuffers(1, &gVbo_texture);
            gVbo_texture = 0;
    }
    if (gTexture_Smiley)
    {
            glDeleteTextures(1, &gTexture_Smiley);
            gTexture_Smiley = 0;
    }

    if(colorRenderbuffer)
    {
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            colorRenderbuffer = 0;
    }

    if(defaultFramebuffer)
    {
            glDeleteFramebuffers(1,&defaultFramebuffer);
            defaultFramebuffer = 0;
    }

    if([EAGLContext currentContext] == eaglContext)
    {
            [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;

    [super dealloc];
}

@end
