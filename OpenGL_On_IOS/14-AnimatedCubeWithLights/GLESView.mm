//
//  GLESView.m
//  Multi Colored Triangle
//
//  Created by ASTROMEDICOMP on 17/06/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

@implementation GLESView
{
    EAGLContext *eaglContext;

    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;

	GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
	
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;

    GLuint gVao_cube;
    GLuint gVbo_cube_position;
    GLuint gVbo_cube_normal;

    GLuint gModelViewMatrixUniform, gProjectionMatrixUniform;
    GLuint gLdUniform, gKdUniform, gLightPositionUniform;

    GLuint gLKeyPressedUniform;

    vmath::mat4 gPerspectiveProjectionMatrix;

    GLfloat gAngle;

    BOOL gbAnimate;
    BOOL gbLight;
}

- (id)initWithFrame:(CGRect)frame;
{
	self = [super initWithFrame:frame];

	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;

		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
		eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
		if(eaglContext == nil)
		{
			[self release];
			return(nil);
		}
		[EAGLContext setCurrentContext:eaglContext];

		glGenFramebuffers(1,&defaultFramebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

		glGenRenderbuffers(1,&colorRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);

		GLint backingWidth;
		GLint backingHeight;

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);

		glGenRenderbuffers(1,&depthRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
			glDeleteFramebuffers(1,&defaultFramebuffer);
			glDeleteRenderbuffers(1,&colorRenderbuffer);
			glDeleteRenderbuffers(1,&depthRenderbuffer);
			[self release];
			return(nil);
		}

		printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

		// hard coded initialization
		isAnimating = NO;
		animationFrameInterval = 60;	// default since IOS 8.2

                // *** VERTEX SHADER ***
                // create shader
                gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

                // provide source code to shader
                const GLchar *vertexShaderSourceCode =
                    "#version 300 core" \
                    "\n" \
                    "in vec4 vPosition;" \
                    "in vec3 vNormal;" \
                    "uniform mat4 u_model_view_matrix;" \
                    "uniform mat4 u_projection_matrix;" \
                    "uniform int u_LKeyPressed;" \
                    "uniform vec3 u_Ld;" \
                    "uniform vec3 u_Kd;" \
                    "uniform vec4 u_light_position;" \
                    "out vec3 diffuse_light;" \
                    "void main(void)" \
                    "{" \
                    "if(u_LKeyPressed == 1)" \
                    "{" \
                    "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
                    "vec3 tnorm = normalize(mat3(u_model_view_matrix) * vNormal);" \
                    "vec3 s = normalize(vec3(u_light_position - eyeCoordinates));" \
                    "diffuse_light = u_Ld * u_Kd * max(dot(s,tnorm),0.0);" \
                    "}" \
                    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
                    "}";

                glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

                // compile shader
                glCompileShader(gVertexShaderObject);
                GLint iInfoLogLength = 0;
                GLint iShaderCompiledStatus = 0;
                char *szInfoLog = NULL;
                glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
                if (iShaderCompiledStatus == GL_FALSE)
                {
                        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
                        if (iInfoLogLength > 0)
                        {
                                szInfoLog = (char *)malloc(iInfoLogLength);
                                if (szInfoLog != NULL)
                                {
                                        GLsizei written;
                                        glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                                        printf("vs szInfoLog : %s\n",szInfoLog);
                                        free(szInfoLog);
                                        [self release];
                                        exit(0);
                                }
                        }
                }

                // *** FRAGMENT SHAER *** //
                // create fragment shader
                gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

                // provide source code to shader
                const GLchar *fragmentShaderSourceCode =
                    "#version 300 core" \
                    "\n" \
                    "precision highp float;" \
                    "in vec3 diffuse_light;" \
                    "out vec4 FragColor;" \
                    "uniform highp int u_LKeyPressed;" \
                    "void main(void)" \
                    "{" \
                    "vec4 color;" \
                    "if(u_LKeyPressed == 1)" \
                    "{" \
                    "color = vec4(diffuse_light,1.0);" \
                    "}" \
                    "else" \
                    "{" \
                    "color = vec4(1.0,1.0,1.0,1.0);" \
                    "}" \
                    "FragColor = color;" \
                    "}";

                glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
                // compile shader
                glCompileShader(gFragmentShaderObject);
                glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
                if (iShaderCompiledStatus == GL_FALSE)
                {
                    glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
                    if (iInfoLogLength > 0)
                    {
                        szInfoLog = (char *)malloc(iInfoLogLength);
                        if (szInfoLog != NULL)
                        {
                            GLsizei written;
                            glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                            printf("fs szInfoLog : %s\n",szInfoLog);
                            free(szInfoLog);
                            exit(0);
                        }
                    }
                }

                // *** SHADER PROGRAM ***
                // create program
                gShaderProgramObject = glCreateProgram();
        
                // attach vertex shader to shader program
                glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
                // attach fragment shader to shader program
                glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
                // pre-link binding of shader program object with vertex shader position attribute
                glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
                // pre-link binding of shader program object with vertex shader texture attribute
                glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
                // link shader
                glLinkProgram(gShaderProgramObject);
                GLint iShaderProgramLinkStatus = 0;
                glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
                if (iShaderProgramLinkStatus == GL_FALSE)
                {
                    glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
                    if (iInfoLogLength>0)
                    {
                        szInfoLog = (char *)malloc(iInfoLogLength);
                        if (szInfoLog != NULL)
                        {
                            GLsizei written;
                            glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                            printf("link szInfoLog : %s\n",szInfoLog);
                            free(szInfoLog);
                            exit(0);
                        }
                    }
                }

                // get uniform location
                gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_view_matrix");
                gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
                gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject,"u_LKeyPressed");
                gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
                gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
                gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

                // vertices, colors, shader attribs, vbo, vao initializations
                GLfloat cubeVertices[] =
                {
                        1.0f,1.0f,-1.0f,
                        -1.0f,1.0f,-1.0f,
                        -1.0f,1.0f,1.0f,
                        1.0f,1.0f,1.0f,

                        1.0f,-1.0f,1.0f,
                        -1.0f,-1.0f,1.0f,
                        -1.0f,-1.0f,-1.0f,
                        1.0f,-1.0f,-1.0f,

                        1.0f,1.0f,1.0f,
                        -1.0f,1.0f,1.0f,
                        -1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,1.0f,

                        1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,
                        -1.0f,1.0f,-1.0f,
                        1.0f,1.0f,-1.0f,

                        -1.0f,1.0f,1.0f,
                        -1.0f,1.0f,-1.0f,
                        -1.0f,-1.0f,-1.0f,
                        -1.0f,-1.0f,1.0f,

                        1.0f,1.0f,-1.0f,
                        1.0f,1.0f,1.0f,
                        1.0f,-1.0f,1.0f,
                        1.0f,-1.0f,-1.0f,
                };

                // If above -1.0f or +1.0f values make cube much larger than pyramid
                // then follow the code in following loop which will convert all 1 and -1 to 0.75 and -0.75
                for (int i = 0; i < 72; i++)
                {
                        if (cubeVertices[i] < 0.0f)
                        cubeVertices[i] = cubeVertices[i] + 0.25f;
                        else if (cubeVertices[i] > 0.0f)
                        cubeVertices[i] = cubeVertices[i] - 0.25f;
                        else
                        cubeVertices[i] = cubeVertices[i];

                }

                const GLfloat cubeNormals[] =
                {
                        0.0f,1.0f,0.0f,
                        0.0f,1.0f,0.0f,
                        0.0f,1.0f,0.0f,
                        0.0f,1.0f,0.0f,

                        0.0f,-1.0f,0.0f,
                        0.0f,-1.0f,0.0f,
                        0.0f,-1.0f,0.0f,
                        0.0f,-1.0f,0.0f,

                        0.0f,0.0f,1.0f,
                        0.0f,0.0f,1.0f,
                        0.0f,0.0f,1.0f,
                        0.0f,0.0f,1.0f,

                        0.0f,0.0f,-1.0f,
                        0.0f,0.0f,-1.0f,
                        0.0f,0.0f,-1.0f,
                        0.0f,0.0f,-1.0f,

                        -1.0f,0.0f,0.0f,
                        -1.0f,0.0f,0.0f,
                        -1.0f,0.0f,0.0f,
                        -1.0f,0.0f,0.0f,

                        1.0f,0.0f,0.0f,
                        1.0f,0.0f,0.0f,
                        1.0f,0.0f,0.0f,
                        1.0f,0.0f,0.0f,
                };

                // *************************
                // VAO FOR QUAD
                // *************************

                // generate and bind vao for quad
                glGenVertexArrays(1, &gVao_cube);
                glBindVertexArray(gVao_cube);

                // ******************
                // VBO FOR POSITION
                // ******************
                glGenBuffers(1, &gVbo_cube_position);
                glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
                glBindBuffer(GL_ARRAY_BUFFER, 0);

                // ******************
                // VBO FOR NORMAL
                // ******************
                glGenBuffers(1, &gVbo_cube_normal);
                glBindBuffer(GL_ARRAY_BUFFER,gVbo_cube_normal);
                glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
                glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
                glBindBuffer(GL_ARRAY_BUFFER,0);

                // unbind from vao for cube
                glBindVertexArray(0);

                //glShadeModel(GL_SMOOTH);
                //glClearDepth(1.0f);
                glEnable(GL_DEPTH_TEST);
                glDepthFunc(GL_LEQUAL);
                //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
                //glEnable(GL_CULL_FACE);
    
		// clear color
		glClearColor(0.0f,0.0f,0.0f,1.0f);	// black color

		// set perspective matrix to identity matrix
    		gPerspectiveProjectionMatrix = vmath::mat4::identity();

                gbAnimate = NO;
                gbLight = NO;
    
		// GESTURE RECOGNITION
		// Tap Gesture Code
		UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
		[singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];	// touch of 1 finger
		[singleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:singleTapGestureRecognizer];

        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
		[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
		[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];	// touch of 1 finger
		[doubleTapGestureRecognizer setDelegate:self];
		[self addGestureRecognizer:doubleTapGestureRecognizer];

		// this will allow to differentiate between single tap and double tap
		[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

		// swipe gesture
		UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
		[self addGestureRecognizer:swipeGestureRecognizer];
		[self addGestureRecognizer:swipeGestureRecognizer];

		// long-press gesture
		UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
		[self addGestureRecognizer:longPressGestureRecognizer];
	}
	return(self);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation
- (void)drawRect:(CGRect)rect
{
	// Drawing code
}
*/

+(Class)layerClass
{
	// code
	return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
	// Code
	[EAGLContext setCurrentContext:eaglContext];

	//glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        // start using opengl program object
        glUseProgram(gShaderProgramObject);

        // opengl drawing

        if (gbLight)
        {
                glUniform1i(gLKeyPressedUniform, 1);

                glUniform3f(gLdUniform, 1.0f, 1.0f, 1.0f);
                glUniform3f(gKdUniform, 0.5f, 0.5f, 0.5f);

                float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
                glUniform4fv(gLightPositionUniform, 1, (GLfloat *)lightPosition);
        }
        else
        {
                glUniform1i(gLKeyPressedUniform, 0);
        }

        // Opengl drawing
        // set model, modelview, rotation matrices to identity
        vmath::mat4 modelMatrix = vmath::mat4::identity();
        vmath::mat4 modelViewMatrix = vmath::mat4::identity();
        vmath::mat4 rotationMatrix = vmath::mat4::identity();

        // apply z axis translation to go deep into the screen by -5.0
        // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
        modelMatrix = vmath::translate(0.0f, 0.0f, -5.0f);

        // all axis rotation by gAngle angle
        rotationMatrix = vmath::rotate(gAngle, gAngle, gAngle);

        // multiply rotation matrix and model matrix to get modelview matrix
        modelViewMatrix = modelMatrix * rotationMatrix;	// ORDER IS IMPORTANT

        // pass above modelview matrix to the vertex shader in "u_model_view_matrix" shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);

        // pass projection matrix to the vertex shader in 'u_projection_matrix' shader variable
        // whose position we already calculated in initialiize() by using glGetUniformLocation()
        glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

        // bind to vao of cube
        glBindVertexArray(gVao_cube);

        // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 3(each with its x,y,z) vertices in triangleVertices array
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

        // unbind from vao of quad
        glBindVertexArray(0);

        // stop using opengl program object
        glUseProgram(0);
    
    printf("gbAnimate is %d\n",gbAnimate);
    if (gbAnimate == 1)
    {
        //printf("gbAnimate is true\n");
        [self updateAngle];
    }
    
	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
	[eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)updateAngle
{
    gAngle = gAngle + 3.0f;
    if (gAngle >= 360.0f)
            gAngle = gAngle - 360.0f;
    //printf("gAngle : %f\n",gAngle);
}

- (void)layoutSubviews
{
	// code
	GLint width;
	GLint height;

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
	[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);

	glGenRenderbuffers(1,&depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);

	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

	glViewport(0,0,width,height);
        gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}

        if (gbAnimate)
        {
            [self updateAngle];
        }
	[self drawView:nil];
}

- (void)startAnimation
{
	if(!isAnimating)
	{
		displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
		[displayLink setPreferredFramesPerSecond:animationFrameInterval];
		[displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];

		isAnimating = YES;
	}
}

-(void)stopAnimation
{
	if(isAnimating)
	{
		[displayLink invalidate];
		displayLink = nil;

		isAnimating = NO;
	}
}

// to become first responder
-(BOOL)acceptFirstResponder
{
	// code
	return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(!gbAnimate)
        gbAnimate = YES;
    else if(gbAnimate)
        gbAnimate = NO;
    //printf("gbAnimate : %d\n",gbAnimate);
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if(!gbLight)
        gbLight = YES;
    else if(gbLight)
        gbLight = NO;
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];
	exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{

}

-(void)dealloc
{
    // destroy vao
    if (gVao_cube)
    {
            glDeleteVertexArrays(1, &gVao_cube);
            gVao_cube = 0;
    }
    // destroy position vbo
    if (gVbo_cube_position)
    {
            glDeleteBuffers(1, &gVbo_cube_position);
            gVbo_cube_position = 0;
    }
    // destroy normal vbo
    if (gVbo_cube_normal)
    {
            glDeleteBuffers(1, &gVbo_cube_normal);
            gVbo_cube_normal = 0;
    }

    if(colorRenderbuffer)
    {
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            colorRenderbuffer = 0;
    }

    if(defaultFramebuffer)
    {
            glDeleteFramebuffers(1,&defaultFramebuffer);
            defaultFramebuffer = 0;
    }

    if([EAGLContext currentContext] == eaglContext)
    {
            [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;

    [super dealloc];
}

@end
