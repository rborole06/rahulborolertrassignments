//
//  GLESView.m
//  Multi Colored Triangle
//
//  Created by ASTROMEDICOMP on 17/06/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat light_1_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Specular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Position[] = { 2.0f,0.8f,1.0f,0.0f };

GLfloat light_2_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_2_Diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_2_Specular[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_2_Position[] = { -2.0f,0.8f,1.0f,0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint gVao_pyramid;
    GLuint gVbo_pyramid_position;
    GLuint gVbo_pyramid_normal;
    GLuint gVbo_pyramid_element;
    
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    
    GLuint L_KeyPressed_uniform;
    
    GLuint La_1_uniform;
    GLuint Ld_1_uniform;
    GLuint Ls_1_uniform;
    
    GLuint La_2_uniform;
    GLuint Ld_2_uniform;
    GLuint Ls_2_uniform;
    
    GLuint light_1_position_uniform;
    GLuint light_2_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
    
    GLfloat anglePyramid;
    bool gbAnimate;
    bool gbLight;
}

- (id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    
    anglePyramid = 0.0f;
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            [self release];
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initialization
        isAnimating = NO;
        animationFrameInterval = 60;    // default since IOS 8.2
        
        // *** VERTEX SHADER ***
        // create shader
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light_1_position;" \
        "uniform vec4 u_light_2_position;" \
        "uniform mediump int u_lighting_enabled;" \
        "out vec3 transformed_normals;" \
        "out vec3 light_1_direction;" \
        "out vec3 light_2_direction;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "if(u_lighting_enabled == 1)" \
        "{" \
        "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
        "transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
        "light_1_direction = vec3(u_light_1_position) - eyeCoordinates.xyz;" \
        "light_2_direction = vec3(u_light_2_position) - eyeCoordinates.xyz;" \
        "viewer_vector = -eyeCoordinates.xyz;" \
        "}" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gVertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // *** FRAGMENT SHAER *** //
        // create fragment shader
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 transformed_normals;" \
        "in vec3 light_1_direction;" \
        "in vec3 light_2_direction;" \
        "in vec3 viewer_vector;" \
        "out vec4 FragColor;" \
        "uniform vec3 u_La_1;" \
        "uniform vec3 u_Ld_1;" \
        "uniform vec3 u_Ls_1;" \
        "uniform vec3 u_La_2;" \
        "uniform vec3 u_Ld_2;" \
        "uniform vec3 u_Ls_2;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform int u_lighting_enabled;" \
        "void main(void)" \
        "{" \
        "vec3 phong_ads_color;" \
        "if(u_lighting_enabled == 1)" \
        "{" \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
        "vec3 normalized_light_1_direction = normalize(light_1_direction);" \
        "vec3 normalized_light_2_direction = normalize(light_2_direction);" \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
        "vec3 ambient_1 = u_La_1 * u_Ka;" \
        "vec3 ambient_2 = u_La_2 * u_Ka;" \
        "float tn1_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_1_direction),0.0);" \
        "float tn2_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_2_direction),0.0);" \
        "vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;" \
        "vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;" \
        "vec3 reflection_vector_1 = reflect(-normalized_light_1_direction,normalized_transformed_normals);" \
        "vec3 reflection_vector_2 = reflect(-normalized_light_2_direction,normalized_transformed_normals);" \
        "vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,normalized_viewer_vector),0.0),u_material_shininess);" \
        "vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,normalized_viewer_vector),0.0),u_material_shininess);" \
        "phong_ads_color = (ambient_1+ambient_2) + (diffuse_1+diffuse_2) + (specular_1+specular_2);" \
        "}"\
        "else" \
        "{" \
        "phong_ads_color = vec3(1.0,1.0,1.0);" \
        "}" \
        "FragColor = vec4(phong_ads_color,1.0);" \
        "}";
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gFragmentShaderObject);
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create program
        gShaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
        // pre-link binding of shader program object with vertex shader texture attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(gShaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // get uniform location
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
        
        // L or l key pressed or not
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
        
        // ambient color intensity of light
        La_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_1");
        // diffuse color intensity of light
        Ld_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_1");
        // specular color intensity of light
        Ls_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_1");
        // position of light
        light_1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_1_position");
        
        // ambient color intensity of light
        La_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_2");
        // diffuse color intensity of light
        Ld_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_2");
        // specular color intensity of light
        Ls_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_2");
        // position of light
        light_2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_2_position");
        
        // amient reflective color intensity of light
        Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
        // diffuse reflective color intensity of light
        Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
        // specular reflective color intensity of light
        Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
        // shininess of material (value is conventionally between 0 to 200)
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
        
        // *** vertices, colors, shader attribs, vbo, vao initialization *** //
        const GLfloat pyramidVertices[] =
        {
            // FRONT FACE
            0.0f,1.0f,0.0f,    // apex
            -1.0f,-1.0f,1.0f,    // left-bottom
            1.0f,-1.0f,1.0f,    // right-bottom
            
            // RIGHT FACE
            0.0f,1.0f,0.0f,
            1.0f,-1.0f,1.0f,
            1.0f,-1.0f,-1.0f,
            
            // BACK FACE
            0.0f,1.0f,0.0f,
            1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            
            // LEFT FACE
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f
        };
        
        const GLfloat pyramidNormals[] =
        {
            0.0f,0.447214f,0.894427f,    // normal for front face
            0.0f,0.447214f,0.894427f,
            0.0f,0.447214f,0.894427f,
            
            0.894427f,0.447214f,0.0f,    // normal for right face
            0.894427f,0.447214f,0.0f,
            0.894427f,0.447214f,0.0f,
            
            0.0f,0.447214f,-0.894427f,    // normal for back face
            0.0f,0.447214f,-0.894427f,
            0.0f,0.447214f,-0.894427f,
            
            -0.894427f,0.447214f,0.0f,    // normal for left face
            -0.894427f,0.447214f,0.0f,
            -0.894427f,0.447214f,0.0f
        };
        
        // *************************
        // VAO FOR PYRAMID
        // *************************
        
        // generate and bind vao for pyramid
        glGenVertexArrays(1, &gVao_pyramid);
        glBindVertexArray(gVao_pyramid);
        
        // ******************
        // VBO FOR POSITION
        // ******************
        glGenBuffers(1, &gVbo_pyramid_position);
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // ******************
        // VBO FOR NORMAL
        // ******************
        glGenBuffers(1, &gVbo_pyramid_normal);
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // unbind from vao for pyramid
        glBindVertexArray(0);
        
        //glShadeModel(GL_SMOOTH);
        //glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_CULL_FACE);
        
        // clear color
        glClearColor(0.0f,0.0f,0.0f,1.0f);    // black color
        
        // set perspective matrix to identity matrix
        gPerspectiveProjectionMatrix = vmath::mat4::identity();
        
        gbLight = false;
        gbAnimate = false;
        
        // GESTURE RECOGNITION
        // Tap Gesture Code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

- (void)updateAngle
{
    anglePyramid = anglePyramid + 1.0f;
    if (anglePyramid >= 360)
    {
        anglePyramid = 0.0f;
    }
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    // Code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using opengl program object
    glUseProgram(gShaderProgramObject);
    
    // opengl drawing
    
    if (gbLight == true)
    {
        // set u_lighting_enabled uniform
        glUniform1i(L_KeyPressed_uniform, 1);
        
        // setting light properties
        glUniform3fv(La_1_uniform, 1, light_1_Ambient);
        glUniform3fv(Ld_1_uniform, 1, light_1_Diffuse);
        glUniform3fv(Ls_1_uniform, 1, light_1_Specular);
        glUniform4fv(light_1_position_uniform, 1, light_1_Position);
        
        glUniform3fv(La_2_uniform, 1, light_2_Ambient);
        glUniform3fv(Ld_2_uniform, 1, light_2_Diffuse);
        glUniform3fv(Ls_2_uniform, 1, light_2_Specular);
        glUniform4fv(light_2_position_uniform, 1, light_2_Position);
        
        // setting materials properties
        glUniform3fv(Ka_uniform, 1, material_ambient);
        glUniform3fv(Kd_uniform, 1, material_diffuse);
        glUniform3fv(Ks_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);
    }
    else
    {
        // set u_lighting_enabled uniform
        glUniform1i(L_KeyPressed_uniform, 0);
    }
    
    // Opengl drawing
    // set model, modelview, rotation matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    // rotation
    rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
    
    // apply z axis translation to go deep into the screen by -2.0
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    modelMatrix = modelMatrix * rotationMatrix;
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
    
    // bind to vao of pyramid
    glBindVertexArray(gVao_pyramid);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glDrawArrays(GL_TRIANGLES, 0, 12);    // 3(each with its x,y,z) vertices in triangleVertices array
    
    // unbind from vao of pyramid
    glBindVertexArray(0);
    
    // stop using opengl program object
    glUseProgram(0);
    
    [self updateAngle];
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
        [self updateAngle];
    [self drawView:nil];
}

- (void)startAnimation
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

// to become first responder
-(BOOL)acceptFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(gbAnimate == false)
        gbAnimate = true;
    else if(gbAnimate == true)
        gbAnimate = false;
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if(gbLight == false)
        gbLight = true;
    else if(gbLight == true)
        gbLight = false;
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

-(void)dealloc
{
    // destroy vao
    if (gVao_pyramid)
    {
        glDeleteVertexArrays(1, &gVao_pyramid);
        gVao_pyramid = 0;
    }
    // destroy position vbo
    if (gVbo_pyramid_position)
    {
        glDeleteBuffers(1, &gVbo_pyramid_position);
        gVbo_pyramid_position = 0;
    }
    // destroy normal vbo
    if (gVbo_pyramid_normal)
    {
        glDeleteBuffers(1, &gVbo_pyramid_normal);
        gVbo_pyramid_normal = 0;
    }
    // destroy element vbo
    if (gVbo_pyramid_element)
    {
        glDeleteBuffers(1, &gVbo_pyramid_element);
        gVbo_pyramid_element = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}

@end
