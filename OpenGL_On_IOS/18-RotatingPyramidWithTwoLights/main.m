//
//  main.m
//  RotatingPyramidWithTwoLights
//
//  Created by Pratik Kamble on 14/07/18.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
