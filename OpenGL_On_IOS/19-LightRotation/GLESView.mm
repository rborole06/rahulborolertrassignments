//
//  GLESView.m
//  Multi Colored Triangle
//
//  Created by ASTROMEDICOMP on 17/06/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat light_1_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Specular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat light_2_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_2_Diffuse[] = { 0.0f,1.0f,0.0f,0.0f };
GLfloat light_2_Specular[] = { 0.0f,1.0f,0.0f,0.0f };
GLfloat light_2_Position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat light_3_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_3_Diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_3_Specular[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_3_Position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    unsigned short *elements;
    float *verts;
    float *norms;
    float *texCoords;
    
    unsigned int numElements;
    unsigned int numVertices;
    unsigned int maxElements;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    GLuint vao;
    GLuint vbo_position;
    GLuint vbo_normal;
    GLuint vbo_texture;
    GLuint vbo_index;
    
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    
    GLuint L_KeyPressed_uniform;
    GLuint V_KeyPressed_uniform;
    
    GLuint La_1_uniform;
    GLuint Ld_1_uniform;
    GLuint Ls_1_uniform;
    
    GLuint La_2_uniform;
    GLuint Ld_2_uniform;
    GLuint Ls_2_uniform;
    
    GLuint La_3_uniform;
    GLuint Ld_3_uniform;
    GLuint Ls_3_uniform;
    
    GLuint light_1_position_uniform;
    GLuint light_2_position_uniform;
    GLuint light_3_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
    
    GLfloat anglePyramid;
    bool gbAnimate;
    bool gbLight;
    
    // color angles
    GLfloat angleRedLight;
    GLfloat angleGreenLight;
    GLfloat angleBlueLight;
    
    // If 1, then per vertex lighting
    // If 0, per fragment lighting
    GLuint perVertexLighting;
}

- (id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    
    // color angles
    angleRedLight = 0.0f;
    angleGreenLight = 0.0f;
    angleBlueLight = 0.0f;
    
    perVertexLighting = 1;
    
    // color angles
    angleRedLight = 0.0f;
    angleGreenLight = 0.0f;
    angleBlueLight = 0.0f;
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            [self release];
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initialization
        isAnimating = NO;
        animationFrameInterval = 60;    // default since IOS 8.2
        
        // *** VERTEX SHADER ***
        // create shader
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // vPosition - position vertices for sphere
        // vNormal - normal coordinates
        // u_model_matrix - model matrix
        // u_view_matrix - view matrix
        // u_projection_matrix - projection matrix
        // u_lighting_enabled - flag to check if lighting is enabled
        // u_La - Light ambient component
        // u_Ld - light diffuse component
        // u_Ls - light specular component
        // u_light_position - light position coordinates
        // u_Ka - light ambient material component
        // u_Kd - light diffuse material component
        // u_Ks - light specular material component
        // u_material_shininess - material shininess
        // phong_ads_color - phong lighting model ambient diffuse specular color
        // viewer_vector - opposite of where eye is looking
        // ambient(Ia) - La * Ka
        // diffuse(Id) - Ld * Kd * (light direction * normals)
        // specular(Is) - Ls * Ks * pow((reflection,view_vector),power)
        // phong_ads_color(I) - Ia + Id + Is
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mediump int u_v_key_pressed;"\
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light_1_position;" \
        "uniform vec4 u_light_2_position;" \
        "uniform vec4 u_light_3_position;" \
        "uniform mediump int u_lighting_enabled;" \
        "uniform vec3 u_La_1;" \
        "uniform vec3 u_Ld_1;" \
        "uniform vec3 u_Ls_1;" \
        "uniform vec3 u_La_2;" \
        "uniform vec3 u_Ld_2;" \
        "uniform vec3 u_Ls_2;" \
        "uniform vec3 u_La_3;" \
        "uniform vec3 u_Ld_3;" \
        "uniform vec3 u_Ls_3;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "out vec3 transformed_normals;" \
        "out vec3 light_1_direction;" \
        "out vec3 light_2_direction;" \
        "out vec3 light_3_direction;" \
        "out vec3 viewer_vector;" \
        "out vec3 v_phong_ads_color;"\
        "void main(void)" \
        "{" \
        "if(u_lighting_enabled == 1)" \
        "{" \
        "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
        "if(u_v_key_pressed == 1)" \
        "{" \
        "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
        "vec3 light_1_direction = normalize(vec3(u_light_1_position) - eyeCoordinates.xyz);" \
        "vec3 light_2_direction = normalize(vec3(u_light_2_position) - eyeCoordinates.xyz);" \
        "vec3 light_3_direction = normalize(vec3(u_light_3_position) - eyeCoordinates.xyz);" \
        "float tn1_dot_ld1 = max(dot(transformed_normals,light_1_direction),0.0);" \
        "float tn2_dot_ld2 = max(dot(transformed_normals,light_2_direction),0.0);" \
        "float tn3_dot_ld3 = max(dot(transformed_normals,light_3_direction),0.0);" \
        "vec3 ambient_1 = u_La_1 * u_Ka;" \
        "vec3 ambient_2 = u_La_2 * u_Ka;" \
        "vec3 ambient_3 = u_La_3 * u_Ka;" \
        "vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;" \
        "vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;" \
        "vec3 diffuse_3 = u_Ld_3 * u_Kd * tn3_dot_ld3;" \
        "vec3 reflection_vector_1 = reflect(-light_1_direction,transformed_normals);" \
        "vec3 reflection_vector_2 = reflect(-light_2_direction,transformed_normals);" \
        "vec3 reflection_vector_3 = reflect(-light_3_direction,transformed_normals);" \
        "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
        "vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,viewer_vector),0.0),u_material_shininess);" \
        "vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,viewer_vector),0.0),u_material_shininess);" \
        "vec3 specular_3 = u_Ls_3 * u_Ks * pow(max(dot(reflection_vector_3,viewer_vector),0.0),u_material_shininess);" \
        "v_phong_ads_color = ambient_1 + ambient_2 + ambient_3 + diffuse_1 + diffuse_2 + diffuse_3 + specular_1 + specular_2 + specular_3;" \
        "}" \
        "else" \
        "{" \
        "transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
        "light_1_direction = vec3(u_light_1_position) - eyeCoordinates.xyz;" \
        "light_2_direction = vec3(u_light_2_position) - eyeCoordinates.xyz;" \
        "light_3_direction = vec3(u_light_3_position) - eyeCoordinates.xyz;" \
        "viewer_vector = -eyeCoordinates.xyz;" \
        "}"\
        "}" \
        "else" \
        "{" \
        "v_phong_ads_color = vec3(1.0,1.0,1.0);" \
        "}" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gVertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("vertex shader - %s",szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // *** FRAGMENT SHADER *** //
        // create fragment shader
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 v_phong_ads_color;"\
        "in vec3 transformed_normals;" \
        "in vec3 light_1_direction;" \
        "in vec3 light_2_direction;" \
        "in vec3 light_3_direction;" \
        "in vec3 viewer_vector;" \
        "out vec4 FragColor;" \
        "uniform int u_v_key_pressed;"\
        "uniform vec3 u_La_1;" \
        "uniform vec3 u_Ld_1;" \
        "uniform vec3 u_Ls_1;" \
        "uniform vec3 u_La_2;" \
        "uniform vec3 u_Ld_2;" \
        "uniform vec3 u_Ls_2;" \
        "uniform vec3 u_La_3;" \
        "uniform vec3 u_Ld_3;" \
        "uniform vec3 u_Ls_3;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform mediump int u_lighting_enabled;" \
        "void main(void)" \
        "{" \
        "vec3 f_phong_ads_color;" \
        "if(u_lighting_enabled == 1)" \
        "{" \
        "if(u_v_key_pressed == 0)" \
        "{" \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
        "vec3 normalized_light_1_direction = normalize(light_1_direction);" \
        "vec3 normalized_light_2_direction = normalize(light_2_direction);" \
        "vec3 normalized_light_3_direction = normalize(light_3_direction);" \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
        "vec3 ambient_1 = u_La_1 * u_Ka;" \
        "vec3 ambient_2 = u_La_2 * u_Ka;" \
        "vec3 ambient_3 = u_La_3 * u_Ka;" \
        "float tn1_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_1_direction),0.0);" \
        "float tn2_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_2_direction),0.0);" \
        "float tn3_dot_ld3 = max(dot(normalized_transformed_normals,normalized_light_3_direction),0.0);" \
        "vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;" \
        "vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;" \
        "vec3 diffuse_3 = u_Ld_3 * u_Kd * tn3_dot_ld3;" \
        "vec3 reflection_vector_1 = reflect(-normalized_light_1_direction,normalized_transformed_normals);" \
        "vec3 reflection_vector_2 = reflect(-normalized_light_2_direction,normalized_transformed_normals);" \
        "vec3 reflection_vector_3 = reflect(-normalized_light_3_direction,normalized_transformed_normals);" \
        "vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,normalized_viewer_vector),0.0),u_material_shininess);" \
        "vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,normalized_viewer_vector),0.0),u_material_shininess);" \
        "vec3 specular_3 = u_Ls_3 * u_Ks * pow(max(dot(reflection_vector_3,normalized_viewer_vector),0.0),u_material_shininess);" \
        "f_phong_ads_color = (ambient_1 + ambient_2 + ambient_3) +  (diffuse_1 + diffuse_2 + diffuse_3) + (specular_1 + specular_2 + specular_3);" \
        "}" \
        "else" \
        "{" \
        "FragColor = vec4(v_phong_ads_color,1.0);" \
        "}" \
        "}"\
        "if (u_v_key_pressed == 0)" \
        "{" \
        "FragColor = vec4(f_phong_ads_color, 1.0);" \
        "}" \
        "else" \
        "{" \
        "FragColor = vec4(v_phong_ads_color, 1.0);" \
        "}" \
        "}";
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gFragmentShaderObject);
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("fragment shader - %s",szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create program
        gShaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
        // pre-link binding of shader program object with vertex shader texture attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(gShaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("link program - %s",szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // get uniform location
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
        
        // L or l key pressed or not
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
        V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_v_key_pressed");
        
        // ambient color intensity of light
        La_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_1");
        // diffuse color intensity of light
        Ld_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_1");
        // specular color intensity of light
        Ls_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_1");
        // position of light
        light_1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_1_position");
        
        // ambient color intensity of light
        La_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_2");
        // diffuse color intensity of light
        Ld_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_2");
        // specular color intensity of light
        Ls_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_2");
        // position of light
        light_2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_2_position");
        
        // ambient color intensity of light
        La_3_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_3");
        // diffuse color intensity of light
        Ld_3_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_3");
        // specular color intensity of light
        Ls_3_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_3");
        // position of light
        light_3_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_3_position");
        
        // amient reflective color intensity of light
        Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
        // diffuse reflective color intensity of light
        Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
        // specular reflective color intensity of light
        Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
        // shininess of material (value is conventionally between 0 to 200)
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
        
        // *** vertices, colors, shader attribs, vbo, vao initialization *** //
        [self makeSphere:2.0f :30 :30];
        
        //glShadeModel(GL_SMOOTH);
        //glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_CULL_FACE);
        
        // clear color
        glClearColor(0.0f,0.0f,0.0f,1.0f);    // black color
        
        // set perspective matrix to identity matrix
        gPerspectiveProjectionMatrix = vmath::mat4::identity();
        
        gbLight = false;
        gbAnimate = false;
        
        // GESTURE RECOGNITION
        // Tap Gesture Code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

-(void)allocate:(int)numIndices
{
    // code
    // first cleanup, if not initially empty
    // [self cleanupMeshData];
    
    maxElements = numIndices;
    numElements = 0;
    numVertices = 0;
    
    int iNumIndices = numIndices/3;
    
    elements = (unsigned short *)malloc(iNumIndices * 3 * sizeof(unsigned short)); // 3 is x,y,z and 2 is sizeof short
    verts = (float *)malloc(iNumIndices * 3 * sizeof(float)); // 3 is x,y,z and 4 is sizeof float
    norms = (float *)malloc(iNumIndices * 3 * sizeof(float)); // 3 is x,y,z and 4 is sizeof float
    texCoords = (float *)malloc(iNumIndices * 2 * sizeof(float)); // 2 is s,t and 4 is sizeof float
}

// Add 3 vertices, 3 normal and 2 texcoords i.e. one triangle to the geometry.
// This searches the current list for identical vertices (exactly or nearly) and
// if one is found, it is added to the index array.
// if not, it is added to both the index array and the vertex array.
-(void)addTriangle:(float **)single_vertex :(float **)single_normal :(float **)single_texture
{
    const float diff = 0.00001f;
    int i, j;
    
    // code
    // normals should be of unit length
    [self normalizeVector:single_normal[0]];
    [self normalizeVector:single_normal[1]];
    [self normalizeVector:single_normal[2]];
    
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
        {
            if ([self isFoundIdentical:verts[j * 3] :single_vertex[i][0] :diff] &&
                [self isFoundIdentical:verts[(j * 3) + 1] :single_vertex[i][1] :diff] &&
                [self isFoundIdentical:verts[(j * 3) + 2] :single_vertex[i][2] :diff] &&
                
                [self isFoundIdentical:norms[j * 3] :single_normal[i][0] :diff] &&
                [self isFoundIdentical:norms[(j * 3) + 1] :single_normal[i][1] :diff] &&
                [self isFoundIdentical:norms[(j * 3) + 2] :single_normal[i][2] :diff] &&
                
                [self isFoundIdentical:texCoords[j * 2] :single_texture[i][0] :diff] &&
                [self isFoundIdentical:texCoords[(j * 2) + 1] :single_texture[i][1] :diff])
            {
                elements[numElements] = (short)j;
                numElements++;
                break;
            }
        }
        
        //If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
        if (j == numVertices && numVertices < maxElements && numElements < maxElements)
        {
            verts[numVertices * 3] = single_vertex[i][0];
            verts[(numVertices * 3) + 1] = single_vertex[i][1];
            verts[(numVertices * 3) + 2] = single_vertex[i][2];
            
            norms[numVertices * 3] = single_normal[i][0];
            norms[(numVertices * 3) + 1] = single_normal[i][1];
            norms[(numVertices * 3) + 2] = single_normal[i][2];
            
            texCoords[numVertices * 2] = single_texture[i][0];
            texCoords[(numVertices * 2) + 1] = single_texture[i][1];
            
            elements[numElements] = (short)numVertices; //adding the index to the end of the list of elements/indices
            numElements++; //incrementing the 'end' of the list
            numVertices++; //incrementing coun of vertices
        }
    }
}

-(void)prepareToDraw
{
    // vao
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    
    // vbo for position
    glGenBuffers(1, &vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
    glBufferData(GL_ARRAY_BUFFER, (maxElements * 3 * sizeof(float) / 3), verts, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind with vbo_position
    
    // vbo for normals
    glGenBuffers(1, &vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, (maxElements * 3 * sizeof(float) / 3), norms, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind with vbo_normal
    
    // vbo for texture
    glGenBuffers(1, &vbo_texture);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);
    glBufferData(GL_ARRAY_BUFFER, (maxElements * 2 * sizeof(float) / 3), texCoords, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind with vbo_texture
    
    // vbo for index
    glGenBuffers(1, &vbo_index);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, (maxElements * 3 * sizeof(unsigned short) / 3), elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // Unbind with vbo_index
    
    glBindVertexArray(0); // Unbind with vao
    
    // after sending data to GPU, now we can free our arrays
    // [self cleanupMeshData];
}

-(void)drawSphere
{
    // code
    // bind vao
    glBindVertexArray(vao);
    
    // draw
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind vao
    glBindVertexArray(0); // Unbind with vao
}

-(int)getIndexCount
{
    // code
    return(numElements);
}

-(int)getVertexCount
{
    // code
    return(numVertices);
}

-(void)normalizeVector:(float *)v
{
    // code
    
    // square the vector length
    float squaredVectorLength = (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
    
    // get square root of above 'squared vector length'
    float squareRootOfSquaredVectorLength = (float)sqrt(squaredVectorLength);
    
    // scale the vector with 1/squareRootOfSquaredVectorLength
    v[0] = v[0] * 1.0f/squareRootOfSquaredVectorLength;
    v[1] = v[1] * 1.0f/squareRootOfSquaredVectorLength;
    v[2] = v[2] * 1.0f/squareRootOfSquaredVectorLength;
}

-(BOOL)isFoundIdentical:(float)val1 :(float)val2 :(float)diff
{
    // code
    if(fabs(val1 - val2) < diff)
        return(true);
    else
        return(false);
}

-(void)cleanupMeshData
{
    // code
    if(elements != NULL)
    {
        free(elements);
        elements = NULL;
    }
    
    if(verts != NULL)
    {
        free(verts);
        verts = NULL;
    }
    
    if(norms != NULL)
    {
        free(norms);
        norms = NULL;
    }
    
    if(texCoords != NULL)
    {
        free(texCoords);
        texCoords = NULL;
    }
}

-(void)releaseMemory:(float **)vertex :(float **)normal :(float **)texture
{
    for(int a = 0; a < 4; a++)
    {
        free(vertex[a]);
        free(normal[a]);
        free(texture[a]);
    }
    free(vertex);
    free(normal);
    free(texture);
}

-(void)makeSphere:(float)fRadius :(int)iSlices :(int)iStacks
{
    const float VDG_PI = 3.14159265358979323846;
    
    // code
    float drho = (float)VDG_PI / (float)iStacks;
    float dtheta = 2.0 * (float)VDG_PI / (float)iSlices;
    float ds = 1.0 / (float)(iSlices);
    float dt = 1.0 / (float)(iStacks);
    float t = 1.0;
    float s = 0.0;
    int i = 0;
    int j = 0;
    
    [self allocate:iSlices * iStacks * 6];
    
    for (i = 0; i < iStacks; i++)
    {
        float rho = (float)(i * drho);
        float srho = (float)(sin(rho));
        float crho = (float)(cos(rho));
        float srhodrho = (float)(sin(rho + drho));
        float crhodrho = (float)(cos(rho + drho));
        
        // Many sources of OpenGL sphere drawing code uses a triangle fan
        // for the caps of the sphere. This however introduces texturing
        // artifacts at the poles on some OpenGL implementations
        s = 0.0;
        
        // initialization of three 2-D arrays, two are 4 x 3 and one is 4 x 2
        float **vertex = (float **)malloc(sizeof(float *) * 4); // 4 rows
        for(int a = 0; a < 4; a++)
            vertex[a]= (float *)malloc(sizeof(float) * 3); // 3 columns
        float **normal = (float **)malloc(sizeof(float *) * 4); // 4 rows
        for(int a = 0;a < 4;a++)
            normal[a]= (float *)malloc(sizeof(float) * 3); // 3 columns
        float **texture = (float **)malloc(sizeof(float *) * 4); // 4 rows
        for(int a = 0;a < 4;a++)
            texture[a]= (float *)malloc(sizeof(float) * 2); // 2 columns
        
        for ( j = 0; j < iSlices; j++)
        {
            float theta = (j == iSlices) ? 0.0 : j * dtheta;
            float stheta = (float)(-sin(theta));
            float ctheta = (float)(cos(theta));
            
            float x = stheta * srho;
            float y = ctheta * srho;
            float z = crho;
            
            texture[0][0] = s;
            texture[0][1] = t;
            normal[0][0] = x;
            normal[0][1] = y;
            normal[0][2] = z;
            vertex[0][0] = x * fRadius;
            vertex[0][1] = y * fRadius;
            vertex[0][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            texture[1][0] = s;
            texture[1][1] = t - dt;
            normal[1][0] = x;
            normal[1][1] = y;
            normal[1][2] = z;
            vertex[1][0] = x * fRadius;
            vertex[1][1] = y * fRadius;
            vertex[1][2] = z * fRadius;
            
            theta = ((j+1) == iSlices) ? 0.0 : (j+1) * dtheta;
            stheta = (float)(-sin(theta));
            ctheta = (float)(cos(theta));
            
            x = stheta * srho;
            y = ctheta * srho;
            z = crho;
            
            s += ds;
            texture[2][0] = s;
            texture[2][1] = t;
            normal[2][0] = x;
            normal[2][1] = y;
            normal[2][2] = z;
            vertex[2][0] = x * fRadius;
            vertex[2][1] = y * fRadius;
            vertex[2][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            texture[3][0] = s;
            texture[3][1] = t - dt;
            normal[3][0] = x;
            normal[3][1] = y;
            normal[3][2] = z;
            vertex[3][0] = x * fRadius;
            vertex[3][1] = y * fRadius;
            vertex[3][2] = z * fRadius;
            
            [self addTriangle:vertex :normal :texture];
            
            // Rearrange for next triangle
            vertex[0][0]=vertex[1][0];
            vertex[0][1]=vertex[1][1];
            vertex[0][2]=vertex[1][2];
            normal[0][0]=normal[1][0];
            normal[0][1]=normal[1][1];
            normal[0][2]=normal[1][2];
            texture[0][0]=texture[1][0];
            texture[0][1]=texture[1][1];
            
            vertex[1][0]=vertex[3][0];
            vertex[1][1]=vertex[3][1];
            vertex[1][2]=vertex[3][2];
            normal[1][0]=normal[3][0];
            normal[1][1]=normal[3][1];
            normal[1][2]=normal[3][2];
            texture[1][0]=texture[3][0];
            texture[1][1]=texture[3][1];
            
            [self addTriangle:vertex :normal :texture];
        }
        t -= dt;
        [self releaseMemory:vertex :normal :texture];
    }
    
    [self prepareToDraw];
}

- (void)updateAngle
{
    angleRedLight = angleRedLight + 0.001f;
    if (angleRedLight >= 360)
    {
        angleRedLight = 0.1f;
    }
    angleGreenLight = angleGreenLight + 0.001f;
    if (angleGreenLight >= 360)
    {
        angleGreenLight = 0.1f;
    }
    angleBlueLight = angleBlueLight + 0.001f;
    if (angleBlueLight >= 360)
    {
        angleBlueLight = 0.01f;
    }
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    // Code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using opengl program object
    glUseProgram(gShaderProgramObject);
    
    // opengl drawing
    
    if (gbLight == true)
    {
        // set u_lighting_enabled uniform
        glUniform1i(L_KeyPressed_uniform, 1);
        glUniform1i(V_KeyPressed_uniform, perVertexLighting);
        
        // setting light properties
        glUniform3fv(La_1_uniform, 1, light_1_Ambient);
        glUniform3fv(Ld_1_uniform, 1, light_1_Diffuse);
        glUniform3fv(Ls_1_uniform, 1, light_1_Specular);
        light_1_Position[1] = 100.0f * cos(angleRedLight);
        light_1_Position[2] = 100.0f * sin(angleRedLight);
        glUniform4fv(light_1_position_uniform, 1, light_1_Position);
        
        glUniform3fv(La_2_uniform, 1, light_2_Ambient);
        glUniform3fv(Ld_2_uniform, 1, light_2_Diffuse);
        glUniform3fv(Ls_2_uniform, 1, light_2_Specular);
        light_2_Position[0] = 100.0f * sin(angleGreenLight);
        light_2_Position[2] = 100.0f * cos(angleGreenLight);
        glUniform4fv(light_2_position_uniform, 1, light_2_Position);
        
        glUniform3fv(La_3_uniform, 1, light_3_Ambient);
        glUniform3fv(Ld_3_uniform, 1, light_3_Diffuse);
        glUniform3fv(Ls_3_uniform, 1, light_3_Specular);
        light_3_Position[0] = 100.0f * cos(angleBlueLight);
        light_3_Position[1] = 100.0f * sin(angleBlueLight);
        glUniform4fv(light_3_position_uniform, 1, light_3_Position);
        
        // setting materials properties
        glUniform3fv(Ka_uniform, 1, material_ambient);
        glUniform3fv(Kd_uniform, 1, material_diffuse);
        glUniform3fv(Ks_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);
    }
    else
    {
        // set u_lighting_enabled uniform
        glUniform1i(L_KeyPressed_uniform, 0);
        glUniform1i(V_KeyPressed_uniform, perVertexLighting);
    }
    
    // Opengl drawing
    // set model, view matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.0
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -8.0f);
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // stop using opengl program object
    glUseProgram(0);
    
    if (gbLight == true)
    {
        [self updateAngle];
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    if (gbLight == true)
    {
        [self updateAngle];
    }
    [self drawView:nil];
}

- (void)startAnimation
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

// to become first responder
-(BOOL)acceptFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(perVertexLighting == 0)
        perVertexLighting = 1;
    else if(perVertexLighting == 1)
        perVertexLighting = 0;
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if(gbLight == false)
        gbLight = true;
    else if(gbLight == true)
        gbLight = false;
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

-(void)dealloc
{
    // destroy vao
    if (vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao = 0;
    }
    // destroy position vbo
    if (vbo_position)
    {
        glDeleteBuffers(1, &vbo_position);
        vbo_position = 0;
    }
    // destroy normal vbo
    if (vbo_normal)
    {
        glDeleteBuffers(1, &vbo_normal);
        vbo_normal = 0;
    }
    // destroy element vbo
    if (vbo_index)
    {
        glDeleteBuffers(1, &vbo_index);
        vbo_index = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}

@end
