//
//  GLESView.m
//  Multi Colored Triangle
//
//  Created by ASTROMEDICOMP on 17/06/18.
//

#import <OPENGLES/ES3/gl.h>
#import <OPENGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

GLfloat light_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_Diffuse[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_Specular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_Position[] = { 0.0f,0.0f,0.0f,0.0f };

// define material array for first sphere of first column
GLfloat material_1_1_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
GLfloat material_1_1_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
GLfloat material_1_1_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
GLfloat material_1_1_shininess = { 0.6f * 128.0f };

// define material array for second sphere of first column
GLfloat material_1_2_ambient[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_1_2_diffuse[] = { 0.54f,0.89f,0.63f,1.0f };
GLfloat material_1_2_specular[] = { 0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_1_2_shininess = { 0.1f * 128.0f };

// define material array for third sphere of first column
GLfloat material_1_3_ambient[] = { 0.05375f,0.05f,0.06625f,1.0f };
GLfloat material_1_3_diffuse[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_1_3_specular[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_1_3_shininess = { 0.3f * 128.0f };

// define material array for fourth sphere of first column
GLfloat material_1_4_ambient[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_1_4_diffuse[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_1_4_specular[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_1_4_shininess = { 0.088f * 128.0f };

// define material array for fifth sphere of first column
GLfloat material_1_5_ambient[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_1_5_diffuse[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_1_5_specular[] = { 0.727811f,0.626959f,0.626959f,1.0f };
GLfloat material_1_5_shininess = { 0.6f * 128.0f };

// define material array for six sphere of first column
GLfloat material_1_6_ambient[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_1_6_diffuse[] = { 0.396f,0.74151f,0.69102f,1.0f };
GLfloat material_1_6_specular[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_1_6_shininess = { 0.6f * 128.0f };

// define material array for first sphere of second column
GLfloat material_2_1_ambient[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_2_1_diffuse[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_2_1_specular[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_2_1_shininess = { 0.21794872f * 128.0f };

// define material array for second sphere of second column
GLfloat material_2_2_ambient[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_2_2_diffuse[] = { 0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_2_2_specular[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_2_2_shininess = { 0.2f * 128.0f };

// define material array for third sphere of second column
GLfloat material_2_3_ambient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat material_2_3_diffuse[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_2_3_specular[] = { 0.774597f,0.774597f,0.774597f,1.0f };
GLfloat material_2_3_shininess = { 0.6f * 128.0f };

// define material array for fourth sphere of second column
GLfloat material_2_4_ambient[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_2_4_diffuse[] = { 0.7038f,0.27048f,0.0828f,1.0f };
GLfloat material_2_4_specular[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_2_4_shininess = { 0.1f * 128.0f };

// define material array for fifth sphere of second column
GLfloat material_2_5_ambient[] = { 0.24725f,0.1995f,0.0745f,1.0f };
GLfloat material_2_5_diffuse[] = { 0.75164f,0.60648f,0.22648f,1.0f };
GLfloat material_2_5_specular[] = { 0.628281f,0.555802f,0.366065f,1.0f };
GLfloat material_2_5_shininess = { 0.4f * 128.0f };

// define material array for six sphere of second column
GLfloat material_2_6_ambient[] = { 0.19225f,0.19225f,0.19225f,1.0f };
GLfloat material_2_6_diffuse[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_2_6_specular[] = { 0.508273f,0.508273f,0.508273f,1.0f };
GLfloat material_2_6_shininess = { 0.4f * 128.0f };

// define material array for first sphere of third column
GLfloat material_3_1_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_3_1_specular[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_3_1_shininess = { 0.25f * 128.0f };

// define material array for second sphere of third column
GLfloat material_3_2_ambient[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_3_2_diffuse[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_3_2_specular[] = { 0.50196078f,0.50196078f,0.50196078f,1.0f };
GLfloat material_3_2_shininess = { 0.25f * 128.0f };

// define material array for third sphere of third column
GLfloat material_3_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_3_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_3_3_specular[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_3_3_shininess = { 0.25f * 128.0f };

// define material array for fourth sphere of third column
GLfloat material_3_4_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_4_diffuse[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_3_4_specular[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_3_4_shininess = { 0.25f * 128.0f };

// define material array for fifth sphere of third column
GLfloat material_3_5_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_5_diffuse[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_3_5_specular[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_3_5_shininess = { 0.25f * 128.0f };

// define material array for six sphere of third column
GLfloat material_3_6_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_6_diffuse[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_3_6_specular[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_3_6_shininess = { 0.25f * 128.0f };

// define material array for first sphere of fourth column
GLfloat material_4_1_ambient[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_4_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_4_1_specular[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_4_1_shininess = { 0.078125f * 128.0f };

// define material array for second sphere of fourth column
GLfloat material_4_2_ambient[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_4_2_diffuse[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_4_2_specular[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_4_2_shininess = { 0.078125f * 128.0f };

// define material array for third sphere of fourth column
GLfloat material_4_3_ambient[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_4_3_diffuse[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_4_3_specular[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_4_3_shininess = { 0.078125f * 128.0f };

// define material array for fourth sphere of fourth column
GLfloat material_4_4_ambient[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_4_4_diffuse[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_4_4_specular[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_4_4_shininess = { 0.078125f * 128.0f };

// define material array for fifth sphere of fourth column
GLfloat material_4_5_ambient[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_4_5_diffuse[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_4_5_specular[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_4_5_shininess = { 0.078125f * 128.0f };

// define material array for six sphere of fourth column
GLfloat material_4_6_ambient[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_4_6_diffuse[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_4_6_specular[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_4_6_shininess = { 0.078125f * 128.0f };

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    int iWidth;
    int iHeight;
    
    GLuint numVertices;
    GLuint numElements;
    GLuint maxElements;
    unsigned short *elements;
    float *verts;
    float *norms;
    float *texCoords;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    GLuint vao;
    GLuint vbo_position;
    GLuint vbo_normal;
    GLuint vbo_index;
    GLuint vbo_texture;
    
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    
    GLuint L_KeyPressed_uniform;
    
    // light uniforms
    GLuint La_uniform;
    GLuint Ld_uniform;
    GLuint Ls_uniform;
    GLuint light_position_uniform;
    
    // first column, first material
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
    
    bool gbLight;
    
    // color angle
    GLfloat angleWhiteLight;
    GLint singleTap;
    
    GLint viewportX;
    GLint viewportY;
    GLint viewportWidth;
    GLint viewportHeight;
    GLint spaceFromTopInEachViewport;
    
    const GLchar *vertexShaderSourceCode;
    const GLchar *fragmentShaderSourceCode;
}

- (id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    
    iWidth = 600;
    iHeight = 300;
    // color angle
    angleWhiteLight = 0.0f;
    
    vertexShaderSourceCode = "";
    fragmentShaderSourceCode = "";
    
    //surfaceWidth = width;
    //surfaceHeight = height;
    
    viewportWidth = iWidth / 4;        // width of viewport
    viewportHeight = iHeight / 6;        // height of viewport
    
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            [self release];
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initialization
        isAnimating = NO;
        animationFrameInterval = 60;    // default since IOS 8.2
        
        // *** VERTEX SHADER ***
        // create shader
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light_position;" \
        "uniform mediump int u_lighting_enabled;" \
        "out vec3 transformed_normals;" \
        "out vec3 light_direction;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "if(u_lighting_enabled == 1)" \
        "{" \
        "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
        "transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
        "light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
        "viewer_vector = -eyeCoordinates.xyz;" \
        "}" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gVertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // *** FRAGMENT SHAER *** //
        // create fragment shader
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 transformed_normals;" \
        "in vec3 light_direction;" \
        "in vec3 viewer_vector;" \
        "out vec4 FragColor;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Ls;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform int u_lighting_enabled;" \
        "void main(void)" \
        "{" \
        "vec3 f_phong_ads_color;" \
        "if(u_lighting_enabled == 1)" \
        "{" \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
        "vec3 normalized_light_direction = normalize(light_direction);" \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
        "vec3 ambient = u_La * u_Ka;" \
        "float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);" \
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);" \
        "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);" \
        "f_phong_ads_color = ambient + diffuse + specular;" \
        "}"\
        "else" \
        "{" \
        "f_phong_ads_color = vec3(1.0,1.0,1.0);" \
        "}" \
        "FragColor = vec4(f_phong_ads_color, 1.0);" \
        "}";
        
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gFragmentShaderObject);
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create program
        gShaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        // pre-building of shader program object with vertex shader position attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
        // pre-link binding of shader program object with vertex shader texture attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(gShaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    free(szInfoLog);
                    [self release];
                    exit(0);
                }
            }
        }
        
        // get uniform location
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
        
        // L or l key pressed or not
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
        
        // ambient color intensity of light
        La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
        // diffuse color intensity of light
        Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
        // specular color intensity of light
        Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
        // position of light
        light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
        
        // amient reflective color intensity of light
        Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
        // diffuse reflective color intensity of light
        Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
        // specular reflective color intensity of light
        Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
        // shininess of material (value is conventionally between 0 to 200)
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
        
        // vertices, colors, shader attribs, vbo, vao initializations
        [self makeSphere:2.0f :30 :30];
        
        //glShadeModel(GL_SMOOTH);
        //glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        //glEnable(GL_CULL_FACE);
        
        // clear color
        glClearColor(0.0f,0.0f,0.0f,1.0f);    // black color
        
        // set perspective matrix to identity matrix
        gPerspectiveProjectionMatrix = vmath::mat4::identity();
        
        gbLight = false;
        isAnimating = false;
        
        // GESTURE RECOGNITION
        // Tap Gesture Code
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];    // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

-(void)allocate:(int)numIndices
{
    // code
    // first cleanup, if not initially empty
    // [self cleanupMeshData];
    
    maxElements = numIndices;
    numElements = 0;
    numVertices = 0;
    
    int iNumIndices = numIndices/3;
    
    elements = (unsigned short *)malloc(iNumIndices * 3 * sizeof(unsigned short)); // 3 is x,y,z and 2 is sizeof short
    verts = (float *)malloc(iNumIndices * 3 * sizeof(float)); // 3 is x,y,z and 4 is sizeof float
    norms = (float *)malloc(iNumIndices * 3 * sizeof(float)); // 3 is x,y,z and 4 is sizeof float
    texCoords = (float *)malloc(iNumIndices * 2 * sizeof(float)); // 2 is s,t and 4 is sizeof float
}

// Add 3 vertices, 3 normal and 2 texcoords i.e. one triangle to the geometry.
// This searches the current list for identical vertices (exactly or nearly) and
// if one is found, it is added to the index array.
// if not, it is added to both the index array and the vertex array.
-(void)addTriangle:(float **)single_vertex :(float **)single_normal :(float **)single_texture
{
    const float diff = 0.00001f;
    int i, j;
    
    // code
    // normals should be of unit length
    [self normalizeVector:single_normal[0]];
    [self normalizeVector:single_normal[1]];
    [self normalizeVector:single_normal[2]];
    
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
        {
            if ([self isFoundIdentical:verts[j * 3] :single_vertex[i][0] :diff] &&
                [self isFoundIdentical:verts[(j * 3) + 1] :single_vertex[i][1] :diff] &&
                [self isFoundIdentical:verts[(j * 3) + 2] :single_vertex[i][2] :diff] &&
                
                [self isFoundIdentical:norms[j * 3] :single_normal[i][0] :diff] &&
                [self isFoundIdentical:norms[(j * 3) + 1] :single_normal[i][1] :diff] &&
                [self isFoundIdentical:norms[(j * 3) + 2] :single_normal[i][2] :diff] &&
                
                [self isFoundIdentical:texCoords[j * 2] :single_texture[i][0] :diff] &&
                [self isFoundIdentical:texCoords[(j * 2) + 1] :single_texture[i][1] :diff])
            {
                elements[numElements] = (short)j;
                numElements++;
                break;
            }
        }
        
        //If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
        if (j == numVertices && numVertices < maxElements && numElements < maxElements)
        {
            verts[numVertices * 3] = single_vertex[i][0];
            verts[(numVertices * 3) + 1] = single_vertex[i][1];
            verts[(numVertices * 3) + 2] = single_vertex[i][2];
            
            norms[numVertices * 3] = single_normal[i][0];
            norms[(numVertices * 3) + 1] = single_normal[i][1];
            norms[(numVertices * 3) + 2] = single_normal[i][2];
            
            texCoords[numVertices * 2] = single_texture[i][0];
            texCoords[(numVertices * 2) + 1] = single_texture[i][1];
            
            elements[numElements] = (short)numVertices; //adding the index to the end of the list of elements/indices
            numElements++; //incrementing the 'end' of the list
            numVertices++; //incrementing coun of vertices
        }
    }
}

- (void)prepareToDraw
{
    // vao
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    
    // vbo for position
    glGenBuffers(1, &vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
    glBufferData(GL_ARRAY_BUFFER, (maxElements * 3 * sizeof(float) / 3), verts, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind with vbo_position
    
    // vbo for normals
    glGenBuffers(1, &vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, (maxElements * 3 * sizeof(float) / 3), norms, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind with vbo_normal
    
    // vbo for texture
    glGenBuffers(1, &vbo_texture);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);
    glBufferData(GL_ARRAY_BUFFER, (maxElements * 2 * sizeof(float) / 3), texCoords, GL_STATIC_DRAW);
    
    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind with vbo_texture
    
    // vbo for index
    glGenBuffers(1, &vbo_index);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, (maxElements * 3 * sizeof(unsigned short) / 3), elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // Unbind with vbo_index
    
    glBindVertexArray(0); // Unbind with vao
    
    // after sending data to GPU, now we can free our arrays
    // [self cleanupMeshData];
}

- (void)drawSphere
{
    // code
    // bind vao
    glBindVertexArray(vao);
    
    // draw
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind vao
    glBindVertexArray(0); // Unbind with vao
}

- (int)getIndexCount
{
    // code
    return(numElements);
}

- (int)getVertexCount
{
    // code
    return(numVertices);
}

- (void)normalizeVector:(float *)v
{
    // code
    
    // square the vector length
    float squaredVectorLength = (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
    
    // get square root of above 'squared vector length'
    float squareRootOfSquaredVectorLength = (float)sqrt(squaredVectorLength);
    
    // scale the vector with 1/squareRootOfSquaredVectorLength
    v[0] = v[0] * 1.0f/squareRootOfSquaredVectorLength;
    v[1] = v[1] * 1.0f/squareRootOfSquaredVectorLength;
    v[2] = v[2] * 1.0f/squareRootOfSquaredVectorLength;
}

- (bool)isFoundIdentical:(float)val1 :(float)val2 :(float)diff
{
    // code
    if(fabs(val1 - val2) < diff)
        return(true);
    else
        return(false);
}

- (void)cleanupMeshData
{
    // code
    if(elements != NULL)
    {
        free(elements);
        elements = NULL;
    }
    
    if(verts != NULL)
    {
        free(verts);
        verts = NULL;
    }
    
    if(norms != NULL)
    {
        free(norms);
        norms = NULL;
    }
    
    if(texCoords != NULL)
    {
        free(texCoords);
        texCoords = NULL;
    }
}

- (void)releaseMemory:(float **)vertex :(float **)normal :(float **)texture
{
    for(int a = 0; a < 4; a++)
    {
        free(vertex[a]);
        free(normal[a]);
        free(texture[a]);
    }
    free(vertex);
    free(normal);
    free(texture);
}

- (void)makeSphere:(float)fRadius :(int)iSlices :(int)iStacks
{
    const float VDG_PI = 3.14159265358979323846;
    
    // code
    float drho = (float)VDG_PI / (float)iStacks;
    float dtheta = 2.0 * (float)VDG_PI / (float)iSlices;
    float ds = 1.0 / (float)(iSlices);
    float dt = 1.0 / (float)(iStacks);
    float t = 1.0;
    float s = 0.0;
    int i = 0;
    int j = 0;
    
    [self allocate:iSlices * iStacks * 6];
    
    for (i = 0; i < iStacks; i++)
    {
        float rho = (float)(i * drho);
        float srho = (float)(sin(rho));
        float crho = (float)(cos(rho));
        float srhodrho = (float)(sin(rho + drho));
        float crhodrho = (float)(cos(rho + drho));
        
        // Many sources of OpenGL sphere drawing code uses a triangle fan
        // for the caps of the sphere. This however introduces texturing
        // artifacts at the poles on some OpenGL implementations
        s = 0.0;
        
        // initialization of three 2-D arrays, two are 4 x 3 and one is 4 x 2
        float **vertex = (float **)malloc(sizeof(float *) * 4); // 4 rows
        for(int a = 0; a < 4; a++)
            vertex[a]= (float *)malloc(sizeof(float) * 3); // 3 columns
        float **normal = (float **)malloc(sizeof(float *) * 4); // 4 rows
        for(int a = 0;a < 4;a++)
            normal[a]= (float *)malloc(sizeof(float) * 3); // 3 columns
        float **texture = (float **)malloc(sizeof(float *) * 4); // 4 rows
        for(int a = 0;a < 4;a++)
            texture[a]= (float *)malloc(sizeof(float) * 2); // 2 columns
        
        for ( j = 0; j < iSlices; j++)
        {
            float theta = (j == iSlices) ? 0.0 : j * dtheta;
            float stheta = (float)(-sin(theta));
            float ctheta = (float)(cos(theta));
            
            float x = stheta * srho;
            float y = ctheta * srho;
            float z = crho;
            
            texture[0][0] = s;
            texture[0][1] = t;
            normal[0][0] = x;
            normal[0][1] = y;
            normal[0][2] = z;
            vertex[0][0] = x * fRadius;
            vertex[0][1] = y * fRadius;
            vertex[0][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            texture[1][0] = s;
            texture[1][1] = t - dt;
            normal[1][0] = x;
            normal[1][1] = y;
            normal[1][2] = z;
            vertex[1][0] = x * fRadius;
            vertex[1][1] = y * fRadius;
            vertex[1][2] = z * fRadius;
            
            theta = ((j+1) == iSlices) ? 0.0 : (j+1) * dtheta;
            stheta = (float)(-sin(theta));
            ctheta = (float)(cos(theta));
            
            x = stheta * srho;
            y = ctheta * srho;
            z = crho;
            
            s += ds;
            texture[2][0] = s;
            texture[2][1] = t;
            normal[2][0] = x;
            normal[2][1] = y;
            normal[2][2] = z;
            vertex[2][0] = x * fRadius;
            vertex[2][1] = y * fRadius;
            vertex[2][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            texture[3][0] = s;
            texture[3][1] = t - dt;
            normal[3][0] = x;
            normal[3][1] = y;
            normal[3][2] = z;
            vertex[3][0] = x * fRadius;
            vertex[3][1] = y * fRadius;
            vertex[3][2] = z * fRadius;
            
            [self addTriangle:vertex :normal :texture];
            
            // Rearrange for next triangle
            vertex[0][0]=vertex[1][0];
            vertex[0][1]=vertex[1][1];
            vertex[0][2]=vertex[1][2];
            normal[0][0]=normal[1][0];
            normal[0][1]=normal[1][1];
            normal[0][2]=normal[1][2];
            texture[0][0]=texture[1][0];
            texture[0][1]=texture[1][1];
            
            vertex[1][0]=vertex[3][0];
            vertex[1][1]=vertex[3][1];
            vertex[1][2]=vertex[3][2];
            normal[1][0]=normal[3][0];
            normal[1][1]=normal[3][1];
            normal[1][2]=normal[3][2];
            texture[1][0]=texture[3][0];
            texture[1][1]=texture[3][1];
            
            [self addTriangle:vertex :normal :texture];
        }
        t -= dt;
        [self releaseMemory:vertex :normal :texture];
    }
    
    [self prepareToDraw];
}

- (void)updateAngle
{
    angleWhiteLight = angleWhiteLight + 0.01f;
    if (angleWhiteLight >= 360)
    {
        angleWhiteLight = 0.1f;
    }
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

- (void)drawView:(id)sender
{
    // Code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using opengl program object
    glUseProgram(gShaderProgramObject);
    
    // opengl drawing
    
    if (gbLight == true)
    {
        // set u_lighting_enabled uniform
        glUniform1i(L_KeyPressed_uniform, 1);
        
        // setting light properties
        glUniform3fv(La_uniform, 1, light_Ambient);
        glUniform3fv(Ld_uniform, 1, light_Diffuse);
        glUniform3fv(Ls_uniform, 1, light_Specular);
        
        //vmath::mat4 rotationMatrix = vmath::mat4::identity();
        
        if (singleTap == 1)
        {
            light_Position[1] = 100.0f * cos(angleWhiteLight);
            light_Position[2] = 100.0f * sin(angleWhiteLight);
            glUniform4fv(light_position_uniform, 1, light_Position);
        }
        else if (singleTap == 2)
        {
            light_Position[0] = 100.0f * sin(angleWhiteLight);
            light_Position[2] = 100.0f * cos(angleWhiteLight);
            glUniform4fv(light_position_uniform, 1, light_Position);
        }
        else if (singleTap == 3)
        {
            light_Position[0] = 100.0f * cos(angleWhiteLight);
            light_Position[1] = 100.0f * sin(angleWhiteLight);
            glUniform4fv(light_position_uniform, 1, light_Position);
        }
    }
    else
    {
        // set u_lighting_enabled uniform
        glUniform1i(L_KeyPressed_uniform, 0);
    }
    
    // Opengl drawing
    
    // -------------------
    // FIRST COLUMN SPHERE
    // -------------------
    
    // DRAW FIRST COLUMN, FIRST SPHERE
    
    // make model and view matrix to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    
    // pass view and projection matrix to shader through uniform
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
    
    // set viewport
    viewportX = iWidth - (viewportWidth * 4);    // x axis starting position for viewport
    viewportY = iHeight - (viewportHeight * 1);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_1_1_ambient);
    glUniform3fv(Kd_uniform, 1, material_1_1_diffuse);
    glUniform3fv(Ks_uniform, 1, material_1_1_specular);
    glUniform1f(material_shininess_uniform, material_1_1_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FIRST COLUMN, SECOND SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 2);    // y axis starting position for viewport
    //layoutSubviews(viewportX, viewportY, viewportWidth, viewportHeight);
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    // Ideally there is no need to reset model matrix and translate it by same x,y and z axis every time
    // but to avoid any uncertain output, i am doing it
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_1_2_ambient);
    glUniform3fv(Kd_uniform, 1, material_1_2_diffuse);
    glUniform3fv(Ks_uniform, 1, material_1_2_specular);
    glUniform1f(material_shininess_uniform, material_1_2_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FIRST COLUMN, THIRD SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 3);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_1_3_ambient);
    glUniform3fv(Kd_uniform, 1, material_1_3_diffuse);
    glUniform3fv(Ks_uniform, 1, material_1_3_specular);
    glUniform1f(material_shininess_uniform, material_1_3_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FIRST COLUMN, FOURTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 4);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_1_4_ambient);
    glUniform3fv(Kd_uniform, 1, material_1_4_diffuse);
    glUniform3fv(Ks_uniform, 1, material_1_4_specular);
    glUniform1f(material_shininess_uniform, material_1_4_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FIRST COLUMN, FIFTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 5);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_1_5_ambient);
    glUniform3fv(Kd_uniform, 1, material_1_5_diffuse);
    glUniform3fv(Ks_uniform, 1, material_1_5_specular);
    glUniform1f(material_shininess_uniform, material_1_5_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FIRST COLUMN, SIXTH SPHERE
    
    // set viwport
    viewportY = iHeight - (viewportHeight * 6);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_1_6_ambient);
    glUniform3fv(Kd_uniform, 1, material_1_6_diffuse);
    glUniform3fv(Ks_uniform, 1, material_1_6_specular);
    glUniform1f(material_shininess_uniform, material_1_6_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // -------------------
    // SECOND COLUMN SPHERE
    // -------------------
    
    // DRAW SECOND COLUMN, FIRST SPHERE
    
    // set viewport
    viewportX = iWidth - (viewportWidth * 3);    // x axis starting position for viewport
    viewportY = iHeight - (viewportHeight * 1);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_2_1_ambient);
    glUniform3fv(Kd_uniform, 1, material_2_1_diffuse);
    glUniform3fv(Ks_uniform, 1, material_2_1_specular);
    glUniform1f(material_shininess_uniform, material_2_1_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW SECOND COLUMN, SECOND SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 2);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_2_2_ambient);
    glUniform3fv(Kd_uniform, 1, material_2_2_diffuse);
    glUniform3fv(Ks_uniform, 1, material_2_2_specular);
    glUniform1f(material_shininess_uniform, material_2_2_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW SECOND COLUMN, THIRD SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 3);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_2_3_ambient);
    glUniform3fv(Kd_uniform, 1, material_2_3_diffuse);
    glUniform3fv(Ks_uniform, 1, material_2_3_specular);
    glUniform1f(material_shininess_uniform, material_2_3_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW SECOND COLUMN, FOURTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 4);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_2_4_ambient);
    glUniform3fv(Kd_uniform, 1, material_2_4_diffuse);
    glUniform3fv(Ks_uniform, 1, material_2_4_specular);
    glUniform1f(material_shininess_uniform, material_2_4_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW SECOND COLUMN, FIFTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 5);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_2_5_ambient);
    glUniform3fv(Kd_uniform, 1, material_2_5_diffuse);
    glUniform3fv(Ks_uniform, 1, material_2_5_specular);
    glUniform1f(material_shininess_uniform, material_2_5_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW SECOND COLUMN, SIXTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 6);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_2_6_ambient);
    glUniform3fv(Kd_uniform, 1, material_2_6_diffuse);
    glUniform3fv(Ks_uniform, 1, material_2_6_specular);
    glUniform1f(material_shininess_uniform, material_2_6_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // -------------------
    // THIRD COLUMN SPHERE
    // -------------------
    
    // DRAW THIRD COLUMN, FIRST SPHERE
    
    // set viewport
    viewportX = iWidth - (viewportWidth * 2);    // x axis starting position for viewport
    viewportY = iHeight - (viewportHeight * 1);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_3_1_ambient);
    glUniform3fv(Kd_uniform, 1, material_3_1_diffuse);
    glUniform3fv(Ks_uniform, 1, material_3_1_specular);
    glUniform1f(material_shininess_uniform, material_3_1_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW THIRD COLUMN, SECOND SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 2);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_3_2_ambient);
    glUniform3fv(Kd_uniform, 1, material_3_2_diffuse);
    glUniform3fv(Ks_uniform, 1, material_3_2_specular);
    glUniform1f(material_shininess_uniform, material_3_2_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW THIRD COLUMN, THIRD SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 3);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_3_3_ambient);
    glUniform3fv(Kd_uniform, 1, material_3_3_diffuse);
    glUniform3fv(Ks_uniform, 1, material_3_3_specular);
    glUniform1f(material_shininess_uniform, material_3_3_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW THIRD COLUMN, FOURTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 4);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_3_4_ambient);
    glUniform3fv(Kd_uniform, 1, material_3_4_diffuse);
    glUniform3fv(Ks_uniform, 1, material_3_4_specular);
    glUniform1f(material_shininess_uniform, material_3_4_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW THIRD COLUMN, FIFTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 5);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_3_5_ambient);
    glUniform3fv(Kd_uniform, 1, material_3_5_diffuse);
    glUniform3fv(Ks_uniform, 1, material_3_5_specular);
    glUniform1f(material_shininess_uniform, material_3_5_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW THIRD COLUMN, SIXTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 6);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_3_6_ambient);
    glUniform3fv(Kd_uniform, 1, material_3_6_diffuse);
    glUniform3fv(Ks_uniform, 1, material_3_6_specular);
    glUniform1f(material_shininess_uniform, material_3_6_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // -------------------
    // FOURTH COLUMN SPHERE
    // -------------------
    
    // DRAW FOURTH COLUMN, FIRST SPHERE
    
    // set viewport
    viewportX = iWidth - (viewportWidth * 1);    // x axis starting position for viewport
    viewportY = iHeight - (viewportHeight * 1);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_4_1_ambient);
    glUniform3fv(Kd_uniform, 1, material_4_1_diffuse);
    glUniform3fv(Ks_uniform, 1, material_4_1_specular);
    glUniform1f(material_shininess_uniform, material_4_1_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FOURTH COLUMN, SECOND SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 2);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_4_2_ambient);
    glUniform3fv(Kd_uniform, 1, material_4_2_diffuse);
    glUniform3fv(Ks_uniform, 1, material_4_2_specular);
    glUniform1f(material_shininess_uniform, material_4_2_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FOURTH COLUMN, THIRD SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 3);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_4_3_ambient);
    glUniform3fv(Kd_uniform, 1, material_4_3_diffuse);
    glUniform3fv(Ks_uniform, 1, material_4_3_specular);
    glUniform1f(material_shininess_uniform, material_4_3_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FOURTH COLUMN, FOURTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 4);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_4_4_ambient);
    glUniform3fv(Kd_uniform, 1, material_4_4_diffuse);
    glUniform3fv(Ks_uniform, 1, material_4_4_specular);
    glUniform1f(material_shininess_uniform, material_4_4_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FOURTH COLUMN, FIFTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 5);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_4_5_ambient);
    glUniform3fv(Kd_uniform, 1, material_4_5_diffuse);
    glUniform3fv(Ks_uniform, 1, material_4_5_specular);
    glUniform1f(material_shininess_uniform, material_4_5_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // DRAW FOURTH COLUMN, FIFTH SPHERE
    
    // set viewport
    viewportY = iHeight - (viewportHeight * 6);    // y axis starting position for viewport
    [self viewportChange:viewportX :viewportY :viewportWidth :viewportHeight];
    
    // make model matrix to identity
    modelMatrix = vmath::mat4::identity();
    
    // apply z axis translation to go deep into the screen by -2.5
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -7.0f);
    
    // pass model matrix and material components to shader through uniform
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniform3fv(Ka_uniform, 1, material_4_6_ambient);
    glUniform3fv(Kd_uniform, 1, material_4_6_diffuse);
    glUniform3fv(Ks_uniform, 1, material_4_6_specular);
    glUniform1f(material_shininess_uniform, material_4_6_shininess);
    
    // bind to vao of sphere
    glBindVertexArray(vao);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind from vao of sphere
    glBindVertexArray(0);
    
    // stop using opengl program object
    glUseProgram(0);
    
    [self updateAngle];
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)viewportChange:(int)x :(int)y :(int)width :(int)height
{
    if (height == 0)
        height = 1;
    
    glViewport(x, y, (GLsizei)width, (GLsizei)height);
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

- (void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    [self viewportChange:0 :0 :width :height];
    [self drawView:nil];
}

- (void)startAnimation
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

// to become first responder
-(BOOL)acceptFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)onSingleTap:(UITapGestureRecognizer *)gr
{
    singleTap++;
    if(singleTap == 1)
    {
        // resest light position
        light_Position[0] = 0.0f;
        light_Position[1] = 0.0f;
        light_Position[2] = 0.0f;
        light_Position[3] = 0.0f;
    }
    else if(singleTap == 2)
    {
        // resest light position
        light_Position[0] = 0.0f;
        light_Position[1] = 0.0f;
        light_Position[2] = 0.0f;
        light_Position[3] = 0.0f;
    }
    else if(singleTap == 3)
    {
        // resest light position
        light_Position[0] = 0.0f;
        light_Position[1] = 0.0f;
        light_Position[2] = 0.0f;
        light_Position[3] = 0.0f;
    }
    else if(singleTap > 3)
    {
        singleTap = 0;
    }
}

- (void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if(gbLight == false)
        gbLight = true;
    else if(gbLight == true)
        gbLight = false;
}

- (void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

-(void)dealloc
{
    // destroy vao
    if (vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao = 0;
    }
    // destroy position vbo
    if (vbo_position)
    {
        glDeleteBuffers(1, &vbo_position);
        vbo_position = 0;
    }
    // destroy normal vbo
    if (vbo_normal)
    {
        glDeleteBuffers(1, &vbo_normal);
        vbo_normal = 0;
    }
    // destroy element vbo
    if (vbo_index)
    {
        glDeleteBuffers(1, &vbo_index);
        vbo_index = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}

@end
