#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

// namespace
using namespace std;

// global variable declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

void draw3DRotatingShapes(void);

float angle = 0.0f;

FILE *fp=NULL;

void drawPyramid(void);
void drawCube(void);

GLXContext gGLXContext;

// entry point function
int main(void)
{
	// function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void update(void);
	void resize(int,int);
	void uninitialize(void);

	// variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;

	// open file for log writing
	fp = fopen("log.txt","w");
	if(fp == NULL)
	{
		printf("Can not open log file");
		exit(0);
	}

	// code
	CreateWindow();

	// initialize
	initialize();

	// message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
						break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					uninitialize();
					exit(0);
				default:
					break;
			}
		}
		display();
		update();
	}
	return(0);
}

void CreateWindow(void)
{
	fprintf(fp,"\nEntering CreateWindow function\n");

	// function prototype
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("Error : Unable To Open X Display. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);

	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window\n Exiting Now\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay,gWindow,"3d Rendering using log file");

	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);

	fprintf(fp,"Exiting CreateWindow function\n");
}

void ToggleFullscreen(void)

{
	fprintf(fp,"Entering ToggleFullScreen function\n");

	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	// code
	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1] = fullscreen;
	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);

	fprintf(fp,"Exiting ToggleFullScreen function\n");
}

void initialize(void)
{
	fprintf(fp,"Entering initilize function\n");

	// function prototype
	void resize(int,int);

	// code
	gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// To avoid aliasing
	glShadeModel(GL_SMOOTH);
	// To avoid distortion
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	resize(giWindowWidth,giWindowHeight);

	fprintf(fp,"Exiting initilize function\n");
}

void display(void)
{
	fprintf(fp,"Entering display function\n");

	// code
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-1.5f,0.0f,-5.0f);
	glRotatef(angle,0.0f,1.0f,0.0f);
	glScalef(0.5f,0.5f,0.5f);

	draw3DRotatingShapes();

	glXSwapBuffers(gpDisplay,gWindow);

	fprintf(fp,"Exiting display function\n");
}

void draw3DRotatingShapes()
{
	fprintf(fp,"Entering draw3DRotatingShapes function\n");

	drawPyramid();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f,0.0f,-5.0f);
	glRotatef(-angle,0.0f,1.0f,0.0f);
	glScalef(0.5f,0.5f,0.5f);
	drawCube();

	fprintf(fp,"Exiting draw3DRotatingShapes function\n");
}

void drawPyramid()
{
	fprintf(fp,"Entering drawPyramid function\n");

	glBegin(GL_TRIANGLES);

	// FRONT FACE
	glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left corner of front face

	glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(1.0f, -1.0f, 1.0f);	// right corner of front face

	// RIGHT FACE
	glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(1.0f, -1.0f, 1.0f);	// left corner of right face

	glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(1.0f, -1.0f, -1.0f);	// right corner of right face

	// BACK FACE
	glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(1.0f, -1.0f, -1.0f);	// left corner of back face

	glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(-1.0f, -1.0f, -1.0f);// right corner of back face

	// LEFT FACE
	glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(-1.0f, -1.0f, -1.0f);// left corner of left face

	glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(-1.0f, -1.0f, 1.0f);	// right corner of left face

	glEnd();

	fprintf(fp,"Exiting drawPyramid function\n");
}

void drawCube()
{
	fprintf(fp,"Entering drawCube function\n");

	glBegin(GL_QUADS);

	// FRONT FACE
	glColor3f(0.0f, 0.0f, 1.0f);	// Blue
	glVertex3f(1.0f, 1.0f, 1.0f);	// right-top corner of front face
	glVertex3f(-1.0f, 1.0f, 1.0f);	// left-top corner of front face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left-bottom corner of front face
	glVertex3f(1.0f, -1.0f, 1.0f);	// right-bottom corner of front face

	// RIGHT FACE
	glColor3f(1.0f, 0.0f, 1.0f);	// MAGENTA
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of right face
	glVertex3f(1.0f, 1.0f, 1.0f);	// left-top corner of right face
	glVertex3f(1.0f, -1.0f, 1.0f);	// left-bottom corner of right face
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-bottom corner of right face

	// BACK FACE
	glColor3f(0.0f, 1.0f, 1.0f);	// CYAN
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of back face
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of back face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-bottom corner of back face
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-bottom corner of back face

	// LEFT FACE
	glColor3f(1.0f,1.0f,0.0f);	// YELLOW
	glVertex3f(-1.0f, 1.0f, 1.0f);	// right-top corner of left face 
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of left face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-bottom corner of left face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// right-bottom corner of left face

	// TOP FACE
	glColor3f(1.0f, 0.0f, 0.0f);	// RED
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of top face
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of top face
	glVertex3f(-1.0f, 1.0f, 1.0f);	// left-bottom corner of top face
	glVertex3f(1.0f, 1.0f, 1.0f);	// right-bottom corner of top face

	// BOTTOM FACE
	glColor3f(0.0f, 1.0f, 0.0f);	// GREEN
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-top corner of bottom face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-top corner of bottom face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left-bottom corner of bottom face
	glVertex3f(1.0f, -1.0f, 1.0f);	// right-bottom corner of bottom face

	glEnd();

	fprintf(fp,"Exiting drawCube function\n");
}

void update()
{
	fprintf(fp,"Entering update function\n");

	if(angle<360)
		angle = angle + 0.1f;
	else
		angle = 0.0f;

	fprintf(fp,"Exiting drawCube function\n");
}

void resize(int width, int height)
{
	fprintf(fp,"Entering resize function\n");

	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,100.0f);

	fprintf(fp,"Exiting resize function\n");
}

void uninitialize(void)
{
	fprintf(fp,"Entering uninitialize function\n");

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);

	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	fprintf(fp,"Exiting uninitialize function\n");
}

