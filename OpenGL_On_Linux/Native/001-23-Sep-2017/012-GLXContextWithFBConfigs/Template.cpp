//headers
#include <iostream>
#include <stdio.h> //for printf()
#include <stdlib.h> //for exit()
#include <memory.h> //for memset()

//headers for XServer
#include <X11/Xlib.h> //analogous to windows.h
#include <X11/Xutil.h> //for visuals
#include <X11/XKBlib.h> //XkbKeycodeToKeysym()
#include <X11/keysym.h> //for 'Keysym'

#include <GL/gl.h>
#include <GL/glx.h> //for 'glx' functions

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declarations
FILE *gpFile = NULL;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext; //parallel to HGLRC

bool gbFullscreen = false;

// entry point function
int main(int argc, char *argv[])
{
	// function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void resize(int,int);
	void display(void);
	void uninitialize(void);

	// code
	// create log file
	gpFile = fopen("Log.txt","w");
	if(gpFile == NULL)
	{
		printf("Log file can not be created. Exiting now\n");
		exit(0);
	}
	else
	{
		fprintf(gpFile,"Log file is created successfully\n");
	}

	// create window
	CreateWindow();

	// initialize
	initialize();

	// message loop

	// variable declarations
	XEvent event;
	KeySym keySym;
	int winWidth;
	int winHeight;
	bool bDone = false;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);	// parallel to GetMessage()
			switch(event.type)		// parallel to iMsg
			{
				case MapNotify:		//parallel to WM_CREATE
					break;
				case KeyPress:		// parallel to WM_KEYDOWN
					keySym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keySym)
					{
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(gbFullscreen == false)
							{
								ToggleFullscreen();
								gbFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								gbFullscreen = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:	// left button
							break;
						case 2:	// middle button
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:	// parallel to WM_MOUSEMOVE
					break;
				case ConfigureNotify:	// parallel to WM_SIZE
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}
	uninitialize();
	return(0);
}


void CreateWindow(void)
{
	// function prototype
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int styleMask;
	int i;

	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,8,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		//GLX_SAMPLE_BUFFERS,1,
		//GLX_SAMPLES,4,
		None};	// array must be terminated by 0

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable to obtain X Display\n");
		uninitialize();
		exit(1);
	}

	// get a new framebuffer config that meets our attrib requirements
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	if(pGLXFBConfigs == NULL)
	{
		printf("Failed to get valid framebuffer config. Exiting now..\n");
		uninitialize();
		exit(1);
	}
	printf("%d Matching FB Configs Found\n",iNumFBConfigs);

	// pick tat FB Config/visual with the most samples per pixel
	int bestFramebufferconfig=-1,worstFramebufferconfig=-1,bestNumberOfSamples=-1,worstNumberOfSamples=999;

	for(i=0;i<iNumFBConfigs;i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffer,samples;
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
			printf("Matching Framebuffer Config = %d : Visual ID = 0x%lu : SAMPLE_BUFFERS = %d : SAMPLES = %d\n",i,pTempXVisualInfo->visualid,sampleBuffer,samples);
			if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferconfig = i;
				bestNumberOfSamples = samples;
			}
			if(worstFramebufferconfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferconfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];
	// set global GLXFBConfig
	gGLXFBConfig = bestGLXFBConfig;

	// be sure to free FBConfig list allocated by glXChooseFBConfig()
	XFree(pGLXFBConfigs);

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
	printf("Chosen Visual ID = 0x%lu\n",gpXVisualInfo->visualid);

	// setting window's attributes
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
						RootWindow(gpDisplay,gpXVisualInfo->screen),
						// you can give defaultscreen as well
						gpXVisualInfo->visual,
						AllocNone);	// for 'movable' memory allocation
	winAttribs.event_mask = StructureNotifyMask | KeyPressMask | ButtonPressMask | ExposureMask | VisibilityChangeMask | PointerMotionMask;
	styleMask = CWBorderPixel | CWEventMask | CWColormap;
	gColormap = winAttribs.colormap;

	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				0,
				0,
				WIN_WIDTH,
				WIN_HEIGHT,
				0,	// border width
				gpXVisualInfo->depth, // depth of visual(depth for colormap)
				InputOutput, 	// class of your window
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if(!gWindow)
	{
		printf("Failure In Window Creation\n");
		uninitialize();
		exit(1);
	}
	XStoreName(gpDisplay,gWindow,"OpenGL Window");
	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	Atom wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);	// normal window size
	XEvent event;
	memset(&event,0,sizeof(XEvent));

	event.type = ClientMessage;
	event.xclient.window = gWindow;
	event.xclient.message_type = wm_state;
	event.xclient.format = 32;
	event.xclient.data.l[0] = gbFullscreen ? 0 : 1;

	Atom fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	event.xclient.data.l[1] = fullscreen;

	// parallel to SendMessage
	XSendEvent(gpDisplay,
			RootWindow(gpDisplay,gpXVisualInfo->screen),
			False, // do not send this message to sibling window
			StructureNotifyMask,	// resizing mask (event_mask)
			&event);
}


void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int,int);

	// code
	// create a new GL context 4.5 for rendering
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
printf("in initialize\n");
	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,3,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,0};	// array must be terminated by 0

	gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	

	if(!gGLXContext)	// fallback to safe old style 2.x context
	{
		// when a context version below 3.0 is requested, implementation will return
		// the newest context version compatible with OpenGL versions less than version 3.0
		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0};	// array must be terminated by 0

		printf("Failed to create GLX 4.5 context. Hence using Old-Style GLX Context\n");
		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	else
	{
		printf("OpenGL context 4.5 is created\n");
	}

	// verifying that context is a direct context
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Indirect GLX Rendering context obtained\n");
	}
	else
	{
		printf("Direct GLX rendering context obtained\n");
	}

	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	// code
	glShadeModel(GL_SMOOTH);
	// set up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice perceptive calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	// we will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	// set background clearing color
	glClearColor(0.0f,0.0f,1.0f,0.0f);

	// resize
	resize(WIN_WIDTH,WIN_HEIGHT);
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
}


void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glXSwapBuffers(gpDisplay,gWindow);
}

void uninitialize()
{
	// releasing opengl related and xwindow related objects
	GLXContext currentContext = glXGetCurrentContext();
	if(currentContext!=NULL && currentContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	if(gpFile)
	{
		fprintf(gpFile,"Log file is created successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
