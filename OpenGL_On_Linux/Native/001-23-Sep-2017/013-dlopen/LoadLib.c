#include<stdio.h>
#include<dlfcn.h>
#include <stdlib.h>
#include <gnu/lib-names.h>

typedef int(*my_add)(int,int);
my_add fpn;

int main()
{
	void *handle;
	handle = dlopen("libmy_calc.so",RTLD_LAZY);
	if(!handle)
	{
		fprintf(stderr,"%s\n",dlerror());
		exit(EXIT_FAILURE);
	}

	fpn=(my_add)dlsym(handle,"add");

	int sum = fpn(10,15);
	printf("addition is %d",sum);

	return 0;
}
