#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<SOIL/SOIL.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

// namespace
using namespace std;

// global variable declarations
bool bFullscreen = false;
// flag for enabled lighting
bool gbLighting = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;
GLfloat angleSphere = 0.0f;

GLUquadric *quadric = NULL;

// light 1 components (right side red light)
GLfloat light_1_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_1_diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_specular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_position[] = { 2.0f,0.8f,1.0f,0.0f };

// light 2 components (left side blue light)
GLfloat light_2_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_2_diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_2_specular[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_2_position[] = { -2.0f,0.8f,1.0f,0.0f };

// material array
GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess[] = { 50.0f,50.0f,50.0f,0.0f };

// flag to maintain which object is showing currently in window
GLboolean gbPyramidActive = GL_TRUE;
GLboolean gbCubeActive = GL_FALSE;
GLboolean gbSphereActive = GL_FALSE;

// entry point function
int main(void)
{
	// function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void update(void);
	void resize(int,int);
	void uninitialize(void);

	// variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;

	// code
	CreateWindow();

	// initialize
	initialize();

	// message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						case XK_l:
							if (gbLighting == false)
							{
								glEnable(GL_LIGHTING);
								gbLighting = true;
							}
							else
							{
								glDisable(GL_LIGHTING);
								gbLighting = false;
							}
							break;
						case XK_p:	// on p keypress, enable pyramid flag
							gbPyramidActive = GL_TRUE;
							gbCubeActive = GL_FALSE;
							gbSphereActive = GL_FALSE;
							break;
						case XK_c:	// on c keypress, enable cube flag
							gbPyramidActive = GL_FALSE;
							gbCubeActive = GL_TRUE;
							gbSphereActive = GL_FALSE;
							break;
						case XK_s:	// on s keypress, enable sphere flag
							gbPyramidActive = GL_FALSE;
							gbCubeActive = GL_FALSE;
							gbSphereActive = GL_TRUE;
							break;
						default:
							break;
					}
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
						break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					uninitialize();
					exit(0);
				default:
					break;
			}
		}
		update();
		display();
	}
	return(0);
}

void CreateWindow(void)
{
	// function prototype
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("Error : Unable To Open X Display. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);

	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window\n Exiting Now\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay,gWindow,"Spinning pyramid, cube, sphere using 2 lights");

	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	// code
	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1] = fullscreen;
	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);
}

void initialize(void)
{
	// function prototype
	void resize(int,int);

	// code
	gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// To avoid aliasing
	glShadeModel(GL_SMOOTH);
	// To avoid distortion
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	// set light source parameters (right side light)
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_1_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_1_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_1_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_1_position);

	// Enable the lighting
	glEnable(GL_LIGHT0);

	// set light source parameters (left side light)
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_2_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_2_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_2_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light_2_position);

	// Enable the lighting
	glEnable(GL_LIGHT1);

	// material array
	glMaterialfv(GL_FRONT, GL_SHININESS, material_ambient);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	quadric = gluNewQuadric();

	resize(giWindowWidth,giWindowHeight);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Prototype declaration
	void drawPyramid();
	void drawCube();

	if (gbPyramidActive == GL_TRUE)
	{
		// Show rotating pyramid
		glTranslatef(0.0f, 0.0f, -6.0f);
		glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);
		drawPyramid();
	}
	else if (gbCubeActive == GL_TRUE)
	{
		// Show rotating cube
		glTranslatef(0.0f, 0.0f, -6.0f);
		glScalef(0.75f, 0.75f, 0.75f);
		glRotatef(angleCube, 0.0f, 1.0f, 0.0f);
		drawCube();
	}
	else if (gbSphereActive == GL_TRUE)
	{
		// Show rotating sphere
		glTranslatef(0.0f, 0.0f, -3.0f);
		glRotatef(angleSphere, 0.0f, 1.0f, 0.0f);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		gluSphere(quadric, 0.75, 30, 30);
	}
	else
	{
	}

	glXSwapBuffers(gpDisplay,gWindow);
}

// draw multi colored triangle
void drawPyramid()
{
	glBegin(GL_TRIANGLES);

	// FRONT FACE
	glNormal3f(0.0f, 0.447214f, 0.894427f);		// set normal for front face

	//glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	//glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left corner of front face

	//glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(1.0f, -1.0f, 1.0f);	// right corner of front face

	// RIGHT FACE
	glNormal3f(0.894427f, 0.447214f, 0.0f);	// set normal for right face

	//glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	//glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(1.0f, -1.0f, 1.0f);	// left corner of right face

	//glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(1.0f, -1.0f, -1.0f);	// right corner of right face

	// BACK FACE
	glNormal3f(0.0f, 0.447214f, -0.894427f);	// set normal for back face

	//glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	//glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(1.0f, -1.0f, -1.0f);	// left corner of back face

	//glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(-1.0f, -1.0f, -1.0f);// right corner of back face

	// LEFT FACE
	glNormal3f(-0.894427f, 0.447214f, 0.0f);	// set normal for left face

	//glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	//glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(-1.0f, -1.0f, -1.0f);// left corner of left face

	//glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(-1.0f, -1.0f, 1.0f);	// right corner of left face

	glEnd();
}

// draw multi colored cube
void drawCube()
{
	glBegin(GL_QUADS);

	// FRONT FACE
	glNormal3f(0.0f, 0.0f, 1.0f);	// set normal for front face
	//glColor3f(0.0f, 0.0f, 1.0f);	// Blue
	glVertex3f(1.0f, 1.0f, 1.0f);	// right-top corner of front face
	glVertex3f(-1.0f, 1.0f, 1.0f);	// left-top corner of front face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left-bottom corner of front face
	glVertex3f(1.0f, -1.0f, 1.0f);	// right-bottom corner of front face

									// RIGHT FACE
	glNormal3f(1.0f, 0.0f, 0.0f);		// set normal for right face
	//glColor3f(1.0f, 0.0f, 1.0f);	// MAGENTA
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of right face
	glVertex3f(1.0f, 1.0f, 1.0f);	// left-top corner of right face
	glVertex3f(1.0f, -1.0f, 1.0f);	// left-bottom corner of right face
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-bottom corner of right face

									// BACK FACE
	glNormal3f(0.0f, 0.0f, -1.0f);	// set normal for back face
	//glColor3f(0.0f, 1.0f, 1.0f);	// CYAN
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of back face
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of back face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-bottom corner of back face
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-bottom corner of back face

									// LEFT FACE
	glNormal3f(-1.0f, 0.0f, 0.0f);	// set normal for left face
	//glColor3f(1.0f, 1.0f, 0.0f);	// YELLOW
	glVertex3f(-1.0f, 1.0f, 1.0f);	// right-top corner of left face 
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of left face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-bottom corner of left face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// right-bottom corner of left face

									// TOP FACE
	glNormal3f(0.0f, 1.0f, 0.0f);	// set normal for top face
	//glColor3f(1.0f, 0.0f, 0.0f);	// RED
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of top face
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of top face
	glVertex3f(-1.0f, 1.0f, 1.0f);	// left-bottom corner of top face
	glVertex3f(1.0f, 1.0f, 1.0f);	// right-bottom corner of top face

									// BOTTOM FACE
	glNormal3f(0.0f, -1.0f, 0.0f);	// set normal for bottom face
	//glColor3f(0.0f, 1.0f, 0.0f);	// GREEN
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-top corner of bottom face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-top corner of bottom face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left-bottom corner of bottom face
	glVertex3f(1.0f, -1.0f, 1.0f);	// right-bottom corner of bottom face

	glEnd();
}

void update(void)
{
	anglePyramid = anglePyramid + 1.0f;
	if (anglePyramid >= 360)
	{
		anglePyramid = 0.0f;
	}
	angleCube = angleCube + 1.0f;
	if (angleCube >= 360)
	{
		angleCube = 0.0f;
	}
	angleSphere = angleSphere + 1.0f;
	if (angleSphere >= 360)
	{
		angleSphere = 0.0f;
	}
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,100.0f);
}

void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);

	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

