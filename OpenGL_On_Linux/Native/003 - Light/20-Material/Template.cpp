#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<SOIL/SOIL.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

// namespace
using namespace std;

// global variable declarations
bool bFullscreen = false;
// flag for enabled lighting
bool gbLighting = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

GLUquadric *quadric = NULL;

// define material array for first sphere of first column
GLfloat material_1_1_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
GLfloat material_1_1_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
GLfloat material_1_1_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
GLfloat material_1_1_shininess = {0.6f * 128.0f};

// define material array for second sphere of first column
GLfloat material_1_2_ambient[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_1_2_diffuse[] = { 0.54f,0.89f,0.63f,1.0f };
GLfloat material_1_2_specular[] = { 0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_1_2_shininess = { 0.1f * 128.0f };

// define material array for third sphere of first column
GLfloat material_1_3_ambient[] = { 0.05375f,0.05f,0.06625f,1.0f };
GLfloat material_1_3_diffuse[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_1_3_specular[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_1_3_shininess = { 0.3f * 128.0f };

// define material array for fourth sphere of first column
GLfloat material_1_4_ambient[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_1_4_diffuse[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_1_4_specular[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_1_4_shininess = { 0.088f * 128.0f };

// define material array for fifth sphere of first column
GLfloat material_1_5_ambient[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_1_5_diffuse[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_1_5_specular[] = { 0.727811f,0.626959f,0.626959f,1.0f };
GLfloat material_1_5_shininess = { 0.6f * 128.0f };

// define material array for six sphere of first column
GLfloat material_1_6_ambient[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_1_6_diffuse[] = { 0.396f,0.74151f,0.69102f,1.0f };
GLfloat material_1_6_specular[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_1_6_shininess = { 0.6f * 128.0f };

// define material array for first sphere of second column
GLfloat material_2_1_ambient[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_2_1_diffuse[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_2_1_specular[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_2_1_shininess = { 0.21794872f * 128.0f };

// define material array for second sphere of second column
GLfloat material_2_2_ambient[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_2_2_diffuse[] = { 0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_2_2_specular[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_2_2_shininess = { 0.2f * 128.0f };

// define material array for third sphere of second column
GLfloat material_2_3_ambient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat material_2_3_diffuse[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_2_3_specular[] = { 0.774597f,0.774597f,0.774597f,1.0f };
GLfloat material_2_3_shininess = { 0.6f * 128.0f };

// define material array for fourth sphere of second column
GLfloat material_2_4_ambient[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_2_4_diffuse[] = { 0.7038f,0.27048f,0.0828f,1.0f };
GLfloat material_2_4_specular[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_2_4_shininess = { 0.1f * 128.0f };

// define material array for fifth sphere of second column
GLfloat material_2_5_ambient[] = { 0.24725f,0.1995f,0.0745f,1.0f };
GLfloat material_2_5_diffuse[] = { 0.75164f,0.60648f,0.22648f,1.0f };
GLfloat material_2_5_specular[] = { 0.628281f,0.555802f,0.366065f,1.0f };
GLfloat material_2_5_shininess = { 0.4f * 128.0f };

// define material array for six sphere of second column
GLfloat material_2_6_ambient[] = { 0.19225f,0.19225f,0.19225f,1.0f };
GLfloat material_2_6_diffuse[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_2_6_specular[] = { 0.508273f,0.508273f,0.508273f,1.0f };
GLfloat material_2_6_shininess = { 0.4f * 128.0f };

// define material array for first sphere of third column
GLfloat material_3_1_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_3_1_specular[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_3_1_shininess = { 0.25f * 128.0f };

// define material array for second sphere of third column
GLfloat material_3_2_ambient[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_3_2_diffuse[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_3_2_specular[] = { 0.50196078f,0.50196078f,0.50196078f,1.0f };
GLfloat material_3_2_shininess = { 0.25f * 128.0f };

// define material array for third sphere of third column
GLfloat material_3_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_3_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_3_3_specular[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_3_3_shininess = { 0.25f * 128.0f };

// define material array for fourth sphere of third column
GLfloat material_3_4_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_4_diffuse[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_3_4_specular[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_3_4_shininess = { 0.25f * 128.0f };

// define material array for fifth sphere of third column
GLfloat material_3_5_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_5_diffuse[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_3_5_specular[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_3_5_shininess = { 0.25f * 128.0f };

// define material array for six sphere of third column
GLfloat material_3_6_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_6_diffuse[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_3_6_specular[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_3_6_shininess = { 0.25f * 128.0f };

// define material array for first sphere of fourth column
GLfloat material_4_1_ambient[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_4_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_4_1_specular[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_4_1_shininess = { 0.078125f * 128.0f };

// define material array for second sphere of fourth column
GLfloat material_4_2_ambient[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_4_2_diffuse[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_4_2_specular[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_4_2_shininess = { 0.078125f * 128.0f };

// define material array for third sphere of fourth column
GLfloat material_4_3_ambient[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_4_3_diffuse[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_4_3_specular[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_4_3_shininess = { 0.078125f * 128.0f };

// define material array for fourth sphere of fourth column
GLfloat material_4_4_ambient[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_4_4_diffuse[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_4_4_specular[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_4_4_shininess = { 0.078125f * 128.0f };

// define material array for fifth sphere of fourth column
GLfloat material_4_5_ambient[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_4_5_diffuse[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_4_5_specular[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_4_5_shininess = { 0.078125f * 128.0f };

// define material array for six sphere of fourth column
GLfloat material_4_6_ambient[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_4_6_diffuse[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_4_6_specular[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_4_6_shininess = { 0.078125f * 128.0f };

// 
GLfloat light_model_ambient[] = { 0.2f,0.2f,0.2f,0.0f };
GLfloat light_model_local_viewer[] = { 0.0f };

// light 0 components (red light)
GLfloat light_0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_0_diffuse[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_0_specular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_0_position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat angleWhiteLight = 0.0f;
GLint keyPressed;

// prototype of drawSphere
void drawSphere();

// entry point function
int main(void)
{
	// function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void update(void);
	void resize(int,int);
	void uninitialize(void);

	// variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;

	// code
	CreateWindow();

	// initialize
	initialize();

	// message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						case XK_l:
							// if gbLighting is true then enable lighting
							// else disable lighting
							if (gbLighting == true)
							{
								glDisable(GL_LIGHTING);
								gbLighting = false;
							}
							else if (gbLighting == false)
							{
								glEnable(GL_LIGHTING);
								gbLighting = true;

								// if lighting enabled then set keyPressed to 0
								// also set light position from postivive z direction to negative z direction
								keyPressed = 0;
								light_0_position[0] = 0.0f;
								light_0_position[1] = 0.0f;
								light_0_position[2] = 1.0f;
								light_0_position[3] = 0.0f;
								glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
							}
							break;
						case XK_x:
							keyPressed = 1;
							// resest light position
							light_0_position[0] = 0.0f;
							light_0_position[1] = 0.0f;
							light_0_position[2] = 0.0f;
							light_0_position[3] = 0.0f;
							glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
							break;
						case XK_y:
							keyPressed = 2;
							// resest light position
							light_0_position[0] = 0.0f;
							light_0_position[1] = 0.0f;
							light_0_position[2] = 0.0f;
							light_0_position[3] = 0.0f;
							glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
							break;
						case XK_z:
							keyPressed = 3;
							// resest light position
							light_0_position[0] = 0.0f;
							light_0_position[1] = 0.0f;
							light_0_position[2] = 0.0f;
							light_0_position[3] = 0.0f;
							glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
							break;
						default:
							break;
					}
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
						break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					uninitialize();
					exit(0);
				default:
					break;
			}
		}
		update();
		display();
	}
	return(0);
}

void CreateWindow(void)
{
	// function prototype
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("Error : Unable To Open X Display. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);

	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window\n Exiting Now\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay,gWindow,"Material");

	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	// code
	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1] = fullscreen;
	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);
}

void initialize(void)
{
	// function prototype
	void resize(int,int);

	// code
	gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// set light source parameters for light 0
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_0_specular);

	// Enable the lighting for light 0
	glEnable(GL_LIGHT0);

	quadric = gluNewQuadric();

	// To avoid aliasing
	glShadeModel(GL_SMOOTH);
	// To avoid distortion
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	// describe light model
	// describe intensity of ambient light
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	// define location of viewer
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	// specify clear values for the color buffers
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	resize(giWindowWidth,giWindowHeight);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();

	// set camera position
	gluLookAt(0, 0, 0.1, 0, 0, 0, 0, 1, 0);

	if (keyPressed == 1)
	{
		glPushMatrix();
		glRotatef(angleWhiteLight, 1, 0, 0);
		light_0_position[1] = angleWhiteLight;
		glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
		glPopMatrix();
	}
	else if (keyPressed == 2)
	{
		glPushMatrix();
		glRotatef(angleWhiteLight, 0, 1, 0);
		light_0_position[0] = angleWhiteLight;
		glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
		glPopMatrix();
	}
	else if (keyPressed == 3)
	{
		glPushMatrix();
		glRotatef(angleWhiteLight, 0, 0, 1);
		light_0_position[0] = angleWhiteLight;
		glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
		glPopMatrix();
	}

	drawSphere();

	glPopMatrix();

	glXSwapBuffers(gpDisplay,gWindow);
}

void drawSphere()
{
	// first Column, first sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_1_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_1_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_1_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_1_1_shininess);

	glTranslatef(-1.4f, 1.9f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// first Column, second sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_1_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_1_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_1_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_1_2_shininess);

	glTranslatef(-1.4f, 1.1f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// first Column, third sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_1_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_1_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_1_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_1_3_shininess);

	glTranslatef(-1.4f, 0.3f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// first Column, fourth sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_1_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_1_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_1_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_1_4_shininess);

	glTranslatef(-1.4f, -0.5f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// first Column, fifth sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_1_5_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_1_5_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_1_5_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_1_5_shininess);

	glTranslatef(-1.4f, -1.3f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// first Column, six sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_1_6_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_1_6_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_1_6_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_1_6_shininess);

	glTranslatef(-1.4f, -2.1f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);
	glPopMatrix();

	// second Column, first sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_2_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_2_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_2_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_2_1_shininess);

	glTranslatef(-0.5f, 1.9f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// second Column, second sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_2_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_2_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_2_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_2_2_shininess);

	glTranslatef(-0.5f, 1.1f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// second Column, third sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_2_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_2_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_2_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_2_3_shininess);

	glTranslatef(-0.5f, 0.3f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// second Column, fourth sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_2_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_2_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_2_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_2_4_shininess);

	glTranslatef(-0.5f, -0.5f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// second Column, fifth sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_2_5_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_2_5_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_2_5_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_2_5_shininess);

	glTranslatef(-0.5f, -1.3f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// second Column, six sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_2_6_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_2_6_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_2_6_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_2_6_shininess);

	glTranslatef(-0.5f, -2.1f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// third Column, first sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_3_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_3_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_3_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_3_1_shininess);

	glTranslatef(0.4f, 1.9f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);
	glPopMatrix();

	// third Column, second sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_3_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_3_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_3_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_3_2_shininess);

	glTranslatef(0.4f, 1.1f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);
	glPopMatrix();

	// third Column, third sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_3_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_3_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_3_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_3_3_shininess);

	glTranslatef(0.4f, 0.3f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// third Column, fourth sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_3_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_3_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_3_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_3_4_shininess);

	glTranslatef(0.4f, -0.5f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);
	glPopMatrix();

	// third Column, fifth sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_3_5_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_3_5_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_3_5_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_3_5_shininess);

	glTranslatef(0.4f, -1.3f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// third Column, six sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_3_6_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_3_6_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_3_6_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_3_6_shininess);

	glTranslatef(0.4f, -2.1f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// Fourth Column, first sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_4_1_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_4_1_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_4_1_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_4_1_shininess);

	glTranslatef(1.3f, 1.9f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// Fourth Column, second sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_4_2_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_4_2_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_4_2_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_4_2_shininess);

	glTranslatef(1.3f, 1.1f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// Fourth Column, third sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_4_3_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_4_3_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_4_3_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_4_3_shininess);

	glTranslatef(1.3f, 0.3f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// Fourth Column, fourth sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_4_4_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_4_4_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_4_4_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_4_4_shininess);

	glTranslatef(1.3f, -0.5f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// Fourth Column, fifth sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_4_5_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_4_5_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_4_5_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_4_5_shininess);

	glTranslatef(1.3f, -1.3f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();

	// Fourth Column, six sphere
	glPushMatrix();

	// apply material
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_4_6_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_4_6_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_4_6_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material_4_6_shininess);

	glTranslatef(1.3f, -2.1f, -6.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.35, 30, 30);

	glPopMatrix();
}

void update(void)
{
	angleWhiteLight = angleWhiteLight + 1.0f;
	if (angleWhiteLight >= 360)
	{
		angleWhiteLight = 0.0f;
	}
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,100.0f);
}

void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);

	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}

