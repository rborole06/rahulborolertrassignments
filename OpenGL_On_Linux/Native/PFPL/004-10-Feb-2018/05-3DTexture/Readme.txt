
*****************************************************************
Application shows how to render pyramid and cube with 3d texture
*****************************************************************

NOTE : 

1 - As we know that this is a programmable function pipeline code
    There is no API gluBuild2DMipmap in programmable function pipeline
    But during doing this code, i copy paste the LoadGLTexture function as it is from Fix Function Pipeline Code for 3D Texture
    And forgot to replace gluBuild2DMipmap by glTexImage2D and glGenerateMipmap
    Though i expected but the output was "ANDHAR" as per said by sir always...even geometry/objects was not visible
    So MUST DO this changes everytime when moving code from FFPL to PFPL for texture