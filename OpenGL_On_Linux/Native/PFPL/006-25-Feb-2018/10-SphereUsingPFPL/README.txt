
1 - This application shows how to render a 3D rotating sphere with light using perspective projection in PFPL

2 - To compile this program, command is as per below
    g++ -o Template Template.cpp -L . -lX11 -lGL -lGLEW -lSphere
    

3 - To run enter below commnad
    ./Template

Note - 
      1 - You will get error as below after running a programm
          ./Template: error while loading shared libraries: libSphere.so: cannot open shared object file: No such file or directory

      2 - To avoid this, execute below command
          export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH

      3 - and run program as ./Template

      4 - You will need to do this everytime when you open a new command prompt to run this program

      5 - If you want to avoid this add current directory to path, but generally no one does this

      6 - To add current directory to path PERMANANTLY, do following steps
          a - goto home direcoty
          b - enter command ls -al
          c - check any one file from .bashrc, .bash-profile, .profile exists
          d - If any of these file exists enter command "sudo vi <filename>"
          e - goto end of file  and without doing any syntactical mistake add below line
              export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
