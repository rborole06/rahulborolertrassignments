
1 - This application shows how to render cube with lighting using perspective projection in programmable function pipeline
2 - Also application shows effect of light on cube
3 - To see effect of light on cube, press key l
4 - By default, when lighting will be enabled, cube will have per vertex lighting
5 - Also on key press V or v, cube will have per vertex lighting and on key press F or f, cube will have per fragment lighting

Note - 
      1 - You will get error as below after running a programm
          ./Template: error while loading shared libraries: libSphere.so: cannot open shared object file: No such file or directory

      2 - To avoid this, execute below command
          export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH

      3 - and run program as ./Template

      4 - You will need to do this everytime when you open a new command prompt to run this program

      5 - If you want to avoid this add current directory to path, but generally no one does this

      6 - To add current directory to path PERMANANTLY, do following steps
          a - goto home direcoty
          b - enter command ls -al
          c - check any one file from .bashrc, .bash-profile, .profile exists
          d - If any of these file exists enter command "sudo vi <filename>"
          e - goto end of file  and without doing any syntactical mistake add below line
              export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
