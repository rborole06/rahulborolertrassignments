//headers
#include <iostream>
#include <stdio.h> //for printf()
#include <stdlib.h> //for exit()
#include <memory.h> //for memset()
#include<SOIL/SOIL.h>

//headers for XServer
#include <X11/Xlib.h> //analogous to windows.h
#include <X11/Xutil.h> //for visuals
#include <X11/XKBlib.h> //XkbKeycodeToKeysym()
#include <X11/keysym.h> //for 'Keysym'

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h> //for 'glx' functions
#include <GL/glu.h>

#include "vmath.h"
#include "sphere.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	VDG_ATTRIBUTE_VERTEX,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//global variable declarations
FILE *gpFile = NULL;

Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext; //parallel to HGLRC

bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gNumVertices;
GLuint gNumElements;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

// light uniforms
GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

GLuint gLKeyPressedUniform;

vmath::mat4 gPerspectiveProjectionMatrix;

bool gbLight;

GLfloat light_Ambient[] = {0.0f,0.0f,0.0f,0.0f};
GLfloat light_Diffuse[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_Specular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_Position[] = { 0.0f,0.0f,0.0f,0.0f };

// define material array for first sphere of first column
GLfloat material_1_1_ambient[] = { 0.0215f,0.1745f,0.0215f,1.0f };
GLfloat material_1_1_diffuse[] = { 0.07568f,0.61424f,0.07568f,1.0f };
GLfloat material_1_1_specular[] = { 0.633f,0.727811f,0.633f,1.0f };
GLfloat material_1_1_shininess = { 0.6f * 128.0f };

// define material array for second sphere of first column
GLfloat material_1_2_ambient[] = { 0.135f,0.2225f,0.1575f,1.0f };
GLfloat material_1_2_diffuse[] = { 0.54f,0.89f,0.63f,1.0f };
GLfloat material_1_2_specular[] = { 0.316228f,0.316228f,0.316228f,1.0f };
GLfloat material_1_2_shininess = { 0.1f * 128.0f };

// define material array for third sphere of first column
GLfloat material_1_3_ambient[] = { 0.05375f,0.05f,0.06625f,1.0f };
GLfloat material_1_3_diffuse[] = { 0.18275f,0.17f,0.22525f,1.0f };
GLfloat material_1_3_specular[] = { 0.332741f,0.328634f,0.346435f,1.0f };
GLfloat material_1_3_shininess = { 0.3f * 128.0f };

// define material array for fourth sphere of first column
GLfloat material_1_4_ambient[] = { 0.25f,0.20725f,0.20725f,1.0f };
GLfloat material_1_4_diffuse[] = { 1.0f,0.829f,0.829f,1.0f };
GLfloat material_1_4_specular[] = { 0.296648f,0.296648f,0.296648f,1.0f };
GLfloat material_1_4_shininess = { 0.088f * 128.0f };

// define material array for fifth sphere of first column
GLfloat material_1_5_ambient[] = { 0.1745f,0.01175f,0.01175f,1.0f };
GLfloat material_1_5_diffuse[] = { 0.61424f,0.04136f,0.04136f,1.0f };
GLfloat material_1_5_specular[] = { 0.727811f,0.626959f,0.626959f,1.0f };
GLfloat material_1_5_shininess = { 0.6f * 128.0f };

// define material array for six sphere of first column
GLfloat material_1_6_ambient[] = { 0.1f,0.18725f,0.1745f,1.0f };
GLfloat material_1_6_diffuse[] = { 0.396f,0.74151f,0.69102f,1.0f };
GLfloat material_1_6_specular[] = { 0.297254f,0.30829f,0.306678f,1.0f };
GLfloat material_1_6_shininess = { 0.6f * 128.0f };

// define material array for first sphere of second column
GLfloat material_2_1_ambient[] = { 0.329412f,0.223529f,0.027451f,1.0f };
GLfloat material_2_1_diffuse[] = { 0.780392f,0.568627f,0.113725f,1.0f };
GLfloat material_2_1_specular[] = { 0.992157f,0.941176f,0.807843f,1.0f };
GLfloat material_2_1_shininess = { 0.21794872f * 128.0f };

// define material array for second sphere of second column
GLfloat material_2_2_ambient[] = { 0.2125f,0.1275f,0.054f,1.0f };
GLfloat material_2_2_diffuse[] = { 0.714f,0.4284f,0.18144f,1.0f };
GLfloat material_2_2_specular[] = { 0.393548f,0.271906f,0.166721f,1.0f };
GLfloat material_2_2_shininess = { 0.2f * 128.0f };

// define material array for third sphere of second column
GLfloat material_2_3_ambient[] = { 0.25f,0.25f,0.25f,1.0f };
GLfloat material_2_3_diffuse[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_2_3_specular[] = { 0.774597f,0.774597f,0.774597f,1.0f };
GLfloat material_2_3_shininess = { 0.6f * 128.0f };

// define material array for fourth sphere of second column
GLfloat material_2_4_ambient[] = { 0.19125f,0.0735f,0.0225f,1.0f };
GLfloat material_2_4_diffuse[] = { 0.7038f,0.27048f,0.0828f,1.0f };
GLfloat material_2_4_specular[] = { 0.256777f,0.137622f,0.086014f,1.0f };
GLfloat material_2_4_shininess = { 0.1f * 128.0f };

// define material array for fifth sphere of second column
GLfloat material_2_5_ambient[] = { 0.24725f,0.1995f,0.0745f,1.0f };
GLfloat material_2_5_diffuse[] = { 0.75164f,0.60648f,0.22648f,1.0f };
GLfloat material_2_5_specular[] = { 0.628281f,0.555802f,0.366065f,1.0f };
GLfloat material_2_5_shininess = { 0.4f * 128.0f };

// define material array for six sphere of second column
GLfloat material_2_6_ambient[] = { 0.19225f,0.19225f,0.19225f,1.0f };
GLfloat material_2_6_diffuse[] = { 0.50754f,0.50754f,0.50754f,1.0f };
GLfloat material_2_6_specular[] = { 0.508273f,0.508273f,0.508273f,1.0f };
GLfloat material_2_6_shininess = { 0.4f * 128.0f };

// define material array for first sphere of third column
GLfloat material_3_1_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_3_1_specular[] = { 0.50f,0.50f,0.50f,1.0f };
GLfloat material_3_1_shininess = { 0.25f * 128.0f };

// define material array for second sphere of third column
GLfloat material_3_2_ambient[] = { 0.0f,0.1f,0.06f,1.0f };
GLfloat material_3_2_diffuse[] = { 0.0f,0.50980392f,0.50980392f,1.0f };
GLfloat material_3_2_specular[] = { 0.50196078f,0.50196078f,0.50196078f,1.0f };
GLfloat material_3_2_shininess = { 0.25f * 128.0f };

// define material array for third sphere of third column
GLfloat material_3_3_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_3_diffuse[] = { 0.1f,0.35f,0.1f,1.0f };
GLfloat material_3_3_specular[] = { 0.45f,0.55f,0.45f,1.0f };
GLfloat material_3_3_shininess = { 0.25f * 128.0f };

// define material array for fourth sphere of third column
GLfloat material_3_4_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_4_diffuse[] = { 0.5f,0.0f,0.0f,1.0f };
GLfloat material_3_4_specular[] = { 0.7f,0.6f,0.6f,1.0f };
GLfloat material_3_4_shininess = { 0.25f * 128.0f };

// define material array for fifth sphere of third column
GLfloat material_3_5_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_5_diffuse[] = { 0.55f,0.55f,0.55f,1.0f };
GLfloat material_3_5_specular[] = { 0.70f,0.70f,0.70f,1.0f };
GLfloat material_3_5_shininess = { 0.25f * 128.0f };

// define material array for six sphere of third column
GLfloat material_3_6_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_3_6_diffuse[] = { 0.5f,0.5f,0.0f,1.0f };
GLfloat material_3_6_specular[] = { 0.60f,0.60f,0.50f,1.0f };
GLfloat material_3_6_shininess = { 0.25f * 128.0f };

// define material array for first sphere of fourth column
GLfloat material_4_1_ambient[] = { 0.02f,0.02f,0.02f,1.0f };
GLfloat material_4_1_diffuse[] = { 0.01f,0.01f,0.01f,1.0f };
GLfloat material_4_1_specular[] = { 0.4f,0.4f,0.4f,1.0f };
GLfloat material_4_1_shininess = { 0.078125f * 128.0f };

// define material array for second sphere of fourth column
GLfloat material_4_2_ambient[] = { 0.0f,0.05f,0.05f,1.0f };
GLfloat material_4_2_diffuse[] = { 0.4f,0.5f,0.5f,1.0f };
GLfloat material_4_2_specular[] = { 0.04f,0.7f,0.7f,1.0f };
GLfloat material_4_2_shininess = { 0.078125f * 128.0f };

// define material array for third sphere of fourth column
GLfloat material_4_3_ambient[] = { 0.0f,0.05f,0.0f,1.0f };
GLfloat material_4_3_diffuse[] = { 0.4f,0.5f,0.4f,1.0f };
GLfloat material_4_3_specular[] = { 0.04f,0.7f,0.04f,1.0f };
GLfloat material_4_3_shininess = { 0.078125f * 128.0f };

// define material array for fourth sphere of fourth column
GLfloat material_4_4_ambient[] = { 0.05f,0.0f,0.0f,1.0f };
GLfloat material_4_4_diffuse[] = { 0.5f,0.4f,0.4f,1.0f };
GLfloat material_4_4_specular[] = { 0.7f,0.04f,0.04f,1.0f };
GLfloat material_4_4_shininess = { 0.078125f * 128.0f };

// define material array for fifth sphere of fourth column
GLfloat material_4_5_ambient[] = { 0.05f,0.05f,0.05f,1.0f };
GLfloat material_4_5_diffuse[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat material_4_5_specular[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat material_4_5_shininess = { 0.078125f * 128.0f };

// define material array for six sphere of fourth column
GLfloat material_4_6_ambient[] = { 0.05f,0.05f,0.0f,1.0f };
GLfloat material_4_6_diffuse[] = { 0.5f,0.5f,0.4f,1.0f };
GLfloat material_4_6_specular[] = { 0.7f,0.7f,0.04f,1.0f };
GLfloat material_4_6_shininess = { 0.078125f * 128.0f };

// color angle
GLfloat angleWhiteLight = 0.0f;
GLint keyPressed;

static bool bIsLKeyPressed = false;

//entry-point function
int main(int argc, char *argv[])
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void resize(int,int);
	void display(void);
	void update(void);
	void uninitialize(void);
	
	// open file for log writing
	gpFile = fopen("log.txt","w");
	if(gpFile == NULL)
	{
		printf("Can not open log file");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Created.\n");
	}

	// create the window
	CreateWindow();
	
	//initialize()
	initialize();
	
	//Message Loop

	//variable declarations
	XEvent event; //parallel to 'MSG' structure
	KeySym keySym;
	int winWidth;
	int winHeight;
	bool bDone=false;
	
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event); //parallel to GetMessage()
			switch(event.type) //parallel to 'iMsg'
			{
				case MapNotify: //parallel to WM_CREATE
					break;
				case KeyPress: //parallel to WM_KEYDOWN
					keySym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keySym)
					{
						case XK_Escape:
							bDone=true;
							break;
						case XK_F:
						case XK_f:
							if(gbFullscreen==false)
							{
								ToggleFullscreen();
								gbFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								gbFullscreen=false;
							}
							break;
						case XK_L:
						case XK_l:	// for L or l
							if(bIsLKeyPressed == false)
							{
								gbLight = true;
								bIsLKeyPressed = true;

								light_Position[0] = 0.0f;
								light_Position[1] = 0.0f;
								light_Position[2] = 1.0f;
								light_Position[3] = 0.0f;
							}
							else
							{
								gbLight = false;
								bIsLKeyPressed = false;
							}
							break;
						case XK_X:
						case XK_x:
							keyPressed = 1;
							// resest light position
							light_Position[0] = 0.0f;
							light_Position[1] = 0.0f;
							light_Position[2] = 0.0f;
							light_Position[3] = 0.0f;
							break;
						case XK_Y:
						case XK_y:
							keyPressed = 2;
							// resest light position
							light_Position[0] = 0.0f;
							light_Position[1] = 0.0f;
							light_Position[2] = 0.0f;
							light_Position[3] = 0.0f;
							break;
						case XK_Z:
						case XK_z:
							keyPressed = 3;
							// resest light position
							light_Position[0] = 0.0f;
							light_Position[1] = 0.0f;
							light_Position[2] = 0.0f;
							light_Position[3] = 0.0f;
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1: //Left Button
							break;
						case 2: //Middle Button
							break;
						case 3: //Right Button
							break;
						default: 
							break;
					}
					break;
				case MotionNotify: //parallel to WM_MOUSEMOVE
					break;
				case ConfigureNotify: //parallel to WM_SIZE
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose: //parallel to WM_PAINT
					break;
				case DestroyNotify:
					break;
				case 33: //close button, system menu -> close
					bDone=true;
					break;
				default:
					break;
			}
		}

		display();
		if (gbLight == true)
		{
			update();
		}
	}
	
	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//function prototype
	void uninitialize(void);
	
	//variable declarations
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNumFBConfigs=0;
	int styleMask;
	int i;
	
	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		//GLX_SAMPLE_BUFFERS,1,
		//GLX_SAMPLES,4,
		None}; // array must be terminated by 0
	
	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable To Obtain X Display.\n");
		uninitialize();
		exit(1);
	}
	
	// get a new framebuffer config that meets our attrib requirements
	pGLXFBConfigs=glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	if(pGLXFBConfigs==NULL)
	{
		printf( "Failed To Get Valid Framebuffer Config. Exitting Now ...\n");
		uninitialize();
		exit(1);
	}
	
	// pick that FB config/visual with the most samples per pixel
	int bestFramebufferconfig=-1,worstFramebufferConfig=-1,bestNumberOfSamples=-1,worstNumberOfSamples=999;
	for(i=0;i<iNumFBConfigs;i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffer,samples;
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
			if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferconfig=i;
				bestNumberOfSamples=samples;
			}
			if( worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];
	// set global GLXFBConfig
	gGLXFBConfig=bestGLXFBConfig;
	
	// be sure to free FBConfig list allocated by glXChooseFBConfig()
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
	
	//setting window's attributes
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,gpXVisualInfo->screen), //you can give defaultScreen as well
										gpXVisualInfo->visual,
										AllocNone); //for 'movable' memory allocation
										
	winAttribs.event_mask=StructureNotifyMask | KeyPressMask | ButtonPressMask |
						  ExposureMask | VisibilityChangeMask | PointerMotionMask;
	
	styleMask=CWBorderPixel | CWEventMask | CWColormap;
	gColormap=winAttribs.colormap;										           
	
	gWindow=XCreateWindow(gpDisplay,
						  RootWindow(gpDisplay,gpXVisualInfo->screen),
						  0,
						  0,
						  WIN_WIDTH,
						  WIN_HEIGHT,
						  0, //border width
						  gpXVisualInfo->depth, //depth of visual (depth for Colormap)          
						  InputOutput, //class(type) of your window
						  gpXVisualInfo->visual,
						  styleMask,
						  &winAttribs);
	if(!gWindow)
	{
		printf("Failure In Window Creation.\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"Material Using Programmable Function Pipeline");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	//code
	Atom wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False); //normal window state
	
	XEvent event;
	memset(&event,0,sizeof(XEvent));
	
	event.type=ClientMessage;
	event.xclient.window=gWindow;
	event.xclient.message_type=wm_state;
	event.xclient.format=32; //32-bit
	event.xclient.data.l[0]=gbFullscreen ? 0 : 1;

	Atom fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);	
	event.xclient.data.l[1]=fullscreen;
	
	//parallel to SendMessage()
	XSendEvent(gpDisplay,
			   RootWindow(gpDisplay,gpXVisualInfo->screen),
			   False, //do not send this message to Sibling windows
			   StructureNotifyMask, //resizing mask (event_mask)
			   &event);	
}

void initialize(void)
{
	// function declarations
	void uninitialize(void);
	void resize(int,int);
	void LoadGLTexture(void);

	//code
	// create a new GL context 4.5 for rendering
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	
	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,3,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		0 }; // array must be terminated by 0
		
	gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) // fallback to safe old style 2.x context
	{
		// When a context version below 3.0 is requested, implementations will return 
		// the newest context version compatible with OpenGL versions less than version 3.0.
		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 }; // array must be terminated by 0
		printf("Failed To Create GLX 4.5 context. Hence Using Old-Style GLX Context\n");
		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	else // successfully created 4.1 context
	{
		printf("OpenGL Context 4.3 Is Created.\n");
	}
	
	// verifying that context is a direct context
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Indirect GLX Rendering Context Obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering Context Obtained\n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
	GLenum glew_error = glewInit();

	// *** VERTEX SHADER *** //
	// create vertex shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled == 1)" \
		"{" \
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eyeCoordinates.xyz;" \
		"viewer_vector = -eyeCoordinates.xyz;" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHAER *** //
	// create fragment shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 f_phong_ads_color;" \
		"if(u_lighting_enabled == 1)" \
		"{" \
		"vec3 normalized_transformed_normals = normalize(transformed_normals);" \
		"vec3 normalized_light_direction = normalize(light_direction);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);" \
		"f_phong_ads_color = ambient + diffuse + specular;" \
		"}"\
		"else" \
		"{" \
		"f_phong_ads_color = vec3(1.0,1.0,1.0);" \
		"}" \
		"FragColor = vec4(f_phong_ads_color, 1.0);" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create program
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

	// pre-link binding of shader program object with vertex shader normal attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform location
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	// L or l key pressed or not
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// ambient color intensity of light
	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	// diffuse color intensity of light
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	// specular color intensity of light
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
	// position of light
	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	// amient reflective color intensity of light
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of light
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of light
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material (value is conventionally between 0 to 200)
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	// get sphere vertex data
	getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// **************************
	// VAO FOR CUBE
	// **************************

	// generate and bind vao for quad
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// ********************
	// VBO FOR POSITION
	// ********************
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// ******************
	// VBO FOR NORMAL
	// ******************
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER,gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	// ******************
	// VBO FOR ELEMENT
	// ******************
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	// unbind from vao for sphere
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);
	
	// set perspective matrix to identity matrix
	gPerspectiveProjectionMatrix = vmath::mat4::identity();

	gbLight = false;

	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// start using opengl program object
	glUseProgram(gShaderProgramObject);

	// opengl drawing

	if (gbLight == true)
	{
		// set u_lighting_enabled uniform
		glUniform1i(L_KeyPressed_uniform, 1);

		// setting light properties
		glUniform3fv(La_uniform, 1, light_Ambient);
		glUniform3fv(Ld_uniform, 1, light_Diffuse);
		glUniform3fv(Ls_uniform, 1, light_Specular);

		vmath::mat4 rotationMatrix = vmath::mat4::identity();

		if (keyPressed == 1)
		{
			light_Position[1] = 100.0f * (float)cos(angleWhiteLight);
			light_Position[2] = 100.0f * (float)sin(angleWhiteLight);
			glUniform4fv(light_position_uniform, 1, light_Position);
		}
		else if (keyPressed == 2)
		{
			light_Position[0] = 100.0f * (float)sin(angleWhiteLight);
			light_Position[2] = 100.0f * (float)cos(angleWhiteLight);
			glUniform4fv(light_position_uniform, 1, light_Position);
		}
		else if (keyPressed == 3)
		{
			light_Position[0] = 100.0f * (float)cos(angleWhiteLight);
			light_Position[1] = 100.0f * (float)sin(angleWhiteLight);
			glUniform4fv(light_position_uniform, 1, light_Position);
		}
	}
	else
	{
		// set u_lighting_enabled uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	// Opengl drawing

	// -------------------
	// FIRST COLUMN SPHERE
	// -------------------

	// draw first column, first sphere

	// set model, modelview, rotation matrices to identity
	vmath::mat4 modelMatrix = vmath::mat4::identity();
	vmath::mat4 viewMatrix = vmath::mat4::identity();

	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-2.0f, 2.6f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	
	glUniform3fv(Ka_uniform, 1, material_1_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_1_specular);
	glUniform1f(material_shininess_uniform, material_1_1_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, second sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-2.0f, 1.5f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_1_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_2_specular);
	glUniform1f(material_shininess_uniform, material_1_2_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);
	
	// draw first column, third sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-2.0f, 0.4f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_1_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_3_specular);
	glUniform1f(material_shininess_uniform, material_1_3_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, fourth sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-2.0f, -0.7f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_1_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_4_specular);
	glUniform1f(material_shininess_uniform, material_1_4_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, fifth sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-2.0f, -1.8f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_1_5_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_5_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_5_specular);
	glUniform1f(material_shininess_uniform, material_1_5_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, six sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-2.0f, -2.9f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_1_6_ambient);
	glUniform3fv(Kd_uniform, 1, material_1_6_diffuse);
	glUniform3fv(Ks_uniform, 1, material_1_6_specular);
	glUniform1f(material_shininess_uniform, material_1_6_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// -------------------
	// SECOND COLUMN SPHERE
	// -------------------

	// draw first column, first sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-0.8f, 2.6f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_2_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_1_specular);
	glUniform1f(material_shininess_uniform, material_2_1_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, second sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-0.8f, 1.5f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_2_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_2_specular);
	glUniform1f(material_shininess_uniform, material_2_2_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, third sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-0.8f, 0.4f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_2_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_3_specular);
	glUniform1f(material_shininess_uniform, material_2_3_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, fourth sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-0.8f, -0.7f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_2_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_4_specular);
	glUniform1f(material_shininess_uniform, material_2_4_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, fifth sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-0.8f, -1.8f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_2_5_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_5_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_5_specular);
	glUniform1f(material_shininess_uniform, material_2_5_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, six sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(-0.8f, -2.9f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_2_6_ambient);
	glUniform3fv(Kd_uniform, 1, material_2_6_diffuse);
	glUniform3fv(Ks_uniform, 1, material_2_6_specular);
	glUniform1f(material_shininess_uniform, material_2_6_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// -------------------
	// THIRD COLUMN SPHERE
	// -------------------

	// draw first column, first sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.4f, 2.6f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_3_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_1_specular);
	glUniform1f(material_shininess_uniform, material_3_1_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, second sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.4f, 1.5f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_3_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_2_specular);
	glUniform1f(material_shininess_uniform, material_3_2_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, third sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.4f, 0.4f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_3_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_3_specular);
	glUniform1f(material_shininess_uniform, material_3_3_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, fourth sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.4f, -0.7f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_3_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_4_specular);
	glUniform1f(material_shininess_uniform, material_3_4_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, fifth sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.4f, -1.8f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_3_5_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_5_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_5_specular);
	glUniform1f(material_shininess_uniform, material_3_5_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, six sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.4f, -2.9f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_3_6_ambient);
	glUniform3fv(Kd_uniform, 1, material_3_6_diffuse);
	glUniform3fv(Ks_uniform, 1, material_3_6_specular);
	glUniform1f(material_shininess_uniform, material_3_6_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// -------------------
	// FOURTH COLUMN SPHERE
	// -------------------

	// draw first column, first sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(1.6f, 2.6f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_4_1_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_1_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_1_specular);
	glUniform1f(material_shininess_uniform, material_4_1_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, second sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(1.6f, 1.5f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_4_2_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_2_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_2_specular);
	glUniform1f(material_shininess_uniform, material_4_2_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, third sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(1.6f, 0.4f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_4_3_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_3_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_3_specular);
	glUniform1f(material_shininess_uniform, material_4_3_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, fourth sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(1.6f, -0.7f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_4_4_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_4_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_4_specular);
	glUniform1f(material_shininess_uniform, material_4_4_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, fifth sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(1.6f, -1.8f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_4_5_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_5_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_5_specular);
	glUniform1f(material_shininess_uniform, material_4_5_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// draw first column, six sphere
	modelMatrix = vmath::mat4::identity();

	// apply z axis translation to go deep into the screen by -9.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(1.6f, -2.9f, -9.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(Ka_uniform, 1, material_4_6_ambient);
	glUniform3fv(Kd_uniform, 1, material_4_6_diffuse);
	glUniform3fv(Ks_uniform, 1, material_4_6_specular);
	glUniform1f(material_shininess_uniform, material_4_6_shininess);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	
	// stop using opengl program object
	glUseProgram(0);

	glXSwapBuffers(gpDisplay,gWindow);
}

void update(void)
{
	angleWhiteLight = angleWhiteLight + 0.1f;
	if (angleWhiteLight >= 360)
	{
		angleWhiteLight = 0.1f;
	}
}

void resize(int width,int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

void uninitialize(void)
{
	//code
	// Releasing OpenGL related and XWindow related objects 	
	GLXContext currentContext=glXGetCurrentContext();
	if(currentContext!=NULL && currentContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}
	// destroy position vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	// destroy texture vbo
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
