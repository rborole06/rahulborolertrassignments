#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

// C++ Headers
#include <vector>

// Symbolic Constants
#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE 256		// maximum length of string in mesh file
#define S_EQUAL		0	// return value of strcmp when strings are equal
#define WIN_INIT_X	100	// x coordinates to top left point
#define WIN_INIT_Y	100	// y coordinate to top left point
#define WIN_WIDTH	800	// initial width of window
#define WIN_HEIGHT	600	// initial height of window

#define VK_F		0x46	// virtual key code of capital F key
#define VK_f		0x60	// virtual key code of small f key

#define NR_POINT_COORDS	3	// number of point coordinates
#define NR_TEXTURE_COORDS 2	// number of texture coordinates
#define NR_NORMAL_COORDS 3	// number of normal coordinates
#define NR_FACE_TOKENS 3	// minimum number of entries in face data

#define FOY_ANGLE	45	// field of view in Y direction
#define ZNEAR	0.1	// distance from viewer to near plane of viewing volume
#define ZFAR 200.0	// distance from viewer to far plane of viewing volume

#define VIEWPORT_BOTTOMLEFT_X 0	// X coordinate of bottom left point of viewport rectangle
#define VIEWPORT_BOTTOMLEFT_Y 0	// y coordinate of bottom left point of viewport rectangle

#define MONKEYHEAD_X_TRANSLATE 0.0f	// x translation of monkeyhead
#define MONKEYHEAD_Y_TRANSLATE -0.0f	// y translation of monkeyhead
#define MONKEYHEAD_Z_TRANSLATE -5.0f // z translation of monkeyhead

#define MONKEYHEAD_X_SCALE_FACTOR 1.5f // x scale factor of monkeyhead
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f // y scale factor of monkeyhead
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f	// z scale factor of monkeyhead

#define START_ANGLE_POS	0.0f	// marks begining angle position of rotation
#define END_ANGLE_POS 360.0f	// marks terminating angle position rotation
#define MONKEYHEAD_ANGLE_INCREMENT 0.5f	// increment angle for monkeyhead

// namespace
using namespace std;

// global variable declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbLighting = false;

GLfloat g_rotate;

GLfloat light_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess[] = { 50.0f };

// vector of vector of floats to hold vertex data
std::vector<std::vector<float> > g_vertices;

// vector of vector of floats to hold texture data
std::vector<std::vector<float> > g_texture;

// vector of vector of floats to hold normal data
std::vector<std::vector<float> > g_normals;

// vector of vector of int to hold index data in g_vertices
std::vector<std::vector<int> > g_face_tri, g_face_texture, g_face_normals;

// handle to mesh file
FILE *g_fp_meshfile = NULL;

// handle to log file
FILE *g_fp_logfile = NULL;

// hold line in a file
char line[BUFFER_SIZE];

GLXContext gGLXContext;

// entry point function
int main(void)
{
	// function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void update(void);
	void resize(int,int);
	void uninitialize(void);

	// variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;

	// code
	CreateWindow();

	// initialize
	initialize();

	// message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						case XK_l:
							if (gbLighting == false)
							{
								glEnable(GL_LIGHTING);
								gbLighting = true;
							}
							else
							{
								glDisable(GL_LIGHTING);
								gbLighting = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					uninitialize();
					exit(0);
				default:
					break;
			}
		}
		update();
		display();
	}
	return(0);
}

void CreateWindow(void)
{
	// function prototype
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("Error : Unable To Open X Display. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);

	gColormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);

	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window\n Exiting Now\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay,gWindow,"Yellow Bordered Triangle");

	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	// code
	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1] = fullscreen;
	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),False,StructureNotifyMask,&xev);
}

void initialize(void)
{
	// function prototype
	void resize(int,int);
	void uninitialize(void);
	void LoadMeshData(void);

	g_fp_logfile = fopen("MONKEY_HEAD_LOADER.LOG", "w");
	if (!g_fp_logfile)
		uninitialize();

	// code
	gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// To avoid aliasing
	glShadeModel(GL_SMOOTH);
	// To avoid distortion
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);
	glEnable(GL_LIGHT0);

	// read mesh file and load global vectors with appropriate data
	LoadMeshData();

	resize(giWindowWidth,giWindowHeight);
}

void LoadMeshData(void)
{
	void uninitialize(void);

	g_fp_meshfile = fopen("MonkeyHead.OBJ", "r");
	if (!g_fp_meshfile)
		uninitialize();

	// seperator strings
	char const *sep_space = " ";
	char const *sep_fslash = "/";
	char *first_token = NULL;
	char *token = NULL;
	char *face_tokens[NR_FACE_TOKENS];
	int nr_tokens;
	char *token_vertex_index = NULL;
	char *token_texture_index = NULL;
	char *token_normal_index = NULL;

	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		first_token = strtok(line, sep_space);

		// If first token indicate vertex data
		if (strcmp(first_token, "v") == S_EQUAL)
		{
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for (int i = 0; i != NR_POINT_COORDS; i++)
				vec_point_coord[i] = atof(strtok(NULL, sep_space));
			g_vertices.push_back(vec_point_coord);
		}

		// If first token indicates texture data
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			g_texture.push_back(vec_texture_coord);
		}

		// If first token indicates normal data
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);
			for (int i = 0; i != NR_NORMAL_COORDS; i++)
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			g_normals.push_back(vec_normal_coord);
		}

		// If fist token indicates face data
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);

			nr_tokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			for (int i = 0; i != NR_FACE_TOKENS; ++i)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		memset((void*)line, (int)'\0', BUFFER_SIZE);
	}
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	fprintf(g_fp_logfile, "g_vertices:%llu g_texture:%llu g_normals:%llu g_face_tri:%llu\n", g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());
}

void update()
{
	// increment monkey head rotation angle
	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;
	// if rotation angle equal or exceeds END_ANGLE_POS then
	// reset to START_ANGLE_POS
	if (g_rotate >= END_ANGLE_POS)
		g_rotate = START_ANGLE_POS;
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);

	// Keep counter-clockwise winding of vertices of geometry
	glFrontFace(GL_CCW);
	// Set polygon mode mentioning front and back faces ang GL_LINE
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	for (int i = 0; i != g_face_tri.size(); ++i)
	{
		glBegin(GL_TRIANGLES);
		for (int j = 0; j != g_face_tri[i].size(); j++)
		{
			int vi = g_face_tri[i][j] - 1;
			int ni = g_face_normals[i][j] - 1;
			glNormal3f(g_normals[ni][0], g_normals[ni][1], g_normals[ni][2]);	// set normal for front face
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]);
		}
		glEnd();
	}

	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,1.0f,100.0f);
}

void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);

	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	fclose(g_fp_logfile);
	g_fp_logfile = NULL;
}

