
// headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface MyView : NSView
@end

// Entry point function
int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	MyView *view;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// code
	// window
	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect
									   styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
									     backing:NSBackingStoreBuffered
										   defer:NO];
	[window setTitle:@"macOS Window"];
	[window center];

	view=[[MyView alloc]initWithFrame:win_rect];

	[window setContentView:view];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	// code
}

- (void)windowWillClose:(NSNotification *)notification
{
	// code
	[NSApp terminate:self];
}

- (void)dealloc
{
	// code
	[view release];

	[window release];

	[super dealloc];
}
@end

@implementation MyView
{
	NSString *centralText;
}

-(id)initWithFrame:(NSRect)frame;
{
	// code
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		centralText=@"Hello World !!!";
	}
	return(self);
}

- (void)drawRect:(NSRect)dirtyRect
{
	// code
	// black background
	NSColor *fillColor=[NSColor blackColor];
	[fillColor set];
	NSRectFill(dirtyRect);

	// dictionary with kvc
	NSDictionary *dictionaryForTextAttributes=[NSDictionary dictionaryWithObjectsAndKeys:
											   [NSFont fontWithName:@"Helvetica" size:32], NSFontAttributeName,
											   [NSColor greenColor], NSForegroundColorAttributeName,
											   nil];

	NSSize textSize=[centralText sizeWithAttributes:dictionaryForTextAttributes];

	NSPoint point;
	point.x=(dirtyRect.size.width/2)-(textSize.width/2);
	point.y=(dirtyRect.size.height/2)-(textSize.height/2);

	[centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
}

-(BOOL)acceptsFirstResponder
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	// code
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			centralText=@"'F' or 'f' key is Pressed";
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
	centralText=@"Left Mouse Button Is Clicked";
	[self setNeedsDisplay:YES]; // repainting
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
	centralText=@"Right Mouse Button Is Clicked";
	[self setNeedsDisplay:YES]; // repainting
}

-(void)dealloc
{
	// code
	[super dealloc];
}

@end



//********************************
PHONG LIGHTING MODEL PER VERTEX
********************************//


// headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"
#include "Sphere.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0;
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// code
	
	// log file
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
            printf("Can not create log file\n Exiting...\n");
            [self release];
            [NSApp terminate:self];
	}
        fprintf(gpFile, "Program is started successfully\n");
	
	// window
	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect
									   styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
									     backing:NSBackingStoreBuffered
										   defer:NO];
	[window setTitle:@"Phong Lighting Model Per Vertex - Programmable Pipeline"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    // code
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
	// code
	[NSApp terminate:self];
}

- (void)dealloc
{
	// code
	[view release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private:
    CVDisplayLink displayLink;

    // global variables
    FILE *gpFile = NULL;

    bool gbActiveWindow = false;
    bool gbEscapeKeyIsPressed = false;
    bool gbFullscreen = false;

    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint gNumVertices;
    GLuint gNumElements;
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];

    GLuint gVao_sphere;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_normal;
    GLuint gVbo_sphere_element;

    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

    GLuint L_KeyPressed_uniform;

    GLuint La_uniform;
    GLuint Ld_uniform;
    GLuint Ls_uniform;
    GLuint light_position_uniform;

    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;

    vmath::mat4 gPerspectiveProjectionMatrix;

    GLfloat gAngle = 0.0f;

    bool gbLight;

    GLfloat lightAmbient[] = {0.0f,0.0f,0.0f,1.0f};
    GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

    GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
    GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat material_shininess = 50.0f;
}

-(id)initWithFrame:(NSRect)frame;
{
	// code
	self=[super initWithFrame:frame];

	if(self)
	{
            [[self window]setContentView:self];
            NSOpenGLPixelFormatAttribute attrs[] ={
                // Must specify the 4.1 Core Profile to use OpenGL 4.1
                NSOpenGLPFAOpenGLProfile,
                NSOpenGLProfileVersion4_1Core,
                // Specify the display ID to associate the GL context with (main display for now)
                NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                NSOpenGLPFANoRecovery,
                NSOpenGLPFAAccelerated,
                NSOpenGLPFAColorSize,24,
                NSOpenGLPFADepthSize,24,
                NSOpenGLPFAAlphaSize,8,
                NSOpenGLPFADoubleBuffer,
                0}; // last 0 is must
            
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
            if(pixelFormat == nil)
            {
                fprintf(gpFile, "No valid OpenGL pixel format is available Exitting...");
                [self release];
                [NSApp terminate:self];
            }
            
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
            [self setPixelFormat:pixelFormat];
            [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
	}
	return(self);
}

- (CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    
    if (gbAnimate == true)
    {
        [self update];
    }
    
    [pool release];
    return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile, "OpenGL Version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // *** VERTEX SHADER ***
    // create shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // provide source code to shader
    const GLchar *vertexShaderSourceCode =
        "#version 430 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_lighting_enabled;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Ls;" \
        "uniform vec4 u_light_position;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "out vec3 phong_ads_color;" \
        "void main(void)" \
        "{" \
        "if(u_lighting_enabled == 1)" \
        "{" \
        "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
        "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
        "vec3 light_direction = normalize(vec3(u_light_position) - eyeCoordinates.xyz);" \
        "float tn_dot_ld = max(dot(transformed_normals,light_direction),0.0);" \
        "vec3 ambient = u_La * u_Ka;" \
        "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
        "vec3 reflection_vector = reflect(-light_direction,transformed_normals);" \
        "vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
        "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,viewer_vector),0.0),u_material_shininess);" \
        "phong_ads_color = ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_color = vec3(1.0,1.0,1.0);" \
        "}" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
    
    glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
    
    // *** FRAGMENT SHAER *** //
    // create fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
        "#version 430 core" \
        "\n" \
        "in vec3 phong_ads_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = vec4(phong_ads_color,1.0);" \
        "}";
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

    // compile shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
            glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
        
    // *** SHADER PROGRAM ***
    // create program
    gShaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    // attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // pre-building of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
    // pre-link binding of shader program object with vertex shader texture attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
    // link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
            glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength>0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
    
    // get uniform location
    model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
    view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    // L or l key pressed or not
    L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

    // ambient color intensity of light
    La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
    // diffuse color intensity of light
    Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
    // specular color intensity of light
    Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
    // position of light
    light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

    // amient reflective color intensity of light
    Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
    // diffuse reflective color intensity of light
    Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
    // specular reflective color intensity of light
    Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
    // shininess of material (value is conventionally between 0 to 200)
    material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
    
    // vertices, colors, shader attribs, vbo, vao initializations
    [self makeSphere:2.0f :30 :30];

    // *************************
    // VAO FOR QUAD
    // *************************

    // generate and bind vao for quad
    glGenVertexArrays(1, &gVao_sphere);
    glBindVertexArray(gVao_sphere);

    // ******************
    // VBO FOR POSITION
    // ******************
    glGenBuffers(1, &gVbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // ******************
    // VBO FOR NORMAL
    // ******************
    glGenBuffers(1, &gVbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    // ******************
    // VBO FOR ELEMENTS
    // ******************
    glGenBuffers(1, &gVbo_sphere_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,gVbo_sphere_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    
    // unbind from vao for sphere
    glBindVertexArray(0);

    glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_CULL_FACE);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    // set perspective matrix to identity matrix
    gPerspectiveProjectionMatrix = vmath::mat4::identity();

    gbAnimate = false;
    gbLight = false;
    
    // ****************************************************
    // MAY BE WE NEED TO PROVIDE REASHAPE FUNCTION CALL HERE
    // ****************************************************
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkStart(displayLink);
}

- (void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    gPerspectiveProjectionMatrix = perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
        
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	// code
	[self drawView];
        if (gbAnimate == true)
        {
            [self update];
        }
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // start using opengl program object
    glUseProgram(gShaderProgramObject);

    // opengl drawing

    if (gbLight == true)
    {
            glUniform1i(gLKeyPressedUniform, 1);

            glUniform3f(gLdUniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(gKdUniform, 0.5f, 0.5f, 0.5f);

            float lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
            glUniform4fv(gLightPositionUniform, 1, (GLfloat *)lightPosition);
    }
    else
    {
            glUniform1i(gLKeyPressedUniform, 0);
    }

    // Opengl drawing
    // set model, modelview, rotation matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();

    // apply z axis translation to go deep into the screen by -5.0
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -5.0f);

    // all axis rotation by gAngle angle
    rotationMatrix = rotate(gAngle, gAngle, gAngle);

    // multiply rotation matrix and model matrix to get modelview matrix
    modelViewMatrix = modelMatrix * rotationMatrix;	// ORDER IS IMPORTANT

    // pass above modelview matrix to the vertex shader in "u_model_view_matrix" shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);

    // pass projection matrix to the vertex shader in 'u_projection_matrix' shader variable
    // whose position we already calculated in initialiize() by using glGetUniformLocation()
    glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

    // bind to vao of cube
    glBindVertexArray(gVao_sphere);

    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 3(each with its x,y,z) vertices in triangleVertices array
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

    // unbind from vao of quad
    glBindVertexArray(0);

    // stop using opengl program object
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)update
{
    gAngle = gAngle + 0.01f;
    if (gAngle >= 360.0f)
            gAngle = gAngle - 360.0f;
}

-(BOOL)acceptsFirstResponder
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	// code
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			centralText=@"'F' or 'f' key is Pressed";
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
                        
		case 'A':	// for A or a
                case 'a':
			if (bIsAKeyPressed == false)
			{
				gbAnimate = true;
				bIsAKeyPressed = true;
			}
			else
			{
				gbAnimate = false;
				bIsAKeyPressed = false;
			}
			break;
		case 'L':	// for L or l
                case 'l':
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
	centralText=@"Right Mouse Button Is Clicked";
	[self setNeedsDisplay:YES]; // repainting
}

-(void)dealloc
{
	// code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    // destroy vao
    if (gVao_sphere)
    {
            glDeleteVertexArrays(1, &gVao_sphere);
            gVao_sphere = 0;
    }
    // destroy position vbo
    if (gVbo_sphere_position)
    {
            glDeleteBuffers(1, &gVbo_sphere_position);
            gVbo_sphere_position = 0;
    }
    // destroy normal vbo
    if (gVbo_sphere_normal)
    {
            glDeleteBuffers(1, &gVbo_sphere_normal);
            gVbo_sphere_normal = 0;
    }

    // detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    // detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

    if (gpFile)
    {
        fprintf(gpFile, "Log File Is Successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }
        
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
