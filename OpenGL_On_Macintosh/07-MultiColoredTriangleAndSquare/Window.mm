
// headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0;
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// code
	
	// log file
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
            printf("Can not create log file\n Exiting...\n");
            [self release];
            [NSApp terminate:self];
	}
        fprintf(gpFile, "Program is started successfully\n");
	
	// window
	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect
									   styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
									     backing:NSBackingStoreBuffered
										   defer:NO];
	[window setTitle:@"Multi Colored Triangle and Square - Programmable Pipeline"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    // code
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
	// code
	[NSApp terminate:self];
}

- (void)dealloc
{
	// code
	[view release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private:
    CVDisplayLinkRef displayLink;

    // global variables
    FILE *gpFile = NULL;

    bool gbActiveWindow = false;
    bool gbEscapeKeyIsPressed = false;
    bool gbFullscreen = false;

    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint gVao_triangle;
    GLuint gVao_square;
    GLuint gVbo_position;
    GLuint gVbo_color;
    GLuint gMVPUniform;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
	// code
	self=[super initWithFrame:frame];

	if(self)
	{
            [[self window]setContentView:self];
            NSOpenGLPixelFormatAttribute attrs[] ={
                // Must specify the 4.1 Core Profile to use OpenGL 4.1
                NSOpenGLPFAOpenGLProfile,
                NSOpenGLProfileVersion4_1Core,
                // Specify the display ID to associate the GL context with (main display for now)
                NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                NSOpenGLPFANoRecovery,
                NSOpenGLPFAAccelerated,
                NSOpenGLPFAColorSize,24,
                NSOpenGLPFADepthSize,24,
                NSOpenGLPFAAlphaSize,8,
                NSOpenGLPFADoubleBuffer,
                0}; // last 0 is must
            
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
            if(pixelFormat == nil)
            {
                fprintf(gpFile, "No valid OpenGL pixel format is available Exitting...");
                [self release];
                [NSApp terminate:self];
            }
            
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
            [self setPixelFormat:pixelFormat];
            [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
	}
	return(self);
}

- (CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile, "OpenGL Version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // *** VERTEX SHADER ***
    // create shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // provide source code to shader
    const GLchar *vertexShaderSourceCode =
        "#version 430 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color = vColor;" \
        "}";
    
    glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
    
    // *** FRAGMENT SHAER *** //
    // create fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
        "#version 430" \
        "\n" \
        "in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
        "}";
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

    // compile shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
            glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
        
    // *** SHADER PROGRAM ***
    // create program
    gShaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    // attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // pre-building of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
    // link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
            glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength>0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
    
    // get MVP uniform location
    gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

    // vertices, colors, shader attribs, vbo, vao initializations
    const GLfloat triangleVertices[] =
    {
        0.0f,1.0f,0.0f,	// apex
        -1.0f,-1.0f,0.0f,	// left-bottom
        1.0f,-1.0f,0.0f	// right-bottom
    };

    const GLfloat colorVertices[] =
    {
            1.0f,0.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,0.0f,1.0f
    };
        
    const GLfloat squareVertices[] = 
    {
        -1.0f,1.0f,0.0f,
        -1.0f,-1.0f,0.0f,
        1.0f,-1.0f,0.0f,
        1.0f,1.0f,0.0f
    };
    
    // *************************
    // VAO FOR TRIANGLE
    // *************************

    // generate vao for triangle
    glGenVertexArrays(1, &gVao_triangle);

    // bind to vao for triangle
    glBindVertexArray(gVao_triangle);

    // ******************
    // VBO FOR VERTICES
    // ******************
    glGenBuffers(1, &gVbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // ******************
    // VBO FOR COLOR
    // ******************
    glGenBuffers(1, &gVbo_color);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_color);
    glBufferData(GL_ARRAY_BUFFER,sizeof(colorVertices),colorVertices,GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    // unbind from vao for triangle
    glBindVertexArray(0);

    // *************************
    // VAO FOR SQUARE
    // *************************

    // generate vao for square
    glGenVertexArrays(1, &gVao_square);

    // bind to vao for square
    glBindVertexArray(gVao_square);

    // ******************
    // VBO FOR VERTICES
    // ******************
    glGenBuffers(1, &gVbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // ******************
    // COLOR FOR SQUARE
    // ******************
    glVertexAttrib3f(VDG_ATTRIBUTE_COLOR, 0.3960f, 0.5764f, 0.9607f);
        
    glBindVertexArray(0);

    glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_CULL_FACE);
        
    // set background color
    glClearColor(0.0f,0.0f,1.0f,0.0f);  // blue

    // set perspective matrix to identity matrix
    gPerspectiveProjectionMatrix = vmath::mat4::identity();
        
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkStart(displayLink);
}

- (void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    gPerspectiveProjectionMatrix = perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
        
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	// code
	[self drawView];
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // start using opengl program object
    glUseProgram(gShaderProgramObject);

    // opengl drawing

    // ****************************************************
    // TRIANGLE BLOCK
    // ****************************************************

    // set modelview & modelviewprojection  matrices to identity
    mat4 modelViewMatrix = mat4::identity();
    modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);

    mat4 modelViewProjectionMatrix = mat4::identity();

    // multiply modelview and orthographic matrix to get modelviewprojection matrix
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

    // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    // bind vao
    glBindVertexArray(gVao_triangle);

    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glDrawArrays(GL_TRIANGLES, 0, 3);	// 3(each with its x,y,z) vertices in triangleVertices array

    // ****************************************************
    // SQUARE BLOCK
    // ****************************************************

    // set modelview & modelviewprojection  matrices to identity
    modelViewMatrix = mat4::identity();
    modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
    modelViewProjectionMatrix = mat4::identity();

    // multiply modelview and orthographic matrix to get modelviewprojection matrix
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

    // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    // bind vao
    glBindVertexArray(gVao_square);

    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 4(each with its x,y,z) vertices in squareVertices array

    // unbind vao
    glBindVertexArray(0);

    // stop using opengl program object
    glUseProgram(0);

    // stop using opengl program object
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	// code
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			centralText=@"'F' or 'f' key is Pressed";
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
	centralText=@"Right Mouse Button Is Clicked";
	[self setNeedsDisplay:YES]; // repainting
}

-(void)dealloc
{
	// code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    // detach vao
    if (gVao_triangle)
    {
            glDeleteVertexArrays(1, &gVao_triangle);
            gVao_triangle = 0;
    }
    if (gVao_square)
    {
            glDeleteVertexArrays(1, &gVao_square);
            gVao_square = 0;
    }
    if (gVbo_position)
    {
            glDeleteBuffers(1, &gVbo_position);
            gVbo_position = 0;
    }
    if (gVbo_color)
    {
            glDeleteBuffers(1, &gVbo_color);
            gVbo_color = 0;
    }

    // detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    // detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

    if (gpFile)
    {
        fprintf(gpFile, "Log File Is Successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }
        
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
