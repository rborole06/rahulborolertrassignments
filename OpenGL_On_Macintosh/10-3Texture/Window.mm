
// headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0;
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// code
	
	// log file
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
            printf("Can not create log file\n Exiting...\n");
            [self release];
            [NSApp terminate:self];
	}
        fprintf(gpFile, "Program is started successfully\n");
	
	// window
	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect
									   styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
									     backing:NSBackingStoreBuffered
										   defer:NO];
	[window setTitle:@"3D Texture - Programmable Pipeline"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    // code
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
	// code
	[NSApp terminate:self];
}

- (void)dealloc
{
	// code
	[view release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private:
    CVDisplayLinkRef displayLink;

    // global variables
    FILE *gpFile = NULL;

    bool gbActiveWindow = false;
    bool gbEscapeKeyIsPressed = false;
    bool gbFullscreen = false;

    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint gVao_pyramid;
    GLuint gVbo_pyramid_position;
    GLuint gVbo_pyramid_texture;

    GLuint gVao_cube;
    GLuint gVbo_cube_position;
    GLuint gVbo_cube_texture;
    GLuint gMVPUniform;
    
    GLfloat anglePyramid = 0.0f;
    GLfloat angleCube = 0.0f;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
    
    GLuint gTexture_sampler_uniform;
    GLuint gTexture_Kundali;
    GLuint gTexture_Stone;
}

-(id)initWithFrame:(NSRect)frame;
{
	// code
	self=[super initWithFrame:frame];

	if(self)
	{
            [[self window]setContentView:self];
            NSOpenGLPixelFormatAttribute attrs[] ={
                // Must specify the 4.1 Core Profile to use OpenGL 4.1
                NSOpenGLPFAOpenGLProfile,
                NSOpenGLProfileVersion4_1Core,
                // Specify the display ID to associate the GL context with (main display for now)
                NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                NSOpenGLPFANoRecovery,
                NSOpenGLPFAAccelerated,
                NSOpenGLPFAColorSize,24,
                NSOpenGLPFADepthSize,24,
                NSOpenGLPFAAlphaSize,8,
                NSOpenGLPFADoubleBuffer,
                0}; // last 0 is must
            
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
            if(pixelFormat == nil)
            {
                fprintf(gpFile, "No valid OpenGL pixel format is available Exitting...");
                [self release];
                [NSApp terminate:self];
            }
            
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
            [self setPixelFormat:pixelFormat];
            [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
	}
	return(self);
}

- (CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self update];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile, "OpenGL Version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // *** VERTEX SHADER ***
    // create shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // provide source code to shader
    const GLchar *vertexShaderSourceCode =
        "#version 430 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexture0_Coord;" \
        "out vec2 out_texture0_coord;" \
        "uniform mat4 u_mvp_matrix;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_texture0_coord = vTexture0_Coord;" \
        "}";
    
    glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
    
    // *** FRAGMENT SHAER *** //
    // create fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
        "#version 430 core" \
        "\n" \
        "in vec2 out_texture0_coord;" \
        "out vec4 FragColor;" \
        "uniform sampler2D u_texture0_sampler;" \
        "void main(void)" \
        "{" \
        "FragColor = texture(u_texture0_sampler,out_texture0_coord);" \
        "}";
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

    // compile shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
            glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
        
    // *** SHADER PROGRAM ***
    // create program
    gShaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    // attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // pre-building of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
    // pre-link binding of shader program object with vertex shader texture attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
        
    // link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
            glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength>0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
    
    // get MVP uniform location
    gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");
    
    // vertices, colors, shader attribs, vbo, vao initializations
    const GLfloat pyramidVertices[] =
    {
            // FRONT FACE
            0.0f,1.0f,0.0f,	// apex
            -1.0f,-1.0f,1.0f,	// left-bottom
            1.0f,-1.0f,1.0f,	// right-bottom

            // RIGHT FACE
            0.0f,1.0f,0.0f,
            1.0f,-1.0f,1.0f,
            1.0f,-1.0f,-1.0f,

            // BACK FACE
            0.0f,1.0f,0.0f,
            1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,

            // LEFT FACE
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f
    };

    const GLfloat pyramidTexcoords[] =
    {
            // FRONT FACE
            0.5f,1.0f,	// front-top
            0.0f,0.0f,	// front-left
            1.0f,0.0f,	// front-right

            // RIGHT FACE
            0.5f,1.0f,	// right-top
            1.0f,0.0f,	// right-left
            0.0f,0.0f,	// right-right

            // BACK FACE
            0.5f,1.0f,	// back-top
            1.0f,0.0f,	// back-left
            0.0f,0.0f,	// back-right

            // LEFT FACE
            0.5f,1.0f,	// left-top
            0.0f,0.0f,	// left-left
            1.0f,0.0f	// left-right
    };
        
    const GLfloat cubeVertices[] =
    {
            // FRONT FACE
            1.0f,1.0f,1.0f,
            -1.0f,1.0f,1.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f,

            // RIGHT FACE
            1.0f,1.0f,-1.0f,
            1.0f,1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            1.0f,-1.0f,-1.0f,

            // BACK FACE
            1.0f,1.0f,-1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            1.0f,-1.0f,-1.0f,

            // LEFT FACE
            -1.0f,1.0f,1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f,

            // TOP FACE
            1.0f,1.0f,-1.0f,
            -1.0f,1.0f,-1.0f,
            -1.0f,1.0f,1.0f,
            1.0f,1.0f,1.0f,

            //	BOTTOM FACE
            1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,-1.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f
    };
    
    // If above -1.0f or +1.0f values make cube much larger than pyramid
    // then follow the code in following loop which will convert all 1 and -1 to 0.75 and -0.75
    /*for (int i = 0; i < 72; i++)
    {
            if (cubeVertices[i] < 0.0f)
                    cubeVertices[i] = cubeVertices[i] + 0.25f;
            else if (cubeVertices[i] > 0.0f)
                    cubeVertices[i] = cubeVertices[i] - 0.25f;
            else
                    cubeVertices[i] = cubeVertices[i];

    }*/
    const GLfloat cubeTexcoords[] = 
    {
            // TOP FACE
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,

            // BOTTOM FACE
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,

            // FRONT FACE
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,

            // BACK FACE
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,

            // LEFT FACE
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,

            // RIGHT FACE
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,

    };
        
    // *************************
    // VAO FOR PYRAMID
    // *************************

    // generate and bind vao for pyramid
    glGenVertexArrays(1, &gVao_pyramid);
    glBindVertexArray(gVao_pyramid);

    // ******************
    // VBO FOR POSITION
    // ******************
    glGenBuffers(1, &gVbo_pyramid_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // ******************
    // VBO FOR TEXTURE
    // ******************
    glGenBuffers(1, &gVbo_pyramid_texture);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_pyramid_texture);
    glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidTexcoords),pyramidTexcoords,GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    // unbind from vao for pyramid
    glBindVertexArray(0);

    // *************************
    // VAO FOR CUBE
    // *************************

    // generate and bind vao for cube
    glGenVertexArrays(1, &gVao_cube);
    glBindVertexArray(gVao_cube);

    // ******************
    // VBO FOR POSITION
    // ******************
    glGenBuffers(1, &gVbo_cube_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // ******************
    // VBO FOR COLOR
    // ******************
    glGenBuffers(1, &gVbo_cube_texture);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_texture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoords), cubeTexcoords, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // unbind from vao for cube
    glBindVertexArray(0);

    glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    //glEnable(GL_CULL_FACE);

    LoadGLTextures(&gTexture_Kundali, MAKEINTRESOURCE(IDBITMAP_KUNDALI));
    LoadGLTextures(&gTexture_Stone, MAKEINTRESOURCE(IDBITMAP_STONE));

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    // set perspective matrix to identity matrix
    gPerspectiveProjectionMatrix = vmath::mat4::identity();
    
    // ****************************************************
    // MAY BE WE NEED TO PROVIDE REASHAPE FUNCTION CALL HERE
    // ****************************************************
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkStart(displayLink);
}

- (int)LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	// variable declarations
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	// code

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)	// if bitmap exists (means hBitmap is not null)
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);

		glGenTextures(1, texture);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// pixel storage mode (word alignment/4 bytes)
		glBindTexture(GL_TEXTURE_2D, *texture);	// bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		// generate mipmapped texture (3 bytes, width, height & data from bmp)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		// create mipmaps for this texture for better image quality
		glGenerateMipmap(GL_TEXTURE_2D);
		DeleteObject(hBitmap);
	}
	return(iStatus);
}

- (void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    gPerspectiveProjectionMatrix = perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
        
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	// code
        [self update];
	[self drawView];
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // start using opengl program object
    glUseProgram(gShaderProgramObject);

    // opengl drawing

    // ****************************************************
    // PYRAMID BLOCK
    // ****************************************************

    // set modelview, modelviewprojection & rotation matrices to identity
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();

    // rotation
    rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
    // translate modelview matrix
    modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);

    // multiply model view by rotation matrix
    modelViewMatrix = modelViewMatrix * rotationMatrix;

    // multiply modelview and perspective projection matrix to get modelviewprojection matrix
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

    // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    // bind with pyramid texture
    glActiveTexture(GL_TEXTURE0);	// 0th texture (corrospond to VDG_ATTRIBUTE_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, gTexture_Stone);
    glUniform1i(gTexture_sampler_uniform, 0);	// 0th sampler enable (as we have only 1 texture sampler in fragment shader)

    // bind to vao of pyramid
    glBindVertexArray(gVao_pyramid);

    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glDrawArrays(GL_TRIANGLES, 0, 12);	// 3(each with its x,y,z) vertices in triangleVertices array

    // unbind from vao of pyramid
    glBindVertexArray(0);

    // ****************************************************
    // CUBE BLOCK
    // ****************************************************

    // set modelview, modelviewprojection & rotation matrices to identity
    modelViewMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    vmath::mat4 scaleMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    // scale
    scaleMatrix = vmath::scale(0.75f,0.75f,0.75f);
    // rotation
    rotationMatrix = vmath::rotate(angleCube, angleCube, angleCube);
    //rotationMatrix = vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
    // translate modelview matrix
    modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);

    // multiply modelview matrix by rotation matrix
    modelViewMatrix = modelViewMatrix * rotationMatrix * scaleMatrix;

    // multiply modelview and perspective projection matrix to get modelviewprojection matrix
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

    // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    //bind with cube texture
    glActiveTexture(GL_TEXTURE0);	// 0th texture (corresponds to VDG_ATTRIBUTE_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, gTexture_Kundali);
    glUniform1i(gTexture_sampler_uniform, 0);	// 0th sampler enable (as we have only 1 texture sampler in fragment shader)

    // bind to vao of cube
    glBindVertexArray(gVao_cube);

    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    // actually 2 triangles make 1 square, so there should be 6 vertices
    // but as 2 triangles while making square meet each other at diagonal
    // 2 of 6 vertices are common to both triangles and hence 6-2=4
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 4(each with its x,y,z) vertices in squareVertices array
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

    // unbind from vao of cube
    glBindVertexArray(0);

    // stop using opengl program object
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)update
{
    anglePyramid = anglePyramid + 0.1f;
    if (anglePyramid >= 360)
    {
            anglePyramid = 0.0f;
    }

    angleCube = angleCube - 0.1f;
    if (angleCube <= -360)
    {
            angleCube = 0.0f;
    }
}

-(BOOL)acceptsFirstResponder
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	// code
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			centralText=@"'F' or 'f' key is Pressed";
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
	centralText=@"Right Mouse Button Is Clicked";
	[self setNeedsDisplay:YES]; // repainting
}

-(void)dealloc
{
	// code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    // destroy pyramid vao
    if (gVao_pyramid)
    {
            glDeleteVertexArrays(1, &gVao_pyramid);
            gVao_pyramid = 0;
    }
    // destroy pyramid position vbo
    if (gVbo_pyramid_position)
    {
            glDeleteBuffers(1, &gVbo_pyramid_position);
            gVbo_pyramid_position = 0;
    }
    // destroy pyramid texture vbo
    if (gVbo_pyramid_texture)
    {
            glDeleteBuffers(1, &gVbo_pyramid_texture);
            gVbo_pyramid_texture = 0;
    }
    if (gTexture_Stone)
    {
            glDeleteTextures(1, &gTexture_Stone);
            gTexture_Stone = 0;
    }

    // destroy cube vao
    if (gVao_cube)
    {
            glDeleteVertexArrays(1, &gVao_cube);
            gVao_cube = 0;
    }
    // destroy cube position vbo
    if (gVbo_cube_position)
    {
            glDeleteBuffers(1, &gVbo_cube_position);
            gVbo_cube_position = 0;
    }
    // destroy cube texture vbo
    if (gVbo_cube_texture)
    {
            glDeleteBuffers(1, &gVbo_cube_texture);
            gVbo_cube_texture = 0;
    }
    if (gTexture_Kundali)
    {
            glDeleteTextures(1, &gTexture_Kundali);
            gTexture_Kundali = 0;
    }

    // detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    // detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

    if (gpFile)
    {
        fprintf(gpFile, "Log File Is Successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }
        
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
