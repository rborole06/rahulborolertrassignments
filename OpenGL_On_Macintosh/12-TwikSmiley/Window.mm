
// headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

#define checkImageWidth 64
#define checkImageHeight 64

enum
{
    VDG_ATTRIBUTE_VERTEX = 0;
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

	NSApp=[NSApplication sharedApplication];

	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// code
	
	// log file
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
            printf("Can not create log file\n Exiting...\n");
            [self release];
            [NSApp terminate:self];
	}
        fprintf(gpFile, "Program is started successfully\n");
	
	// window
	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect
									   styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
									     backing:NSBackingStoreBuffered
										   defer:NO];
	[window setTitle:@"Twik Smiley Texture - Programmable Pipeline"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    // code
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
	// code
	[NSApp terminate:self];
}

- (void)dealloc
{
	// code
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private:
    CVDisplayLinkRef displayLink;

    // global variables
    FILE *gpFile = NULL;

    bool gbActiveWindow;
    bool gbEscapeKeyIsPressed;
    bool gbFullscreen;

    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint gVao;
    GLuint gVbo_position;
    GLuint gVbo_texture;

    GLuint gMVPUniform;

    vmath::mat4 gPerspectiveProjectionMatrix;

    GLuint gTexture_sampler_uniform;
    GLuint gTexture_Smiley;

    GLuint gDigitIsPressed;

    GLubyte checkImage[checkImageHeight][checkImageWidth][4];
    GLuint texName;
}

-(id)initWithFrame:(NSRect)frame;
{
	// code
	self=[super initWithFrame:frame];

        gbActiveWindow = false;
        gbEscapeKeyIsPressed = false;
        gbFullscreen = false;
        
	if(self)
	{
            [[self window]setContentView:self];
            NSOpenGLPixelFormatAttribute attrs[] ={
                // Must specify the 4.1 Core Profile to use OpenGL 4.1
                NSOpenGLPFAOpenGLProfile,
                NSOpenGLProfileVersion4_1Core,
                // Specify the display ID to associate the GL context with (main display for now)
                NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                NSOpenGLPFANoRecovery,
                NSOpenGLPFAAccelerated,
                NSOpenGLPFAColorSize,24,
                NSOpenGLPFADepthSize,24,
                NSOpenGLPFAAlphaSize,8,
                NSOpenGLPFADoubleBuffer,
                0}; // last 0 is must
            
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
            if(pixelFormat == nil)
            {
                fprintf(gpFile, "No valid OpenGL pixel format is available Exitting...");
                [self release];
                [NSApp terminate:self];
            }
            
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
            [self setPixelFormat:pixelFormat];
            [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
	}
	return(self);
}

- (CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile, "OpenGL Version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // *** VERTEX SHADER ***
    // create shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // provide source code to shader
    const GLchar *vertexShaderSourceCode =
        "#version 430 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexture0_Coord;" \
        "out vec2 out_texture0_coord;" \
        "uniform mat4 u_mvp_matrix;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_texture0_coord = vTexture0_Coord;" \
        "}";
    
    glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
    
    // *** FRAGMENT SHAER *** //
    // create fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
        "#version 430 core" \
        "\n" \
        "in vec2 out_texture0_coord;" \
        "out vec4 FragColor;" \
        "uniform sampler2D u_texture0_sampler;" \
        "void main(void)" \
        "{" \
        "FragColor = texture(u_texture0_sampler,out_texture0_coord);" \
        "}";
    
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

    // compile shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
            glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                            free(szInfoLog);
                            [self release];
                            [NSApp terminate:self];
                            exit(0);
                    }
            }
    }
        
    // *** SHADER PROGRAM ***
    // create program
    gShaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    // attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

    // pre-link binding of shader program object with vertex shader texture attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

    // link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
            glGetProgramiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if (iInfoLogLength>0)
            {
                    szInfoLog = (char *)malloc(iInfoLogLength);
                    if (szInfoLog != NULL)
                    {
                            GLsizei written;
                            glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                            fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
                            free(szInfoLog);
                            uninitialize();
                            exit(0);
                    }
            }
    }

    // get MVP uniform location
    gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

    // load textures
    gTexture_Smiley = [self loadTextureFromBMPFile:"Smiley.bmp"];
    
    // vertices, colors, shader attribs, vbo, vao initializations
    const GLfloat quadVertices[] =
    {
            1.0f,1.0f,0.0f,
            -1.0f,1.0f,0.0f,
            -1.0f,-1.0f,0.0f,
            1.0f,-1.0f,0.0f,
    };

    // *************************
    // VAO FOR PYRAMID
    // *************************

    // generate and bind vao for quad
    glGenVertexArrays(1, &gVao);
    glBindVertexArray(gVao);

    // ******************
    // VBO FOR POSITION
    // ******************
    glGenBuffers(1, &gVbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // ******************
    // VBO FOR TEXTURE
    // ******************
    glGenBuffers(1, &gVbo_texture);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_texture);
    glBufferData(GL_ARRAY_BUFFER,sizeof(quadTexcoords),quadTexcoords,GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    // unbind from vao for quad
    glBindVertexArray(0);

    glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    //glEnable(GL_CULL_FACE);

    LoadGLTextures(&gTexture_Smiley, MAKEINTRESOURCE(IDBITMAP_SMILEY));

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    // set perspective matrix to identity matrix
    gPerspectiveProjectionMatrix = vmath::mat4::identity();
    
    // ****************************************************
    // MAY BE WE NEED TO PROVIDE REASHAPE FUNCTION CALL HERE
    // ****************************************************
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkStart(displayLink);
}

// function loads texture image
-(GLuint)loadTextureFromBMPFile:(const char *)texFileName
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *textureFileNameWithPath=[NSString stringWithFormat:@"%@/%s",parentDirPath,texFileName];
    
    NSImage *bmpImage=[[NSImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if (!bmpImage)
    {
        NSLog(@"can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);

    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4, for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    // Create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTexture);
}

// function load procedural generated texture
- (void)LoadGLTexture
{
	// function declarations
	void MakeCheckImage(void);

	// code
	[self MakeCheckImage];

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glGenTextures(1, &texName);

	glBindTexture(GL_TEXTURE_2D, texName);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, checkImageWidth, checkImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

// function generates procedural white color texture
// Note : we can generate checkerboard texture just by assigning c to 0th, 1st and 2nd element of checkImage array
- (void)MakeCheckImage
{
	// code
	int i, j, c;

	for (i = 0; i < checkImageHeight; i++)
	{
		for (j = 0; j < checkImageWidth; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			checkImage[i][j][0] = (GLubyte)255;
			checkImage[i][j][1] = (GLubyte)255;
			checkImage[i][j][2] = (GLubyte)255;
			checkImage[i][j][3] = (GLubyte)255;
		}
	}
}

- (void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    gPerspectiveProjectionMatrix = perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
        
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	// code
	[self drawView];
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // start using opengl program object
    glUseProgram(gShaderProgramObject);

    // opengl drawing

    // ****************************************************
    // QUAD BLOCK
    // ****************************************************

    // set modelview, modelviewprojection matrices to identity
    mat4 modelViewMatrix = mat4::identity();
    mat4 modelViewProjectionMatrix = mat4::identity();

    // translate modelview matrix
    modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

    // multiply modelview and perspective projection matrix to get modelviewprojection matrix
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	// ORDER IS IMPORTANT

    // pass above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    // bind to vao of quad
    glBindVertexArray(gVao);

    GLfloat quad_texture[8];
    if (gDigitIsPressed == 0)
    {
            LoadGLTexture();

            quad_texture[0] = 1.0f;
            quad_texture[1] = 1.0f;
            quad_texture[2] = 0.0f;
            quad_texture[3] = 1.0f;
            quad_texture[4] = 0.0f;
            quad_texture[5] = 0.0f;
            quad_texture[6] = 1.0f;
            quad_texture[7] = 0.0f;
    }
    else if (gDigitIsPressed == 1)
    {
            quad_texture[0] = 0.5f;
            quad_texture[1] = 0.5f;
            quad_texture[2] = 0.0f;
            quad_texture[3] = 0.5f;
            quad_texture[4] = 0.0f;
            quad_texture[5] = 0.0f;
            quad_texture[6] = 0.5f;
            quad_texture[7] = 0.0f;
    }
    else if (gDigitIsPressed == 2)
    {
            quad_texture[0] = 1.0f;
            quad_texture[1] = 1.0f;
            quad_texture[2] = 0.0f;
            quad_texture[3] = 1.0f;
            quad_texture[4] = 0.0f;
            quad_texture[5] = 0.0f;
            quad_texture[6] = 1.0f;
            quad_texture[7] = 0.0f;
    }
    else if (gDigitIsPressed == 3)
    {
            quad_texture[0] = 2.0f;
            quad_texture[1] = 2.0f;
            quad_texture[2] = 0.0f;
            quad_texture[3] = 2.0f;
            quad_texture[4] = 0.0f;
            quad_texture[5] = 0.0f;
            quad_texture[6] = 2.0f;
            quad_texture[7] = 0.0f;
    }
    else if (gDigitIsPressed == 4)
    {
            quad_texture[0] = 0.5f;
            quad_texture[1] = 0.5f;
            quad_texture[2] = 0.5f;
            quad_texture[3] = 0.5f;
            quad_texture[4] = 0.5f;
            quad_texture[5] = 0.5f;
            quad_texture[6] = 0.5f;
            quad_texture[7] = 0.5f;
    }

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_texture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_texture), quad_texture, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // bind with quad texture
    glActiveTexture(GL_TEXTURE0);	// 0th texture (corrospond to VDG_ATTRIBUTE_TEXTURE0)
    if (gDigitIsPressed == 0)
    {
            glBindTexture(GL_TEXTURE_2D, texName);
    } 
    else
    {
            glBindTexture(GL_TEXTURE_2D, gTexture_Smiley);
    }
    glUniform1i(gTexture_sampler_uniform, 0);	// 0th sampler enable (as we have only 1 texture sampler in fragment shader)

    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 3(each with its x,y,z) vertices in triangleVertices array

    // unbind from vao of quad
    glBindVertexArray(0);

    // stop using opengl program object
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	// code
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[ self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
		case 0x31:
		case 0x61:
			gDigitIsPressed = 1;
			break;
		case 0x32:
		case 0x62:
			gDigitIsPressed = 2;
			break;
		case 0x33:
		case 0x63:
			gDigitIsPressed = 3;
			break;
		case 0x34:
		case 0x64:
			gDigitIsPressed = 4;
			break;
		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
	[self setNeedsDisplay:YES]; // repainting
}

-(void)dealloc
{
	// code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    // destroy vao
    if (gVao)
    {
            glDeleteVertexArrays(1, &gVao);
            gVao = 0;
    }
    // destroy position vbo
    if (gVbo_position)
    {
            glDeleteBuffers(1, &gVbo_position);
            gVbo_position = 0;
    }
    // destroy texture vbo
    if (gVbo_texture)
    {
            glDeleteBuffers(1, &gVbo_texture);
            gVbo_texture = 0;
    }
    if (gTexture_Smiley)
    {
            glDeleteTextures(1, &gTexture_Smiley);
            gTexture_Smiley = 0;
    }

    // detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    // detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

    if (gpFile)
    {
        fprintf(gpFile, "Log File Is Successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }
        
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
