
// headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

GLfloat light_1_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Specular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Position[] = { 2.0f,0.8f,1.0f,0.0f };

GLfloat light_2_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_2_Diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_2_Specular[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_2_Position[] = { -2.0f,0.8f,1.0f,0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView    // This make our application CGL
@end

// Entry point function
int main(int argc, const char * argv[])
{
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    
    // log file
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath,"w");
    if(gpFile == NULL)
    {
        printf("Can not create log file\n Exiting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program is started successfully\n");
    
    // window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    
    // create simple window
    window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
    [window setTitle:@"Rotating pyramid with 2 lights - Programmable Pipeline"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    // code
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

- (void)dealloc
{
    // code
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    bool gbActiveWindow;
    bool gbEscapeKeyIsPressed;
    bool gbFullscreen;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint gVao_pyramid;
    GLuint gVbo_pyramid_position;
    GLuint gVbo_pyramid_normal;
    GLuint gVbo_pyramid_element;
    
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    
    GLuint L_KeyPressed_uniform;
    
    GLuint La_1_uniform;
    GLuint Ld_1_uniform;
    GLuint Ls_1_uniform;
    
    GLuint La_2_uniform;
    GLuint Ld_2_uniform;
    GLuint Ls_2_uniform;
    
    GLuint light_1_position_uniform;
    GLuint light_2_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
    
    GLfloat anglePyramid;
    bool gbAnimate;
    bool gbLight;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    
    gbActiveWindow = false;
    gbEscapeKeyIsPressed = false;
    gbFullscreen = false;
    
    anglePyramid = 0.0f;
    
    if(self)
    {
        [[self window]setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[] ={
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion3_2Core,
            // Specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0}; // last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No valid OpenGL pixel format is available Exitting...");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
    }
    return(self);
}

- (CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    [self drawView];
    if (gbAnimate == true)
    {
        [self updateAngle];
    }
    
    [pool release];
    return(kCVReturnSuccess);
}

// Override method of NSOpenGLView
- (void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile, "OpenGL Version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // *** VERTEX SHADER ***
    // create shader
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // provide source code to shader
    const GLchar *vertexShaderSourceCode =
    "#version 330 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform vec4 u_light_1_position;" \
    "uniform vec4 u_light_2_position;" \
    "uniform int u_lighting_enabled;" \
    "out vec3 transformed_normals;" \
    "out vec3 light_1_direction;" \
    "out vec3 light_2_direction;" \
    "out vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "if(u_lighting_enabled == 1)" \
    "{" \
    "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
    "transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
    "light_1_direction = vec3(u_light_1_position) - eyeCoordinates.xyz;" \
    "light_2_direction = vec3(u_light_2_position) - eyeCoordinates.xyz;" \
    "viewer_vector = -eyeCoordinates.xyz;" \
    "}" \
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    
    glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(gVertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                exit(0);
            }
        }
    }
    
    // *** FRAGMENT SHAER *** //
    // create fragment shader
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
    "#version 330 core" \
    "\n" \
    "in vec3 transformed_normals;" \
    "in vec3 light_1_direction;" \
    "in vec3 light_2_direction;" \
    "in vec3 viewer_vector;" \
    "out vec4 FragColor;" \
    "uniform vec3 u_La_1;" \
    "uniform vec3 u_Ld_1;" \
    "uniform vec3 u_Ls_1;" \
    "uniform vec3 u_La_2;" \
    "uniform vec3 u_Ld_2;" \
    "uniform vec3 u_Ls_2;" \
    "uniform vec3 u_Ka;" \
    "uniform vec3 u_Kd;" \
    "uniform vec3 u_Ks;" \
    "uniform float u_material_shininess;" \
    "uniform int u_lighting_enabled;" \
    "void main(void)" \
    "{" \
    "vec3 phong_ads_color;" \
    "if(u_lighting_enabled == 1)" \
    "{" \
    "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
    "vec3 normalized_light_1_direction = normalize(light_1_direction);" \
    "vec3 normalized_light_2_direction = normalize(light_2_direction);" \
    "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
    "vec3 ambient_1 = u_La_1 * u_Ka;" \
    "vec3 ambient_2 = u_La_2 * u_Ka;" \
    "float tn1_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_1_direction),0.0);" \
    "float tn2_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_2_direction),0.0);" \
    "vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;" \
    "vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;" \
    "vec3 reflection_vector_1 = reflect(-normalized_light_1_direction,normalized_transformed_normals);" \
    "vec3 reflection_vector_2 = reflect(-normalized_light_2_direction,normalized_transformed_normals);" \
    "vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,normalized_viewer_vector),0.0),u_material_shininess);" \
    "vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,normalized_viewer_vector),0.0),u_material_shininess);" \
    "phong_ads_color = (ambient_1+ambient_2) + (diffuse_1+diffuse_2) + (specular_1+specular_2);" \
    "}"\
    "else" \
    "{" \
    "phong_ads_color = vec3(1.0,1.0,1.0);" \
    "}" \
    "FragColor = vec4(phong_ads_color,1.0);" \
    "}";
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(gFragmentShaderObject);
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                exit(0);
            }
        }
    }
    
    // *** SHADER PROGRAM ***
    // create program
    gShaderProgramObject = glCreateProgram();
    
    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    // attach fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    
    // pre-link binding of shader program object with vertex shader texture attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
    
    // link shader
    glLinkProgram(gShaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength>0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                exit(0);
            }
        }
    }
    
    // get uniform location
    model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
    view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    // L or l key pressed or not
    L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
    
    // ambient color intensity of light
    La_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_1");
    // diffuse color intensity of light
    Ld_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_1");
    // specular color intensity of light
    Ls_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_1");
    // position of light
    light_1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_1_position");
    
    // ambient color intensity of light
    La_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_2");
    // diffuse color intensity of light
    Ld_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_2");
    // specular color intensity of light
    Ls_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_2");
    // position of light
    light_2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_2_position");
    
    // amient reflective color intensity of light
    Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
    // diffuse reflective color intensity of light
    Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
    // specular reflective color intensity of light
    Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
    // shininess of material (value is conventionally between 0 to 200)
    material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
    
    // *** vertices, colors, shader attribs, vbo, vao initialization *** //
    const GLfloat pyramidVertices[] =
    {
        // FRONT FACE
        0.0f,1.0f,0.0f,    // apex
        -1.0f,-1.0f,1.0f,    // left-bottom
        1.0f,-1.0f,1.0f,    // right-bottom
        
        // RIGHT FACE
        0.0f,1.0f,0.0f,
        1.0f,-1.0f,1.0f,
        1.0f,-1.0f,-1.0f,
        
        // BACK FACE
        0.0f,1.0f,0.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,
        
        // LEFT FACE
        0.0f,1.0f,0.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,1.0f
    };
    
    const GLfloat pyramidNormals[] =
    {
        0.0f,0.447214f,0.894427f,    // normal for front face
        0.0f,0.447214f,0.894427f,
        0.0f,0.447214f,0.894427f,
        
        0.894427f,0.447214f,0.0f,    // normal for right face
        0.894427f,0.447214f,0.0f,
        0.894427f,0.447214f,0.0f,
        
        0.0f,0.447214f,-0.894427f,    // normal for back face
        0.0f,0.447214f,-0.894427f,
        0.0f,0.447214f,-0.894427f,
        
        -0.894427f,0.447214f,0.0f,    // normal for left face
        -0.894427f,0.447214f,0.0f,
        -0.894427f,0.447214f,0.0f
    };
    
    // *************************
    // VAO FOR PYRAMID
    // *************************
    
    // generate and bind vao for pyramid
    glGenVertexArrays(1, &gVao_pyramid);
    glBindVertexArray(gVao_pyramid);
    
    // ******************
    // VBO FOR POSITION
    // ******************
    glGenBuffers(1, &gVbo_pyramid_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // ******************
    // VBO FOR NORMAL
    // ******************
    glGenBuffers(1, &gVbo_pyramid_normal);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // unbind from vao for pyramid
    glBindVertexArray(0);
    
    //glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_CULL_FACE);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    
    // set perspective matrix to identity matrix
    gPerspectiveProjectionMatrix = vmath::mat4::identity();
    
    gbLight = false;
    gbAnimate = false;
    
    // ****************************************************
    // MAY BE WE NEED TO PROVIDE REASHAPE FUNCTION CALL HERE
    // ****************************************************
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkStart(displayLink);
}

- (void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    // code
    [self drawView];
    if (gbAnimate == true)
    {
        [self updateAngle];
    }
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using opengl program object
    glUseProgram(gShaderProgramObject);
    
    // opengl drawing
    
    if (gbLight == true)
    {
        // set u_lighting_enabled uniform
        glUniform1i(L_KeyPressed_uniform, 1);
        
        // setting light properties
        glUniform3fv(La_1_uniform, 1, light_1_Ambient);
        glUniform3fv(Ld_1_uniform, 1, light_1_Diffuse);
        glUniform3fv(Ls_1_uniform, 1, light_1_Specular);
        glUniform4fv(light_1_position_uniform, 1, light_1_Position);
        
        glUniform3fv(La_2_uniform, 1, light_2_Ambient);
        glUniform3fv(Ld_2_uniform, 1, light_2_Diffuse);
        glUniform3fv(Ls_2_uniform, 1, light_2_Specular);
        glUniform4fv(light_2_position_uniform, 1, light_2_Position);
        
        // setting materials properties
        glUniform3fv(Ka_uniform, 1, material_ambient);
        glUniform3fv(Kd_uniform, 1, material_diffuse);
        glUniform3fv(Ks_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);
    }
    else
    {
        // set u_lighting_enabled uniform
        glUniform1i(L_KeyPressed_uniform, 0);
    }
    
    // Opengl drawing
    // set model, modelview, rotation matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    // rotation
    rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
    
    // apply z axis translation to go deep into the screen by -2.0
    // so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
    modelMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    modelMatrix = modelMatrix * rotationMatrix;
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
    
    // bind to vao of pyramid
    glBindVertexArray(gVao_pyramid);
    
    // draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
    glDrawArrays(GL_TRIANGLES, 0, 12);    // 3(each with its x,y,z) vertices in triangleVertices array
    
    // unbind from vao of pyramid
    glBindVertexArray(0);
    
    // stop using opengl program object
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)updateAngle
{
    anglePyramid = anglePyramid + 0.1f;
    if (anglePyramid >= 360)
    {
        anglePyramid = 0.0f;
    }
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    static BOOL bIsAKeyPressed = false;
    static BOOL bIsLKeyPressed = false;
    
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: // Esc key
            [ self release];
            [NSApp terminate:self];
            break;
        case 'A':
        case 'a':
            if (bIsAKeyPressed == false)
            {
                gbAnimate = true;
                bIsAKeyPressed = true;
            }
            else
            {
                gbAnimate = false;
                bIsAKeyPressed = false;
            }
            break;
        case 'L':    // for L or l
        case 'l':
            if (bIsLKeyPressed == false)
            {
                gbLight = true;
                bIsLKeyPressed = true;
            }
            else
            {
                gbLight = false;
                bIsLKeyPressed = false;
            }
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)dealloc
{
    // code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    // destroy vao
    if (gVao_pyramid)
    {
        glDeleteVertexArrays(1, &gVao_pyramid);
        gVao_pyramid = 0;
    }
    // destroy position vbo
    if (gVbo_pyramid_position)
    {
        glDeleteBuffers(1, &gVbo_pyramid_position);
        gVbo_pyramid_position = 0;
    }
    // destroy normal vbo
    if (gVbo_pyramid_normal)
    {
        glDeleteBuffers(1, &gVbo_pyramid_normal);
        gVbo_pyramid_normal = 0;
    }
    // destroy element vbo
    if (gVbo_pyramid_element)
    {
        glDeleteBuffers(1, &gVbo_pyramid_element);
        gVbo_pyramid_element = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    // detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    
    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    // unlink shader program
    glUseProgram(0);
    
    if (gpFile)
    {
        fprintf(gpFile, "Log File Is Successfully Closed.\n");
        fclose(gpFile);
        gpFile = NULL;
    }
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}

