#include <GL/freeglut.h>

// Global variable declaration
bool bFullScreen = false;	// Variable to toggle for fullscreen

int main(int argc, char** argv)
{
	// function prototype
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	// Code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);

	glutInitWindowSize(800,600);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Yellow Bordered Triangle");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	// to clear all pixels
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// 20 equally spaced, full length BLUE colored horizontal lines, each of width 1
	glLineWidth(1);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	float yUpperCount = 0.05;
	for (int i = 0; i < 20; i++)
	{
		glVertex3f(-1.0f, yUpperCount, 0.0f);
		glVertex3f(1.0f, yUpperCount, 0.0f);
		yUpperCount = yUpperCount + 0.05;
	}
	glEnd();

	// 20 equally spaced, full length BLUE colored horizontal lines, each of width 1
	glLineWidth(1);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	float yLowerCount = -0.05;
	for (int i = 0; i < 20; i++)
	{
		glVertex3f(-1.0f, yLowerCount, 0.0f);
		glVertex3f(1.0f, yLowerCount, 0.0f);
		yLowerCount = yLowerCount - 0.05;
	}
	glEnd();

	// 20 equally spaced, full length BLUE colored vertical lines, each of width 1
	glLineWidth(1);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	float xRightCount = 0.05;
	for (int i = 0; i < 20; i++)
	{
		glVertex3f(xRightCount, -1.0f, 0.0f);
		glVertex3f(xRightCount, 1.0f, 0.0f);
		xRightCount = xRightCount + 0.05;
	}
	glEnd();

	// 20 equally spaced, full length BLUE colored vertical lines, each of width 1
	glLineWidth(1);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	float xLeftCount = -0.05;
	for (int i = 0; i < 20; i++)
	{
		glVertex3f(xLeftCount, -1.0f, 0.0f);
		glVertex3f(xLeftCount, 1.0f, 0.0f);
		xLeftCount = xLeftCount - 0.05;
	}
	glEnd();

	// Single RED colored horizontal line of width 3
	glLineWidth(3);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();

	// Single RED colored vertical line of width 3
	glLineWidth(3);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glEnd();

	// Triangle
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glEnd();

	// To process buffred opengl routines
	glutSwapBuffers();

}

void initialize(void)
{
	// To select clearing(background) color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
			break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{

}