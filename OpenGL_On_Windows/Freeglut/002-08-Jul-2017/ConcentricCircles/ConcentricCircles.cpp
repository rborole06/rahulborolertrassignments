#include <GL/freeglut.h>
#include <math.h>

// Global variable declaration
bool bFullScreen = false;	// Variable to toggle for fullscreen

typedef struct color
{
	float red, green, blue;
} color_t;

int main(int argc, char** argv)
{
	// function prototype
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	// Code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);

	glutInitWindowSize(800,600);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Concentric Circles");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	// Prototype declaration
	void drawConcentricCircles();

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Draw Kundali
	drawConcentricCircles();

	// To process buffred opengl routines
	glutSwapBuffers();

}

// draw kundali
void drawConcentricCircles()
{
	GLfloat PI = 3.1415f;
	color_t color_array[] = {
		{ 1.0f,0.0f,0.0f },
		{ 1.0f,0.0f,0.0f },
		{ 0.0f,1.0f,0.0f },
		{ 0.0f,0.0f,1.0f },
		{ 0.0f,1.0f,1.0f },
		{ 1.0f,0.0f,1.0f },
		{ 1.0f,1.0f,0.0f },
		{ 1.0f,1.0f,1.0f },
		{ 1.0f,0.52f,0.0f },
		{ 0.5f,0.5f,0.5f },
		{ 0.53f,1.0f,0.75f },
	};
	GLint c = 10;
	for (GLfloat i = 1.0f; i >= 0.0f; i = i - 0.1f, c--)
	{
		glLineWidth(3);
		glBegin(GL_POINTS);
		glColor3f(color_array[c].red, color_array[c].green, color_array[c].blue);
		for (GLfloat angle = 0.0f; angle < 2.0f*PI; angle = angle + 0.01f)
		{
			glVertex3f((cos(angle)*i), (sin(angle)*i), 0.0f);
		}
		glEnd();
	}
}

void initialize(void)
{
	// To select clearing(background) color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
			break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{

}