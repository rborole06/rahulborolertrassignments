#include <GL/freeglut.h>
#include <math.h>

// Prototype declaration
void drawTriangle(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
void CalcRadius(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
void drawMedian(GLfloat, GLfloat, GLfloat, GLfloat);

// Global variable declaration
bool bFullScreen = false;	// Variable to toggle for fullscreen

typedef struct color
{
	float red, green, blue;
} color_t;

int main(int argc, char** argv)
{
	// function prototype
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	// Code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);

	glutInitWindowSize(800,600);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Deathly Hollow Sign");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	// Prototype declaration
	void drawDeathlyHollow();

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	drawDeathlyHollow();

	// To process buffred opengl routines
	glutSwapBuffers();

}

// Draw deathly hollow sign
void drawDeathlyHollow()
{
	GLfloat x1 = 0.0f;
	GLfloat x2 = 1.0f;
	GLfloat x3 = -1.0f;
	GLfloat y1 = 1.0f;
	GLfloat y2 = -1.0f;
	GLfloat y3 = -1.0f;

	drawTriangle(x1, y1, x2, y2, x3, y3);
	CalcRadius(x1, y1, x2, y2, x3, y3);
	drawMedian(x2, y2, x3, y3);
}

void drawTriangle(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3)
{
	glBegin(GL_LINE_LOOP);
	glLineWidth(3);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(x1, y1, 0.0f);	// C
	glVertex3f(x2, y2, 0.0f);	// B
	glVertex3f(x3, y3, 0.0f);	// A
	glEnd();
}

void CalcRadius(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3)
{
	void drawCircle(GLfloat, GLfloat, GLfloat);

	// Length of sides of triangle
	GLfloat a = sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
	GLfloat b = sqrt(pow((x3 - x1), 2) + pow((y3 - y1), 2));
	GLfloat c = sqrt(pow((x3 - x2), 2) + pow((y3 - y2), 2));

	// Perimeter of triangle
	GLfloat perimeter = a + b + c;

	// Semi Perimeter of triangle
	GLfloat s = (a + b + c) / 2;

	GLfloat Ox = ((a*(-1.0f)) + (b*1.0f) + (c*0.0f)) / perimeter;
	GLfloat Oy = ((a*(-1.0f)) + (b*(-1.0f)) + (c*1.0f)) / perimeter;
	GLfloat r = sqrt(s*(s - a)*(s - b)*(s - c)) / s;

	drawCircle(Ox, Oy, r);

}

void drawCircle(GLfloat Ox, GLfloat Oy, GLfloat r)
{
	GLfloat PI = 3.1415f;
	glBegin(GL_POINTS);
	glPointSize(3);
	glColor3f(0.0f, 1.0f, 0.0f);
	for (GLfloat angle = 0.0f; angle < 2.0f*PI; angle = angle + 0.01f)
	{
		glVertex3f(((cos(angle)*r) + Ox), ((sin(angle)*r) + Oy), 0.0f);
	}
	glEnd();
}

void drawMedian(GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3)
{
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f((x2 + x3) / 2.0f, (y2 + y3) / 2.0f, 0.0f);
	glEnd();
}

void initialize(void)
{
	// To select clearing(background) color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
			break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{

}