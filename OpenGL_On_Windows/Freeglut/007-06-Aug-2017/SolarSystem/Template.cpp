#include <GL/freeglut.h>

// Global variable declaration
bool bFullScreen = false;	// Variable to toggle for fullscreen
static int year = 0;
static int day = 0;

int main(int argc, char** argv)
{
	// function prototype
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	// Code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);

	glutInitWindowSize(800,600);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Solar System Using Freeglut");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// Clear color and depth

	glMatrixMode(GL_MODELVIEW);		// select modelview matrix
	glLoadIdentity();		// reset modelview matrix

	gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);	// view  transformation

	glPushMatrix();	// push CTM(Current translation matrix) on matrix stack 

	glColor3f(1.0f, 1.0f, 1.0f);

	glutWireSphere(1.0, 20, 16);	// Draw sun

	glColor3f(0.4f, 0.9f, 1.0f);

	glRotatef(GLfloat(year), 0.0f, 1.0f, 0.0f);		// Rotate earth around y axis
	glTranslatef(2.0f, 0.0f, 0.0f);		// Translate earth
	glRotatef(GLfloat(day), 0.0f, 1.0f, 0.0f);

	glutWireSphere(0.2, 10, 8);

	glPopMatrix();

	// To process buffred opengl routines
	glutSwapBuffers();
}

void initialize(void)
{
	// To select clearing(background) color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_FLAT);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'D':
		day = (day + 6) % 360;
		glutPostRedisplay();
		break;
	case 'd':
		day = (day - 6) % 360;
		glutPostRedisplay();
		break;
	case 'Y':
		year = (year + 3) % 360;
		glutPostRedisplay();
		break;
	case 'y':
		year = (year - 3) % 360;
		glutPostRedisplay();
		break;
	case 'F':
	case 'f':
		if (bFullScreen == false)
		{
			glutFullScreen();
			bFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
			break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

void uninitialize(void)
{

}