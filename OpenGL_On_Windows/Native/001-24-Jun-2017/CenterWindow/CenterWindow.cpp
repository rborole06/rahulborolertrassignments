#include<windows.h>
#include<stdio.h>
#include "resource.h"

// Prototype of WndProc
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	// Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Center Window");

	// Code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(hInstance, MAKEINTRESOURCE(MYCURSOR));
	wndclass.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(0,0,255));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));

	// Register class
	RegisterClassEx(&wndclass);

	// Get Window width and hight
	int widthOfWindow = GetSystemMetrics(SM_CXFULLSCREEN);
	int heightOfWindow = GetSystemMetrics(SM_CYFULLSCREEN);

	// Get Screen width and hight
	int widthOfScreen = GetSystemMetrics(SM_CXSCREEN);
	int heightOfScreen = GetSystemMetrics(SM_CYSCREEN);

	if (heightOfScreen > 0) {
		TCHAR str[255];
		swprintf_s(str, TEXT("Width Of Screen, is %d, Width Of Window is %d, Height Of Screen is %d, Height Of Window is %d,"), widthOfScreen, widthOfWindow, heightOfScreen, heightOfWindow);
		MessageBox(NULL, str, TEXT("Message"), MB_OK);
	}

	// Create Window
	hwnd = CreateWindow(szAppName,
		TEXT("Center Window"),
		WS_OVERLAPPEDWINDOW,
		((widthOfScreen - 800) / 2),
		((heightOfScreen - 600) / 2),
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd,nCmdShow);
	UpdateWindow(hwnd);

	// Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_PAINT:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}