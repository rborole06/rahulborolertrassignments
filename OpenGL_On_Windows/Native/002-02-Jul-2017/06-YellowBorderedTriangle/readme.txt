
==========================================================================================
Draw a YELLOW bordered (not filled), triangle on the graph paper background
The width of the lines making up the triangle must be the same as the width of the blue lines of the graph paper
The size of the triangle must be half that of the graph paper.
==========================================================================================