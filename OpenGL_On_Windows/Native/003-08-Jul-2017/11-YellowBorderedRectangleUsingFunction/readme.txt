
==========================================================================================

Draw a YELLOW bordered (not filled), square on the graph paper background
And draw rectangle using function

The width of the lines making up the square must be the same as the width of the blue lines of the graph paper

The size of the square must be half that of the graph paper.

==========================================================================================