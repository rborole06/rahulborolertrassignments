
==========================================================================================
Draw a YELLOW bordered (not filled), circle on the graph paper background using GL_LINES
and the equation of circle as taught with the help of following code as
in the RED BOOK 3rd Edition (make changes in it as required) :

#define PI 3.1415926535898 //or look for 'M_PI' in 'math.h'
GLint circle_points = 100; //you can try using 1000 or 10000
glBegin(GL_LINE_LOOP); //GL_LINES will also do
    for(int i=0; i < circle_points; i++)
    {
        angle = 2 * PI * i/circle_points;
        glVertex2f(cos(angle), sin(angle))
    }
glEnd();

Make a function to draw circle so that we can re-use it
==========================================================================================