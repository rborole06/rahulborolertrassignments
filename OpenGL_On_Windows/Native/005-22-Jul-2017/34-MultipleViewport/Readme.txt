
1 - Multile viewport projection assignment
2 - Perform 9 types of viewport projections on key press from 1 to 9
	Key 1 - Put viewport in left half top of window
	Key 2 - Put viewport in right half top of window
	Key 3 - Put viewport in left bottom half of window
	Key 4 - Put viewport in right bottom half of window
	Key 5 - Put viewport in left half of window
	Key 6 - Put viewport in right half of window
	Key 7 - Put viewport in upper half of window
	Key 8 - Put viewport in lower half of window
	Key 9 - Put viewport in center of window
	