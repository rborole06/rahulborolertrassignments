#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include <gl/GLU.h>
#include <math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI 3.14159

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

// Prototype of WndProc declared globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global Variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
GLfloat angleDeg = 0.0f;
GLfloat angleRadian = 0.0f;

GLfloat identityMatrix[16];
GLfloat translationMatrix[16];
GLfloat scaleMatrix[16];
GLfloat xRotationMatrix[16];
GLfloat yRotationMatrix[16];
GLfloat zRotationMatrix[16];

// main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function prototype

	void display(void);
	void initialize(void);
	void uninitialize(void);
	void update(void);

	// Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTR - OGL Using Double Buffer");
	bool bDone = false;
	int iWidth;
	int iHeight;

	// code
	// initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, NULL);
	wndclass.hCursor = LoadCursor(hInstance, NULL);
	wndclass.hIconSm = LoadIcon(hInstance, NULL);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// Registering Class
	RegisterClassEx(&wndclass);

	// Retrive Width
	iWidth = GetSystemMetrics(SM_CXSCREEN);

	// Retrive Height
	iHeight = GetSystemMetrics(SM_CYSCREEN);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Rotating cube without using transformation functions"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((iWidth - WIN_WIDTH) / 2),
		((iHeight - WIN_HEIGHT) / 2),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;

			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				update();
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

// WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Prototype
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// Variable Declarations
	MONITORINFO mi;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// Function Prototype
	void resize(int, int);

	// Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of pixel format descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// To avoid aliasing
	//glShadeModel(GL_SMOOTH);
	// To avoid distortion
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);

	// Initialization of identity matrix
	identityMatrix[0] = 1.0f;
	identityMatrix[1] = 0.0f;
	identityMatrix[2] = 0.0f;
	identityMatrix[3] = 0.0f;

	identityMatrix[4] = 0.0f;
	identityMatrix[5] = 1.0f;
	identityMatrix[6] = 0.0f;
	identityMatrix[7] = 0.0f;

	identityMatrix[8] = 0.0f;
	identityMatrix[9] = 0.0f;
	identityMatrix[10] = 1.0f;
	identityMatrix[11] = 0.0f;

	identityMatrix[12] = 0.0f;
	identityMatrix[13] = 0.0f;
	identityMatrix[14] = 0.0f;
	identityMatrix[15] = 1.0f;

	// Initialization of translation matrix
	translationMatrix[0] = 1.0f;
	translationMatrix[1] = 0.0f;
	translationMatrix[2] = 0.0f;
	translationMatrix[3] = 0.0f;

	translationMatrix[4] = 0.0f;
	translationMatrix[5] = 1.0f;
	translationMatrix[6] = 0.0f;
	translationMatrix[7] = 0.0f;

	translationMatrix[8] = 0.0f;
	translationMatrix[9] = 0.0f;
	translationMatrix[10] = 1.0f;
	translationMatrix[11] = 0.0f;

	translationMatrix[12] = 0.0f;
	translationMatrix[13] = 0.0f;
	translationMatrix[14] = -6.0f;
	translationMatrix[15] = 1.0f;

	// Initialization of scale matrix
	scaleMatrix[0] = 0.75f;
	scaleMatrix[1] = 0.0f;
	scaleMatrix[2] = 0.0f;
	scaleMatrix[3] = 0.0f;

	scaleMatrix[4] = 0.0f;
	scaleMatrix[5] = 0.75f;
	scaleMatrix[6] = 0.0f;
	scaleMatrix[7] = 0.0f;

	scaleMatrix[8] = 0.0f;
	scaleMatrix[9] = 0.0f;
	scaleMatrix[10] = 0.75f;
	scaleMatrix[11] = 0.0f;

	scaleMatrix[12] = 0.0f;
	scaleMatrix[13] = 0.0f;
	scaleMatrix[14] = 0.0f;
	scaleMatrix[15] = 1.0f;

}

void display(void)
{
	// Prototype declaration
	void drawCube();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(identityMatrix);
	glMultMatrixf(translationMatrix);
	glMultMatrixf(scaleMatrix);

	angleRadian = angleDeg*(PI / 180.0f);

	// Initialization of x-axis rotation matrix
	xRotationMatrix[0] = 1.0f;
	xRotationMatrix[1] = 0.0f;
	xRotationMatrix[2] = 0.0f;
	xRotationMatrix[3] = 0.0f;

	xRotationMatrix[4] = 0.0f;
	xRotationMatrix[5] = cos(angleRadian);
	xRotationMatrix[6] = sin(angleRadian);
	xRotationMatrix[7] = 0.0f;

	xRotationMatrix[8] = 0.0f;
	xRotationMatrix[9] = -sin(angleRadian);
	xRotationMatrix[10] = cos(angleRadian);
	xRotationMatrix[11] = 0.0f;

	xRotationMatrix[12] = 0.0f;
	xRotationMatrix[13] = 0.0f;
	xRotationMatrix[14] = 0.0f;
	xRotationMatrix[15] = 1.0f;

	glMultMatrixf(xRotationMatrix);

	// Initialization of y-axis rotation matrix
	yRotationMatrix[0] = cos(angleRadian);
	yRotationMatrix[1] = 0.0f;
	yRotationMatrix[2] = -sin(angleRadian);
	yRotationMatrix[3] = 0.0f;

	yRotationMatrix[4] = 0.0f;
	yRotationMatrix[5] = 1.0f;
	yRotationMatrix[6] = 0.0f;
	yRotationMatrix[7] = 0.0f;

	yRotationMatrix[8] = sin(angleRadian);
	yRotationMatrix[9] = 0.0f;
	yRotationMatrix[10] = cos(angleRadian);
	yRotationMatrix[11] = 0.0f;

	yRotationMatrix[12] = 0.0f;
	yRotationMatrix[13] = 0.0f;
	yRotationMatrix[14] = 0.0f;
	yRotationMatrix[15] = 1.0f;

	glMultMatrixf(yRotationMatrix);

	// Initialization of z-axis rotation matrix
	zRotationMatrix[0] = cos(angleRadian);
	zRotationMatrix[1] = sin(angleRadian);
	zRotationMatrix[2] = 0.0f;
	zRotationMatrix[3] = 0.0f;

	zRotationMatrix[4] = -sin(angleRadian);
	zRotationMatrix[5] = cos(angleRadian);
	zRotationMatrix[6] = 0.0f;
	zRotationMatrix[7] = 0.0f;

	zRotationMatrix[8] = 0.0f;
	zRotationMatrix[9] = 0.0f;
	zRotationMatrix[10] = 1.0f;
	zRotationMatrix[11] = 0.0f;

	zRotationMatrix[12] = 0.0f;
	zRotationMatrix[13] = 0.0f;
	zRotationMatrix[14] = 0.0f;
	zRotationMatrix[15] = 1.0f;

	glMultMatrixf(zRotationMatrix);

	//glRotatef(angleDeg, 1.0f, 1.0f, 1.0f);

	drawCube();

	SwapBuffers(ghdc);
}

// draw multi colored cube
void drawCube()
{
	glBegin(GL_QUADS);

	// FRONT FACE
	glColor3f(0.0f, 0.0f, 1.0f);	// Blue
	glVertex3f(1.0f, 1.0f, 1.0f);	// right-top corner of front face
	glVertex3f(-1.0f, 1.0f, 1.0f);	// left-top corner of front face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left-bottom corner of front face
	glVertex3f(1.0f, -1.0f, 1.0f);	// right-bottom corner of front face

	// RIGHT FACE
	glColor3f(1.0f, 0.0f, 1.0f);	// MAGENTA
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of right face
	glVertex3f(1.0f, 1.0f, 1.0f);	// left-top corner of right face
	glVertex3f(1.0f, -1.0f, 1.0f);	// left-bottom corner of right face
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-bottom corner of right face

	// BACK FACE
	glColor3f(0.0f, 1.0f, 1.0f);	// CYAN
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of back face
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of back face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-bottom corner of back face
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-bottom corner of back face

	// LEFT FACE
	glColor3f(1.0f,1.0f,0.0f);		// YELLOW
	glVertex3f(-1.0f, 1.0f, 1.0f);	// right-top corner of left face 
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of left face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-bottom corner of left face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// right-bottom corner of left face

	// TOP FACE
	glColor3f(1.0f, 0.0f, 0.0f);	// RED
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of top face
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of top face
	glVertex3f(-1.0f, 1.0f, 1.0f);	// left-bottom corner of top face
	glVertex3f(1.0f, 1.0f, 1.0f);	// right-bottom corner of top face

	// BOTTOM FACE
	glColor3f(0.0f, 1.0f, 0.0f);	// GREEN
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-top corner of bottom face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-top corner of bottom face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left-bottom corner of bottom face
	glVertex3f(1.0f, -1.0f, 1.0f);	// right-bottom corner of bottom face

	glEnd();
}

void update(void)
{
	angleDeg = angleDeg + 0.1f;
	if (angleDeg >= 360)
	{
		angleDeg = 0.0f;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}