#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include <gl/GLU.h>
#include <math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

// Prototype of WndProc declared globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void DrawI();
void DrawN();
void DrawD();
void DrawI2();
void DrawA();
void DrawIndiaStrip();
void ResetModelView();

// Global Variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

// Initialize for translation of I from left to right of screen
GLfloat iXTranslate = -3.0f;
// Initialize for translation of I from top to bottom of screen
GLfloat nYTranslate = 2.5f;
// Initialize for translation of D from far to near of screen (i.e from -z axis to +z axis)
GLfloat dZTranslate = -4.5f;
// Initialize for translation of I from bottom to top of screen
GLfloat i2YTranslate = -2.5f;
// Initialize for translation of A from right to left of screen
GLfloat aXTranslate = 3.0f;

// increment variables used for saffron, white, and green strip respectively
GLfloat i = -1.0f;
GLfloat j = -1.0f;
GLfloat k = -1.0f;

// Flags used to track expanding and shrinking of strip along X axis
GLint increseStripFlag = 1;
GLint decreseStripFlag = 0;

// Initialize Saffron, Red ang Green color value for saffron
GLfloat RValueForSaffron = 0.0f;
GLfloat GValueForSaffron = 0.0f;
GLfloat BValueForSaffron = 0.0f;

// Starting Green value for green color
GLfloat GValueForGreen = 0.0f;

// main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function prototype

	void display(void);
	void initialize(void);
	void uninitialize(void);

	// Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTR - OGL Using Double Buffer");
	bool bDone = false;
	int iWidth;
	int iHeight;

	// code
	// initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, NULL);
	wndclass.hCursor = LoadCursor(hInstance, NULL);
	wndclass.hIconSm = LoadIcon(hInstance, NULL);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// Registering Class
	RegisterClassEx(&wndclass);

	// Retrive Width
	iWidth = GetSystemMetrics(SM_CXSCREEN);

	// Retrive Height
	iHeight = GetSystemMetrics(SM_CYSCREEN);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("India - Animation"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((iWidth - WIN_WIDTH) / 2),
		((iHeight - WIN_HEIGHT) / 2),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;

			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

// WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Prototype
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// Variable Declarations
	MONITORINFO mi;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// Function Prototype
	void resize(int, int);

	// variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of pixel format descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// To avoid aliasing
	//glShadeModel(GL_SMOOTH);
	// To avoid distortion
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	// Prototype declaration
	void render();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// reset model view matrix
	ResetModelView();

	// translate I
	glTranslatef(iXTranslate, 0.0f, -4.0f);
	// trnaslation of I along X axis to create animation 
	if(iXTranslate<=-2.0f)
		iXTranslate = iXTranslate + 0.0001f;

	render();

	SwapBuffers(ghdc);
}

// Reset Model-View matrix
void ResetModelView()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// draw text INDIA
void render()
{
	// set width for text "INDIA"
	glLineWidth(5.0f);

	// Draw I from INDIA
	DrawI();

	// Start rendering of N after completion of I animation
	if (iXTranslate > -2.0f)
	{
		// reset model view matrix
		ResetModelView();

		// translate N
		glTranslatef(-1.5f, nYTranslate, -4.0f);
		// trnaslation of N along Y axis to create animation 
		if (nYTranslate >= 0.0f)
			nYTranslate = nYTranslate - 0.0001f;

		DrawN();
	}

	// Start rendering of D after completion of N animation
	if (nYTranslate < 0.0f)
	{
		// reset model view matrix
		ResetModelView();

		// translate D
		glTranslatef(-0.25f, 0.0f, -4.0f);
		// trnaslation of D along Z axis to create animation 
		if (dZTranslate <= -4.0f)
			dZTranslate = dZTranslate + 0.0001f;

		DrawD();
	}

	// Start rendering of I after completion of D animation
	if (RValueForSaffron > 1.0f && GValueForSaffron > 0.5f)
	{
		// reset model view matrix
		ResetModelView();

		// translate I
		glTranslatef(0.5f, i2YTranslate, -4.0f);
		// trnaslation of I along Y axis to create animation 
		if (i2YTranslate <= 0.0f)
			i2YTranslate = i2YTranslate + 0.0001f;

		DrawI2();
	}

	// Start rendering of A after completion of I animation
	if (i2YTranslate > 0.0f)
	{
		// reset model view matrix
		ResetModelView();

		// translate A
		glTranslatef(aXTranslate, 0.0f, -4.0f);
		// trnaslation of I along A axis to create animation 
		if (aXTranslate >= 1.5f)
			aXTranslate = aXTranslate - 0.0001f;

		DrawA();
	}

	// Start rendering of strip after completion of A animation
	if (aXTranslate < 1.5f)
	{
		// reset model view matrix
		ResetModelView();

		glTranslatef(-1.5f, 0.0f, -4.0f);

		DrawIndiaStrip();
	}

}

// Draw I
void DrawI()
{
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glEnd();
}

// Draw N
void DrawN()
{
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glEnd();
}

// Draw D
void DrawD()
{
	glBegin(GL_LINES);

	glColor3f(RValueForSaffron, GValueForSaffron, BValueForSaffron);
	glVertex3f(-0.250f, 1.0f, 0.0f);
	glColor3f(0.0f, GValueForGreen, 0.0f);
	glVertex3f(-0.250f, -1.0f, 0.0f);

	glColor3f(RValueForSaffron, GValueForSaffron, BValueForSaffron);
	glVertex3f(-0.3f, 1.0f, 0.0f);
	glVertex3f(0.250f, 1.0f, 0.0f);

	glColor3f(RValueForSaffron, GValueForSaffron, BValueForSaffron);
	glVertex3f(0.250f, 1.01f, 0.0f);
	glColor3f(0.0f, GValueForGreen, 0.0f);
	glVertex3f(0.250f, -1.01f, 0.0f);

	glColor3f(0.0f, GValueForGreen, 0.0f);
	glVertex3f(0.250f, -1.0f, 0.0f);
	glVertex3f(-0.3f, -1.0f, 0.0f);

	if (RValueForSaffron <= 1.0f)
	{
		RValueForSaffron = RValueForSaffron + 0.0001f;
	}
	if (GValueForSaffron <= 0.6f)
	{
		GValueForSaffron = GValueForSaffron + 0.0001f;
	}
	if (BValueForSaffron <= 0.2f)
	{
		BValueForSaffron = BValueForSaffron + 0.0001f;
	}
	if (GValueForGreen <= 0.5f)
	{
		GValueForGreen = GValueForGreen + 0.0001f;
	}
	glEnd();
}

// Draw I
void DrawI2()
{
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glEnd();
}

// Draw A
void DrawA()
{
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glEnd();
}

// Draw strip
void DrawIndiaStrip()
{
	glBegin(GL_LINES);

	// If increaseStripFlag = 1 then strip goes expanding from -X to +X axis direction
	// Else If decreseStripFlag = 1 strip goes shrinking from -X to +X direction
	if (increseStripFlag == 1) {
		// draw saffron strip
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(-1.0f, 0.06f, 0.1f);
		glVertex3f(i, 0.06f, 0.1f);
		if (i <= 3.190f) {
			i = i + 0.001f;
		}

		// draw white strip
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-1.0f, 0.03f, 0.1f);
		glVertex3f(j, 0.03f, 0.1f);
		if (j <= 3.200f) {
			j = j + 0.001f;
		}

		// draw green strip
		glColor3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-1.0f, 0.0f, 0.1f);
		glVertex3f(k, 0.0f, 0.1f);
		if (k <= 3.210f) {
			k = k + 0.001f;
		}
		else if (k > 3.210f && j > 3.200f && i > 3.190f)
		{
			// after completion of expanding strip rendering, reset i,j,k to 1.0
			i = -1.0f;
			j = -1.0f;
			k = -1.0f;
			// also set decreaseStripFlag to 1 for rendering of shrinking strip
			decreseStripFlag = 1;
			// MOST IMP : reset increaseStripFlag to 0 to stop rendering of expanding strip
			increseStripFlag = 0;
		}
	}
	else if (decreseStripFlag == 1)
	{
			// draw saffron strip
			glColor3f(1.0f, 0.6f, 0.2f);
			glVertex3f(i, 0.06f, 0.1f);
			glVertex3f(3.190f, 0.06f, 0.1f);
			if (i <= 2.730f) {
				i = i + 0.001f;
			}

			// draw white strip
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(j, 0.03f, 0.1f);
			glVertex3f(3.200f, 0.03f, 0.1f);
			if (j <= 2.720f) {
				j = j + 0.001f;
			}

			// draw green strip
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(k, 0.0f, 0.1f);
			glVertex3f(3.210f, 0.0f, 0.1f);
			if (k <= 2.710f) {
				k = k + 0.001f;
			}
	}
	glEnd();
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}