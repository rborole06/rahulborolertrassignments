#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include <gl/GLU.h>
#include <math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

// Prototype of WndProc declared globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void ResetModelView();
void DrawFirstPrimitive();
void DrawSecondPrimitive();
void DrawThirdPrimitive();
void DrawFourthPrimitive();
void DrawFifthPrimitive();
void DrawSixthPrimitive();

// Global Variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

// main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function prototype

	void display(void);
	void initialize(void);
	void uninitialize(void);

	// Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTR - OGL Using Double Buffer");
	bool bDone = false;
	int iWidth;
	int iHeight;

	// code
	// initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, NULL);
	wndclass.hCursor = LoadCursor(hInstance, NULL);
	wndclass.hIconSm = LoadIcon(hInstance, NULL);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// Registering Class
	RegisterClassEx(&wndclass);

	// Retrive Width
	iWidth = GetSystemMetrics(SM_CXSCREEN);

	// Retrive Height
	iHeight = GetSystemMetrics(SM_CYSCREEN);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Opengl Primitives"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((iWidth - WIN_WIDTH) / 2),
		((iHeight - WIN_HEIGHT) / 2),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;

			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

// WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Prototype
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// Variable Declarations
	MONITORINFO mi;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// Function Prototype
	void resize(int, int);

	// variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of pixel format descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// To avoid aliasing
	//glShadeModel(GL_SMOOTH);
	// To avoid distortion
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	// Prototype declaration
	void render();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// reset model view matrix
	ResetModelView();

	render();

	SwapBuffers(ghdc);
}

// Reset Model-View matrix
void ResetModelView()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// draw text INDIA
void render()
{
	// Translate first primitive
	glTranslatef(-2.3f, 0.8f, -4.0f);
	DrawFirstPrimitive();

	// reset model view matrix
	ResetModelView();
	// Translate seconf primitive
	glTranslatef(0.0f, 0.8f, -4.0f);
	DrawSecondPrimitive();

	// reset model view matrix
	ResetModelView();
	// Translate third primitive
	glTranslatef(2.3f, 0.8f, -4.0f);
	DrawThirdPrimitive();

	// reset model view matrix
	ResetModelView();
	// Translate fourth primitive
	glTranslatef(-2.3f, -0.8f, -4.0f);
	DrawFourthPrimitive();

	// reset model view matrix
	ResetModelView();
	// Translate fifth primitive
	glTranslatef(0.0f, -0.8f, -4.0f);
	DrawFifthPrimitive();

	// reset model view matrix
	ResetModelView();
	// Translate fifth primitive
	glTranslatef(2.3f, -0.8f, -4.0f);
	DrawSixthPrimitive();
}

// Draw first primitive
void DrawFirstPrimitive()
{
	glColor3f(1.0f, 1.0f, 1.0f);
	glPointSize(2.0f);

	glBegin(GL_POINTS);

	GLfloat yAxis = 0.6f;
	for (GLfloat i = 1.0f; i <= 4.0f; i=i+1.0f)
	{
		GLfloat xAxis = -0.6f;
		for (GLfloat j = 1.0f; j <= 4.0f; j=j+1.0f)
		{
			glVertex3f(xAxis, yAxis, 0.0f);
			xAxis = xAxis + 0.3f;
		}
		yAxis = yAxis - 0.3f;
	}
	glEnd();
}

// Draw second primitive
void DrawSecondPrimitive()
{
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.3f,0.6f,0.0f);
	glVertex3f(-0.6f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glVertex3f(-0.6f, 0.3f, 0.0f);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);
	glVertex3f(-0.3f, -0.3f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(-0.6f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glVertex3f(-0.6f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);
	glEnd();
}

// Draw third primitive
void DrawThirdPrimitive()
{
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glVertex3f(0.3f, -0.3f, 0.0f);
	glVertex3f(-0.6f, -0.3f, 0.0f);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.3f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(-0.3f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);
	glEnd();
}

// Draw fourth primitive
void DrawFourthPrimitive()
{
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glVertex3f(0.3f, -0.3f, 0.0f);
	glVertex3f(-0.6f, -0.3f, 0.0f);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glVertex3f(-0.6f, 0.3f, 0.0f);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);
	glVertex3f(-0.3f, -0.3f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);
	glVertex3f(-0.6f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, -0.3f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glVertex3f(-0.6f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);
	glEnd();
}

// Draw fifth primitive
void DrawFifthPrimitive()
{
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glVertex3f(0.3f, -0.3f, 0.0f);
	glVertex3f(-0.6f, -0.3f, 0.0f);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(-0.3f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.3f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);
	glEnd();
}

// Draw sixth primitive
void DrawSixthPrimitive()
{
	glColor3f(1.0f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(-0.3f, -0.3f, 0.0f);
	glVertex3f(-0.6f, -0.3f, 0.0f);
	glEnd();

	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);
	glVertex3f(-0.3f, -0.3f, 0.0f);
	glEnd();

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glVertex3f(0.3f, -0.3f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);
	glEnd();

	glLineWidth(1.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.3f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.6f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glEnd();
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}