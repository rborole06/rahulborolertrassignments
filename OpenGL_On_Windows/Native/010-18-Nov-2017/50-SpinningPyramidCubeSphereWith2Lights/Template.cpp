#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include <gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

// Prototype of WndProc declared globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global Variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;
GLfloat angleSphere = 0.0f;

GLUquadric *quadric = NULL;

// flag for enabled lighting
bool gbLighting = false;

// light 1 components (right side red light)
GLfloat light_1_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_1_diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_specular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_position[] = { 2.0f,0.8f,1.0f,0.0f };

// light 2 components (left side blue light)
GLfloat light_2_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_2_diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_2_specular[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_2_position[] = { -2.0f,0.8f,1.0f,0.0f };

// material array
GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess[] = { 50.0f,50.0f,50.0f,0.0f };

// flag to maintain which object is showing currently in window
GLboolean gbPyramidActive = GL_TRUE;
GLboolean gbCubeActive = GL_FALSE;
GLboolean gbSphereActive = GL_FALSE;

// main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function prototype

	void display(void);
	void initialize(void);
	void uninitialize(void);
	void update(void);

	// Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTR - OGL Using Double Buffer");
	bool bDone = false;
	int iWidth;
	int iHeight;

	// code
	// initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, NULL);
	wndclass.hCursor = LoadCursor(hInstance, NULL);
	wndclass.hIconSm = LoadIcon(hInstance, NULL);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// Registering Class
	RegisterClassEx(&wndclass);

	// Retrive Screen Width and Height
	iWidth = GetSystemMetrics(SM_CXSCREEN);
	iHeight = GetSystemMetrics(SM_CYSCREEN);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Spinning pyramid, cube, sphere using 2 lights"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((iWidth - WIN_WIDTH) / 2),
		((iHeight - WIN_HEIGHT) / 2),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;

			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				update();
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

// WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Prototype
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		case 0x4C:
			// if gbLighting is true then enable lighting
			// else disable lighting
			if (gbLighting == false)
			{
				glEnable(GL_LIGHTING);
				gbLighting = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbLighting = false;
			}
			break;
		case 0x50:	// on P keypress, enable pyramid flag
			gbPyramidActive = GL_TRUE;
			gbCubeActive = GL_FALSE;
			gbSphereActive = GL_FALSE;
			break;
		case 0x43:	// on P keypress, enable cube flag
			gbPyramidActive = GL_FALSE;
			gbCubeActive = GL_TRUE;
			gbSphereActive = GL_FALSE;
			break;
		case 0x53:	// on P keypress, enable sphere flag
			gbPyramidActive = GL_FALSE;
			gbCubeActive = GL_FALSE;
			gbSphereActive = GL_TRUE;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// Variable Declarations
	MONITORINFO mi;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// Function Prototype
	void resize(int, int);

	// Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of pixel format descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// set light source parameters (right side light)
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_1_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_1_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_1_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_1_position);

	// Enable the lighting
	glEnable(GL_LIGHT0);

	// set light source parameters (left side light)
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_2_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_2_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_2_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light_2_position);

	// Enable the lighting
	glEnable(GL_LIGHT1);

	// material array
	glMaterialfv(GL_FRONT, GL_SHININESS, material_ambient);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	quadric = gluNewQuadric();

	// To avoid aliasing
	glShadeModel(GL_SMOOTH);
	// To avoid distortion
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// specify clear values for the color buffers
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Prototype declaration
	void drawPyramid();
	void drawCube();

	if (gbPyramidActive == GL_TRUE)
	{
		// Show rotating pyramid
		glTranslatef(0.0f, 0.0f, -6.0f);
		glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);
		drawPyramid();
	}
	else if (gbCubeActive == GL_TRUE)
	{
		// Show rotating cube
		glTranslatef(0.0f, 0.0f, -6.0f);
		glScalef(0.75f, 0.75f, 0.75f);
		glRotatef(angleCube, 0.0f, 1.0f, 0.0f);
		drawCube();
	}
	else if (gbSphereActive == GL_TRUE)
	{
		// Show rotating sphere
		glTranslatef(0.0f, 0.0f, -3.0f);
		glRotatef(angleSphere, 0.0f, 1.0f, 0.0f);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		gluSphere(quadric, 0.75, 30, 30);
	}
	else
	{
	}

	SwapBuffers(ghdc);
}

// draw multi colored triangle
void drawPyramid()
{
	glBegin(GL_TRIANGLES);

	// FRONT FACE
	glNormal3f(0.0f, 0.447214f, 0.894427f);		// set normal for front face

	//glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	//glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left corner of front face

	//glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(1.0f, -1.0f, 1.0f);	// right corner of front face

	// RIGHT FACE
	glNormal3f(0.894427f, 0.447214f, 0.0f);	// set normal for right face

	//glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	//glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(1.0f, -1.0f, 1.0f);	// left corner of right face

	//glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(1.0f, -1.0f, -1.0f);	// right corner of right face

	// BACK FACE
	glNormal3f(0.0f, 0.447214f, -0.894427f);	// set normal for back face

	//glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	//glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(1.0f, -1.0f, -1.0f);	// left corner of back face

	//glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(-1.0f, -1.0f, -1.0f);// right corner of back face

	// LEFT FACE
	glNormal3f(-0.894427f, 0.447214f, 0.0f);	// set normal for left face

	//glColor3f(1.0f, 0.0f, 0.0f);	// red
	glVertex3f(0.0f, 1.0f, 0.0f);	// apex

	//glColor3f(0.0f, 0.0f, 1.0f);	// blue
	glVertex3f(-1.0f, -1.0f, -1.0f);// left corner of left face

	//glColor3f(0.0f, 1.0f, 0.0f);	// green
	glVertex3f(-1.0f, -1.0f, 1.0f);	// right corner of left face

	glEnd();
}

// draw multi colored cube
void drawCube()
{
	glBegin(GL_QUADS);

	// FRONT FACE
	glNormal3f(0.0f, 0.0f, 1.0f);	// set normal for front face
	//glColor3f(0.0f, 0.0f, 1.0f);	// Blue
	glVertex3f(1.0f, 1.0f, 1.0f);	// right-top corner of front face
	glVertex3f(-1.0f, 1.0f, 1.0f);	// left-top corner of front face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left-bottom corner of front face
	glVertex3f(1.0f, -1.0f, 1.0f);	// right-bottom corner of front face

									// RIGHT FACE
	glNormal3f(1.0f, 0.0f, 0.0f);		// set normal for right face
	//glColor3f(1.0f, 0.0f, 1.0f);	// MAGENTA
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of right face
	glVertex3f(1.0f, 1.0f, 1.0f);	// left-top corner of right face
	glVertex3f(1.0f, -1.0f, 1.0f);	// left-bottom corner of right face
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-bottom corner of right face

									// BACK FACE
	glNormal3f(0.0f, 0.0f, -1.0f);	// set normal for back face
	//glColor3f(0.0f, 1.0f, 1.0f);	// CYAN
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of back face
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of back face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-bottom corner of back face
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-bottom corner of back face

									// LEFT FACE
	glNormal3f(-1.0f, 0.0f, 0.0f);	// set normal for left face
	//glColor3f(1.0f, 1.0f, 0.0f);	// YELLOW
	glVertex3f(-1.0f, 1.0f, 1.0f);	// right-top corner of left face 
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of left face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-bottom corner of left face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// right-bottom corner of left face

									// TOP FACE
	glNormal3f(0.0f, 1.0f, 0.0f);	// set normal for top face
	//glColor3f(1.0f, 0.0f, 0.0f);	// RED
	glVertex3f(1.0f, 1.0f, -1.0f);	// right-top corner of top face
	glVertex3f(-1.0f, 1.0f, -1.0f);	// left-top corner of top face
	glVertex3f(-1.0f, 1.0f, 1.0f);	// left-bottom corner of top face
	glVertex3f(1.0f, 1.0f, 1.0f);	// right-bottom corner of top face

									// BOTTOM FACE
	glNormal3f(0.0f, -1.0f, 0.0f);	// set normal for bottom face
	//glColor3f(0.0f, 1.0f, 0.0f);	// GREEN
	glVertex3f(1.0f, -1.0f, -1.0f);	// right-top corner of bottom face
	glVertex3f(-1.0f, -1.0f, -1.0f);// left-top corner of bottom face
	glVertex3f(-1.0f, -1.0f, 1.0f);	// left-bottom corner of bottom face
	glVertex3f(1.0f, -1.0f, 1.0f);	// right-bottom corner of bottom face

	glEnd();
}

void update(void)
{
	anglePyramid = anglePyramid + 0.05f;
	if (anglePyramid >= 360)
	{
		anglePyramid = 0.0f;
	}
	angleCube = angleCube + 0.05f;
	if (angleCube >= 360)
	{
		angleCube = 0.0f;
	}
	angleSphere = angleSphere + 0.05f;
	if (angleSphere >= 360)
	{
		angleSphere = 0.0f;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}
}