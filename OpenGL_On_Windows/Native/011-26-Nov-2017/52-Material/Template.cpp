#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

// Prototype of WndProc declared globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global Variable Declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

GLUquadric *quadric = NULL;

// store ambient, diffuse, specular, position values of different materials
GLfloat material_array[4][6][4][4] = {
	{
		{
			// first sphere on first column, emerald material
			{ 0.0215f,0.1745f,0.0215f,1.0f },		// ambient
			{ 0.07568f,0.61424f,0.07568f,1.0f },	// diffuse
			{ 0.633f,0.727811f,0.633f,1.0f },		// specular
			{ 0.6f * 128.0f }						// position
		},
		{
			// second sphere on first column, jade material
			{ 0.135f,0.2225f,0.1575f,1.0f },
			{ 0.54f,0.89f,0.63f,1.0f },
			{ 0.316228f,0.316228f,0.316228f,1.0f },
			{ 0.1f * 128.0f }
		},
		{
			// third sphere on first column, obsidian material
			{ 0.05375f,0.05f,0.06625f,1.0f },
			{ 0.18275f,0.17f,0.22525f,1.0f },
			{ 0.332741f,0.328634f,0.346435f,1.0f },
			{ 0.3f * 128.0f }
		},
		{
			// fourth sphere on first column, pearl material
			{ 0.25f,0.20725f,0.20725f,1.0f },
			{ 1.0f,0.829f,0.829f,1.0f },
			{ 0.296648f,0.296648f,0.296648f,1.0f },
			{ 0.088f * 128.0f }
		},
		{
			// fifth sphere on first column, ruby material
			{ 0.1745f,0.01175f,0.01175f,1.0f },
			{ 0.61424f,0.04136f,0.04136f,1.0f },
			{ 0.727811f,0.626959f,0.626959f,1.0f },
			{ 0.6f * 128.0f }
		},
		{
			// sixth sphere on first column, turquoise material
			{ 0.1f,0.18725f,0.1745f,1.0f },
			{ 0.396f,0.74151f,0.69102f,1.0f },
			{ 0.297254f,0.30829f,0.306678f,1.0f },
			{ 0.6f * 128.0f }
		}
	},
	{
		{
			// first sphere on second column, brass material
			{ 0.329412f,0.223529f,0.027451f,1.0f },
			{ 0.780392f,0.568627f,0.113725f,1.0f },
			{ 0.992157f,0.941176f,0.807843f,1.0f },
			{ 0.21794872f * 128.0f }
		},
		{
			// second sphere on second column, bronze material
			{ 0.2125f,0.1275f,0.054f,1.0f },
			{ 0.714f,0.4284f,0.18144f,1.0f },
			{ 0.393548f,0.271906f,0.166721f,1.0f },
			{ 0.2f * 128.0f }
		},
		{
			// third sphere on second column, chrome material
			{ 0.25f,0.25f,0.25f,1.0f },
			{ 0.4f,0.4f,0.4f,1.0f },
			{ 0.774597f,0.774597f,0.774597f,1.0f },
			{ 0.6f * 128.0f }
		},
		{
			// fourth sphere on second column, copper material
			{ 0.19125f,0.0735f,0.0225f,1.0f },
			{ 0.7038f,0.27048f,0.0828f,1.0f },
			{ 0.256777f,0.137622f,0.086014f,1.0f },
			{ 0.1f * 128.0f }
		},
		{
			// fifth sphere on second column, gold material
			{ 0.24725f,0.1995f,0.0745f,1.0f },
			{ 0.75164f,0.60648f,0.22648f,1.0f },
			{ 0.628281f,0.555802f,0.366065f,1.0f },
			{ 0.4f * 128.0f }
		},
		{
			// sixth sphere on second column, silver material
			{ 0.19225f,0.19225f,0.19225f,1.0f },
			{ 0.50754f,0.50754f,0.50754f,1.0f },
			{ 0.508273f,0.508273f,0.508273f,1.0f },
			{ 0.4f * 128.0f }
		}
	},
	{
		{
			// first sphere on third column, black material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.01f,0.01f,0.01f,1.0f },
			{ 0.50f,0.50f,0.50f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// second sphere on third column, cyan material
			{ 0.0f,0.1f,0.06f,1.0f },
			{ 0.0f,0.50980392f,0.50980392f,1.0f },
			{ 0.50196078f,0.50196078f,0.50196078f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// third sphere on third column, green material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.1f,0.35f,0.1f,1.0f },
			{ 0.45f,0.55f,0.45f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// fourth sphere on third column, red material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.5f,0.0f,0.0f,1.0f },
			{ 0.7f,0.6f,0.6f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// fifth sphere on third column, white material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.55f,0.55f,0.55f,1.0f },
			{ 0.70f,0.70f,0.70f,1.0f },
			{ 0.25f * 128.0f }
		},
		{
			// sixth sphere on third column, yellow plastic material
			{ 0.0f,0.0f,0.0f,1.0f },
			{ 0.5f,0.5f,0.0f,1.0f },
			{ 0.60f,0.60f,0.50f,1.0f },
			{ 0.25f * 128.0f }
		}
	},
	{
		{
			// first sphere on fourth column, black material
			{ 0.02f,0.02f,0.02f,1.0f },
			{ 0.01f,0.01f,0.01f,1.0f },
			{ 0.4f,0.4f,0.4f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// second sphere on fourth column, cyan material
			{ 0.0f,0.05f,0.05f,1.0f },
			{ 0.4f,0.5f,0.5f,1.0f },
			{ 0.04f,0.7f,0.7f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// third sphere on fourth column, green material
			{ 0.0f,0.05f,0.0f,1.0f },
			{ 0.4f,0.5f,0.4f,1.0f },
			{ 0.04f,0.7f,0.04f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// fourth sphere on fourth column, red material
			{ 0.05f,0.0f,0.0f,1.0f },
			{ 0.5f,0.4f,0.4f,1.0f },
			{ 0.7f,0.04f,0.04f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// fifth sphere on fourth column, white material
			{ 0.05f,0.05f,0.05f,1.0f },
			{ 0.5f,0.5f,0.5f,1.0f },
			{ 0.7f,0.7f,0.7f,1.0f },
			{ 0.078125f * 128.0f }
		},
		{
			// sixth sphere on fourth column, yellow rubber material
			{ 0.05f,0.05f,0.0f,1.0f },
			{ 0.5f,0.5f,0.4f,1.0f },
			{ 0.7f,0.7f,0.04f,1.0f },
			{ 0.078125f * 128.0f }
		}
	},
};

GLfloat light_model_ambient[] = { 0.2f,0.2f,0.2f,0.0f };
GLfloat light_model_local_viewer[] = { 0.0f };

// light 0 components (red light)
GLfloat light_0_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_0_diffuse[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_0_specular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat light_0_position[] = { 0.0f,0.0f,0.0f,0.0f };

// flag for enabled lighting
bool gbLighting = false;
GLfloat angleWhiteLight = 0.0f;
GLint keyPressed;

// prototype of drawSphere
void drawSphere();

// main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function prototype

	void display(void);
	void initialize(void);
	void uninitialize(void);
	void update(void);

	// Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTR - OGL Using Double Buffer");
	bool bDone = false;
	int iWidth;
	int iHeight;

	// code
	// initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, NULL);
	wndclass.hCursor = LoadCursor(hInstance, NULL);
	wndclass.hIconSm = LoadIcon(hInstance, NULL);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// Registering Class
	RegisterClassEx(&wndclass);

	// Retrive Screen Width and Height
	iWidth = GetSystemMetrics(SM_CXSCREEN);
	iHeight = GetSystemMetrics(SM_CYSCREEN);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("52 - Material"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		((iWidth - WIN_WIDTH) / 2),
		((iHeight - WIN_HEIGHT) / 2),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;

			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				update();
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

// WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Prototype
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		case 0x4C:
			// if gbLighting is true then enable lighting
			// else disable lighting
			if (gbLighting == true)
			{
				glDisable(GL_LIGHTING);
				gbLighting = false;
			}
			else if (gbLighting == false)
			{
				glEnable(GL_LIGHTING);
				gbLighting = true;

				// if lighting enabled then set keyPressed to 0
				// also set light position from postivive z direction to negative z direction
				keyPressed = 0;
				light_0_position[0] = { 0.0f };
				light_0_position[1] = { 0.0f };
				light_0_position[2] = { 1.0f };
				light_0_position[3] = { 0.0f };
				glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
			}
			break;
		case 0x58:
			keyPressed = 1;
			// resest light position
			light_0_position[0] = { 0.0f };
			light_0_position[1] = { 0.0f };
			light_0_position[2] = { 0.0f };
			light_0_position[3] = { 0.0f };
			glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
			break;
		case 0x59:
			keyPressed = 2;
			// resest light position
			light_0_position[0] = { 0.0f };
			light_0_position[1] = { 0.0f };
			light_0_position[2] = { 0.0f };
			light_0_position[3] = { 0.0f };
			glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
			break;
		case 0x5A:
			keyPressed = 3;
			// resest light position
			light_0_position[0] = { 0.0f };
			light_0_position[1] = { 0.0f };
			light_0_position[2] = { 0.0f };
			light_0_position[3] = { 0.0f };
			glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// Variable Declarations
	MONITORINFO mi;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// Function Prototype
	void resize(int, int);

	// Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of pixel format descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// set light source parameters for light 0
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_0_specular);

	// Enable the lighting for light 0
	glEnable(GL_LIGHT0);

	quadric = gluNewQuadric();

	// To avoid aliasing
	glShadeModel(GL_SMOOTH);
	// To avoid distortion
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	// describe light model
	// describe intensity of ambient light
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	// define location of viewer
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	// specify clear values for the color buffers
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();

	// set camera position
	gluLookAt(0, 0, 0.1, 0, 0, 0, 0, 1, 0);

	if (keyPressed == 1)
	{
		glPushMatrix();
		glRotatef(angleWhiteLight, 1, 0, 0);
		light_0_position[1] = angleWhiteLight;
		glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
		glPopMatrix();
	}
	else if (keyPressed == 2)
	{
		glPushMatrix();
		glRotatef(angleWhiteLight, 0, 1, 0);
		light_0_position[0] = angleWhiteLight;
		glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
		glPopMatrix();
	}
	else if (keyPressed == 3)
	{
		glPushMatrix();
		glRotatef(angleWhiteLight, 0, 0, 1);
		light_0_position[0] = angleWhiteLight;
		glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
		glPopMatrix();
	}

	drawSphere();

	glPopMatrix();

	SwapBuffers(ghdc);
}

void drawSphere()
{
	GLfloat xAxis = -1.4f;
	GLfloat yAxis = 1.9f;
	GLfloat zAxis = -6.0f;

	// apply materials to all sphere
	for (GLint i = 0; i < 4; i++)
	{
		for (GLint j = 0; j < 6; j++)
		{
			glPushMatrix();

			glMaterialfv(GL_FRONT, GL_AMBIENT, material_array[i][j][0]);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, material_array[i][j][1]);
			glMaterialfv(GL_FRONT, GL_SPECULAR, material_array[i][j][2]);
			glMaterialfv(GL_FRONT, GL_SHININESS, material_array[i][j][3]);

			glTranslatef(xAxis, yAxis, zAxis);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluSphere(quadric, 0.35, 30, 30);

			glPopMatrix();

			yAxis = yAxis - 0.8f;
		}
		xAxis = xAxis + 0.9f;
		yAxis = 1.9f;
	}

	glPopMatrix();
}

void update(void)
{
	angleWhiteLight = angleWhiteLight + 1.0f;
	if (angleWhiteLight >= 360)
	{
		angleWhiteLight = 0.0f;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}
}