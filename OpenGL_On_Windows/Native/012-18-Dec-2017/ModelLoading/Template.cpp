// Windows Headers
#include <Windows.h>

// Opengl Headers
#include <gl/GL.h>
#include <gl/GLU.h>

// C Headers
#include <stdio.h>
#include <stdlib.h>

// C++ Headers
#include <vector>

// Symbolic Constants
#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE 256	// maximum length of string in mesh file
#define S_EQUAL		0	// return value of strcmp when strings are equal
#define WIN_INIT_X	100	// x coordinates to top left point
#define WIN_INIT_Y	100	// y coordinate to top left point
#define WIN_WIDTH	800	// initial width of window
#define WIN_HEIGHT	600	// initial height of window

#define VK_F		0x46	// virtual key code of capital F key
#define VK_f		0x60	// virtual key code of small f key

#define NR_POINT_COORDS	3	// number of point coordinates
#define NR_TEXTURE_COORDS 2	// number of texture coordinates
#define NR_NORMAL_COORDS 3	// number of normal coordinates
#define NR_FACE_TOKENS 3	// minimum number of entries in face data

#define FOY_ANGLE	45	// field of view in Y direction
#define ZNEAR	0.1	// distance from viewer to near plane of viewing volume
#define ZFAR 200.0	// distance from viewer to far plane of viewing volume

#define VIEWPORT_BOTTOMLEFT_X 0	// X coordinate of bottom left point of viewport rectangle
#define VIEWPORT_BOTTOMLEFT_Y 0	// y coordinate of bottom left point of viewport rectangle

#define MONKEYHEAD_X_TRANSLATE 0.0f	// x translation of monkeyhead
#define MONKEYHEAD_Y_TRANSLATE -0.0f	// y translation of monkeyhead
#define MONKEYHEAD_Z_TRANSLATE -5.0f // z translation of monkeyhead

#define MONKEYHEAD_X_SCALE_FACTOR 1.5f // x scale factor of monkeyhead
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f // y scale factor of monkeyhead
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f	// z scale factor of monkeyhead

#define START_ANGLE_POS	0.0f	// marks begining angle position of rotation
#define END_ANGLE_POS 360.0f	// marks terminating angle position rotation
#define MONKEYHEAD_ANGLE_INCREMENT 0.5f	// increment angle for monkeyhead

// Import libraries
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

// Error handling macros
#define ERRORBOX1(lpszErrorMessage, lpszCaption)	{									\
	MessageBox((HWND)NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR);	\
	ExitProcess(EXIT_FAILURE);															\
}
//
#define ERRORBOX2(hwnd,lpszErrorMessage,lpszCaption)	{								\
	MessageBox((HWND)NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR);	\
	DestroyWindow(hwnd);																\
}

// callback procedure declaration
LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

// global definition
HWND ghwnd = NULL;	// handle to window
HDC ghdc = NULL;	// handle to device context
HGLRC ghrc = NULL;	// handle to opengl rendering context

DWORD dwStyle = NULL;	// Window style
WINDOWPLACEMENT wpPrev;	// structure for holding previous window position

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
bool gbLighting = false;

GLfloat g_rotate;

GLfloat light_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat light_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess[] = { 50.0f };

// vector of vector of floats to hold vertex data
std::vector<std::vector<float>> g_vertices;

// vector of vector of floats to hold texture data
std::vector<std::vector<float>> g_texture;

// vector of vector of floats to hold normal data
std::vector<std::vector<float>> g_normals;

// vector of vector of int to hold index data in g_vertices
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;

// handle to mesh file
FILE *g_fp_meshfile = NULL;

// handle to log file
FILE *g_fp_logfile = NULL;

// hold line in a file
char line[BUFFER_SIZE];

// definition of window entry point function

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	// function prototypes user defined functions called withing WinMain
	void initialize(void);	// initialize opengl state machine
	void update(void);	// update state variables
	void display(void);	// render scene
	void uninitialize(void);	// free/destroy resources

	// Winmain : Local data definitions
	static TCHAR szAppName[] = TEXT("Mesh loading version 2");	// name of window class
	HWND hWnd = NULL;	// Handle to window
	HBRUSH hBrush = NULL;	// handle to background painting brush
	HCURSOR hCursor = NULL;	// handle to cursor
	HICON hIcon = NULL;	// handle to icon
	HICON hIconSm = NULL;	// handle to small icon

	bool bDone = false;	// flag indicating whether or not to exit from game loop

	WNDCLASSEX wndEx;	// strucure holding window class attributes
	MSG msg;	// structure holding message attributes

	// zero out structures
	ZeroMemory((void*)&wndEx, sizeof(WNDCLASSEX));
	ZeroMemory((void*)&msg, sizeof(MSG));

	// acquire following resourcess
	// 1. Black brush
	// 2. Cursor
	// 3. Icon
	// 4. Small Icon

	hBrush = (HBRUSH)GetStockObject(BLACK_BRUSH);
	if (!hBrush)
		ERRORBOX1("Error in getting stock object", "GetStockObject Error");

	hCursor = LoadCursor((HINSTANCE)NULL, IDC_ARROW);
	if (!hCursor)
		ERRORBOX1("Error in loading a cursor", "LoadCursor Error");

	hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if (!hIcon)
		ERRORBOX1("Error in loading icon", "LoadIcon Error");

	hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	if (!hIconSm)
		ERRORBOX1("Error in loading icon", "LoadIcon Error");

	// fill window class structure
	wndEx.cbClsExtra = 0;
	wndEx.cbWndExtra = 0;
	wndEx.cbSize = sizeof(WNDCLASSEX);
	wndEx.hbrBackground = hBrush;
	wndEx.hCursor = hCursor;
	wndEx.hIcon = hIcon;
	wndEx.hIconSm = hIconSm;
	wndEx.hInstance = hInstance;
	wndEx.lpfnWndProc = WndProc;
	wndEx.lpszClassName = szAppName;
	wndEx.lpszMenuName = NULL;
	wndEx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	// registering class
	if (!RegisterClassEx(&wndEx)){}
		//ERRORBOX1("Error in registering a class", "RegisterClassEx Error");


	// create window in memory
		hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		szAppName,
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		WIN_INIT_X,
		WIN_INIT_Y,
		WIN_WIDTH,
		WIN_HEIGHT,
		(HWND)NULL,
		(HMENU)NULL,
		hInstance,
		(LPVOID)NULL
	);

	if (!hWnd)
		ERRORBOX1("Error in creating a window in memory", "CreateWindowEx Error");

	// store handle to window in global handle
	ghwnd = hWnd;

	// Initialize opengl rendering context
	initialize();

	ShowWindow(hWnd, SW_SHOW);
	SetForegroundWindow(hWnd);

	SetFocus(hWnd);

	// Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;

			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				else {
					update();
					display();
				}
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

// WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Prototype
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	// Code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		case 0x4C:
			if (gbLighting == false)
			{
				glEnable(GL_LIGHTING);
				gbLighting = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbLighting = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// Variable Declarations
	MONITORINFO mi;

	// Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle && WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// Function Prototype
	void resize(int, int);
	void uninitialize(void);
	void LoadMeshData(void);

	g_fp_logfile = fopen("MONKEY_HEAD_LOADER.LOG", "w");
	if (!g_fp_logfile)
		uninitialize();

	// Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = -1;

	// Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of pixel format descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);
	glEnable(GL_LIGHT0);

	// read mesh file and load global vectors with appropriate data
	LoadMeshData();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(VIEWPORT_BOTTOMLEFT_X, VIEWPORT_BOTTOMLEFT_Y, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(FOY_ANGLE, (GLfloat)width / (GLfloat)height, ZNEAR, ZFAR);
}

void LoadMeshData(void)
{
	void uninitialize(void);

	g_fp_meshfile = fopen("MonkeyHead.OBJ", "r");
	if (!g_fp_meshfile)
		uninitialize();

	// seperator strings
	char *sep_space = " ";
	char *sep_fslash = "/";
	char *first_token = NULL;
	char *token = NULL;
	char *face_tokens[NR_FACE_TOKENS];
	int nr_tokens;
	char *token_vertex_index = NULL;
	char *token_texture_index = NULL;
	char *token_normal_index = NULL;

	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		first_token = strtok(line, sep_space);

		// If first token indicate vertex data
		if (strcmp(first_token, "v") == S_EQUAL)
		{
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for (int i = 0; i != NR_POINT_COORDS; i++)
				vec_point_coord[i] = atof(strtok(NULL, sep_space));
			g_vertices.push_back(vec_point_coord);
		}

		// If first token indicates texture data
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			g_texture.push_back(vec_texture_coord);
		}

		// If first token indicates normal data
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);
			for (int i = 0; i != NR_NORMAL_COORDS; i++)
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			g_normals.push_back(vec_normal_coord);
		}

		// If fist token indicates face data
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);

			nr_tokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			for (int i = 0; i != NR_FACE_TOKENS; ++i)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		memset((void*)line, (int)'\0', BUFFER_SIZE);
	}
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	fprintf(g_fp_logfile, "g_vertices:%llu g_texture:%llu g_normals:%llu g_face_tri:%llu\n", g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());
}

void update()
{
	// increment monkey head rotation angle
	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;
	// if rotation angle equal or exceeds END_ANGLE_POS then
	// reset to START_ANGLE_POS
	if (g_rotate >= END_ANGLE_POS)
		g_rotate = START_ANGLE_POS;
}

void display(void)
{
	void uninitialize(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);

	// Keep counter-clockwise winding of vertices of geometry
	glFrontFace(GL_CCW);
	// Set polygon mode mentioning front and back faces ang GL_LINE
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	for (int i = 0; i != g_face_tri.size(); ++i)
	{
		glBegin(GL_TRIANGLES);
		for (int j = 0; j != g_face_tri[i].size(); j++)
		{
			int vi = g_face_tri[i][j] - 1;
			int ni = g_face_normals[i][j] - 1;
			glNormal3f(g_normals[ni][0], g_normals[ni][1], g_normals[ni][2]);	// set normal for front face
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]);
		}
		glEnd();
	}
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	fclose(g_fp_logfile);
	g_fp_logfile = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}