#include<Windows.h>
#include<stdio.h>
#include "resource.h";

// Prototype of WndProc() declared globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	// Variable Declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Assignment 1 - Basic handling of messages");

	// CODE
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(hInstance, MAKEINTRESOURCE(MYCURSOR));
	wndclass.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(0, 0, 255));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register above class
	RegisterClassEx(&wndclass);

	// Create Window
	hwnd = CreateWindow(szAppName,
		TEXT("Assignment 1 - Basic handling of messages"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	// Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	TCHAR str[255];

	// code
	switch (iMsg)
	{
	case WM_PAINT:
		//MessageBox(hwnd,TEXT("In WM_PAINT"),TEXT("Message"),MB_OK);
		break;
	case WM_CREATE:
		MessageBox(hwnd, TEXT("In WM_CREATE"), TEXT("Message"), MB_OK);
		break;
	case WM_LBUTTONDOWN:
		swprintf_s(str, TEXT("Mouse clicked at %d,%d"), LOWORD(lParam), HIWORD(lParam));
		MessageBox(hwnd, str, TEXT("In WM_LBUTTONDOWN - Mouse Position"), MB_OK);
		break;
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			MessageBox(hwnd, TEXT("Escape key is pressed"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		case 0x41:
			MessageBox(hwnd, TEXT("A key is pressed"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		case 0x42:
			MessageBox(hwnd, TEXT("B key is pressed"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		case 0x46:
			MessageBox(hwnd, TEXT("F key is pressed"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		case 0x52:
			MessageBox(hwnd, TEXT("L key is pressed"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		case 0x57:
			MessageBox(hwnd, TEXT("Q key is pressed"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		case 0x60:
			MessageBox(hwnd, TEXT("T key is pressed"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		default:
			MessageBox(hwnd, TEXT("Key not supported"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;

	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}