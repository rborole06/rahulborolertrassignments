
==========================================================================================
Programm handles the different types of messages posted to message pool for its own window
==========================================================================================

==========================================================================================
Messages Handled

WM_CREATE

WM_LBUTTONDOWN
Display x and y co-ordinate of mouse where it is clicked

WM_KEYDOWN
Displays which key is pressed from keyboard, only limited keys handled

WM_DESTROY

==========================================================================================