#include<windows.h>
#include<stdio.h>
#include "resource.h";

// global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT giPaintFlag = -1;
// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbFullScreen = false;
HWND ghwnd = NULL;

// main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Assignment 3");

	// code
	// initializing member of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// Registering Class
	RegisterClassEx(&wndclass);

	// Create window
	hwnd = CreateWindow(szClassName,
		TEXT("Assignment 3"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	ShowWindow(hwnd,iCmdShow);
	UpdateWindow(hwnd);

	// Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

// WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	HBRUSH hBrush;

	switch (msg)
	{
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);

		if (giPaintFlag == 1) {
			//SetBkColor(hdc, (COLORREF)CreateSolidBrush(RGB(255, 0, 255)));
			FillRect(hdc, &rc, (HBRUSH)CreateSolidBrush(RGB(0, 0, 255)));
			//RedrawWindow(hwnd,NULL,NULL, RDW_ERASE);
		}
		if (giPaintFlag == 2) {
			FillRect(hdc, &rc, (HBRUSH)CreateSolidBrush(RGB(0, 255, 255)));
		}
		if (giPaintFlag == 3) {
			FillRect(hdc, &rc, (HBRUSH)CreateSolidBrush(RGB(0, 255, 0)));
		}
		if (giPaintFlag == 4) {
			FillRect(hdc, &rc, (HBRUSH)CreateSolidBrush(RGB(0, 0, 0)));
		}
		if (giPaintFlag == 5) {
			FillRect(hdc, &rc, (HBRUSH)CreateSolidBrush(RGB(255, 0, 255)));
		}
		if (giPaintFlag == 6) {
			FillRect(hdc, &rc, (HBRUSH)CreateSolidBrush(RGB(255, 0, 0)));
		}
		if (giPaintFlag == 7) {
			FillRect(hdc, &rc, (HBRUSH)CreateSolidBrush(RGB(255, 255, 0)));
		}

		//DeleteObject(hBrush);
		EndPaint(hwnd,&ps);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		// Case Handling for Blue
		case 0x42:
			giPaintFlag = 1;
			InvalidateRect(hwnd, NULL, TRUE);
			//MessageBox(hwnd, TEXT("B key is pressed end"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;

		// Case Handling for Cyan
		case 0x43:
			giPaintFlag = 2;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		// Case Handling for Green
		case 0x47:
			giPaintFlag = 3;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		// Case Handling for Black (K)
		case 0x4B:
			giPaintFlag = 4;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		// Case Handling for Magenta
		case 0x4D:
			giPaintFlag = 5;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		// Case Handling for Red
		case 0x52:
			giPaintFlag = 6;
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		// Case Handling for Yellow
		case 0x59:
			giPaintFlag = 7;
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		default:
			MessageBox(hwnd, TEXT("Key not supported"), TEXT("In WM_KEYDOWN"), MB_OK);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, msg, wParam, lParam));
}