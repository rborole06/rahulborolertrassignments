
1 - This application shows how to render smiley texture on quad using programmable function pipeline
2 - Application changes the co-ordinate of texture to render on quad on keypress event
3 - On keypress 1, bottom right part of smiley is shown
    On keypress 2, full smiley is shown
    On keypress 3, 4 smiley in repeated manner is shown
    On keypress 4, only yellow color from smiley is shown
