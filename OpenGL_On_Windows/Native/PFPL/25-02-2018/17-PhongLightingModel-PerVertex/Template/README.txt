
1 - This application shows how to render cube using perspective projection in programmable function pipeline
2 - Also application shows effect of light on cube
3 - To see effect of light on cube, press key l
4 - This type is called as Gouraud Shading or Per Vertex Lighting