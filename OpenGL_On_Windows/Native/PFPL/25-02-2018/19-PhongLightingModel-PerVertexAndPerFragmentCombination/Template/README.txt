
1 - This application shows how to render cube with lighting using perspective projection in programmable function pipeline
2 - Also application shows effect of light on cube
3 - To see effect of light on cube, press key l
4 - By default, when lighting will be enabled, cube will have per vertex lighting
5 - Also on key press V or v, cube will have per vertex lighting and on key press F or f, cube will have per fragment lighting