#include <windows.h>
#include <stdio.h>

//  for GLSL extensions IMPORTANT : This line should be before #include<gl\gl.h> and #include<gl\glu.h>
#include <gl\glew.h>

#include <gl\GL.h>
#include "Sphere.h"
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gNumVertices;
GLuint gNumElements;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;
GLuint V_KeyPressed_uniform;

// If 1, then per vertex lighting
// If 0, per fragment lighting
GLuint perVertexLighting = 1;

GLuint La_1_uniform;
GLuint Ld_1_uniform;
GLuint Ls_1_uniform;
GLuint light_1_position_uniform;

GLuint La_2_uniform;
GLuint Ld_2_uniform;
GLuint Ls_2_uniform;
GLuint light_2_position_uniform;

GLuint La_3_uniform;
GLuint Ld_3_uniform;
GLuint Ls_3_uniform;
GLuint light_3_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

bool gbLight = false;

GLfloat light_1_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Diffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Specular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat light_1_Position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat light_2_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_2_Diffuse[] = { 0.0f,1.0f,0.0f,0.0f };
GLfloat light_2_Specular[] = { 0.0f,1.0f,0.0f,0.0f };
GLfloat light_2_Position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat light_3_Ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_3_Diffuse[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_3_Specular[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat light_3_Position[] = { 0.0f,0.0f,0.0f,0.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

// color angles
GLfloat angleRedLight = 0.0f;
GLfloat angleGreenLight = 0.0f;
GLfloat angleBlueLight = 0.0f;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created\nExiting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Light Rotation Using Programmable Function Pipeline"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();
			if (gbLight == true)
			{
				fprintf(gpFile, "In gbLight == true condition.\n");
				update();
			}
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x51:	// for Q or q
			gbEscapeKeyIsPressed = true;
			break;
		case VK_ESCAPE:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C:	// for L or l
			if (gbLight == false)
			{
				gbLight = true;
			}
			else
			{
				gbLight = false;
			}
			break;
		case 0x56:	// for V or v
			perVertexLighting = 1;
			break;
		case 0x46:	// for F or f
			perVertexLighting = 0;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// *** VERTEX SHADER *** //
	// create vertex shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// vPosition - position vertices for sphere
	// vNormal - normal coordinates
	// u_model_matrix - model matrix
	// u_view_matrix - view matrix
	// u_projection_matrix - projection matrix
	// u_lighting_enabled - flag to check if lighting is enabled
	// u_La - Light ambient component
	// u_Ld - light diffuse component
	// u_Ls - light specular component
	// u_light_position - light position coordinates
	// u_Ka - light ambient material component
	// u_Kd - light diffuse material component
	// u_Ks - light specular material component
	// u_material_shininess - material shininess
	// phong_ads_color - phong lighting model ambient diffuse specular color
	// viewer_vector - opposite of where eye is looking
	// ambient(Ia) - La * Ka
	// diffuse(Id) - Ld * Kd * (light direction * normals)
	// specular(Is) - Ls * Ks * pow((reflection,view_vector),power)
	// phong_ads_color(I) - Ia + Id + Is

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform int u_v_key_pressed;"\
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_1_position;" \
		"uniform vec4 u_light_2_position;" \
		"uniform vec4 u_light_3_position;" \
		"uniform int u_lighting_enabled;" \
		"uniform vec3 u_La_1;" \
		"uniform vec3 u_Ld_1;" \
		"uniform vec3 u_Ls_1;" \
		"uniform vec3 u_La_2;" \
		"uniform vec3 u_Ld_2;" \
		"uniform vec3 u_Ls_2;" \
		"uniform vec3 u_La_3;" \
		"uniform vec3 u_Ld_3;" \
		"uniform vec3 u_Ls_3;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_1_direction;" \
		"out vec3 light_2_direction;" \
		"out vec3 light_3_direction;" \
		"out vec3 viewer_vector;" \
		"out vec3 v_phong_ads_color;"\
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled == 1)" \
		"{" \
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"if(u_v_key_pressed == 1)" \
		"{" \
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"vec3 light_1_direction = normalize(vec3(u_light_1_position) - eyeCoordinates.xyz);" \
		"vec3 light_2_direction = normalize(vec3(u_light_2_position) - eyeCoordinates.xyz);" \
		"vec3 light_3_direction = normalize(vec3(u_light_3_position) - eyeCoordinates.xyz);" \
		"float tn1_dot_ld1 = max(dot(transformed_normals,light_1_direction),0.0);" \
		"float tn2_dot_ld2 = max(dot(transformed_normals,light_2_direction),0.0);" \
		"float tn3_dot_ld3 = max(dot(transformed_normals,light_3_direction),0.0);" \
		"vec3 ambient_1 = u_La_1 * u_Ka;" \
		"vec3 ambient_2 = u_La_2 * u_Ka;" \
		"vec3 ambient_3 = u_La_3 * u_Ka;" \
		"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;" \
		"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;" \
		"vec3 diffuse_3 = u_Ld_3 * u_Kd * tn3_dot_ld3;" \
		"vec3 reflection_vector_1 = reflect(-light_1_direction,transformed_normals);" \
		"vec3 reflection_vector_2 = reflect(-light_2_direction,transformed_normals);" \
		"vec3 reflection_vector_3 = reflect(-light_3_direction,transformed_normals);" \
		"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);" \
		"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,viewer_vector),0.0),u_material_shininess);" \
		"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,viewer_vector),0.0),u_material_shininess);" \
		"vec3 specular_3 = u_Ls_3 * u_Ks * pow(max(dot(reflection_vector_3,viewer_vector),0.0),u_material_shininess);" \
		"v_phong_ads_color = ambient_1 + ambient_2 + ambient_3 + diffuse_1 + diffuse_2 + diffuse_3 + specular_1 + specular_2 + specular_3;" \
		"}" \
		"else" \
		"{" \
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_1_direction = vec3(u_light_1_position) - eyeCoordinates.xyz;" \
		"light_2_direction = vec3(u_light_2_position) - eyeCoordinates.xyz;" \
		"light_3_direction = vec3(u_light_3_position) - eyeCoordinates.xyz;" \
		"viewer_vector = -eyeCoordinates.xyz;" \
		"}"\
		"}" \
		"else" \
		"{" \
		"v_phong_ads_color = vec3(1.0,1.0,1.0);" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER *** //
	// create fragment shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec3 v_phong_ads_color;"\
		"in vec3 transformed_normals;" \
		"in vec3 light_1_direction;" \
		"in vec3 light_2_direction;" \
		"in vec3 light_3_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform int u_v_key_pressed;"\
		"uniform vec3 u_La_1;" \
		"uniform vec3 u_Ld_1;" \
		"uniform vec3 u_Ls_1;" \
		"uniform vec3 u_La_2;" \
		"uniform vec3 u_Ld_2;" \
		"uniform vec3 u_Ls_2;" \
		"uniform vec3 u_La_3;" \
		"uniform vec3 u_Ld_3;" \
		"uniform vec3 u_Ls_3;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 f_phong_ads_color;" \
		"if(u_lighting_enabled == 1)" \
		"{" \
		"if(u_v_key_pressed == 0)" \
		"{" \
		"vec3 normalized_transformed_normals = normalize(transformed_normals);" \
		"vec3 normalized_light_1_direction = normalize(light_1_direction);" \
		"vec3 normalized_light_2_direction = normalize(light_2_direction);" \
		"vec3 normalized_light_3_direction = normalize(light_3_direction);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"vec3 ambient_1 = u_La_1 * u_Ka;" \
		"vec3 ambient_2 = u_La_2 * u_Ka;" \
		"vec3 ambient_3 = u_La_3 * u_Ka;" \
		"float tn1_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_1_direction),0.0);" \
		"float tn2_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_2_direction),0.0);" \
		"float tn3_dot_ld3 = max(dot(normalized_transformed_normals,normalized_light_3_direction),0.0);" \
		"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;" \
		"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;" \
		"vec3 diffuse_3 = u_Ld_3 * u_Kd * tn3_dot_ld3;" \
		"vec3 reflection_vector_1 = reflect(-normalized_light_1_direction,normalized_transformed_normals);" \
		"vec3 reflection_vector_2 = reflect(-normalized_light_2_direction,normalized_transformed_normals);" \
		"vec3 reflection_vector_3 = reflect(-normalized_light_3_direction,normalized_transformed_normals);" \
		"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,normalized_viewer_vector),0.0),u_material_shininess);" \
		"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,normalized_viewer_vector),0.0),u_material_shininess);" \
		"vec3 specular_3 = u_Ls_3 * u_Ks * pow(max(dot(reflection_vector_3,normalized_viewer_vector),0.0),u_material_shininess);" \
		"f_phong_ads_color = (ambient_1 + ambient_2 + ambient_3) +  (diffuse_1 + diffuse_2 + diffuse_3) + (specular_1 + specular_2 + specular_3);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(v_phong_ads_color,1.0);" \
		"}" \
		"}"\
		"if (u_v_key_pressed == 0)" \
		"{" \
		"FragColor = vec4(f_phong_ads_color, 1.0);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(v_phong_ads_color, 1.0);" \
		"}" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create program
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

	// pre-link binding of shader program object with vertex shader texture attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform location
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	// L or l key pressed or not
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_v_key_pressed");

	// ambient color intensity of light
	La_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_1");
	// diffuse color intensity of light
	Ld_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_1");
	// specular color intensity of light
	Ls_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_1");
	// position of light
	light_1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_1_position");

	// ambient color intensity of light
	La_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_2");
	// diffuse color intensity of light
	Ld_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_2");
	// specular color intensity of light
	Ls_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_2");
	// position of light
	light_2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_2_position");

	// ambient color intensity of light
	La_3_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_3");
	// diffuse color intensity of light
	Ld_3_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_3");
	// specular color intensity of light
	Ls_3_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_3");
	// position of light
	light_3_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_3_position");

	// amient reflective color intensity of light
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of light
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of light
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material (value is conventionally between 0 to 200)
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	// *** vertices, colors, shader attribs, vbo, vao initialization *** //
	// get sphere vertex data
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// *************************
	// VAO FOR SPHERE
	// *************************

	// generate and bind vao for sphere
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// ******************
	// VBO FOR POSITION
	// ******************
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// ******************
	// VBO FOR NORMAL
	// ******************
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// ******************
	// VBO FOR ELEMENT
	// ******************
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind from vao for sphere
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// set perspective matrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// set camera position
	//gluLookAt(0, 0, 0.1, 0, 0, 0, 0, 1, 0);

	// start using opengl program object
	glUseProgram(gShaderProgramObject);

	// opengl drawing

	if (gbLight == true)
	{
		// set u_lighting_enabled uniform
		glUniform1i(L_KeyPressed_uniform, 1);
		glUniform1i(V_KeyPressed_uniform, perVertexLighting);

		// setting light properties
		glUniform3fv(La_1_uniform, 1, light_1_Ambient);
		glUniform3fv(Ld_1_uniform, 1, light_1_Diffuse);
		glUniform3fv(Ls_1_uniform, 1, light_1_Specular);
		light_1_Position[1] = 100.0f * cos(angleRedLight);
		light_1_Position[2] = 100.0f * sin(angleRedLight);
		glUniform4fv(light_1_position_uniform, 1, light_1_Position);

		glUniform3fv(La_2_uniform, 1, light_2_Ambient);
		glUniform3fv(Ld_2_uniform, 1, light_2_Diffuse);
		glUniform3fv(Ls_2_uniform, 1, light_2_Specular);
		light_2_Position[0] = 100.0f * sin(angleGreenLight);
		light_2_Position[2] = 100.0f * cos(angleGreenLight);
		glUniform4fv(light_2_position_uniform, 1, light_2_Position);

		glUniform3fv(La_3_uniform, 1, light_3_Ambient);
		glUniform3fv(Ld_3_uniform, 1, light_3_Diffuse);
		glUniform3fv(Ls_3_uniform, 1, light_3_Specular);
		light_3_Position[0] = 100.0f * cos(angleBlueLight);
		light_3_Position[1] = 100.0f * sin(angleBlueLight);
		glUniform4fv(light_3_position_uniform, 1, light_3_Position);

		// setting materials properties
		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		// set u_lighting_enabled uniform
		glUniform1i(L_KeyPressed_uniform, 0);
		glUniform1i(V_KeyPressed_uniform, perVertexLighting);
	}

	// Opengl drawing
	// set model, view matrices to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	// apply z axis translation to go deep into the screen by -2.0
	// so that triangle with same fullscrenn co-ordinates, but due to above translation will look small
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// bind to vao of sphere
	glBindVertexArray(gVao_sphere);

	// draw either by glDrawTriangles() or glDrawArraya() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind from vao of sphere
	glBindVertexArray(0);

	// stop using opengl program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{
	angleRedLight = angleRedLight + 0.001f;
	if (angleRedLight >= 360)
	{
		angleRedLight = 0.1f;
	}
	angleGreenLight = angleGreenLight + 0.001f;
	if (angleGreenLight >= 360)
	{
		angleGreenLight = 0.1f;
	}
	angleBlueLight = angleBlueLight + 0.001f;
	if (angleBlueLight >= 360)
	{
		angleBlueLight = 0.01f;
	}
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}
	// destroy position vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	// destroy normal vbo
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	// destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	// deselect rendering context
	wglMakeCurrent(NULL, NULL);

	// delete rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	// delete device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
