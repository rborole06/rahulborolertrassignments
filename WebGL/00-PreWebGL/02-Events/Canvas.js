
// global variables
var canvas = null;
var context = null;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining canvas failed\n");
    else
        console.log("Obtaining canvas succeded\n");

    // print canvas width and height on console
    console.log("Canvas Width : " + console.width + " and Canvas Height : " + console.height);

    // get 2D context
    context = canvas.getContext("2d");
    if (!context)
        console.log("Obtaining 2D context failed\n");
    else
        console.log("Obtaining 2D context succeded\n");

    // fill canvas with black color
    context.fillStyle = "black";
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.textAlign = "center";
    context.textBaseline = "middle";

    // center the text

    // text
    var str = "Hello World !!!";

    // text font
    context.font = "48px sans-serif";

    // text color
    context.fillStyle = "white";

    // display the text in center
    context.fillText(str,canvas.width/2,canvas.height/2);

    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
}

function keyDown(event) {
    alert("Key is pressed");
}

function mouseDown(event) {
    alert("Mouse is clicked");
}