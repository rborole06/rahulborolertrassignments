
// global variables
var canvas = null;
var gl = null;  // webgl context
var bFullscreen = false;
var canvas_origional_width;
var canvas_origional_height;

// to start animation : To have requestAnimationFrame() to be called "cross browser" compatible
var requestAnimationFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

// to stop animation : To have  cancelAnimationFrame() to be called "cross browser" compatible
var cancelAnimattionFrame =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining canvas failed\n");
    else
        console.log("Obtaining canvas succeded\n");

    canvas_origional_width = canvas.width;
    canvas_origional_height = canvas.height;
    
    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize webgl
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen() {
    console.log("in toggle");
    var fullscreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    // if not fullscree
    if (fullscreen_element==null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequtestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen = true;
    } else {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen = false;
    }
}

function init() {
    // get webgl 2.0 context
    gl = canvas.getContext("webgl2");
    if (gl == null) {   // failed to get context
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // set clear color
    gl.clearColor(0.0,0.0,1.0,1.0);
}

function resize() {
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    } else {
        canvas.width = canvas_origional_width;
        canvas.height = canvas_origional_height;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    // animation loop
    requestAnimationFrame(draw, canvas);
}

function keyDown(event) {
    switch (event.keyCode) {
        case 70:    // for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}

function mouseDown(event) {
    alert("Mouse is clicked");
}