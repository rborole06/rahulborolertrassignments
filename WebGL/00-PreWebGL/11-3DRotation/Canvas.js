
// global variables
var canvas = null;
var gl = null;  // webgl context
var bFullscreen = false;
var canvas_origional_width;
var canvas_origional_height;

const WebGLMacros = // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
    {
        VDG_ATTRIBUTE_VERTEX: 0,
        VDG_ATTRIBUTE_COLOR: 1,
        VDG_ATTRIBUTE_NORMAL: 2,
        VDG_ATTRIBUTE_TEXTURE: 3,
    };

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_triangle;
var vao_square;
var vbo_position;
var vbo_color;
var mvpUniform;

var angleTri = 0.0;
var angleSquare = 0.0;

var perspectiveProjectionMatrix;

// to start animation : To have requestAnimationFrame() to be called "cross browser" compatible
var requestAnimationFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

// to stop animation : To have  cancelAnimationFrame() to be called "cross browser" compatible
var cancelAnimattionFrame =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

// onload function
function main() {
    // get canvas element
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining canvas failed\n");
    else
        console.log("Obtaining canvas succeded\n");

    canvas_origional_width = canvas.width;
    canvas_origional_height = canvas.height;

    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize webgl
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen() {
    var fullscreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    // if not fullscree
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequtestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen = true;
    } else {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen = false;
    }
}

function init() {
    // get webgl 2.0 context
    gl = canvas.getContext("webgl2");
    if (gl == null) {   // failed to get context
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
	"in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
	"out vec4 out_color;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
	"out_color = vColor;" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
	"in vec4 out_color;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = out_color;" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // pre-link binding of shader program program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject)
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get MVP uniform location
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    // *** vertices, colors, shader attribs, vbo, vao initializations ***
    var pyramidVertices = new Float32Array([
	// FRONT FACE
	0.0,1.0,0.0,	// apex
	-1.0,-1.0,1.0,	// let-bottom
	1.0,-1.0,1.0,	// right-bottom

	// RIGHT FACE
	0.0,1.0,0.0,
	1.0,-1.0,1.0,
	1.0,-1.0,-1.0,

	// BACK FACE
	0.0,1.0,0.0,
	1.0,-1.0,-1.0,
	-1.0,-1.0,-1.0,

	// LEFT ACE
	0.0,1.0,0.0,
	-1.0,-1.0,-1.0,
	-1.0,-1.0,1.0
    ]);

    var pColorVertices = new Float32Array([
	// FRONT FACE
	1.0,0.0,0.0,
	0.0,1.0,0.0,
	0.0,0.0,1.0,

	// RIGHT FACE
	1.0,0.0,0.0,
	0.0,0.0,1.0,
	0.0,1.0,0.0,

	// BACK FACE
	1.0,0.0,0.0,
	0.0,1.0,0.0,
	0.0,0.0,1.0,

	// LEFT ACE
	1.0,0.0,0.0,
	0.0,0.0,1.0,
	0.0,1.0,0.0,
    ]);

    var cubeVertices = new Float32Array([
	// FRONT FACE
	1.0,1.0,1.0,
	-1.0,1.0,1.0,
	-1.0,-1.0,1.0,
	1.0,-1.0,1.0,

	// RIGHT ACE
	1.0,1.0,-1.0,
	1.0,1.0,1.0,
	1.0,-1.0,1.0,
	1.0,-1.0,-1.0,

	// BACK FACE
	1.0,1.0,-1.0,
	-1.0,1.0,-1.0,
	-1.0,-1.0,-1.0,
	1.0,-1.0,-1.0,

	// LEFT FACE
	-1.0,1.0,1.0,
	-1.0,1.0,-1.0,
	-1.0,-1.0,-1.0,
	-1.0,-1.0,1.0,

	// TOP FACE
	1.0,1.0,-1.0,
	-1.0,1.0,-1.0,
	-1.0,1.0,1.0,
	1.0,1.0,1.0,

	// BOTTOM FACE
	1.0,-1.0,-1.0,
	-1.0,-1.0,-1.0,
	-1.0,-1.0,1.0,
	1.0,-1.0,1.0
    ]);

    var cColorVertices = new Float32Array([
	// FRONT FACE
	0.0,0.0,1.0,
	0.0,0.0,1.0,
	0.0,0.0,1.0,
	0.0,0.0,1.0,

	// RIGHT FACE
	1.0,0.0,1.0,
	1.0,0.0,1.0,
	1.0,0.0,1.0,
	1.0,0.0,1.0,

	// BACK FACE
	0.0,1.0,1.0,
	0.0,1.0,1.0,
	0.0,1.0,1.0,
	0.0,1.0,1.0,

	// LEFT FACE
	1.0,1.0,0.0,
	1.0,1.0,0.0,
	1.0,1.0,0.0,
	1.0,1.0,0.0,

	// TOP FACE
	1.0,0.0,0.0,
	1.0,0.0,0.0,
	1.0,0.0,0.0,
	1.0,0.0,0.0,

	// BOTTOM FACE
	0.0,1.0,0.0,
	0.0,1.0,0.0,
	0.0,1.0,0.0,
	0.0,1.0,0.0
    ]);

    // ****************
    // VAO FOR PYRAMID
    // ****************    
    vao_triangle = gl.createVertexArray();
    gl.bindVertexArray(vao_triangle);

    // ****************
    // VBO FOR VERTICES
    // ****************
    vbo_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // ****************
    // VBO FOR COLOR
    // ****************
    vbo_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color);
    gl.bufferData(gl.ARRAY_BUFFER, pColorVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // ****************
    // VAO FOR CUBE
    // ****************    
    vao_square = gl.createVertexArray();
    gl.bindVertexArray(vao_square);

    // ****************
    // VBO FOR VERTICES
    // ****************
    vbo_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position);
    gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // color for square
    gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR, 0.3960, 0.5764, 0.9607);

    gl.bindVertexArray(null);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    } else {
        canvas.width = canvas_origional_width;
        canvas.height = canvas_origional_height;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    // perspective projection
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    // ****************
    // PYRAMID BLOCK
    // ****************
    var modelViewMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -5.0]);

    var rotationMatrix = mat4.create();
    mat4.rotateY(rotationMatrix, rotationMatrix, angleTri);

    var modelViewProjectionMatrix = mat4.create();

    mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    gl.bindVertexArray(vao_triangle);
    gl.drawArrays(gl.TRIANGLES, 0, 12);
    gl.bindVertexArray(null);

    // ****************
    // CUBE BLOCK
    // ****************
    modelViewMatrix = mat4.create();
    mat4.translate(modelViewMatrix,modelViewMatrix,[1.5,0.0,-5.0]);

    rotationMatrix = mat4.create();
    mat4.rotateX(rotationMatrix,rotationMatrix,angleSquare);

    modelViewProjectionMatrix = mat4.create();

    mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    gl.bindVertexArray(vao_square);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
    gl.bindVertexArray(null);

    gl.useProgram(null);

    update();

    // animation loop
    requestAnimationFrame(draw, canvas);
}

function update()
{
    angleTri = angleTri + 0.01;
    if(angleTri>=360)
    {
	angleTri = 0.0
    }

    angleSquare = angleSquare - 0.01;
    if(angleSquare<=-360)
    {
	angleSquare = 0.0;
    }
}

function uninitialize() {
    if (vao_pyramid) {
        gl.deleteVertexArray(vao_pyramid);
        vao_pyramid = null;
    }

    if (vao_cube) {
        gl.deleteVertexArray(vao_cube);
        vao_cube = null;
    }

    if (vbo_position) {
        gl.deleteBuffer(vbo_position);
        vbo_position = null;
    }

    if (vbo_color) {
        gl.deleteBuffer(vbo_color);
        vbo_color = null;
    }

    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject = null;
        }

        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}

function keyDown(event) {
    switch (event.keyCode) {
        case 27:    // Escape
            // uninitialize
            uninitialize();
            // close our application tab
            window.close(); // may not work in firefox but works in Safari and Chrome
            break;

        case 70:    // for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}

function mouseDown(event) {
    alert("Mouse is clicked");
}