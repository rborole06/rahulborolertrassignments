// global variables
var canvas=null;
var gl=null; // webgl context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var numElement;
var numVertices;

var vao_pyramid;
var vbo_pyramid_position;
var vbo_pyramid_normal;
var vbo_pyramid_element;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var LKeyPressedUniform;

var la_1_Uniform, ld_1_Uniform, ls_1_Uniform, light_1_PositinUnfiorm;
var la_2_Uniform, ld_2_Uniform, ls_2_Uniform, light_2_PositinUnfiorm;

var kaUniform, kdUniform, ksUniform, materialShininessUniform;

var perspectiveProjectionMatrix;

var anglePyramid = 0.0;
var gbAnimate;
var gbLight;

var light_1_ambient = [0.0,0.0,0.0];
var light_1_diffuse = [1.0,0.0,0.0];
var light_1_specular = [1.0,0.0,0.0];
var light_1_position = [2.0,0.8,1.0,0.0];

var light_2_ambient = [0.0,0.0,0.0];
var light_2_diffuse = [0.0,0.0,1.0];
var light_2_specular = [0.0,0.0,1.0];
var light_2_position = [-2.0,0.8,1.0,0.0];

var material_ambient = [0.0,0.0,0.0];
var material_diffuse = [1.0,1.0,1.0];
var material_specular = [1.0,1.0,1.0];
var material_shininess = 50.0;

var gbIsAKeyPressed = false;
var gbLKeyPressed = false;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    // code
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader source code
    var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec3 vNormal;"+
	"uniform mat4 u_model_matrix;"+
	"uniform mat4 u_view_matrix;"+
	"uniform mat4 u_projection_matrix;"+
	"uniform mediump int u_LKeyPressed;"+
	"uniform vec4 u_light_1_position;"+
	"uniform vec4 u_light_2_position;"+
	"out vec3 transformed_normals;"+
	"out vec3 light_1_direction;"+
	"out vec3 light_2_direction;"+
	"out vec3 viewer_vector;"+
	"void main(void)"+
	"{"+
	"if(u_LKeyPressed == 1)"+
	"{"+
	"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"+
	"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
	"light_1_direction = vec3(u_light_1_position) - eye_coordinates.xyz;"+
	"light_2_direction = vec3(u_light_2_position) - eye_coordinates.xyz;"+
	"viewer_vector = -eye_coordinates.xyz;"+
	"}"+
	"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
	"}";

    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 transformed_normals;"+
	"in vec3 light_1_direction;"+
	"in vec3 light_2_direction;"+
	"in vec3 viewer_vector;"+
	"out vec4 FragColor;"+
	"uniform vec3 u_La_1;"+
	"uniform vec3 u_Ld_1;"+
	"uniform vec3 u_Ls_1;"+
	"uniform vec3 u_La_2;"+
	"uniform vec3 u_Ld_2;"+
	"uniform vec3 u_Ls_2;"+
	"uniform vec3 u_Ka;"+
	"uniform vec3 u_Kd;"+
	"uniform vec3 u_Ks;"+
	"uniform float u_material_shininess;"+
	"uniform int u_LKeyPressed;"+
	"void main(void)"+
	"{"+
	"vec3 phong_ads_color;"+
	"if(u_LKeyPressed == 1)"+
	"{"+
	"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
	"vec3 normalized_light_1_direction = normalize(light_1_direction);"+
	"vec3 normalized_light_2_direction = normalize(light_2_direction);"+
	"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
	"vec3 ambient_1 = u_La_1 * u_Ka;"+
	"vec3 ambient_2 = u_La_2 * u_Ka;"+
	"float tn1_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_1_direction),0.0);"+
	"float tn2_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_2_direction),0.0);"+
	"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;"+
	"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;"+
	"vec3 reflection_vector_1 = reflect(-normalized_light_1_direction,normalized_transformed_normals);"+
	"vec3 reflection_vector_2 = reflect(-normalized_light_2_direction,normalized_transformed_normals);"+
	"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,normalized_viewer_vector),0.0),u_material_shininess);"+
	"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,normalized_viewer_vector),0.0),u_material_shininess);"+
	"phong_ads_color = (ambient_1+ambient_2) + (diffuse_1+diffuse_2) + (specular_1+specular_2);"+
	"}"+
	"else"+
	"{"+
	"phong_ads_color = vec3(1.0,1.0,1.0);"+
	"}"+
	"FragColor = vec4(phong_ads_color, 1.0);"+
	"}";

    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    // pre-link binding of shader program object with vertex shader and texture shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get Model matrix uniform location
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
    // get projection matrix uniform location
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
    // get single tap detecting uniform
    LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");

    // ambient color intensity of light
    la_1_Uniform = gl.getUniformLocation(shaderProgramObject, "u_La_1");
    // diffuse color intensity of light
    ld_1_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld_1");
    // specular color intensity of light
    ls_1_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls_1");
    // position of light
    light_1_PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_1_position");

    // ambient color intensity of light
    la_2_Uniform = gl.getUniformLocation(shaderProgramObject, "u_La_2");
    // diffuse color intensity of light
    ld_2_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld_2");
    // specular color intensity of light
    ls_2_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls_2");
    // position of light
    light_2_PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_2_position");

    // ambient reflective color intensity of material
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
    // diffuse reflective color intensity of material
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
    // specular reflective color intensity of material
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
    // shininess of material (value is conventionally between 1 to 200)
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");

    // *** vertices, colors, shader attribs, vbo, vao initializations ***

    var pyramidVertices = new Float32Array([
	// FRONT FACE
	0.0,1.0,0.0,	// apex
	-1.0,-1.0,1.0,	// left-bottom
	1.0,-1.0,1.0,	// right-bottom

	// RIGHT FACE
	0.0,1.0,0.0,
	1.0,-1.0,1.0,
	1.0,-1.0,-1.0,

	// BACK FACE
	0.0,1.0,0.0,
	1.0,-1.0,-1.0,
	-1.0,-1.0,-1.0,

	// LEFT FACE
	0.0,1.0,0.0,
	-1.0,-1.0,-1.0,
	-1.0,-1.0,1.0
    ]);

    var pyramidNormals = new Float32Array([
	0.0,0.447214,0.894427,	// normal for front face
	0.0,0.447214,0.894427,
	0.0,0.447214,0.894427,

	0.894427,0.447214,0.0,	// normal for right face
	0.894427,0.447214,0.0,
	0.894427,0.447214,0.0,

	0.0,0.447214,-0.894427,	// normal for back face
	0.0,0.447214,-0.894427,
	0.0,0.447214,-0.894427,

	-0.894427,0.447214,0.0,	// normal for left face
	-0.894427,0.447214,0.0,
	-0.894427,0.447214,0.0
    ]);

    // ****************
    // VAO FOR PYRAMID
    // ****************    
    vao_pyramid = gl.createVertexArray();
    gl.bindVertexArray(vao_pyramid);

    // ****************
    // VBO FOR VERTICES
    // ****************
    vbo_pyramid_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_position);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // ****************
    // VBO FOR NORMAL
    // ****************
    vbo_pyramid_normal = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_normal);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidNormals, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // Depth test will always be enabled
    gl.enable(gl.DEPTH_TEST);
    // depth test to do
    gl.depthFunc(gl.LEQUAL);
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE);

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // black
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);
    
    if(gbLKeyPressed == true)
    {
	gl.uniform1i(LKeyPressedUniform, 1);

	// setting light properties
	gl.uniform3fv(la_1_Uniform, light_1_ambient);	// ambient intensity of light
	gl.uniform3fv(ld_1_Uniform, light_1_diffuse);	// diffuse intensity of light
	gl.uniform3fv(ls_1_Uniform, light_1_specular);	// specular intensity of light
	gl.uniform4fv(light_1_PositionUniform,light_1_position);	// light position

	gl.uniform3fv(la_2_Uniform, light_2_ambient);	// ambient intensity of light
	gl.uniform3fv(ld_2_Uniform, light_2_diffuse);	// diffuse intensity of light
	gl.uniform3fv(ls_2_Uniform, light_2_specular);	// specular intensity of light
	gl.uniform4fv(light_2_PositionUniform,light_2_position);	// light position

	// setting material properties
	gl.uniform3fv(kaUniform, material_ambient);
	gl.uniform3fv(kdUniform, material_diffuse);
	gl.uniform3fv(ksUniform, material_specular);
	gl.uniform1f(materialShininessUniform, material_shininess);
    }
    else
    {
	gl.uniform1i(LKeyPressedUniform, 0);
    }

    var modelMatrix = mat4.create(); // itself creates identity matrix
    var viewMatrix = mat4.create();

    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-4.0]);

    mat4.rotateY(modelMatrix, modelMatrix, degToRad(anglePyramid));

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);

    gl.bindVertexArray(vao_pyramid);
    gl.drawArrays(gl.TRIANGLES, 0, 12);	// 3(each with its x,y,z) vertices in pyramidVertices array
    gl.bindVertexArray(null);

    if (gbAnimate == true)
    {
	anglePyramid = anglePyramid + 1.0;
	if(anglePyramid >= 360.0)    
	    anglePyramid = anglePyramid - 360.0;
    }

    gl.useProgram(null);

    // animation loop
    requestAnimationFrame(draw, canvas);
}

function uninitialize()
{    
    if(vao_pyramid)
    {
        gl.deleteVertexArray(vao_pyramid);
        vao_pyramid = null;
    }

    if(vbo_pyramid_position)
    {
        gl.deleteBuffer(vbo_pyramid_position);
        vbo_pyramid_position = null;
    }

    if(vbo_pyramid_normal)
    {
        gl.deleteBuffer(vbo_pyramid_normal);
        vbo_pyramid_normal = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 81: // q or Q
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
        case 27: // Escape
            toggleFullScreen();
            break;
        case 65: // for 'A' or 'a'
	    if (gbIsAKeyPressed == false)
	    {
		gbAnimate = true;
		gbIsAKeyPressed = true;
	    }
	    else
	    {
		gbAnimate = false;
		gbIsAKeyPressed = false;
	    }
	    break;
        case 70: // for 'F' or 'f'
	    lightingMode = 2;
	    break;
        case 86: // for 'V' or 'v'
	    lightingMode = 1;
	    break;
	case 76: // for 'L' or 'l'
	    if(gbLKeyPressed == false)
		gbLKeyPressed = true;
	    else
		gbLKeyPressed = false;
	    break;
    }
}

function mouseDown()
{
    // code
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}

