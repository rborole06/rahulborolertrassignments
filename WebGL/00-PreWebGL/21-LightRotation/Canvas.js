// global variables
var canvas=null;
var gl=null; // webgl context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_sphere;
var vbo_sphere_position;
var vbo_sphere_normal;
var vbo_sphere_element;

var light_1_ambient = [0.0,0.0,0.0];
var light_1_diffuse = [1.0,0.0,0.0];
var light_1_specular = [1.0,0.0,0.0];
var light_1_position = [0.0,0.0,0.0,0.0];

var light_2_ambient = [0.0,0.0,0.0];
var light_2_diffuse = [0.0,1.0,0.0];
var light_2_specular = [0.0,1.0,0.0];
var light_2_position = [0.0,0.0,0.0,0.0];

var light_3_ambient = [0.0,0.0,0.0];
var light_3_diffuse = [0.0,0.0,1.0];
var light_3_specular = [0.0,0.0,1.0];
var light_3_position = [0.0,0.0,0.0,0.0];

var material_ambient = [0.0,0.0,0.0];
var material_diffuse = [1.0,1.0,1.0];
var material_specular = [1.0,1.0,1.0];
var material_shininess = 50.0;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;

var la_1_Uniform, ld_1_Uniform, ls_1_Uniform, light_1_PositionUniform;
var la_2_Uniform, ld_2_Uniform, ls_2_Uniform, light_2_PositionUniform;
var la_3_Uniform, ld_3_Uniform, ls_3_Uniform, light_3_PositionUniform;

var kaUniform, kdUniform, ksUniform, materialShininessUniform;

var gbLKeyPressed = false;
var LKeyPressedUniform;
var VKeyPressedUniform;

// If 1, then per vertex lighting
// If 0, per fragment lighting
var lightingMode = 1;

var perspectiveProjectionMatrix;

var angleRedLight = 0.0;
var angleGreenLight = 0.0;
var angleBlueLight = 0.0;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    // code
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader source code
    var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mediump int u_v_key_pressed;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform vec4 u_light_1_position;"+
		"uniform vec4 u_light_2_position;"+
		"uniform vec4 u_light_3_position;"+
		"uniform mediump int u_LKeyPressed;"+
		"uniform vec3 u_La_1;"+
		"uniform vec3 u_Ld_1;"+
		"uniform vec3 u_Ls_1;"+
		"uniform vec3 u_La_2;"+
		"uniform vec3 u_Ld_2;"+
		"uniform vec3 u_Ls_2;"+
		"uniform vec3 u_La_3;"+
		"uniform vec3 u_Ld_3;"+
		"uniform vec3 u_Ls_3;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_1_direction;"+
		"out vec3 light_2_direction;"+
		"out vec3 light_3_direction;"+
		"out vec3 viewer_vector;"+
		"out vec3 v_phong_ads_color;"+
		"void main(void)"+
		"{"+
		"if(u_LKeyPressed == 1)"+
		"{"+
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"+
		"if(u_v_key_pressed == 1)"+
		"{"+
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
		"vec3 light_1_direction = normalize(vec3(u_light_1_position) - eyeCoordinates.xyz);"+
		"vec3 light_2_direction = normalize(vec3(u_light_2_position) - eyeCoordinates.xyz);"+
		"vec3 light_3_direction = normalize(vec3(u_light_3_position) - eyeCoordinates.xyz);"+
		"float tn1_dot_ld1 = max(dot(transformed_normals,light_1_direction),0.0);"+
		"float tn2_dot_ld2 = max(dot(transformed_normals,light_2_direction),0.0);"+
		"float tn3_dot_ld3 = max(dot(transformed_normals,light_3_direction),0.0);"+
		"vec3 ambient_1 = u_La_1 * u_Ka;"+
		"vec3 ambient_2 = u_La_2 * u_Ka;"+
		"vec3 ambient_3 = u_La_3 * u_Ka;"+
		"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;"+
		"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;"+
		"vec3 diffuse_3 = u_Ld_3 * u_Kd * tn3_dot_ld3;"+
		"vec3 reflection_vector_1 = reflect(-light_1_direction,transformed_normals);"+
		"vec3 reflection_vector_2 = reflect(-light_2_direction,transformed_normals);"+
		"vec3 reflection_vector_3 = reflect(-light_3_direction,transformed_normals);"+
		"vec3 viewer_vector = normalize(-eyeCoordinates.xyz);"+
		"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_3 = u_Ls_3 * u_Ks * pow(max(dot(reflection_vector_3,viewer_vector),0.0),u_material_shininess);"+
		"v_phong_ads_color = ambient_1 + ambient_2 + ambient_3 + diffuse_1 + diffuse_2 + diffuse_3 + specular_1 + specular_2 + specular_3;"+
		"}"+
		"else"+
		"{"+
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
		"light_1_direction = vec3(u_light_1_position) - eyeCoordinates.xyz;"+
		"light_2_direction = vec3(u_light_2_position) - eyeCoordinates.xyz;"+
		"light_3_direction = vec3(u_light_3_position) - eyeCoordinates.xyz;"+
		"viewer_vector = -eyeCoordinates.xyz;"+
		"}"+
		"}"+
		"else"+
		"{"+
		"v_phong_ads_color = vec3(1.0,1.0,1.0);"+
		"}"+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}";

    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 v_phong_ads_color;"+
		"in vec3 transformed_normals;"+
		"in vec3 light_1_direction;"+
		"in vec3 light_2_direction;"+
		"in vec3 light_3_direction;"+
		"in vec3 viewer_vector;"+
		"out vec4 FragColor;"+
		"uniform mediump int u_v_key_pressed;"+
		"uniform vec3 u_La_1;"+
		"uniform vec3 u_Ld_1;"+
		"uniform vec3 u_Ls_1;"+
		"uniform vec3 u_La_2;"+
		"uniform vec3 u_Ld_2;"+
		"uniform vec3 u_Ls_2;"+
		"uniform vec3 u_La_3;"+
		"uniform vec3 u_Ld_3;"+
		"uniform vec3 u_Ls_3;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"uniform int u_LKeyPressed;"+
		"void main(void)"+
		"{"+
		"vec3 f_phong_ads_color;"+
		"if(u_LKeyPressed == 1)"+
		"{"+
		"if(u_v_key_pressed == 0)"+
		"{"+
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
		"vec3 normalized_light_1_direction = normalize(light_1_direction);"+
		"vec3 normalized_light_2_direction = normalize(light_2_direction);"+
		"vec3 normalized_light_3_direction = normalize(light_3_direction);"+
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
		"vec3 ambient_1 = u_La_1 * u_Ka;"+
		"vec3 ambient_2 = u_La_2 * u_Ka;"+
		"vec3 ambient_3 = u_La_3 * u_Ka;"+
		"float tn1_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light_1_direction),0.0);"+
		"float tn2_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light_2_direction),0.0);"+
		"float tn3_dot_ld3 = max(dot(normalized_transformed_normals,normalized_light_3_direction),0.0);"+
		"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn1_dot_ld1;"+
		"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn2_dot_ld2;"+
		"vec3 diffuse_3 = u_Ld_3 * u_Kd * tn3_dot_ld3;"+
		"vec3 reflection_vector_1 = reflect(-normalized_light_1_direction,normalized_transformed_normals);"+
		"vec3 reflection_vector_2 = reflect(-normalized_light_2_direction,normalized_transformed_normals);"+
		"vec3 reflection_vector_3 = reflect(-normalized_light_3_direction,normalized_transformed_normals);"+
		"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1,normalized_viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2,normalized_viewer_vector),0.0),u_material_shininess);"+
		"vec3 specular_3 = u_Ls_3 * u_Ks * pow(max(dot(reflection_vector_3,normalized_viewer_vector),0.0),u_material_shininess);"+
		"f_phong_ads_color = ambient_1 + ambient_2 + ambient_3 + diffuse_1 + diffuse_2 + diffuse_3 + specular_1 + specular_2 + specular_3;"+
		"}"+
		"else"+
		"{"+
		"FragColor = vec4(v_phong_ads_color,1.0);"+
		"}"+
		"}"+
		"if(u_v_key_pressed == 0)"+
		"{"+
		"FragColor = vec4(f_phong_ads_color,1.0);"+
		"}"+
		"else"+
		"{"+
		"FragColor = vec4(v_phong_ads_color,1.0);"+
		"}"+
		"}";

    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    // pre-link binding of shader program object with vertex shader and texture shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get Model matrix uniform location
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
    // get projection matrix uniform location
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
    // get lighting key pressed uniform
    LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
    // get v key pressed uniform location
    VKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_v_key_pressed");

    // ambient color intensity of light
    la_1_Uniform = gl.getUniformLocation(shaderProgramObject, "u_La_1");
    // diffuse color intensity of light
    ld_1_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld_1");
    // specular color intensity of light
    ls_1_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls_1");
    // position of light
    light_1_PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_1_position");

    // ambient color intensity of light
    la_2_Uniform = gl.getUniformLocation(shaderProgramObject, "u_La_2");
    // diffuse color intensity of light
    ld_2_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld_2");
    // specular color intensity of light
    ls_2_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls_2");
    // position of light
    light_2_PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_2_position");

    // ambient color intensity of light
    la_3_Uniform = gl.getUniformLocation(shaderProgramObject, "u_La_3");
    // diffuse color intensity of light
    ld_3_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld_3");
    // specular color intensity of light
    ls_3_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls_3");
    // position of light
    light_3_PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_3_position");

    // ambient reflective color intensity of material
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
    // diffuse reflective color intensity of material
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
    // specular reflective color intensity of material
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
    // shininess of material (value is conventionally between 1 to 200)
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");

    // *** vertices, colors, shader attribs, vbo, vao initializations ***

    sphere = new Mesh();
    makeSphere(sphere,2.0,30,30);

    // Depth test will always be enabled
    gl.enable(gl.DEPTH_TEST);
    // depth test to do
    gl.depthFunc(gl.LEQUAL);
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE);

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // black
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
    
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    if(gbLKeyPressed == true)
    {
	gl.uniform1i(LKeyPressedUniform, 1);
	gl.uniform1i(VKeyPressedUniform, lightingMode);

	// setting light properties
	gl.uniform3fv(la_1_Uniform, light_1_ambient);	// ambient intensity of light
	gl.uniform3fv(ld_1_Uniform, light_1_diffuse);	// diffuse intensity of light
	gl.uniform3fv(ls_1_Uniform, light_1_specular);	// specular intensity of light
	light_1_position[1] = 100.0 * parseFloat(Math.cos(angleRedLight));
	light_1_position[2] = 100.0 * parseFloat(Math.sin(angleRedLight));
	gl.uniform4fv(light_1_PositionUniform,light_1_position);	// light position

	gl.uniform3fv(la_2_Uniform, light_2_ambient);	// ambient intensity of light
	gl.uniform3fv(ld_2_Uniform, light_2_diffuse);	// diffuse intensity of light
	gl.uniform3fv(ls_2_Uniform, light_2_specular);	// specular intensity of light
	light_2_position[0] = 100.0 * Math.sin(angleGreenLight);
	light_2_position[2] = 100.0 * Math.cos(angleGreenLight);
	gl.uniform4fv(light_2_PositionUniform,light_2_position);	// light position

	gl.uniform3fv(la_3_Uniform, light_3_ambient);	// ambient intensity of light
	gl.uniform3fv(ld_3_Uniform, light_3_diffuse);	// diffuse intensity of light
	gl.uniform3fv(ls_3_Uniform, light_3_specular);	// specular intensity of light
	light_3_position[0] = 100.0 * parseFloat(Math.cos(angleBlueLight));
	light_3_position[1] = 100.0 * parseFloat(Math.sin(angleBlueLight));
	gl.uniform4fv(light_3_PositionUniform,light_3_position);	// light position

	// setting material properties
	gl.uniform3fv(kaUniform, material_ambient);
	gl.uniform3fv(kdUniform, material_diffuse);
	gl.uniform3fv(ksUniform, material_specular);
	gl.uniform1f(materialShininessUniform, material_shininess);
    }
    else
    {
	gl.uniform1i(LKeyPressedUniform, 0);
	gl.uniform1i(VKeyPressedUniform, lightingMode);
    }

    var modelMatrix = mat4.create(); // itself creates identity matrix
    var viewMatrix = mat4.create();

    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);

    sphere.draw();

    angleRedLight = angleRedLight + 0.01;
    if (angleRedLight >= 360)
    {
	angleRedLight = 0.0;
    }
    angleGreenLight = angleGreenLight + 0.01;
    if (angleGreenLight >= 360)
    {
	angleGreenLight = 0.0;
    }
    angleBlueLight = angleBlueLight + 0.01;
    if (angleBlueLight >= 360)
    {
	angleBlueLight = 0.0;
    }

    gl.useProgram(null);

    // animation loop
    requestAnimationFrame(draw, canvas);
}

function uninitialize()
{    
    if(vao_sphere)
    {
        gl.deleteVertexArray(vao_sphere);
        vao_sphere = null;
    }

    if(vbo_sphere_position)
    {
        gl.deleteBuffer(vbo_sphere_position);
        vbo_sphere_position = null;
    }

    if(vbo_sphere_normal)
    {
        gl.deleteBuffer(vbo_sphere_normal);
        vbo_sphere_normal = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 81: // q or Q
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
        case 27: // Escape
            toggleFullScreen();	
            break;
        case 70: // for 'F' or 'f'
	    lightingMode = 0;
	    break;
        case 86: // for 'V' or 'v'
	    lightingMode = 1;
	    break;
	case 76: // for 'L' or 'l'
	    if(gbLKeyPressed == false)
		gbLKeyPressed = true;
	    else
		gbLKeyPressed = false;
	    break;
    }
}

function mouseDown()
{
    // code
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}

