
1 - This application shows how to render cube with 3 rotating lights in 3 x,y,z axis using WebGL
2 - To enable lighting, Press L
3 - To enable Per Vertex Lighting, Press V
4 - To enable Per Fragment Lighting, Press F
5 - By default lighting mode is Per Vertex Lighting
