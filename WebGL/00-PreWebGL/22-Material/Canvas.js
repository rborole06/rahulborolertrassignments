// global variables
var canvas=null;
var gl=null; // webgl context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const', all inside it are automatically 'const'
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_sphere;
var vbo_sphere_position;
var vbo_sphere_normal;
var vbo_sphere_element;

var light_ambient = [0.0,0.0,0.0];
var light_diffuse = [1.0,1.0,1.0];
var light_specular = [1.0,1.0,1.0];
var light_position = [0.0,0.0,0.0,0.0];

var material_1_1_ambient = [0.0215,0.1745,0.0215];
var material_1_1_diffuse = [0.07568,0.61424,0.07568];
var material_1_1_specular = [0.633,0.727811,0.633];
var material_1_1_shininess = 0.6 * 128.0;

var material_1_2_ambient = [0.135,0.2225,0.1575];
var material_1_2_diffuse = [0.54,0.89,0.63];
var material_1_2_specular = [0.316228,0.316228,0.316228];
var material_1_2_shininess = 0.1 * 128.0;

var material_1_3_ambient = [0.05375,0.05,0.06625];
var material_1_3_diffuse = [0.18275,0.17,0.22525];
var material_1_3_specular = [0.332741,0.328634,0.346435];
var material_1_3_shininess = 0.3 * 128.0;

var material_1_4_ambient = [0.25,0.20725,0.20725];
var material_1_4_diffuse = [1.0,0.829,0.829];
var material_1_4_specular = [0.296648,0.296648,0.296648];
var material_1_4_shininess = 0.088 * 128.0;

var material_1_5_ambient = [0.1745,0.01175,0.01175];
var material_1_5_diffuse = [0.61424,0.04136,0.04136];
var material_1_5_specular = [0.727811,0.626959,0.626959];
var material_1_5_shininess = 0.6 * 128.0;

var material_1_6_ambient = [0.1,0.18725,0.1745];
var material_1_6_diffuse = [0.396,0.74151,0.69102];
var material_1_6_specular = [0.297254,0.30829,0.306678];
var material_1_6_shininess = 0.6 * 128.0;

var material_2_1_ambient = [0.329412,0.223529,0.027451];
var material_2_1_diffuse = [0.780392,0.568627,0.113725];
var material_2_1_specular = [0.992157,0.941176,0.807843];
var material_2_1_shininess = 0.21794872 * 128.0;

var material_2_2_ambient = [0.2125,0.1275,0.054];
var material_2_2_diffuse = [0.714,0.4284,0.18144];
var material_2_2_specular = [0.393548,0.271906,0.166721];
var material_2_2_shininess = 0.2 * 128.0;

var material_2_3_ambient = [0.25,0.25,0.25];
var material_2_3_diffuse = [0.4,0.4,0.4];
var material_2_3_specular = [0.774597,0.774597,0.774597];
var material_2_3_shininess = 0.6 * 128.0;

var material_2_4_ambient = [0.19125,0.0735,0.0225];
var material_2_4_diffuse = [0.7038,0.27048,0.0828];
var material_2_4_specular = [0.256777,0.137622,0.086014];
var material_2_4_shininess = 0.1 * 128.0;

var material_2_5_ambient = [0.24725,0.1995,0.0745];
var material_2_5_diffuse = [0.75164,0.60648,0.22648];
var material_2_5_specular = [0.628281,0.555802,0.366065];
var material_2_5_shininess = 0.4 * 128.0;

var material_2_6_ambient = [0.19225,0.19225,0.19225];
var material_2_6_diffuse = [0.50754,0.50754,0.50754];
var material_2_6_specular = [0.508273,0.508273,0.508273];
var material_2_6_shininess = 0.4 * 128.0;

var material_3_1_ambient = [0.0,0.0,0.0];
var material_3_1_diffuse = [0.01,0.01,0.01];
var material_3_1_specular = [0.50,0.50,0.50];
var material_3_1_shininess = 0.25 * 128.0;

var material_3_2_ambient = [0.0,0.1,0.06];
var material_3_2_diffuse = [0.0,0.50980392,0.50980392];
var material_3_2_specular = [0.50196078,0.50196078,0.50196078];
var material_3_2_shininess = 0.25 * 128.0;

var material_3_3_ambient = [0.0,0.0,0.0];
var material_3_3_diffuse = [0.1,0.35,0.1];
var material_3_3_specular = [0.45,0.55,0.45];
var material_3_3_shininess = 0.25 * 128.0;

var material_3_4_ambient = [0.0,0.0,0.0];
var material_3_4_diffuse = [0.5,0.0,0.0];
var material_3_4_specular = [0.7,0.6,0.6];
var material_3_4_shininess = 0.25 * 128.0;

var material_3_5_ambient = [0.0,0.0,0.0];
var material_3_5_diffuse = [0.55,0.55,0.55];
var material_3_5_specular = [0.70,0.70,0.70];
var material_3_5_shininess = 0.25 * 128.0;

var material_3_6_ambient = [0.0,0.0,0.0];
var material_3_6_diffuse = [0.5,0.5,0.0];
var material_3_6_specular = [0.60,0.60,0.50];
var material_3_6_shininess = 0.25 * 128.0;

var material_4_1_ambient = [0.02,0.02,0.02];
var material_4_1_diffuse = [0.01,0.01,0.01];
var material_4_1_specular = [0.4,0.4,0.4];
var material_4_1_shininess = 0.078125 * 128.0;

var material_4_2_ambient = [0.0,0.05,0.05];
var material_4_2_diffuse = [0.4,0.5,0.5];
var material_4_2_specular = [0.04,0.7,0.7];
var material_4_2_shininess = 0.078125 * 128.0;

var material_4_3_ambient = [0.0,0.05,0.0];
var material_4_3_diffuse = [0.4,0.5,0.4];
var material_4_3_specular = [0.04,0.7,0.04];
var material_4_3_shininess = 0.078125 * 128.0;

var material_4_4_ambient = [0.05,0.0,0.0];
var material_4_4_diffuse = [0.5,0.4,0.4];
var material_4_4_specular = [0.7,0.04,0.04];
var material_4_4_shininess = 0.078125 * 128.0;

var material_4_5_ambient = [0.05,0.05,0.05];
var material_4_5_diffuse = [0.5,0.5,0.5];
var material_4_5_specular = [0.7,0.7,0.7];
var material_4_5_shininess = 0.078125 * 128.0;

var material_4_6_ambient = [0.05,0.05,0.0];
var material_4_6_diffuse = [0.5,0.5,0.4];
var material_4_6_specular = [0.7,0.7,0.04];
var material_4_6_shininess = 0.078125 * 128.0;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;

var la_Uniform, ld_Uniform, ls_Uniform, light_PositionUniform;
var kaUniform, kdUniform, ksUniform, materialShininessUniform;

var LKeyPressedUniform;

var perspectiveProjectionMatrix;

var rotationAxis = 'x';
var gbLKeyPressed = false;

var surfaceWidth;
var surfaceHeight;
var viewportX;
var viewportY;
var viewportWidth;
var viewportHeight;

var angleWhiteLight = 0.0;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get <canvas> element
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();
    
    // start drawing here as warming-up
    resize(0, 0, canvas_original_width, canvas_original_height);
    draw();
}

function toggleFullScreen()
{
    // code
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if(gl==null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    // initialize surface width and height
    surfaceWidth = canvas.width;
    surfaceHeight = canvas.height;

    // initialize viewport width and height
    viewportWidth = surfaceWidth / 4;		// width of viewport
    viewportHeight = surfaceHeight / 6;		// height of viewport

    // vertex shader source code
    var vertexShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"in vec4 vPosition;"+
		"in vec3 vNormal;"+
		"uniform mat4 u_model_matrix;"+
		"uniform mat4 u_view_matrix;"+
		"uniform mat4 u_projection_matrix;"+
		"uniform mediump int u_LKeyPressed;"+
		"uniform vec4 u_light_position;"+
		"out vec3 transformed_normals;"+
		"out vec3 light_direction;"+
		"out vec3 viewer_vector;"+
		"void main(void)"+
		"{"+
		"if(u_LKeyPressed == 1)"+
		"{"+
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"+
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"+
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
		"viewer_vector = -eye_coordinates.xyz;"+
		"}"+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
		"}";

    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    var fragmentShaderSourceCode=
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec3 transformed_normals;"+
		"in vec3 light_direction;"+
		"in vec3 viewer_vector;"+
		"out vec4 FragColor;"+
		"uniform vec3 u_La;"+
		"uniform vec3 u_Ld;"+
		"uniform vec3 u_Ls;"+
		"uniform vec3 u_Ka;"+
		"uniform vec3 u_Kd;"+
		"uniform vec3 u_Ks;"+
		"uniform float u_material_shininess;"+
		"uniform mediump int u_LKeyPressed;"+
		"void main(void)"+
		"{"+
		"vec3 phong_ads_color;"+
		"if(u_LKeyPressed == 1)"+
		"{"+
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
		"vec3 normalized_light_direction = normalize(light_direction);"+
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
		"vec3 ambient = u_La * u_Ka;"+
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"+
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"+
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"+
		"phong_ads_color = ambient + diffuse + specular;"+
		"}"+
		"else"+
		"{"+
		"phong_ads_color = vec3(1.0,1.0,1.0);"+
		"}"+
		"FragColor = vec4(phong_ads_color, 1.0);"+
		"}";

    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);

    // pre-link binding of shader program object with vertex shader and texture shader attributes
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // get Model matrix uniform location
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
    // get View Matrix uniform location
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
    // get projection matrix uniform location
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
    // get lighting key pressed uniform
    LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");

    // ambient color intensity of light
    la_Uniform = gl.getUniformLocation(shaderProgramObject, "u_La");
    // diffuse color intensity of light
    ld_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
    // specular color intensity of light
    ls_Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls");
    // position of light
    light_PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");

    // ambient reflective color intensity of material
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
    // diffuse reflective color intensity of material
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
    // specular reflective color intensity of material
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
    // shininess of material (value is conventionally between 1 to 200)
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");

    // *** vertices, colors, shader attribs, vbo, vao initializations ***

    sphere = new Mesh();
    makeSphere(sphere,2.0,30,30);

    // Depth test will always be enabled
    gl.enable(gl.DEPTH_TEST);
    // depth test to do
    gl.depthFunc(gl.LEQUAL);
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE);

    // set clear color
    gl.clearColor(0.25, 0.25, 0.25, 1.0); // black
    
    // initialize projection matrix
    perspectiveProjectionMatrix=mat4.create();
}

function resize(x, y, width, height)
{

    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

	// reset surface width and height
	surfaceWidth = window.innerWidth;
	surfaceHeight = window.innerHeight;

	// reset viewport width and height
	viewportWidth = surfaceWidth / 4;		// width of viewport
	viewportHeight = surfaceHeight / 6;		// height of viewport

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;

	// reset surface width and height
	surfaceWidth = canvas_original_width;
	surfaceHeight = canvas_original_height;

	// reset viewport width and height
	viewportWidth = surfaceWidth / 4;		// width of viewport
	viewportHeight = surfaceHeight / 6;		// height of viewport
    }
   
    // set the viewport to match
    gl.viewport(x, y, width, height);
    
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(width)/parseFloat(height), 0.1, 100.0);
}

function draw()
{

    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    if(gbLKeyPressed == true)
    {
	gl.uniform1i(LKeyPressedUniform, 1);

	// setting light properties
	gl.uniform3fv(la_Uniform, light_ambient);	// ambient intensity of light
	gl.uniform3fv(ld_Uniform, light_diffuse);	// diffuse intensity of light
	gl.uniform3fv(ls_Uniform, light_specular);	// specular intensity of light

	if(rotationAxis == 'x')
	{
	    light_position[1] = 100.0 * Math.cos(angleWhiteLight);
	    light_position[2] = 100.0 * Math.sin(angleWhiteLight);
	    gl.uniform4fv(light_PositionUniform,light_position);	// light position
	}
	else if(rotationAxis == 'y')
	{
	    light_position[0] = 100.0 * Math.cos(angleWhiteLight);
	    light_position[2] = 100.0 * Math.sin(angleWhiteLight);
	    gl.uniform4fv(light_PositionUniform,light_position);	// light position
	}
	else if(rotationAxis == 'z')
	{
	    light_position[0] = 100.0 * Math.cos(angleWhiteLight);
	    light_position[1] = 100.0 * Math.sin(angleWhiteLight);
	    gl.uniform4fv(light_PositionUniform,light_position);	// light position
	}

    }
    else
    {
	gl.uniform1i(LKeyPressedUniform, 0);
    }

    var modelMatrix = mat4.create(); // itself creates identity matrix
    var viewMatrix = mat4.create();

    gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);

    // ****************************************************
    // SPHERE BLOCK
    // ****************************************************

    // *******************
    // FIRST COLUMN SPHERE
    // *******************

    // DRAW FIRST COLUMN, FIRST SPHERE
    viewportX = surfaceWidth - (viewportWidth*4);	// x axis starting position for viewport
    viewportY = surfaceHeight - (viewportHeight * 1);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_1_1_ambient);
    gl.uniform3fv(kdUniform, material_1_1_diffuse);
    gl.uniform3fv(ksUniform, material_1_1_specular);
    gl.uniform1f(materialShininessUniform, material_1_1_shininess);

    sphere.draw();

    // DRAW FIRST COLUMN, SECOND SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 2);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_1_2_ambient);
    gl.uniform3fv(kdUniform, material_1_2_diffuse);
    gl.uniform3fv(ksUniform, material_1_2_specular);
    gl.uniform1f(materialShininessUniform, material_1_2_shininess);

    sphere.draw();

    // DRAW FIRST COLUMN, THIRD SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 3);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_1_3_ambient);
    gl.uniform3fv(kdUniform, material_1_3_diffuse);
    gl.uniform3fv(ksUniform, material_1_3_specular);
    gl.uniform1f(materialShininessUniform, material_1_3_shininess);

    sphere.draw();

    // DRAW FIRST COLUMN, FOURTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 4);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_1_4_ambient);
    gl.uniform3fv(kdUniform, material_1_4_diffuse);
    gl.uniform3fv(ksUniform, material_1_4_specular);
    gl.uniform1f(materialShininessUniform, material_1_4_shininess);

    sphere.draw();

    // DRAW FIRST COLUMN, FIFTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 5);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_1_5_ambient);
    gl.uniform3fv(kdUniform, material_1_5_diffuse);
    gl.uniform3fv(ksUniform, material_1_5_specular);
    gl.uniform1f(materialShininessUniform, material_1_5_shininess);

    sphere.draw();

    // DRAW FIRST COLUMN, SIXTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 6);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_1_6_ambient);
    gl.uniform3fv(kdUniform, material_1_6_diffuse);
    gl.uniform3fv(ksUniform, material_1_6_specular);
    gl.uniform1f(materialShininessUniform, material_1_6_shininess);

    sphere.draw();

    // *******************
    // SECOND COLUMN SPHERE
    // *******************

    // DRAW SECOND COLUMN, FIRST SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportX = surfaceWidth - (viewportWidth * 3);		// x axis starting position for viewport
    viewportY = surfaceHeight - (viewportHeight * 1);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_2_1_ambient);
    gl.uniform3fv(kdUniform, material_2_1_diffuse);
    gl.uniform3fv(ksUniform, material_2_1_specular);
    gl.uniform1f(materialShininessUniform, material_2_1_shininess);

    sphere.draw();

    // DRAW SECOND COLUMN, SECOND SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 2);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_2_2_ambient);
    gl.uniform3fv(kdUniform, material_2_2_diffuse);
    gl.uniform3fv(ksUniform, material_2_2_specular);
    gl.uniform1f(materialShininessUniform, material_2_2_shininess);

    sphere.draw();

    // DRAW SECOND COLUMN, THIRD SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 3);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_2_3_ambient);
    gl.uniform3fv(kdUniform, material_2_3_diffuse);
    gl.uniform3fv(ksUniform, material_2_3_specular);
    gl.uniform1f(materialShininessUniform, material_2_3_shininess);

    sphere.draw();

    // DRAW SECOND COLUMN, FOURTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 4);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_2_4_ambient);
    gl.uniform3fv(kdUniform, material_2_4_diffuse);
    gl.uniform3fv(ksUniform, material_2_4_specular);
    gl.uniform1f(materialShininessUniform, material_2_4_shininess);

    sphere.draw();

    // DRAW SECOND COLUMN, FIFTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 5);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_2_5_ambient);
    gl.uniform3fv(kdUniform, material_2_5_diffuse);
    gl.uniform3fv(ksUniform, material_2_5_specular);
    gl.uniform1f(materialShininessUniform, material_2_5_shininess);

    sphere.draw();

    // DRAW SECOND COLUMN, SIXTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 6);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_2_6_ambient);
    gl.uniform3fv(kdUniform, material_2_6_diffuse);
    gl.uniform3fv(ksUniform, material_2_6_specular);
    gl.uniform1f(materialShininessUniform, material_2_6_shininess);

    sphere.draw();

    // *******************
    // THIRD COLUMN SPHERE
    // *******************

    // DRAW THIRD COLUMN, FIRST SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportX = surfaceWidth - (viewportWidth * 2);		// x axis starting position for viewport
    viewportY = surfaceHeight - (viewportHeight * 1);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_3_1_ambient);
    gl.uniform3fv(kdUniform, material_3_1_diffuse);
    gl.uniform3fv(ksUniform, material_3_1_specular);
    gl.uniform1f(materialShininessUniform, material_3_1_shininess);

    sphere.draw();

    // DRAW THIRD COLUMN, SECOND SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 2);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_3_2_ambient);
    gl.uniform3fv(kdUniform, material_3_2_diffuse);
    gl.uniform3fv(ksUniform, material_3_2_specular);
    gl.uniform1f(materialShininessUniform, material_3_2_shininess);

    sphere.draw();

    // DRAW THIRD COLUMN, THIRD SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 3);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_3_3_ambient);
    gl.uniform3fv(kdUniform, material_3_3_diffuse);
    gl.uniform3fv(ksUniform, material_3_3_specular);
    gl.uniform1f(materialShininessUniform, material_3_3_shininess);

    sphere.draw();

    // DRAW THIRD COLUMN, FOURTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 4);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_3_4_ambient);
    gl.uniform3fv(kdUniform, material_3_4_diffuse);
    gl.uniform3fv(ksUniform, material_3_4_specular);
    gl.uniform1f(materialShininessUniform, material_3_4_shininess);

    sphere.draw();

    // DRAW THIRD COLUMN, FIFTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 5);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_3_5_ambient);
    gl.uniform3fv(kdUniform, material_3_5_diffuse);
    gl.uniform3fv(ksUniform, material_3_5_specular);
    gl.uniform1f(materialShininessUniform, material_3_5_shininess);

    sphere.draw();

    // DRAW THIRD COLUMN, SIXTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 6);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_3_6_ambient);
    gl.uniform3fv(kdUniform, material_3_6_diffuse);
    gl.uniform3fv(ksUniform, material_3_6_specular);
    gl.uniform1f(materialShininessUniform, material_3_6_shininess);

    sphere.draw();

    // *******************
    // FOURTH COLUMN SPHERE
    // *******************

    // DRAW FOURTH COLUMN, FIRST SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportX = surfaceWidth - (viewportWidth * 1);		// x axis starting position for viewport
    viewportY = surfaceHeight - (viewportHeight * 1);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_4_1_ambient);
    gl.uniform3fv(kdUniform, material_4_1_diffuse);
    gl.uniform3fv(ksUniform, material_4_1_specular);
    gl.uniform1f(materialShininessUniform, material_4_1_shininess);

    sphere.draw();

    // DRAW FOURTH COLUMN, SECOND SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 2);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_4_2_ambient);
    gl.uniform3fv(kdUniform, material_4_2_diffuse);
    gl.uniform3fv(ksUniform, material_4_2_specular);
    gl.uniform1f(materialShininessUniform, material_4_2_shininess);

    sphere.draw();

    // DRAW FOURTH COLUMN, THIRD SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 3);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_4_3_ambient);
    gl.uniform3fv(kdUniform, material_4_3_diffuse);
    gl.uniform3fv(ksUniform, material_4_3_specular);
    gl.uniform1f(materialShininessUniform, material_4_3_shininess);

    sphere.draw();

    // DRAW FOURTH COLUMN, FOURTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 4);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_4_4_ambient);
    gl.uniform3fv(kdUniform, material_4_4_diffuse);
    gl.uniform3fv(ksUniform, material_4_4_specular);
    gl.uniform1f(materialShininessUniform, material_4_4_shininess);

    sphere.draw();

    // DRAW FOURTH COLUMN, FiFTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 5);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_4_5_ambient);
    gl.uniform3fv(kdUniform, material_4_5_diffuse);
    gl.uniform3fv(ksUniform, material_4_5_specular);
    gl.uniform1f(materialShininessUniform, material_4_5_shininess);

    sphere.draw();

    // DRAW FOURTH COLUMN, SIXTH SPHERE

    modelMatrix = mat4.create(); // set model matrix to identity

    viewportY = surfaceHeight - (viewportHeight * 6);	// y axis starting position for viewport

    resize(viewportX, viewportY, viewportWidth, viewportHeight);	// set viewport

    // apply z axis transition to go deep into the screen by -8.0
    // so that pyramid with same fullscreen co-ordinates, but due to above transition will look small
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);

    gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);

    // setting material properties
    gl.uniform3fv(kaUniform, material_4_6_ambient);
    gl.uniform3fv(kdUniform, material_4_6_diffuse);
    gl.uniform3fv(ksUniform, material_4_6_specular);
    gl.uniform1f(materialShininessUniform, material_4_6_shininess);

    sphere.draw();

    gl.useProgram(null);

    angleWhiteLight = angleWhiteLight + 0.01;
    if (angleWhiteLight  >= 360)
    {
	angleWhiteLight  = 0.0;
    }

    // animation loop
    requestAnimationFrame(draw, canvas);
}

function uninitialize()
{    
    if(vao_sphere)
    {
        gl.deleteVertexArray(vao_sphere);
        vao_sphere = null;
    }

    if(vbo_sphere_position)
    {
        gl.deleteBuffer(vbo_sphere_position);
        vbo_sphere_position = null;
    }

    if(vbo_sphere_normal)
    {
        gl.deleteBuffer(vbo_sphere_normal);
        vbo_sphere_normal = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 27: // Escape
            // uninitialize
            uninitialize();
            // close our application's tab
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
        case 70: // for 'F' or 'f'
	    toggleFullScreen();	
	    break;
        case 86: // for 'V' or 'v'
	    lightingMode = 1;
	    break;
	case 76: // for 'L' or 'l'
	    if(gbLKeyPressed == false)
		gbLKeyPressed = true;
	    else
		gbLKeyPressed = false;
	    break;
        case 88: // for 'X' or 'x'
	    light_position[0] = 0.0;
	    light_position[1] = 0.0;
	    light_position[2] = 0.0;
	    light_position[3] = 0.0;
	    rotationAxis = 'x';
	    break;
        case 89: // for 'Y' or 'y'
	    light_position[0] = 0.0;
	    light_position[1] = 0.0;
	    light_position[2] = 0.0;
	    light_position[3] = 0.0;
	    rotationAxis = 'y';
	    break;
        case 90: // for 'Z' or 'z'
	    light_position[0] = 0.0;
	    light_position[1] = 0.0;
	    light_position[2] = 0.0;
	    light_position[3] = 0.0;
	    rotationAxis = 'z';
	    break;
    }
}

function mouseDown()
{
    // code
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI / 180);
}

