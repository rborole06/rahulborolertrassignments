
1 - This application shows different types of materials with diffrent shininess property
2 - Also shows the effect of light on materials according to shininess
3 - Canvas.js file contain the code with different viewport for each sphere to avoid distortion
4 - To enable lights on sphere, Press L or l key
5 - To rotate light around x, y and z axis press x, y, z key respectively
6 - At first instance, the code was written for all 24 sphere in one viewport but it had a problem of distortion, and to avoid that, code is refactored by putting 24 shere in 24 viewport